#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>

#include "i2cDriverTU.h"
#include "regTU.h"

//def fce
static void printId (uint8_t value);
static void printMpuSelCnfg (uint8_t value);
static void printXOfreqCnfg (uint8_t value);
static void printNominalFreq (uint8_t * table);
static void printInterrupt (uint8_t * table);
static void printInterruptStatus (uint8_t * table);
static void printInterruptMask (uint8_t * table);
static void printI2cSlaveAddr (uint8_t value);
static void printInCnfg (uint8_t value,int num);
static void printInHfDiv (uint8_t value,int offset, int num);
static void printHfDivCnfg (uint8_t value);
static void printInPdn (uint8_t value,int offset, int num);
static void printInPdnCnfg (uint8_t value);
static void printInFecDiv (uint8_t valueLo,uint8_t valueHi , int num, char txt);
static void printFreqMonFactorCnfg (uint8_t value);
static void printHardFreqMonThresholdCnfg (uint8_t value);
static void printSoftFreqMonThresholdCnfg (uint8_t value);
static void printUpperThresholdCnfg (uint8_t* table);
static void printLowerThresholdCnfg (uint8_t* table);
static void printBucketSizeCnfg (uint8_t* table);
static void printDecayRateCnfg (uint8_t* table);
static void printInFreqReadSts (uint8_t* table);
static void printRemoteInputValidCnfg (uint8_t* table);
static void printLosSts (uint8_t value);
static void printPhaseAlarmTimeCnfg (uint8_t value);
static void printInSts_ (uint8_t value, int num);
static void printInSts (uint8_t * table);
static void printInLosSyncCnfg_ (uint8_t value, int num);
static void printInLosSyncCnfg (uint8_t * table);
static void printInSyncPhase (uint8_t value ,int num,int ofset);
static void printInSyncPhaseCnfg (uint8_t * table);
static void printInPhaseOffsetCnfg (uint8_t * table);
static void printDpllPriorityTableSts (uint8_t * table);
static void printDpllOperatingSts (uint8_t value);
static void printDpllCurrentDpllFreqSts (uint8_t * table);
static void printDpllCurrentDpllPhaseSts (uint8_t * table);
static void printDpllTodSecSts (uint8_t * table);
static void printDpllTodTrigger (uint8_t value);
static void printDpllInputModeCnfg (uint8_t  value);
static void printDpllMonSwPboCnfg (uint8_t value);
static void printDpllInSelPriorityCnfg (uint8_t * table);
static void printDpllInputSelCnfg (uint8_t value);
static void printDpllOperatingModeCnfg (uint8_t value);
static void printDpllFbSelCnfg (uint8_t value);
static void printDpllUpdateEventCnfg (uint8_t value);
static void printDpllPathCnfg (uint8_t value);
static void printDpllStartBwDampingCnfg (uint8_t value);
static void printDpllAcqBwDampingCnfg (uint8_t value);
static void printDpllLockedBwDampingCnfg (uint8_t value);
static void printDpllBwOvershootCnfg (uint8_t value);
static void printDpllPhaseLossCoarseLimitCnfg (uint8_t value);
static void printDpllPhaseLossFineLimitCnfg (uint8_t value);
static void printDpllHoldoverModeCnfg (uint8_t * table);
static void printDpllHoldoverFreqCnfg (uint8_t * table);
static void printDpllFreqSoftLimitCnfg (uint8_t value);
static void printDpllFreqHardLimitCnfg (uint8_t * table);
static void printDpllFrMfrSyncCnfg (uint8_t value);
static void printDpllSyncMonitorCnfg (uint8_t value);
static void printDpllSyncEdgeCnfg (uint8_t value);
static void printDpllPhaseOffsetCnfg (uint8_t * table);
static void printDpllBwSwTimeCnfg (uint8_t * table);
static void printDpllSlaveForceRefSelCnfg (uint8_t value);
static void printDpllProgPhLimitCnfg (uint8_t * table);
static void printApll (uint8_t* table, int num);
static void printApllIcpCtrlCnfg (uint8_t * table, int num);
static void printApllProgPhLimitCnfg (uint8_t * table, int num);
static void printApllDivisorDenCnfg (uint8_t * table, int num);
static void printApllDivisorNumCnfg (uint8_t * table, int num);
static void printApllDsmCnfg (uint8_t * table, int num);
static void printApllDivisorIntCnfg (uint8_t * table, int num);
static void printApllFrRatioCnfg (uint8_t * table, int num);
static void printOutMuxCnfg (uint8_t value, int num);
static void printOutDiv1Cnfg (uint8_t value, int num);
static void printOutDiv2Cnfg (uint8_t * table, int offset, int num);
static void printOutPh1Cnfg (uint8_t value, int num);
static void printOutPh2Cnfg (uint8_t * table, int offset, int num);
static void printOutFinePhCnfg (uint8_t value, int num);


// interni fce
static void printId (uint8_t value)
{
	printf ("# Identification Register\n");
	printf ("dev_id=0x%02x\n",value);
}

static void printMpuSelCnfg (uint8_t value)
{
	printf ("# MPU Selection Configuration Register\n");
	printf ("mpu_sel_cnfg=0x%02x\n",value);
	printf ("eeprom_rd_spd=0x%02x\t\t# ",(value & 0x80)>>7);
	if (value & 0x80 )
		printf ("400kHz\n");
	else
		printf ("100kHz\n");
	printf ("sonet_sdh_cnfg=0x%02x\t\t# ",(value & 0x04 )>>2);
	if (value & 0x04 )
		printf ("SONET\n");
	else
		printf ("SDH\n");
	printf ( "mpu_sel_cnfg=0x%02x\t\t# ",(value & 0x3));
	switch (value & 0x3) {
		case 0x3:
			printf ("EEPROM boot mode\n");
			break;
		case 0x2:
			printf ("UART\n");
			break;
		case 0x1:
			printf ("SPI\n");
			break;
		case 0x0:
			printf ("I2C\n");
			break;
	}
}

static void printXOfreqCnfg (uint8_t value)
{
	printf ("# XO Frequency Configuration Register\n");
	printf ("xo_freq_cnfg=0x%02x\n",value);
	printf ("tosci_edge=0x%02x\t\t\t# ",(value & 0x08 )>>7);
	if (value & 0x08 )
		printf ("falling\n");
	else
		printf ("rising\n");
	printf ( "xo_freq_cnfg=0x%02x\t\t# ",(value & 0x7));
	switch (value & 0x7) {
		case 0x7:
			printf ("30.72 MHz\n");
			break;
		case 0x6:
			printf ("25 MHz\n");
			break;
		case 0x5:
			printf ("24.576 MHz\n");
			break;
		case 0x4:
			printf ("20 MHz\n");
			break;
		case 0x3:
			printf ("19.44 MHz\n");
			break;
		case 0x2:
			printf ("13 MHz\n");
			break;
		case 0x1:
			printf ("12.8 MHz\n");
			break;
		case 0x0:
			printf ("10 MHz\n");
			break;	
	}
}

static void printNominalFreq (uint8_t* table)
{
	unsigned long freq=(table[NOMINAL_FREQ_CNFG_23_16] << 16)+ (table[NOMINAL_FREQ_CNFG_15_8] << 8) +table[NOMINAL_FREQ_CNFG_7_0];
	printf ("#Nominal Frequency Configuration Register\nnominal_freq_cnfg=0x%06lx \t# (%ld)\n",freq,freq);
}

static void printInterrupt (uint8_t* table)
{
	printf ("# Interrupt Configuration Register\n");
	printf ("interrupt_cnfg=0x%02x\n",table[INTERRUPT_CNFG]);
	printf ("tristate_en=0x%02x\t\t# ",(table[INTERRUPT_CNFG] & 0x02)>>1);
	if (table[INTERRUPT_CNFG] & 0x02 )
		printf ("Interrupt pin only driven when active; high impedance when inactive\n");
	else
		printf ("Interrupt pin always driven when inactive\n");
	printf ("int_polarity=0x%02x\t\t# ",table[INTERRUPT_CNFG] & 0x01);
	if (table[INTERRUPT_CNFG] & 0x01 )
		printf ("Active-high pin driven high to indicate active interrupt\n");
	else
		printf ("Active-low pin driven low to indicate active interrupt\n");
}

static void printInterruptStatus (uint8_t* table)
{
	unsigned long status=(table[INTERRUPT_STS_31_24] << 24)+(table[INTERRUPT_STS_23_16] << 16)+(table[INTERRUPT_STS_15_8] << 8)+table[INTERRUPT_STS_7_0];
	printf ("# Interrupt Status Register\ninterrupt_sts=0x%08lx\n",status);
}

static void printInterruptMask (uint8_t* table)
{
	unsigned long mask=(table[INTERRUPT_MASK_CNFG_31_24] << 24)+(table[INTERRUPT_MASK_CNFG_23_16] << 16)+( table[INTERRUPT_MASK_CNFG_15_8] << 8)+table[INTERRUPT_MASK_CNFG_7_0];
	printf ("# Interrupt Mask Configuration Register\ninterrupt_mask_cnfg=0x%08lx\n",mask);
}

static void printI2cSlaveAddr (uint8_t value)
{
	printf ("# I2C Slave Address Configuration Register\ni2c_slave_addr_cnfg=0x%02x\n",value);
}
////////////////////////////////////////////////////////////
static void printInCnfg (uint8_t value, int num)
{
	printf ("# Input %d Configuration Register\n",num);
	printf ("in%d_cnfg=0x%02x\n",num,value);
	printf ("in%d_direct_div=0x%02x\t\t# ",num,(value & 0x80 )>>7);
	if (value & 0x80 )
		printf ("Input goes to DPLL and monitor after pre-divider\n");
	else
		printf ("Input goes directly to DPLL and Monitor\n");
	printf ("in%d_lock_8k=0x%02x\t\t# ",num,(value & 0x40 )>>6);
	if (value & 0x40 )
		printf ("Input goes to DPLL after pre-divider\n");
	else
		printf ("Input goes directly to DPLL\n");
	printf ("in%d_bucket_id=0x%02x\n",num,(value & 0x30)>>4);
	printf ("in%d_freq=0x%02x\t\t\t# ",num,(value & 0xf));
	switch (value & 0xf) {
		case 0x0: 
			printf ("8 kHz only\n");
			break;
		case 0x01:
			printf ("1.544 MHz/2.048 MHz (depends on SONET/SDH mode)\n");
			break;
		case 0x02: 
			printf ("6.48 MHz\n");
			break;
		case 0x03: 
			printf ("19.44 MHz.\n");
			break;
		case 0x04: 
			printf ("25.92 MHz\n");
			break;
		case 0x05: 
			printf ("38.88 MHz\n");
			break;
		case 0x09: 
			printf ("2 kHz\n");
			break;
		case 0x0a: 
			printf ("4 kHz\n");
			break;
		case 0x0b: 
			printf ("1 PPS\n");
			break;
		case 0x0c: 
			printf ("6.25 MHz\n");
			break;
		case 0xe: 
			printf ("25 MHz\n");
			break;
		default :
			printf ("Do not use\n");
			break;
			
	}
}


static void printInHfDiv (uint8_t value,int offset, int num)
{
	printf ("in%d_hf_div=0x%02x\t\t\t# ",num,((value >> offset) & 0x3));
	switch ((value >> offset) & 0x3) {
		case 0x0: 
			printf ("Bypassed\n");
			break;
		case 0x01:
			printf ("Divided by 4\n");
			break;
		case 0x02: 
			printf ("Divided by 5\n");
			break;
		case 0x03: 
			printf ("Reserved\n");
			break;
	}
}


static void printHfDivCnfg (uint8_t value)
{
	printf ("# High Frequency Divider Configuration Register\nhf_div_cnfg=0x%02x\n",value);
	printInHfDiv (value,0, 1);
	printInHfDiv (value,2, 2);
	printInHfDiv (value,4, 3);
	//printInHfDiv (value,6, 4);
}

static void printInPdn (uint8_t value,int offset, int num)
{
	printf ("in%d_pdn=0x%02x\t\t\t# ",num,((value >> offset) & 0x1));
	switch ((value >> offset) & 0x1) {
		case 0x0: 
			printf ("Not powered down (active/enabled)\n");
			break;
		case 0x01:
			printf ("Input powered down (inactive/disabled)\n");
			break;
	}
}
static void printInPdnCnfg (uint8_t value)
{
	printf ("# Input Power-Down Configuration Register\nin_pdn_cnfg=0x%02x\n",value);
	printInPdn (value,2,1);
	printInPdn (value,3,2);
	printInPdn (value,4,3);
//	printInPdn (value,5,6);
}

static void printInFecDiv (uint8_t valueLo,uint8_t valueHi , int num, char txt)
{
	if (txt == 'n') {
		printf ("# Input %d Pre-divider Denominator N Register\n",num);
		printf ("in%d_pre_div%c_cnfg=",num,txt);
	}else {
		if (txt == 'p')
			printf ("# Input %d FEC Divider Numerator P Register\n",num);
		else
			printf ("# Input %d FEC Divider Denominator Q Register\n",num);
		printf ("in%d_fec_div%c_cnfg=",num,txt);
	}
	printf ("%d\n",(valueHi << 8) + valueLo);
}
/////////////////////////////////////////////////////////////
static void printFreqMonFactorCnfg (uint8_t value)
{
	printf ("# Frequency Monitor Factor Configuration Register\n");
	printf ("freq_mon_factor_cnfg=0x%02x\n",value);
	printf ("freq_mon_clk_sel=0x%02x\t\t# ",(value & 0x80 )>>7);
	if (value & 0x80 )
		printf ("Clock that tracks output of dpll1\n");
	else
		printf ("Free-running clock that tracks crystal oscillator frequency\n");
	printf ("in_noise_window=0x%02x\t\t# ",(value & 0x40 )>>6);
	if (value & 0x40 )
		printf ("Enabled\n");
	else
		printf ("Disabled\n");
	printf ("freq_mon_factor=0x%02x\n",value & 0x0f);
	
}

static void printHardFreqMonThresholdCnfg (uint8_t value)
{
	printf ("# Hard Frequency Monitor Threshold Configuration Register\n");
	printf ("hard_freq_mon_threshold_cnfg=0x%02x\n",value);
	printf ("hard_accept_threshold=0x%02x\n",(value & 0xf0 )>>4);
	printf ("hard_reject_threshold=0x%02x\n",value & 0x0f);
}

static void printSoftFreqMonThresholdCnfg (uint8_t value)
{
	printf ("# Soft Frequency Monitor Threshold Configuration Register\n");
	printf ("soft_freq_mon_threshold_cnfg=0x%02x\n",value);
	printf ("soft_accept_threshold=0x%02x\n",(value & 0xf0 )>>4);
	printf ("soft_reject_threshold=0x%02x\n",value & 0x0f);
}

static void printUpperThresholdCnfg (uint8_t* table)
{
	printf ("# Upper Threshold  Configuration Register\n");
	printf ("upper_threshold_0_cnfg=0x%02x\n",table[UPPER_THRESHOLD_0_CNFG]);
	printf ("upper_threshold_1_cnfg=0x%02x\n",table[UPPER_THRESHOLD_1_CNFG]);
	printf ("upper_threshold_2_cnfg=0x%02x\n",table[UPPER_THRESHOLD_2_CNFG]);
	printf ("upper_threshold_3_cnfg=0x%02x\n",table[UPPER_THRESHOLD_3_CNFG]);

}

static void printLowerThresholdCnfg (uint8_t* table)
{
	printf ("# Lower Threshold Configuration Register\n");
	printf ("lower_threshold_0_cnfg=0x%02x\n",table[LOWER_THRESHOLD_0_CNFG]);
	printf ("lower_threshold_1_cnfg=0x%02x\n",table[LOWER_THRESHOLD_1_CNFG]);
	printf ("lower_threshold_2_cnfg=0x%02x\n",table[LOWER_THRESHOLD_2_CNFG]);
	printf ("lower_threshold_3_cnfg=0x%02x\n",table[LOWER_THRESHOLD_3_CNFG]);

}

static void printBucketSizeCnfg (uint8_t* table)
{
	printf ("# Bucket Size Configuration Register\n");
	printf ("bucket_size_0_cnfg=0x%02x\n",table[BUCKET_SIZE_0_CNFG]);
	printf ("bucket_size_1_cnfg=0x%02x\n",table[BUCKET_SIZE_1_CNFG]);
	printf ("bucket_size_2_cnfg=0x%02x\n",table[BUCKET_SIZE_2_CNFG]);
	printf ("bucket_size_3_cnfg=0x%02x\n",table[BUCKET_SIZE_3_CNFG]);

}

static void printDecayRateCnfg (uint8_t* table)
{
	printf ("# Decay Rate Configuration Register\n");
	printf ("decay_rate_0_cnfg=0x%02x\n",0x03 & table[DECAY_RATE_0_CNFG]);
	printf ("decay_rate_1_cnfg=0x%02x\n",0x03 & table[DECAY_RATE_1_CNFG]);
	printf ("decay_rate_2_cnfg=0x%02x\n",0x03 & table[DECAY_RATE_2_CNFG]);
	printf ("decay_rate_3_cnfg=0x%02x\n",0x03 & table[DECAY_RATE_3_CNFG]);

}

static void printInFreqReadSts (uint8_t* table)
{
	printf ("# Input Frequency Read Status Register\n");
	printf ("in_%d_freq_read_sts=0x%02x\n",1,table[IN3_FREQ_READ_STS]);
	printf ("in_%d_freq_read_sts=0x%02x\n",2,table[IN4_FREQ_READ_STS]);
	printf ("in_%d_freq_read_sts=0x%02x\n",3,table[IN5_FREQ_READ_STS]);
	printf ("in_%d_freq_read_sts=0x%02x\n",5,table[IN9_FREQ_READ_STS]);
	printf ("in_%d_freq_read_sts=0x%02x\n",6,table[IN10_FREQ_READ_STS]);
}

static void printRemoteInputValidCnfg (int value,int offset, int num)
{
	printf ("remote_in%d_invalid=0x%02x\t\t# ",num,((value >> offset) & 0x1));
	switch ((value >> offset) & 0x1) {
		case 0x0: 
			printf ("The corresponding input is allowed)\n");
			break;
		case 0x01:
			printf ("The corresponding input is disallowed.)\n");
			break;
	}
}

static void printRemoteInputValidCnfg (uint8_t* table)
{
	unsigned int value=(table[REMOTE_INPUT_VALID_CNFG_14_9] << 8)+table[REMOTE_INPUT_VALID_CNFG_8_1];
	printf ("# Remote Input Valid Configuration Register\nremote_input_valid_cnfg=0x%04x\n",value);
	printRemoteInputValidCnfg (value,2, 1);
	printRemoteInputValidCnfg (value,3, 2);
	printRemoteInputValidCnfg (value,4, 3);
	printRemoteInputValidCnfg (value,8, 5);
	printRemoteInputValidCnfg (value,9, 6);
}

static void printPhaseAlarmTimeCnfg (uint8_t value)
{
	printf ("# Phase Alarm Time Configuration Register\nphase_alarm_time_cnfg=0x%04x\n",value);
	printf ("phase_alarm_multi_factor=0x%02x\n",(0xc0 & value) >> 6);
	printf ("phase_alarm_time_out_value=0x%02x\n",0x3f & value);
}

static void printLosSts (uint8_t value)
{
	printf ("# Loss of Signal Status Register\nlos_pin_sts=0x%04x\n",(0xf0 & value) >> 4);
}

static void printInSts_ (uint8_t value, int num)
{
	printf ("in%d_sts=0x%02x\n",num,value);
	printf ("in%d_valid_dpll2=0x%02x\n",num,0x1&(value>>7));
	printf ("in%d_valid_dpll1=0x%02x\n",num,0x1&(value>>6));
	printf ("in%d_valid_dpll2=0x%02x\n",num,0x1&(value>>5));
	printf ("in%d_freq_hard_alarm=0x%02x\n",num,0x1&(value>>4));
	printf ("in%d_freq_soft_alarm=0x%02x\n",num,0x1&(value>>3));
	printf ("in%d_activity_alarm=0x%02x\n",num,0x1&(value>>2));
	printf ("in%d_phase_lock_alarm_dpll2=0x%02x\n",num,0x1&(value>>1));
	printf ("in%d_phase_lock_alarm_dpll1=0x%02x\n",num,0x1&value);
}

static void printInSts (uint8_t * table)
{
	printf ("# Input Status Register\n");
	printInSts_ (table[IN3_STS], 1);
	printInSts_ (table[IN4_STS], 2);
	printInSts_ (table[IN5_STS], 3);
	printInSts_ (table[IN9_STS], 5);
	printInSts_ (table[IN10_STS], 6);
}


static void printInLosSyncCnfg_ (uint8_t value, int num)
{
	printf ("in%d_los_sync_cnfg=0x%02x\n",num,value);
	printf ("in%d_los_en=0x%02x\n",num,0x01 & (value>>6));
	printf ("in%d_los_sel=0x%02x\t\t# LOS%d pin\n",num,0x03 & (value>>4),0x03 & (value>>4));
	if (num > 2) {
		printf ("in%d_sync_sel=0x%02x\n",num,0x0f & value);
	}
}

static void printInLosSyncCnfg (uint8_t * table)
{
	printf ("# Input LOS Sync Configuration Register\n");
	printInLosSyncCnfg_ (table[IN3_LOS_SYNC_CNFG], 1);
	printInLosSyncCnfg_ (table[IN4_LOS_SYNC_CNFG], 2);
	printInLosSyncCnfg_ (table[IN5_LOS_SYNC_CNFG], 3);
	printInLosSyncCnfg_ (table[IN9_LOS_SYNC_CNFG], 5);
	printInLosSyncCnfg_ (table[IN10_LOS_SYNC_CNFG], 6);
}

static void printInSyncPhase (uint8_t value ,int num,int ofset)
{
	printf ("in%d_sync_phase=0x%02x\t\t# ",num,0x03 & (value>>ofset));
	switch (0x03 & (value>>ofset)) {
		case 0x0: 
			printf ("On target\n");
			break;
		case 0x1:
			printf ("0.5 UI early\n");
			break;
		case 0x2: 
			printf ("1.0 UI late\n");
			break;
		case 0x3:
			printf ("0.5 UI late\n");
			break;
	}
	
}

static void printInSyncPhaseCnfg (uint8_t * table)
{
	printf ("# Input Sync Phase Configuration Register\n");
	printInSyncPhase (table[IN_4_1_SYNC_PHASE_CNFG] ,1,4);
	printInSyncPhase (table[IN_4_1_SYNC_PHASE_CNFG] ,2,6);
	printInSyncPhase (table[IN_8_5_SYNC_PHASE_CNFG] ,3,0);
	printInSyncPhase (table[IN_12_9_SYNC_PHASE_CNFG] ,5,0);
	printInSyncPhase (table[IN_12_9_SYNC_PHASE_CNFG] ,6,2);
}


static void printInPhaseOffsetCnfg (uint8_t * table)
{
	printf ("# Input Phase Offset Configuration Register\n");
	printf ("in%d_phase_offset_cnfg=0x%02x\n",1,table[IN3_PHASE_OFFSET_CNFG]);
	printf ("in%d_phase_offset_cnfg=0x%02x\n",2,table[IN4_PHASE_OFFSET_CNFG]);
	printf ("in%d_phase_offset_cnfg=0x%02x\n",3,table[IN5_PHASE_OFFSET_CNFG]);
	printf ("in%d_phase_offset_cnfg=0x%02x\n",5,table[IN9_PHASE_OFFSET_CNFG]);
	printf ("in%d_phase_offset_cnfg=0x%02x\n",6,table[IN10_PHASE_OFFSET_CNFG]);
}

/////////////////////////////////////////////////////////////
//DPLL
static void printDpllPriorityTableSts (uint8_t * table)
{
	printf ("# DPLL%d Priority Table Status Register\n",1);
	printf ("dpll%d_priority_table_sts=0x%04x\n",1,table[DPLL1_PRIORITY_TABLE_STS_7_0]+(table[DPLL1_PRIORITY_TABLE_STS_15_8] << 8));
	printf ("dpll%d_highest_valid_ref=0x%02x\n",1,0x0f &(table[DPLL1_PRIORITY_TABLE_STS_7_0]>>4));
	printf ("dpll%d_current_sel_ref=0x%02x\n",1,0x0f &(table[DPLL1_PRIORITY_TABLE_STS_7_0]));
	printf ("dpll%d_3rd_valid_re=0x%02x\n",1,0x0f &(table[DPLL1_PRIORITY_TABLE_STS_15_8]>>4));
	printf ("dpll%d_2nd_valid_ref=0x%02x\n",1,0x0f &(table[DPLL1_PRIORITY_TABLE_STS_15_8]));
	
}

static void printDpllOperatingSts (uint8_t value)
{
	printf ("# DPLL%d Operating Status Register\n",1);
	printf ("dpll%d_operating_sts=0x%02x\n",1,value);
	printf ("dpll%d_exsync_mon_alarm=0x%02x\t# ",1,0x1 & (value >> 7));
	if (0x1 & (value >> 7))
		printf ("Alarm; ex_sync out of specification\n");
	else
		printf ("No alarm; ex_sync within specification\n");
	printf ("dpll%d_dpll_soft_alarm=0x%02x\t# ",1,0x1 & (value >> 5));
	if (0x1 & (value >> 5))
		printf ("Alarm; dpll out of \"soft threshold\"\n");
	else
		printf ("No alarm; dpll within \"soft threshold\"\n");
	printf ("dpll%d_master_slave=0x%02x\t\t# ",1,0x1 & (value >> 4));
	if (0x1 & (value >> 4))
		printf ("Master\n");
	else
		printf ("Slave\n");
	printf ("dpll%d_dpll_lock=0x%02x\t\t# ",1,0x1 & (value >> 3));
	if (0x1 & (value >> 3))
		printf ("dpll phase locked\n");
	else
		printf ("dpll out of phase locked\n");
	printf ("dpll%d_dpll_operating_sts=0x%02x\t# ",1,0x7 & (value));
	switch (0x7 & (value)) {
		case 0x0: 
			printf ("Not used\n");
			break;
		case 0x1:
			printf ("Free Run\n");
			break;
		case 0x2: 
			printf ("Holdover\n");
			break;
		case 0x3:
			printf ("Not used\n");
			break;
		case 0x4:
			printf ("Locked\n");
			break;
		case 0x5:
			printf ("Pre-locked2\n");
			break;
		case 0x6:
			printf ("Pre_locked\n");
			break;
		case 0x7:
			printf ("Phase Lost\n");
			break;
	}
	
}

static void printDpllCurrentDpllFreqSts (uint8_t * table)
{
	printf ("# DPLL%d Current DPLL Frequency Status Register\n",1);
	printf ("dpll%d_current_dpll_freq_sts=0x%010llx\n",1,(long long unsigned int)table[DPLL1_CURRENT_DPLL_FREQ_STS_7_0]+(table[DPLL1_CURRENT_DPLL_FREQ_STS_15_8]<<8) +
			(table[DPLL1_CURRENT_DPLL_FREQ_STS_23_16]<<16)+((long long unsigned int)table[DPLL1_CURRENT_DPLL_FREQ_STS_31_24]<< 24)+((long long unsigned int)table[DPLL1_CURRENT_DPLL_FREQ_STS_39_32]<<32));
	
}

static void printDpllCurrentDpllPhaseSts (uint8_t * table)
{
	printf ("# DPLL%d Current DPLL Phase Status Register\n",1);
	printf ("dpll%d_current_dpll_phase_sts=0x%06x\n",1,table[DPLL1_CURRENT_DPLL_PHASE_STS_7_0]+(table[DPLL1_CURRENT_DPLL_PHASE_STS_15_8] << 8)+
			((0x1f*table[DPLL1_CURRENT_DPLL_PHASE_STS_19_16]) << 16));
			
	
}

static void printDpllTodSecSts (uint8_t * table)
{
	printf ("# DPLL%d Time of Day Status Register\n",1);
	printf ("dpll%d_tod_nsec_sts=0x%08x\n",1,table[DPLL1_TOD_NSEC_STS_7_0]+(table[DPLL1_TOD_NSEC_STS_15_8] << 8) +
			(table[DPLL1_TOD_NSEC_STS_23_16] << 16) +( table[DPLL1_TOD_NSEC_STS_31_24] << 24));
	printf ("dpll%d_tod_sec_sts=0x%02x%02x%02x%02x%02x%02x\n",1,table[DPLL1_TOD_SEC_STS_79_72],table[DPLL1_TOD_SEC_STS_71_64],
			table[DPLL1_TOD_SEC_STS_63_56],table[DPLL1_TOD_SEC_STS_55_48],table[DPLL1_TOD_SEC_STS_47_40],table[DPLL1_TOD_SEC_STS_39_32]);

}

static void printDpllTodTrigger (uint8_t value)
{
	printf ("# DPLL%d Time of Day Trigger Register\n",1);
	printf ("dpll%d_tod_trigger=0x%02x\n",1,value);
	printf ("dpll%d_tod_wr_trigger=0x%02x\n",1,0x0f & (value>>4));
	printf ("dpll%d_tod_rd_trigger=0x%02x\n",1,0x0f & (value));
}


static void printDpllInputModeCnfg (uint8_t value)
{
	
	printf ("# DPLL%d Input Mode Configuration Register\n",1);
	printf ("dpll%d_input_mode_cnfg=0x%02x\n",1,value);

	printf ("dpll%d_auto_extsync_en=0x%02x\t# ",1,0x1 & (value >> 7));
	if (0x1 & (value >> 7))
		printf ("External Frame Sync enabled if extsync_en = 1 and dpll locked\n");
	else
		printf ("External Frame Sync enabled/disabled depending on extsync_en bit\n");
	printf ("dpll%d_extsync_en=0x%02x\t\t# ",1,0x1 & (value >> 6));
	if (0x1 & (value >> 6))
		printf ("With external Sync signal; EX_SYNC pin is considered\"\n");
	else
		printf ("No external Sync signal; EX_SYNC pin is ignored\"\n");
	printf ("dpll%d_ph_alarm_timeout=0x%02x\t# ",1,0x1 & (value >> 5));
	if (0x1 & (value >> 5))
		printf ("Phase alarms on sources automatically time out. The time out value\n");
	else
		printf ("Phase alarm on sources only cancelled by software\n");
	printf ("dpll%d_revertive_mode=0x%02x\t# ",1,0x1 & value);
	if (0x1 & value)
		printf ("Revertive switching\n");
	else
		printf ("Non-Revertive switching. (default)\n");
	printf ("dpll%d_sync_freq=0x%02x\t\t# ",1,0x3 & (value >> 3));
	switch (0x3 & (value >> 3)) {
		case 0x0: 
			printf ("8 kHz\n");
			break;
		case 0x1:
			printf ("1 pps\n");
			break;
		case 0x2: 
			printf ("4 kHz\n");
			break;
		case 0x3:
			printf ("2 kHz\n");
			break;
	}
	
}


static void printDpllMonSwPboCnfg (uint8_t value)
{
	
	printf ("# DPLL%d Monitor Software Register\n",1);
	printf ("dpll%d_mon_sw_pbo_cnfg=0x%02x\n",1,value);

	printf ("dpll%d_ultra_fast_switch=0x%02x\t# ",1,0x1 & (value >> 5));
	if (0x1 & (value >> 5))
		printf ("Currently selected input disqualified after less than 3 missing input cycles\n");
	else
		printf ("Currently selected input only disqualified by leaky bucket or frequency monitors\n");
	printf ("dpll%d_hitless_switch_freeze=0x%02x # ",1,0x1 & (value >> 3));
	if (0x1 & (value >> 3))
		printf ("Hitless switch frozen; no further hitless switching events will occur\"\n");
	else
		printf ("Hitless switch not frozen\"\n");
	printf ("dpll%d_hitless_switch_en=0x%02x\t# ",1,0x1 & (value >> 2));
	if (0x1 & (value >> 2))
		printf ("Hitless switch enabled\n");
	else
		printf ("Hitless switch disabled\n");
	printf ("dpll%d_freq_mon_soft_en=0x%02x\t# ",1,0x1 & (value>>1));
	if (0x1 & (value >> 1))
		printf ("Soft frequency monitor alarms enabled\n");
	else
		printf ("Soft frequency monitor alarms disabled\n");
	printf ("dpll%d_freq_mon_hard_en=0x%02x\t# ",1,0x1 & value);
	if (0x1 & value )
		printf ("Hard frequency monitor alarms enabled\n");
	else
		printf ("Hard frequency monitor alarms disabled\n");
	
}

static void printDpllInSelPriorityCnfg (uint8_t * table)
{	
	printf ("# DPLL%d Input Select Priority Configuration Register\n",1);
	printf ("dpll%d_in%d_priority=0x%02x\n",1,1,table[DPLL1_IN3_IN4_SEL_PRIORITY_CNFG]&0x0f);
	printf ("dpll%d_in%d_priority=0x%02x\n",1,2,(table[DPLL1_IN3_IN4_SEL_PRIORITY_CNFG]>>4)&0x0f);
	printf ("dpll%d_in%d_priority=0x%02x\n",1,3,table[DPLL1_IN5_IN6_SEL_PRIORITY_CNFG]&0x0f);
	printf ("dpll%d_in%d_priority=0x%02x\n",1,5,table[DPLL1_IN9_IN10_SEL_PRIORITY_CNFG]&0x0f);
	printf ("dpll%d_in%d_priority=0x%02x\n",1,6,(table[DPLL1_IN9_IN10_SEL_PRIORITY_CNFG]>>4)&0x0f);

}


static void printDpllInputSelCnfg (uint8_t value)
{
	
	printf ("# DPLL%d Input Select Configuration Register\n",1);
	printf ("dpll%d_input_sel_cnfg=0x%02x\n",1,value);
	printf ("dpll%d_input_sel=0x%02x\t\t #",1,value&0x0f);
	switch (0x0f & value) {
		case 0x00 :
			printf ("Automatic Input selection\n");
			break;
		case 0x3 :
			printf ("dpll%d forced to select IN%d\n",1,1);
			break;
		case 0x4 :
			printf ("dpll%d forced to select IN%d\n",1,2);
			break;	
		case 0x5 :
			printf ("dpll%d forced to select IN%d\n",1,3);
			break;
		case 0x9 :
			printf ("dpll%d forced to select IN%d\n",1,5);
			break;
		case 0xa :
			printf ("dpll%d forced to select IN%d\n",1,5);
			break;
		default:
			printf ("Not used\n");
			break;
	}
}
		
static void printDpllOperatingModeCnfg (uint8_t value)
{
	
	printf ("# DPLL%d Operating Mode Configuration Register\n",1);
	printf ("dpll%d_operating_mode_cnfg=0x%02x\n",1,value);
	printf ("dpll%d_comb_mode_sel=0x%02x\n",1,0x3 & (value >> 6));
	printf ("dpll%d_comb_mode_en=0x%02x\n",1,0x1 & (value >> 5));
	printf ("dpll%d_operating_mode_cnfgt=0x%02x\t# ",1,0x1f & value);
	
	switch (0x1f & value) {
		case 0x0: 
			printf ("Automatic\n");
			break;
		case 0x1:
			printf ("Free Run\n");
			break;
		case 0x2: 
			printf ("Holdover\n");
			break;
		case 0x4:
			printf ("Locked\n");
			break;
		case 0x5: 
			printf ("Pre-Locked2\n");
			break;
		case 0x6:
			printf ("Pre-Locked\n");
			break;
		case 0x7:
			printf ("Phase lost\n");
			break;
		case 0xa:
			printf ("Write-Frequency\n");
			break;
		case 0x12:
			printf ("Write-Phase\n");
			break;
		default:
			printf ("Reserved (do not use)\n");
			break;
	}
	
}

static void printDpllFbSelCnfg (uint8_t value)
{
	
	printf ("# DPLL%d Feedback Select Configuration Register\n",1);
	printf ("dpll%d_fb_sel_cnfg=0x%02x\n",1,value);
	printf ("dpll%d_fb_sel=0x%02x\t\t# ",1,value&0x0f);
	if ((value&0x0f) == 0x0f)
		printf ("Reserved\n");
	else if ((value&0x0f) == 0x00)
		printf ("Normal internal feedback mode\n");
	else
		printf ("IN%d used as fb\n",value&0x0f);
}

static void printDpllUpdateEventCnfg (uint8_t value)
{

	printf ("# DPLL%d Update Event Configuration Register\n",1);
	printf ("dpll%d_update_event_cnfg=0x%02x\t# ",1,value);
	switch (0x3 & value) {
		case 0x0: 
			printf ("Internal 1 PPS\n");
			break;
		case 0x1:
			printf ("Input from ex_sync\n");
			break;
		case 0x2: 
			printf ("Read/write event of the corresponding registers\n");
			break;
		case 0x3:
			printf ("Event each time internal timer roll-over\n");
			break;
		
	}
	
}

static void printDpllPathCnfg (uint8_t value)
{
	
	printf ("# DPLL%d Update Event Configuration Register\n",1);
	printf ("dpll%d_dpll_path_cnfg=0x%02x\n",1,value);
	printf ("dpll%d_gsm_obsai_16e1_16t1_sel=0x%02x # ",1,0x3 & (value>>2));
	switch (0x3 & (value >> 2)) {
		case 0x0: 
			printf ("16E1 = 32.768 MHz (SDH)\n");
			break;
		case 0x1:
			printf ("16T1 = 24.704 MHz (SONET)\n");
			break;
		case 0x2: 
			printf ("GSM = 26.000 MHz\n");
			break;
		case 0x3:
			printf ("OBSAI = 30.720 MHz\n");
			break;
	}
	printf ("dpll%d_12e1_gps_e3_t3_sel=0x%02x\t# ",1,0x3 & value);
	switch (0x3 & value) {
		case 0x0: 
			printf ("12E1 = 24.576 MHz (SDH)\n");
			break;
		case 0x1:
			printf ("GPS = 40.000 MHz (SONET)\n");
			break;
		case 0x2: 
			printf ("E3 = 34.368 MHz\n");
			break;
		case 0x3:
			printf ("T3 = 44.736 MHz\n");
			break;
		
	}
	
}

static void printDamping (uint8_t value)
{
	switch (0x7 & value) {
		case 0x1:
			printf ("1.2\n");
			break;
		case 0x2: 
			printf ("2.5\n");
			break;
		case 0x3:
			printf ("5\n");
			break;
		case 0x4:
			printf ("10\n");
			break;
		case 0x5: 
			printf ("20\n");
			break;
		default:
			printf ("Reserved\n");
			break;
	}
}

static void printBw (uint8_t value)
{
	switch (0x1f & value) {
		case 0x0: 
			printf ("0.090 MHz\n");
			break;
		case 0x1:
			printf ("0.27 MHz\n");
			break;
		case 0x2: 
			printf ("0.90 MHz\n");
			break;
		case 0x3:
			printf ("2.9 MHz\n");
			break;
		case 0x4: 
			printf ("4.3 MHz\n");
			break;
		case 0x5:
			printf ("8.7 MHz\n");
			break;
		case 0x6: 
			printf ("17 MHz\n");
			break;
		case 0x7:
			printf ("35 MHz\n");
			break;
		case 0x8: 
			printf ("69 MHz\n");
			break;
		case 0x9:
			printf ("92 MHz\n");
			break;
		case 0xa: 
			printf ("277 MHz\n");
			break;
		case 0xb:
			printf ("554 MHz\n");
			break;
		case 0xc: 
			printf ("1.1 Hz\n");
			break;
		case 0xd:
			printf ("2.2 Hz\n");
			break;
		case 0xe: 
			printf ("4.4 Hz\n");
			break;
		case 0xf:
			printf ("8.9 Hz\n");
			break;	
		case 0x10:
			printf ("18 Hz\n");
			break;
		case 0x11: 
			printf ("35 Hz\n");
			break;
		case 0x12:
			printf ("71 Hz\n");
			break;
		case 0x13: 
			printf ("142 Hz\n");
			break;
		case 0x14:
			printf ("283 Hz\n");
			break;
		case 0x15:
			printf ("567 Hz\n");
			break;
		default:
			printf ("Reserved\n");
			break;	
	}
	
}

static void printDpllStartBwDampingCnfg (uint8_t value)
{
	
	printf ("# DPLL%d DPLL Start Bandwidth Damping Configuration Register\n",1);
	printf ("dpll%d_dpll_start_bw_damping_cnfg=0x%02x\n",1,value);
	printf ("dpll%d_dpll_start_damping=0x%02x\t# ",1,0x7 & (value>>5));
	printDamping (0x7 & (value>>5));
	printf ("dpll%d_dpll_start_bw=0x%02x\t# ",1,0x1f & value);
	printBw (value);
}

static void printDpllAcqBwDampingCnfg (uint8_t value)
{

	printf ("# DPLL%d DPLL Acquired Bandwidth Damping Configuration Register\n",1);
	printf ("dpll%d_dpll_acq_bw_damping_cnfg=0x%02x\n",1,value);
	printf ("dpll%d_dpll_acq_damping=0x%02x\t# ",1,0x7 & (value>>5));
	printDamping (0x7 & (value>>5));
	printf ("dpll%d_dpll_acq_bw=0x%02x\t\t# ",1,0x1f & value);
	printBw (value);
}

static void printDpllLockedBwDampingCnfg (uint8_t value)
{

	printf ("# DPLL%d DPLL Locked Bandwidth Configuration Register\n",1);
	printf ("dpll%d_dpll_locked_bw_damping_cnfg=0x%02x\n",1,value);
	printf ("dpll%d_dpll_locked_damping=0x%02x\t# ",1,0x7 & (value>>5));
	printDamping (0x7 & (value>>5));
	printf ("dpll%d_dpll_locked_bw=0x%02x\t# ",1,0x1f & value);
	printBw (value);
}

static void printDpllBwOvershootCnfg (uint8_t value)
{
	
	printf ("# DPLL%d Bandwidth Overshoot Configuration Register\n",1);
	printf ("dpll%d_bw_overshoot_cnfg=0x%02x\n",1,value);

	printf ("dpll%d_pps_freerun_fast_freq_lock_en=0x%02x # ",1,0x1 & (value >> 7));
	if (0x1 & (value >> 7))
		printf ("Enable fast frequency lock mode when the loop start locks to 1 PPS from freerun\n");
	else
		printf ("Disable fast frequency lock of 1 PPS\n");
	
	printf ("dpll%d_pps_freerun_fast_ph_lock_en=0x%02x # ",1,0x1 & (value >> 6));
	if (0x1 & (value >> 6))
		printf ("Enable fast phase lock of 1PPS\"\n");
	else
		printf ("Disable fast phase lock of 1PPS\"\n");
	printf ("dpll%d_pps_holdover_fast_freq_lock_en=0x%02x # ",1,0x1 & (value >> 5));
	if (0x1 & (value >> 5))
		printf ("Enable fast phase lock mode when the loop relocks to 1PPS from Holdover\n");
	else
		printf ("Disable fast phase lock of 1PPS\n");
	printf ("dpll%d_pps_holdover_fast_ph_lock_en=0x%02x\t# ",1,0x1 & (value>>4));
	if (0x1 & (value >> 4))
		printf ("Enable fast phase lock mode when the loop relocks to 1PPS fromHoldover\n");
	else
		printf ("Disable fast phase lock of 1PPS\n");
	printf ("dpll%d_auto_bw_sel=0x%02x\t\t# ",1,0x1 & value>>3);
	if (0x1 & (value>>3) )
		printf ("Automatically select among start, acq, and locked bandwidth and damping factor\n");
	else
		printf ("Always use locked bandwidth and damping factor\n");
	printf ("dpll%d_ph_limit=0x%02x\t\t# ",1,0x7 & value);
	switch (0x7 & value) {
		case 0x0:
			printf ("GR-1244 ST3: 61 μs/s\n");
			break;
		case 0x1:
			printf ("GR-1244 ST2, 3E, ST3 (objective): 885 ns/s\n");
			break;
		case 0x2: 
			printf ("G.813 opt1, G.8262: 7.5 μs/s\n");
			break;
		case 0x3:
			printf ("No limitation\n");
			break;
		case 0x4:
			printf ("1 ns/s\n");
			break;
		case 0x5: 
			printf ("5 ns/s\n");
			break;
		case 0x6: 
			printf ("10 ns/s\n");
			break;
		default:
			printf ("Programmable through the DPLL{1:2} Program Limit Configuration Register\n");
			break;
	}
}

static void printDpllPhaseLossCoarseLimitCnfg (uint8_t value)
{
	
	printf ("# DPLL%d Phase Loss Coarse Limit Configuration Register\n",1);
	printf ("dpll%d_phase_loss_coarse_limit_cnfg=0x%02x\n",1,value);

	printf ("dpll%d_coarse_phase_loss_limit_en=0x%02x # ",1,0x1 & (value >> 7));
	if (0x1 & (value >> 7))
		printf ("Phase loss triggered when the phase error exceeds the limit programmed in phase_loss_coarse_limit\n");
	else
		printf ("Phase loss not triggered by the coarse phase lock detector\n");
	
	printf ("dpll%d_wide_range_en=0x%02x\t# ",1,0x1 & (value >> 6));
	if (0x1 & (value >> 6))
		printf ("Wide range (coarse) phase detector on\"\n");
	else
		printf ("Wide range (coarse) phase detector off\"\n");
	printf ("dpll%d_multi_phase_app=0x%02x\t# ",1,0x1 & (value >> 5));
	if (0x1 & (value >> 5))
		printf ("DPLL phase detector also uses the full coarse phase detector result\n");
	else
		printf ("DPLL phase detector limited to +/-360 degree (+/- 1 UI)\n");
	printf ("dpll%d_multi_ph_8k_4k_2k_en=0x%02x\t# ",1,0x1 & (value>>4));
	if (0x1 & (value >> 4))
		printf ("ph_loss_coarse_limit is set to +/-1 UI if wide_range_en is set to 0.\n");
	else
		printf ("ph_loss_coarse_limit always set to +/-1 UI\n");
	printf ("dpll%d_phase_loss_coarse_limit=0x%02x # ",1,0xf & value);
	switch (0xf & value) {
		case 0x0:
			printf ("+/- 1 UI\n");
			break;
		case 0x1:
			printf ("+/- 3 UI\n");
			break;
		case 0x2: 
			printf ("+/- 7 UI\n");
			break;
		case 0x3:
			printf ("+/- 15 UI\n");
			break;
		case 0x4:
			printf ("+/- 31 UI\n");
			break;
		case 0x5: 
			printf ("+/- 63 UI\n");
			break;
		case 0x6: 
			printf ("+/- 127 UI\n");
			break;
		case 0x7: 
			printf ("+/- 255 UI\n");
			break;
		case 0x8: 
			printf ("+/- 511 UI\n");
			break;
		case 0x9: 
			printf ("+/- 1023 UI\n");
			break;
		case 0xa: 
			printf ("+/- 2047 UI\n");
			break;
		case 0xb: 
			printf ("+/- 4095 UI\n");
			break;
		default:
			printf ("+/- 8191 UI \n");
			break;
	}
}

static void printDpllPhaseLossFineLimitCnfg (uint8_t value)
{
	
	printf ("# DPLL%d Phase Loss Fine Limit Configuration Register\n",1);
	printf ("dpll%d_phase_loss_fine_limit_cnfg=0x%02x\n",1,value);

	printf ("dpll%d_fine_phase_loss_limit_en=0x%02x # ",1,0x1 & (value >> 7));
	if (0x1 & (value >> 7))
		printf ("Enable\n");
	else
		printf ("Disable\n");
	
	printf ("dpll%d_fast_loss_switch=0x%02x\t# ",1,0x1 & (value >> 6));
	if (0x1 & (value >> 6))
		printf ("Results in the DPLL unlocked\"\n");
	else
		printf ("Does not result in the DPLL unlocked\"\n");
	printf ("dpll%d_phase_loss_fine_limit=0x%02x # ",1,0x7 & value);
	switch (0x7 & value) {
		case 0x0:
			printf ("Do not use. Indicates phase loss continuously\n");
			break;
		case 0x1:
			printf ("+/-45–90 ns\n");
			break;
		case 0x2: 
			printf ("+/-90–180 ns\n");
			break;
		case 0x3:
			printf ("+/-180–360\n");
			break;
		case 0x4:
			printf ("+/-20–25 ns\n");
			break;
		case 0x5: 
			printf ("+/- 60–65 ns\n");
			break;
		case 0x6: 
			printf ("+/- 120–125 ns\n");
			break;
		case 0x7: 
			printf ("+/- 950–955 ns\n");
			break;
	}
}

static void printDpllHoldoverModeCnfg (uint8_t * table)
{
	uint8_t val=table[DPLL1_HOLDOVER_MODE_CNFG_7_0];
	uint8_t val1=table[DPLL1_HOLDOVER_MODE_CNFG_15_8];
	
	printf ("# DPLL%d Holdover Mode Configuration Register\n",1);
	printf ("dpll%d_holdover_mode_cnfg=0x%04x\n",1,val+ (val1 << 8));

	printf ("dpll%d_hist_mode=0x%02x\t\t# ",1,0x3 & (val >> 2));
	switch (0x3 & (val >> 2)) {
		case 0x0:
			printf ("Current averaged value\n");
			break;
		case 0x1:
			printf ("Averaged value 1 second before\n");
			break;
		case 0x2: 
			printf ("Averaged value 10 second before\n");
			break;
		case 0x3:
			printf ("Averaged value 60 second before\n");
			break;
	}
	printf ("dpll%d_avg_mode=0x%02x\t\t# ",1,0x3 & val );
	switch (0x3 & val) {
		case 0x0:
			printf ("0.18 MHz\n");
			break;
		case 0x1:
			printf ("1.5 MHz\n");
			break;
		case 0x2: 
			printf ("12 MHz\n");
			break;
		case 0x3:
			printf ("0.5H\n");
			break;
	}
	printf ("dpll%d_man_holdover=0x%02x\t\t# ",1,0x1 & (val1 >> 7));
	if (0x1 & (val1 >> 7))
		printf ("Holdover frequency is from holdover_freq_cnfg register\n");
	else
		printf ("Holdover frequency is determined automatically\n");
	
	printf ("dpll%d_auto_avgh=0x%02x\t\t# ",1,0x1 & (val1 >> 6));
	if (0x1 & (val1 >> 6))
		printf ("Averaged value is used as holdover frequency\"\n");
	else
		printf ("Instantaneous value is used as holdover frequency\"\n");
	printf ("dpll%d_read_avg=0x%02x\t\t# ",1,0x1 & val>>4);
	if (0x1 & (val1 >> 4))
		printf ("Register is either the averaged value according to dpll%d_avg_mode\"\n",1);
	else
		printf ("Register is the value written into it.\"\n");
	
	printf ("dpll%d_temp_holdover_mode=0x%02x\n",1,0x3 & val1>>2);
}

static void printDpllHoldoverFreqCnfg (uint8_t * table)
{
	printf ("# DPLL%d Holdover Frequency Configuration Register\n",1);
	printf ("dpll%d_holdover_freq_cnfg=0x%02x%02x%02x%02x%02x\n",1,table[DPLL1_HOLDOVER_FREQ_CNFG_39_32],table[DPLL1_HOLDOVER_FREQ_CNFG_31_24],
			table[DPLL1_HOLDOVER_FREQ_CNFG_23_16],table[DPLL1_HOLDOVER_FREQ_CNFG_15_8],table[DPLL1_HOLDOVER_FREQ_CNFG_7_0]);
}

static void printDpllFreqSoftLimitCnfg (uint8_t value)
{
	printf ("# DPLL%d DPLL Frequency Soft Limit Configuration Register\n",1);
	printf ("dpll%d_dpll_freq_soft_limit_cnfg=0x%02x\n",1,value);

	printf ("dpll%d_freq_limit_ph_loss=0x%02x\t# ",1,0x1 & (value >> 7));
	if (0x1 & (value >> 7))
		printf ("Enable\n");
	else
		printf ("Disable\n");
	
	printf ("dpll%d_dpll_soft_limit_cnfg=0x%02x\n",1,0x7f & value);
}

static void printDpllFreqHardLimitCnfg (uint8_t * table)
{
	printf ("# DPLL%d DPLL Frequency Hard Limit Configuration Register\n",1);
	printf ("dpll%d_dpll_hard_limit_cnfg=0x%04x\n",1,table[DPLL1_DPLL_FREQ_HARD_LIMIT_CNFG_7_0]+(table[DPLL1_DPLL_FREQ_HARD_LIMIT_CNFG_15_8]  << 8));
}

static void printDpllFrMfrSyncCnfg (uint8_t value)
{
	
	printf ("# DPLL%d Frame/Multi-Frame Sync Configuration Register\n",1);
	printf ("dpll%d_fr_mfr_sync_cnfg=0x%02x\n",1,value);

	printf ("dpll%d_in_2k_4k_8k_inv=0x%02x\t# ",1,0x1 & (value >> 7));
	if (0x1 & (value >> 7))
		printf ("Inverted\n");
	else
		printf ("Not inverted\n");
	
	printf ("dpll%d_8k_1pps_sel=0x%02x\t\t# ",1,0x1 & (value >> 6));
	if (0x1 & (value >> 6))
		printf ("8 kHz\n");
	else
		printf ("1 PPS\n");
	printf ("dpll%d_2k_1pps_sel=0x%02x\t\t# ",1,0x1 & (value >> 5));
	if (0x1 & (value >> 5))
		printf ("2 kHz\n");
	else
		printf ("1PPS\n");
	printf ("dpll%d_fr_mfrsync_pul_pos=0x%02x\t# ",1,0x1 & (value>>4));
	if (0x1 & (value >> 4))
		printf ("Pulsed on the rising edge of the standard 50:50 duty cycle position\n");
	else
		printf ("Pulsed on the falling edge of the standard 50:50 duty cycle position\n");
	printf ("dpll%d_8k_1pps_inv=0x%02x\t\t# ",1,0x1 & (value >> 3));
	if (0x1 & (value >> 3))
		printf ("Inverted\n");
	else
		printf ("Not inverted\n");
	printf ("dpll%d_frsync_pulse=0x%02x\t\t# ",1,0x1 & (value >> 2));
	if (0x1 & (value >> 2))
		printf ("Pulse\n");
	else
		printf ("50:50 duty cycle\n");
	printf ("dpll%d_2k_1pps_inv=0x%02x\t\t# ",1,0x1 & (value>>1));
	if (0x1 & (value >> 1))
		printf ("Inverted\n");
	else
		printf ("Not inverted\n");
	printf ("dpll%d_mfrsync_pulse=0x%02x\t# ",1,0x1 & value);
	if (0x1 & value )
		printf ("Pulse\n");
	else
		printf ("50:50 duty cycle\n");
}


static void printDpllSyncMonitorCnfg (uint8_t value)
{
	
	printf ("# DPLL%d Sync Monitor Configuration Register\n",1);
	printf ("dpll%d_sync_monitor_cnfg=0x%02x\n",1,value);

	printf ("dpll%d_sync_bypass=0x%02x\t\t# ",1,0x1 & (value >> 7));
	if (0x1 & (value >> 7))
		printf ("ex_sync is enabled if no ex_sync_alarm\n");
	else
		printf ("ex_sync is enabled\n");
	
	printf ("dpll%d_sync_monitor_limit=0x%02x\t# ",1,0x7 & (value >> 4));
	printf ("Sync alarm raised beyond +/- %d UI\n",1+(0x7 & (value >> 4)));
}

static void printDpllSyncEdgeCnfg (uint8_t value)
{

	printf ("# DPLL%d Sync Edge Configuration Register\n",1);
	printf ("dpll%d_sync_edge_cnfg=0x%02x\n",1,value);
	printf ("dpll%d_sync_tod=0x%02x\n",1,0x1 & (value >> 1));
	printf ("dpll%d_sync_edge=0x%02x\n",1,0x1 & value);
}

static void printDpllPhaseOffsetCnfg (uint8_t * table)
{
	printf ("# DPLL%d Phase Offset Configuration Register\n",1);
	printf ("dpll%d_ph_offset_en=0x%02x\t\t# ",1,0x1 & (table[DPLL1_PHASE_OFFSET_CNFG_28_24]>>7));
	if (0x1 & (table[DPLL1_PHASE_OFFSET_CNFG_28_24]>>7))
		printf ("Enable\n");
	else
		printf ("Disable\n");
	
	printf ("dpll%d_phase_offset_cnfg=0x%07x\n",1,table[DPLL1_PHASE_OFFSET_CNFG_7_0]+(table[DPLL1_PHASE_OFFSET_CNFG_15_8] << 8)+
		(table[DPLL1_PHASE_OFFSET_CNFG_23_15] << 16) +((0x1f&table[DPLL1_PHASE_OFFSET_CNFG_28_24]) << 24));
}

static void printDpllBwSwTimeCnfg (uint8_t * table)
{
	printf ("# DPLL%d Bandwidth Software Time Configuration Register\n",1);
	printf ("dpll%d_bw_sw_time1_cnfg=0x%02x\n",1,table[DPLL1_BW_SW_TIME1_CNFG]);
	printf ("dpll%d_bw_sw_time2_cnfg=0x%02x\n",1,table[DPLL1_BW_SW_TIME2_CNFG]);
	printf ("dpll%d_bw_sw_time3_cnfg=0x%02x\n",1,table[DPLL1_BW_SW_TIME3_CNFG]);
}

static void printDpllSlaveForceRefSelCnfg (uint8_t value)
{
	printf ("# DPLL%d Slave Force Reference Select Configuration Register\n",1);
	printf ("dpll%d_slave_force_ref_sel_cnfg=0x%02x # ",1,value);
	switch (value) {
		case 0x0:
			printf ("Reserved\n");
			break;
		case 0xf:
			printf ("Reserved\n");
			break;
		default:
			printf ("dpll forced to select IN%d\n",value);
			break;
	}

}

static void printDpllProgPhLimitCnfg (uint8_t * table)
{
	printf ("# DPLL%d Program Limit Configuration Register\n",1);
	printf ("dpll%d_prog_ph_limit_cnfg=0x%06x\n",1,table[DPLL1_PROG_PH_LIMIT_CNFG_7_0]+ (table[DPLL1_PROG_PH_LIMIT_CNFG_15_8] << 8)+
		(table[DPLL1_PROG_PH_LIMIT_CNFG_23_16] << 16));
}

/////////////////////////////////////////////////////////////
//APLL
static void printApllIcpCtrlCnfg (uint8_t * table, int num)
{
	uint8_t value=table[APLL1_ICP_CTRL_CNFG+ 0xe*(num-1)];
	printf ("# APLL%d Charge Pump Current Control Configuration Register\n",num);
	printf ("apll%d_icp_ctrl_code=0x%02x # %duA",num,value & 0x1f,40*(value & 0x1f));
}

static void printApllProgPhLimitCnfg (uint8_t * table, int num)
{
	uint8_t value_l=table[APLL1_DIVISOR_FRAC_L_CNFG+ 0xe*(num-1)];
	uint8_t value_m=table[APLL1_DIVISOR_FRAC_M_CNFG+ 0xe*(num-1)];
	uint8_t value_h=table[APLL1_DIVISOR_FRAC_H_CNFG+ 0xe*(num-1)];
	printf ("# APLL%d Divisor Fractional Configuration Register\n",num);
	printf ("dpll%d_prog_ph_limit_cnfg=0x%06x\n",num,value_l+(value_m << 8) + (value_h << 16));
}

static void printApllDivisorDenCnfg (uint8_t * table, int num)
{
	uint8_t value_l=table[APLL1_DIVISOR_DEN_L_CNFG+ 0xe*(num-1)];
	uint8_t value_h=table[APLL1_DIVISOR_DEN_H_CNFG+ 0xe*(num-1)];
	printf ("# APLL%d Divisor Denominator Configuration Register\n",num);
	printf ("apll%d_divn_den_cnfg=0x%04x\n",num,value_l+ (value_h << 8));
}

static void printApllDivisorNumCnfg (uint8_t * table, int num)
{
	uint8_t value_l=table[APLL1_DIVISOR_NUM_L_CNFG+ 0xe*(num-1)];
	uint8_t value_h=table[APLL1_DIVISOR_NUM_H_CNFG+ 0xe*(num-1)];
	printf ("# APLL%d Divisor Numerator Configuration Register\n",num);
	printf ("apll%d_divn_num_cnfg=0x%04x\n",num,value_l+(value_h << 8));
}

static void printApllDsmCnfg (uint8_t * table, int num)
{
	uint8_t value=table[APLL1_DSM_CNFG+ 0xe*(num-1)];
	printf ("# APLL%d DSM Configuration Register\n",num);
	printf ("apll%d_dsm_cnfg=0x%02x\n",num,value);
	printf ("apll%d_path_freq_cnfg=0x%02x # ",num,0x7&(value>>5));
	switch (0x7&(value>>5)) {
		case 0x0:
			printf ("622.08 MHz from DPLL1\n");
			break;
		case 0x1:
			printf ("625 MHz from DPLL1\n");
			break;
		case 0x2:
			printf ("625 * FEC MHz from DPLL1\n");
			break;
		case 0x4:
			printf ("622.08 MHz from DPLL2\n");
			break;
		case 0x5:
			printf ("625 MHz from DPLL2\n");
			break;
		case 0x6:
			printf ("625 * FEC MHz from DPLL2\n");
			break;
		default:
			printf ("Reserved\n");
			break;
	}
	printf ("apll%d_dsm_cnfg_en=0x%02x\t# ",num,0x1&(value>>4));
	switch (0x1&(value>>4)) {
		case 0x0:
			printf ("DSM uses pre-set parameters\n");
			break;
		case 0x1:
			printf ("DSM uses programmable parameters\n");
			break;
	}
	printf ("apll%d_dither_gain=0x%02x\t# ",num,0x3&(value>>2));
	switch (0x3&(value>>2)) {
		case 0x0:
			printf ("No dither\n");
			break;
		case 0x1:
			printf ("LSB\n");
			break;
		case 0x2:
			printf ("2*LSB\n");
			break;
		case 0x3:
			printf ("4*LSB\n");
			break;
	}
	printf ("apll%d_dsm_orapll1_der=0x%02x # ",num,0x3&value);
	switch (0x3&value) {
		case 0x0:
			printf ("Integer\n");
			break;
		case 0x1:
			printf ("1st order\n");
			break;
		case 0x2:
			printf ("2nd order\n");
			break;
		case 0x3:
			printf ("3rd order\n");
			break;
	}
}

static void printApllDivisorIntCnfg (uint8_t * table, int num)
{
	uint8_t value=table[APLL1_DIVISOR_INT_CNFG+ 0xe*(num-1)];
	printf ("# APLL%d Divisor Integer Configuration Register\n",num);
	printf ("apll%d_divn_int_cnfg=0x%02x\n",num,value & 0x3f);
}

static void printApllFrRatioCnfg (uint8_t * table, int num)
{
	uint8_t value_l=table[APLL1_FR_RATIO_CNFG_7_0+ 0xe*(num-1)];
	uint8_t value_m=table[APLL1_FR_RATIO_CNFG_15_8+ 0xe*(num-1)];
	uint8_t value_h=table[APLL1_FR_RATIO_CNFG_23_16+ 0xe*(num-1)];
	uint8_t value_x=0x1f*table[APLL1_FR_RATIO_CNFG_28_24+ 0xe*(num-1)];
	printf ("# APLL%d Frame/Multi-Frame Ratio Configuration Register\n",num);
	printf ("apll%d_fr_ratio_cnfg=0x%08x\t# (%d)\n",num,value_l+(value_m << 8)+ (value_h << 16) + (value_x << 24),value_l+(value_m << 8)+ (value_h << 16) + (value_x << 24));
}
/////////////////////////////////////////////////////////////
//OUTPUT
static void printOutMuxCnfg (uint8_t value, int num)
{
	int pdn=0x1&value;
	
	if ( num == 1 )
		pdn = 0x1&(value>>7);
	printf ("# Output %d Mux Configuration Register\n",num);
	printf ("out%d_mux_cnfg=0x%02x\n",num,value);
	printf ("out%d_pdn=0x%02x\t\t# ",num,pdn);
	switch (pdn) {
		case 0x0:
			printf ("Output divider not powered-down\n");
			break;
		case 0x1:
			printf ("Output divider powered-down\n");
			break;
	}
	printf ("out%d_inv=0x%02x\t\t# ",num,0x1&(value>>6));
	switch (0x1&(value>>6)) {
		case 0x0:
			printf ("Output not inverted\n");
			break;
		case 0x1:
			printf ("Output inverted\n");
			break;
	}
	printf ("out%d_squelch=0x%02x\t# ",num,0x3&(value>>4));
	switch (0x3&(value>>4)) {
		case 0x2:
			printf ("Squelch to 0\n");
			break;
		case 0x3:
			printf ("Squelch to 1\n");
			break;
		default:
			printf ("No squelch\n");
			break;
	}
	if (num == 1) {
		printf ("out%d_mux_cnfg=0x%02x\t# ",num,0x0f&value);
		switch (0x0f&value) {
			case 0x0:
				printf ("APLL1\n");
				break;
			case 0x3:
				printf ("DPLL1 25 MHz\n");
				break;
			case 0x4:
				printf ("DPLL1 77.76 MHz\n");
				break;
			case 0x5:
				printf ("DPLL1 12E1/GPS/E3/T3\n");
				break;
			case 0x6:
				printf ("DPLL1 16E1/16T1\n");
				break;
			case 0x7:
				printf ("DPLL1 GSM/OBSAI/16E1/16T1\n");
				break;
			case 0xb:
				printf ("DPLL2 25 MHz\n");
				break;
			case 0xc:
				printf ("DPLL2 77.76 MHz\n");
				break;
			case 0xd:
				printf ("DPLL2 12E1/24T1/E3/T3\n");
				break;
			case 0xe:
				printf ("DPLL2 16E1/16T1\n");
				break;
			case 0xf:
				printf ("DPLL2 GSM/GPS/16E1/16T1\n");
				break;
			default:
				printf ("Reserved\n");
				break;
		}
	}
	if (num == 4 ) {
		printf ("out%d_pad_pdn=0x%02x\t# ",num,0x01&(value>>1));
		switch (0x01&(value>>1)) {
			case 0x0:
				printf ("Output pad not powered-down\n");
				break;
			case 0x3:
				printf ("Output pad powered-down\n");
				break;
		}
		printf ("out%d_lvds_pecl=0x%02x\t# ",num,0x01&(value>>7));
		switch (0x01&(value>>7)) {
			case 0x0:
				printf ("LVDS mode output\n");
				break;
			case 0x3:
				printf ("PECL mode output\n");
				break;
		}
	}
}

static void printOutDiv1Cnfg (uint8_t value, int num)
{

	printf ("# Output %d Divisor 1 Configuration Register\n",num);
	printf ("out%d_div1_cnfg=0x%02x\n",num,value);
	printf ("out%d_sync_en=0x%02x\t# ",num,0x1&(value>>7));
	switch (0x1&(value>>7)) {
		case 0x0:
			printf ("Disable sync\n");
			break;
		case 0x1:
			printf ("Enable sync\n");
			break;
	}
	printf ("out%d_div1_cnfg=0x%02x\n",num,0x1f&value);
}

static void printOutDiv2Cnfg (uint8_t * table, int offset, int num)
{

	printf ("# Output %d Divisor 2 Configuration Register\n",num);
	printf ("out%d_div2_cnfg=0x%02x%02x%02x%02x\n",num,0x7&table[offset+3],table[offset+2],table[offset+1],table[offset]);
}

static void printOutPh1Cnfg (uint8_t value, int num)
{

	printf ("# Output %d Phase 1 Configuration Register\n",num);
	printf ("out%d_ph1_cnfg=0x%02x\n",num,0x1f&value);
}

static void printOutPh2Cnfg (uint8_t * table, int offset, int num)
{
	int mask=0x7;
	if (num == 1)
		mask = 0x1f;
	printf ("# Output %d Phase 2 Configuration Register\n",num);
	printf ("out%d_ph2_cnfg=0x%02x%02x%02x%02x\n",num,mask&table[offset+3],table[offset+2],table[offset+1],table[offset]);
}

static void printOutFinePhCnfg (uint8_t value, int num)
{

	printf ("# Output %d Fine Phase Configuration Register\n",num);
	printf ("out%d_fine_ph_cnfg=0x%02x\n",num,value);
	printf ("out%d_pulse_cnfg=0x%02x\t# ",num,0x3&(value>>6));
	switch (0x3&(value>>6)) {
		case 0x0:
			printf ("2 T\n");
			break;
		case 0x1:
			printf ("20 T\n");
			break;
		case 0x2:
			printf ("2000 T\n");
			break;
		case 0x3:
			printf ("50%c duty cycle\n",'%');
			break;
	}
	printf ("out%d_fine_ph_cnfg=0x%02x\n",num,0x7&value);
}
/////////////////////////////////////////////////////////////
//global fce
void printAllReg (uint8_t* table)
{
	int i;
	uint8_t* ptr=table;
	for (i=0;i<REG_MAX;i++,ptr++) {
		if (!(i%0x10))
			printf("\nReg:%03x: ",i);
		printf("%02x ",*ptr);
	}
	printf("\n");
}

static void printOutput (uint8_t* table, int num)
{
	printf ("###############################################\n");
	printf ("# OUTPUT %d\n", num);
	switch (num) {
		case 1:
			printOutMuxCnfg (table[OUT1_MUX_CNFG], num);
			printOutDiv1Cnfg (table[OUT1_DIV1_CNFG], num);
			printOutDiv2Cnfg (table,OUT1_DIV2_CNFG_7_0, num);
			printOutPh1Cnfg (table[OUT1_PH1_CNFG], num);
			printOutPh2Cnfg (table, OUT1_PH2_CNFG_7_0,num);
			printOutFinePhCnfg (table[OUT1_FINE_PH_CNFG], num);
			break;
		case 2:
			printOutMuxCnfg (table[OUT2_MUX_CNFG], num);
			printOutDiv1Cnfg (table[OUT2_DIV1_CNFG], num);
			printOutDiv2Cnfg (table,OUT2_DIV2_CNFG_7_0, num);
			printOutPh1Cnfg (table[OUT2_PH1_CNFG_4_0], num);
			printOutPh2Cnfg (table, OUT2_PH2_CNFG_7_0,num);
			printOutFinePhCnfg (table[OUT2_FINE_PH_CNFG], num);
			break;
		case 4:
			printOutMuxCnfg (table[OUT4_MUX_CNFG], num);
			printOutDiv1Cnfg (table[OUT4_DIV1_CNFG], num);
			printOutDiv2Cnfg (table,OUT4_DIV2_CNFG_7_0, num);
			printOutPh1Cnfg (table[OUT4_PH1_CNFG_4_0], num);
			printOutPh2Cnfg (table, OUT4_PH2_CNFG_7_0,num);
			printOutFinePhCnfg (table[OUT4_FINE_PH_CNFG], num);
			break;
		case 7:
			printOutMuxCnfg (table[OUT7_MUX_CNFG], num);
			printOutDiv1Cnfg (table[OUT7_DIV1_CNFG], num);
			printOutDiv2Cnfg (table,OUT7_DIV2_CNFG_7_0, num);
			printOutPh1Cnfg (table[OUT7_PH1_CNFG], num);
			printOutPh2Cnfg (table, OUT7_PH2_CNFG_7_0,num);
			printOutFinePhCnfg (table[OUT7_FINE_PH_CNFG], num);
			break;
	}
}

static void printOutput (uint8_t* table)
{
	printf ("\n###############################################\n");
	printf ("# OUTPUT\n");
	printf ("###############################################\n");
	printOutput (table, 1);
	printOutput (table, 2);
	printOutput (table, 4);
	printOutput (table, 7);
}


static void printDpll1 (uint8_t* table)
{
	printf ("\n###############################################\n");
	printf ("# DPLL1\n");
	printf ("###############################################\n");
	printDpllPriorityTableSts (table);
	printDpllOperatingSts (table[DPLL1_OPERATING_STS]);
	printDpllCurrentDpllFreqSts (table);
	printDpllCurrentDpllPhaseSts (table);
	printDpllTodSecSts (table);
	printDpllTodTrigger (table[DPLL1_TOD_TRIGGER]);
	printDpllInputModeCnfg (table[DPLL1_INPUT_MODE_CNFG]);
	printDpllMonSwPboCnfg (table[DPLL1_MON_SW_PBO_CNFG]);
	printDpllInSelPriorityCnfg (table);
	printDpllInputSelCnfg (table[DPLL1_INPUT_SEL_CNFG]);
	printDpllOperatingModeCnfg (table[DPLL1_OPERATING_MODE_CNFG]);
	printDpllFbSelCnfg (table[DPLL1_FB_SEL_CNFG]);
	printDpllUpdateEventCnfg (table[DPLL1_UPDATE_EVENT_CNFG]);
	printDpllPathCnfg (table[DPLL1_DPLL_PATH_CNFG]);
	printDpllStartBwDampingCnfg (table[DPLL1_DPLL_START_BW_DAMPING_CNFG]);
	printDpllAcqBwDampingCnfg (table[DPLL1_DPLL_ACQ_BW_DAMPING_CNFG]);
	printDpllLockedBwDampingCnfg (table[DPLL1_DPLL_LOCKED_BW_DAMPING_CNFG]);
	printDpllBwOvershootCnfg (table[DPLL1_BW_OVERSHOOT_CNFG]);
	printDpllPhaseLossCoarseLimitCnfg (table[DPLL1_PHASE_LOSS_COARSE_LIMIT_CNFG]);
	printDpllPhaseLossFineLimitCnfg (table[DPLL1_PHASE_LOSS_FINE_LIMIT_CNFG]);
	printDpllHoldoverModeCnfg (table);
	printDpllHoldoverFreqCnfg (table);
	printDpllFreqSoftLimitCnfg (table[DPLL1_DPLL_FREQ_SOFT_LIMIT_CNFG]);
	printDpllFreqHardLimitCnfg (table);
	printDpllFrMfrSyncCnfg (table[DPLL1_FR_MFR_SYNC_CNFG]);
	printDpllSyncMonitorCnfg (table[DPLL1_SYNC_MONITOR_CNFG]);
	printDpllSyncEdgeCnfg (table[DPLL1_SYNC_EDGE_CNFG]);
	printDpllPhaseOffsetCnfg (table);
	printDpllBwSwTimeCnfg (table);
	printDpllSlaveForceRefSelCnfg (table[DPLL1_SLAVE_FORCE_REF_SEL_CNFG]);
	printDpllProgPhLimitCnfg (table);
}

static void printApll (uint8_t* table, int num)
{
	printf ("\n###############################################\n");
	printf ("# APLL%d\n",num);
	printf ("###############################################\n");
	
	printApllIcpCtrlCnfg (table, num);
	printApllProgPhLimitCnfg (table,num);
	printApllDivisorDenCnfg (table,num);
	printApllDivisorNumCnfg (table,num);
	printApllDsmCnfg (table, num);
	printApllDivisorIntCnfg (table, num);
	printApllFrRatioCnfg (table,num);

}

void printStatus (uint8_t* table)
{
	//MAP IN RAy3
	//82p33714	doc PORT
	//in1 		in3
	//in2		in4
	//in3		in5
	//in5		in9
	//in6		in10
	
	//out1		out1
	//out2		out2
	//out4		out4
	//out7		out7
	printf ("###############################################\n");
	printf ("# Global Control Registers\n");
	printf ("###############################################\n");
	printId (table[REG_ID]);
	printMpuSelCnfg (table[MPU_SEL_CNFG]);
	printXOfreqCnfg (table[XO_FREQ_CNFG]);
	printNominalFreq (table);
	printInterrupt (table);
	printInterruptStatus (table);
	printInterruptMask (table);
	printI2cSlaveAddr (table[I2C_SLAVE_ADDR_CNFG]);
	printf ("\n###############################################\n");
	printf ("# Configuration Registers\n");
	printf ("###############################################\n");
	printInCnfg (table[IN3_CNFG],1);
	printInCnfg (table[IN4_CNFG],2);
	printInCnfg (table[IN5_CNFG],3);
	printInCnfg (table[IN9_CNFG],5);
	printInCnfg (table[IN10_CNFG],6);
	printHfDivCnfg (table[HF_DIV_CNFG_7_0]);
	printInPdnCnfg (table[IN_PDN_CNFG]);
	printInFecDiv (table[IN3_FEC_DIVP_CNFG_7_0], table[IN3_FEC_DIVP_CNFG_15_8] , 1, 'p');
	printInFecDiv (table[IN3_FEC_DIVQ_CNFG_7_0], table[IN3_FEC_DIVQ_CNFG_15_8] , 1, 'q');
	printInFecDiv (table[IN3_PRE_DIVN_CNFG_7_0], table[IN3_PRE_DIVN_CNFG_7_0] , 1, 'n');
	printInFecDiv (table[IN4_FEC_DIVP_CNFG_7_0], table[IN4_FEC_DIVP_CNFG_15_8] , 2, 'p');
	printInFecDiv (table[IN4_FEC_DIVQ_CNFG_7_0], table[IN4_FEC_DIVQ_CNFG_15_8] , 2, 'q');
	printInFecDiv (table[IN4_PRE_DIVN_CNFG_7_0], table[IN4_PRE_DIVN_CNFG_7_0] , 2, 'n');
	printInFecDiv (table[IN5_FEC_DIVP_CNFG_7_0], table[IN5_FEC_DIVP_CNFG_15_8] , 3, 'p');
	printInFecDiv (table[IN5_FEC_DIVQ_CNFG_7_0], table[IN5_FEC_DIVQ_CNFG_15_8] , 3, 'q');
	printInFecDiv (table[IN5_PRE_DIVN_CNFG_7_0], table[IN5_PRE_DIVN_CNFG_7_0] , 3, 'n');
	printf ("\n###############################################\n");
	printf ("# Reference Monitor Registers\n");
	printf ("###############################################\n");
	printFreqMonFactorCnfg (table[FREQ_MON_FACTOR_CNFG]);
	printHardFreqMonThresholdCnfg (table[HARD_FREQ_MON_THRESHOLD_CNFG]);
	printSoftFreqMonThresholdCnfg (table[SOFT_FREQ_MON_THRESHOLD_CNFG]);
	printUpperThresholdCnfg (table);
	printLowerThresholdCnfg (table);
	printBucketSizeCnfg (table);
	printDecayRateCnfg (table);
	printInFreqReadSts (table);
	printRemoteInputValidCnfg (table);
	printPhaseAlarmTimeCnfg (table[PHASE_ALARM_TIME_CNFG]);
	printLosSts (table[LOS_STS]);
	printInSts (table);
	printInLosSyncCnfg (table);
	printInSyncPhaseCnfg (table);
	printInPhaseOffsetCnfg (table);
	//DPLL1
	printDpll1 (table);
	//DPLL2 neni pouzito
	//printDpll (table,1);
	//DPLL3 neni pouzito
	//APPL1
	printApll (table, 1);
	//APPL2
	printApll (table, 2);
	//output
	printOutput (table);
	printf("\n");
}