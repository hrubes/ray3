#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include <getopt.h>

#include "debugTU.h"
#include "i2cDriverTU.h"
#include "printTU.h"

int verbose;


struct option longopts[] = {
 /* 	{ name  has_arg  *flag  val } */
	{"boot",		1, NULL, 'b'},
	{"set"	,		1, NULL, 's'},	
	{"get"	,		1, NULL, 'g'},
	{"getlen",		1, NULL, 'l'},
	{"getall",		0, NULL, 'G'},
	{"status",		0, NULL, 't'},
	{"help", 		0, NULL, 'h'},	
	{"verbose",		1, NULL, 'v'},
    {"i2c",     	1, NULL, 'i'},
	{ 0, 0, NULL, 0 }
};

char * dev=(char*) TU_DEV;
int reg;
int value;

uint8_t regValue;

uint8_t allReg[REG_MAX];

int checkReg (int reg)
{
	if ((reg < 0) || (reg>=REG_MAX)) {
		printf ("Wrong value 0x%x, must be <0..0x3ff>",reg);
		return -ERANGE;
	}
	return 0;
}

#define SIZE_LINE 4*PAGE_MAX*REG_AREA
int readBootData(char * filename , uint8_t * data)
{
	char * line;
	int num=0; 
	char * ptr;
	uint8_t * ptr_data=data;
	unsigned int val;
	
    DEBUG ("Reading boot data\n;");
	if ((line=(char *)malloc (SIZE_LINE)) == NULL) {
		fprintf(stderr,"Don't allocate memory\n");
		return -ENOMEM;
	}
	FILE* f = fopen(filename,"r");
	if(!f){
		fprintf (stderr,"Opening error: %s\n",filename);
		free (line);
		return -ENOENT;
	}
	if (fgets(line,SIZE_LINE,f)) {
		ptr=strtok (line," ");
		while (ptr != NULL) {
			sscanf (ptr,"%x",&val);
			ptr_data[num]=0xff& val;
			ptr = strtok (NULL, " ");
			num++;
		}	
	}
	printf ("Read from file %d byte\n",num);
	fclose (f);
	free (line);
	return num;
}
/*--------------------------------------------------------------------*/
const char *usage =
"usage: timing-unit [-h] [-i param] [-t | -G | -s | -g | -b | l ] [param]\n"
"   -b, --boot                     boot\n" 
"       param:  filename            filemane with path\n"
"   -s, --set                      set register\n" 
"       param:  REG:VALUE[:MASK]    numeric value\n"
"   -g, --get                      get one register\n"
"       param:  REG                 numeric value\n"
"   -l, --getlen                    get registers\n"
"       param:  REG:NUMBER          numeric value\n"
"   -G, --getall                    get all registers\n"
"   -t, --status                    get status\n"
"   -i, --i2c                       set i2c address\n"
"       param:  ADDR                i2c Address (default 0x50)\n"
"   -v, --verbose                   verbose\n"
"       param:  verbosity           1 or 2\n"
"   -h  --help                      help\n";

int main(int argc, char **argv)
{
	int ret= 0;
	int file;
	int c;
	uint8_t data[PAGE_MAX*REG_AREA];
	int len;
	int i;
	int i2c_addr=TU_ADDR;
    char par=' ';
	
	if (argc == 1) {
		printf ("%s",usage);
		return 0;
	}
	verbose = 0;
	
	while ((c = getopt_long(argc, argv, "hGts:g:b:v:i:l:", longopts, NULL )) != EOF) {
		switch (c) {
			case 'v': 
				verbose=atoi(optarg);
				break;	
			case 'h': 
				printf ("%s",usage);
				return 0;
				break;
			case 'i':
				sscanf(optarg,"%i",&i2c_addr);
				if ((i2c_addr < 0 ) || (i2c_addr > 0x80) ) {
					printf ("Incorrect i2c address");
					exit (-1);
				}
			case 's': 
				sscanf(optarg,"%i:%i",&reg,&value);
				if ((ret=checkReg (reg)) < 0)
					return ret;
				par=c;
				break;
			case 'g': 
				sscanf(optarg,"%i",&reg);
				par=c;
				break;
			
			case 'l': 
				sscanf(optarg,"%i:%i",&reg,&len);
				if ((ret=checkReg (reg)) < 0)
					return ret;
				if ((reg + len)  > REG_MAX)
					return -ERANGE;
				par=c;
				break;
			case 'G': 
				par=c;
				break;
				
			case 't': 
				par=c;
				break;
			case 'b':
				if ((len=readBootData(optarg , data)) < 0) {
					printf ("Error reading boot data. \n");
					exit (len);
				}
				par=c;
				break;
		}
	}
	
	switch (par) {
		case 's': 
			if ((file=initI2cDevice (dev , i2c_addr)) <  0){
				exit (file);
			}
			ret=setReg(file,reg,value);
			closeI2cDevice (file, ret);
			if (ret < 0) {
				printf ("Error writing to the registry 0x%03x value 0x%02x. \n",reg,value);
			} else {
				printf ("Writing to the registry 0x%03x value 0x%02x. \n",reg,value);
			}
			exit (ret);
			break;
		case 'g': 
			if ((file=initI2cDevice (dev , i2c_addr)) <  0){
				exit (file);
			}
			ret=getReg(file,reg,&regValue);
			closeI2cDevice (file, ret);
			if (ret < 0) {
				printf ("Error reading from registry 0x%03x. \n",reg);
			} else {
				printf ("Reg_0x%03x=0x%02x\n",reg,regValue);
			}
			exit (ret);
			break;
		case 'l': 
			if ((file=initI2cDevice (dev , i2c_addr)) <  0){
				exit (file);
			}
			for (i=0; i < len; i ++ ) {
				ret=getReg(file,reg+i,&regValue);
				if (ret < 0) {
					printf ("Error reading from registry 0x%03x. \n",reg+i);
				} else {
					printf ("Reg_0x%03x=0x%02x \n",reg+i,regValue);
				}
			}
			closeI2cDevice (file, ret);
			exit (ret);
			break;
		case 'G': 
			if ((file=initI2cDevice (dev , i2c_addr)) <  0){
				exit (file);
			}
			ret=getAllPageAllReg(file,allReg,REG_MAX);
			closeI2cDevice (file, ret);
			if (ret < 0) {
				printf ("Error reading from all registers. \n");
			} else {
				printAllReg (allReg);
			}
			exit (ret);
			break;
		case 't': 
			if ((file=initI2cDevice (dev , i2c_addr)) <  0){
				exit (file);
			}
			ret=getAllPageAllReg(file,allReg,REG_MAX);
			closeI2cDevice (file, ret);
			if (ret < 0) {
				printf ("Error reading from all registers. \n");
			} else {
				printStatus (allReg);
			}
			exit (ret);
			break;
		case 'b': 
			if ((file=initI2cDevice (dev , i2c_addr)) <  0){
				exit (file);
			}
			if (len == 1024) {
				ret=bootTu (file, data, len);
				if (ret >= 0 )
					ret=swReset (file);
				closeI2cDevice (file, ret);
				if (ret < 0) {
					printf ("Error.\n");
				} else {
					printf ("Success (write %d bytes).\n",len);
				}
			} else {
				printf ("Error - length of data must be 1024.\n");
				ret=-1;
			}
			exit (ret);
			break;
	}
	exit (0);
}





