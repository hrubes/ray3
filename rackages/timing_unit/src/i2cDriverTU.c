#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>

#include "debugTU.h"
#include "i2cDriverTU.h"

#define SYSFS_GPIO_DIR 	"/sys/class/gpio"
#ifdef RR_PRODUCT_bma
    #define TU_RESET 	508
#endif

#ifdef RR_PRODUCT_bk1
    #define TU_RESET 	509		//GPIO pin
#endif

#ifdef RR_PRODUCT_bk2
    #define TU_RESET 	509
#endif


extern int verbose;

uint8_t i2c_addr;

//def fce
static int gpioSetValue(unsigned int gpio, int value);
static int resetTU (void);
static int addrAbsReg (int reg, uint8_t * _reg, uint8_t * _page);
static int getOneRegInPage (int file, uint8_t reg,  uint8_t  *value);
static int setOneRegInPage (int file, uint8_t reg, uint8_t value);
static int getRegInPage (int file, uint8_t reg,  uint8_t  *value, int len);
static int setPage (int file, uint8_t page);
static int getRegInPage (int file, uint8_t reg,  uint8_t  *value, int len);

//internal fce

static int gpioSetValue(unsigned int gpio, int value)
{
	int fd;
	char buf[64];

	if (value > 1)
		return -EINVAL;
	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR"/gpio%d/value", gpio);
	fd=open(buf, O_WRONLY);
	if (fd < 0) {
		fprintf(stderr,"Failed to set GPIO value: %s.\n",strerror (errno));
		return fd;
	}
	if (value == 1)
		write(fd, "1", 2);
	if  (value == 0)
		write(fd, "0", 2);
 
	close(fd);
	return 0;
}

static int resetTU (void)
{

    DEBUG ("Reset Timing Unit\n");
	gpioSetValue(TU_RESET, 1);
	usleep(50);
	gpioSetValue(TU_RESET, 0);
	usleep(2000);
	return 0;
}

static int addrAbsReg (int reg, uint8_t * _reg, uint8_t * _page)
{
    DEBUG ("Calculation the register and pages\n");
    if (reg > REG_MAX) {
		fprintf(stderr,"Wrong address register: 0x%03x.\n",reg);
		return -ERANGE;
	}
	*_reg=reg%REG_AREA;
	*_page=reg/REG_AREA;
    DEBUG_ALL ("Page: 0x%x, reg: 0x%x\n",*_reg,*_page);
	return 0;
}



static int getOneRegInPage (int file, uint8_t reg,  uint8_t  *value)
{
	uint8_t inbuf, outbuf;
	struct i2c_rdwr_ioctl_data i2c_data;
	struct i2c_msg msg[2];

    DEBUG ("Get register 0x%x\n",reg);
	outbuf = reg;
	msg[0].addr = i2c_addr;
	msg[0].flags = 0;
	msg[0].len = sizeof(outbuf);
	msg[0].buf = &outbuf;


	msg[1].addr = i2c_addr;
	msg[1].flags = I2C_M_RD;
	msg[1].len = sizeof(inbuf);
	msg[1].buf = &inbuf;


	i2c_data.msgs = msg;
	i2c_data.nmsgs = 2;
	if( ioctl(file, I2C_RDWR, &i2c_data) < 0 ) {
		fprintf(stderr,"Failed read from i2c :%s.\n",strerror (errno));
		return -ENODEV;
	}
	*value = inbuf;
    DEBUG_ALL ("Get value 0x%x\n",inbuf);
	return 0;
}

static int getRegInPage (int file, uint8_t reg,  uint8_t  *value, int len)
{
	uint8_t outbuf;
	struct i2c_rdwr_ioctl_data i2c_data;
	struct i2c_msg msg[2];


    
    DEBUG ("Get registers 0x%x, len 0x%x\n",reg,len);
	outbuf = reg;
	msg[0].addr = i2c_addr;
	msg[0].flags = 0;
	msg[0].len = sizeof(outbuf);
	msg[0].buf = &outbuf;


	msg[1].addr = i2c_addr;
	msg[1].flags = I2C_M_RD;
	msg[1].len = len;
	msg[1].buf = value;


	i2c_data.msgs = msg;
	i2c_data.nmsgs = 2;
	if( ioctl(file, I2C_RDWR, &i2c_data) < 0 ) {
		fprintf(stderr,"Failed read from i2c :%s.\n",strerror (errno));
		return -ENODEV;
	}
	return 0;
}

static int setOneRegInPage (int file, uint8_t reg, uint8_t value) 
{
	uint8_t buffer[]={reg,value};
	struct i2c_rdwr_ioctl_data i2c_data;
	struct i2c_msg msg[1];

    DEBUG ("Set value 0x%x to register 0x%x\n",value,reg);
	msg[0].addr = i2c_addr;
	msg[0].flags = 0;
	msg[0].len = sizeof(buffer);
	msg[0].buf = buffer;

	i2c_data.msgs = msg;
	i2c_data.nmsgs = 1;

	if(ioctl(file, I2C_RDWR, &i2c_data) < 0) {
		fprintf(stderr,"Failed send to i2c :%s.\n",strerror (errno));
		return -ENODEV;
	}
	return 0;
}

static int setRegInPage (int file, uint8_t reg,  uint8_t  *value, int len)
{
	uint8_t buffer[REG_AREA];
	struct i2c_rdwr_ioctl_data i2c_data;
	struct i2c_msg msg[1];

    DEBUG ("Set values to the registers:0x%x (len:0x%x)\n",reg,len);
    if ( (len+reg) > REG_AREA) {
        fprintf(stderr,"Wrong lenght: %d.\n",len );
		return -ERANGE;
    }
    
    buffer[0]=reg;
    memcpy (&buffer[1],value,len);
    msg[0].addr = i2c_addr;
	msg[0].flags = 0;
	msg[0].len = len+1;
	msg[0].buf = buffer;

	i2c_data.msgs = msg;
	i2c_data.nmsgs = 1;
	if( ioctl(file, I2C_RDWR, &i2c_data) < 0 ) {
		fprintf(stderr,"Failed send to i2c :%s.\n",strerror (errno));
		return -ENODEV;
	}
	return 0;
}

static int setPage (int file ,uint8_t page)
{
	int ret;
	uint8_t actPage;
    DEBUG ("Set page 0x%x\n",page);
	if ((ret=getOneRegInPage (file, REG_PAGE, &actPage)) < 0) {
		fprintf(stderr,"Error reading the page number.\n");
		return -ENODEV;
	}
	if (page !=actPage) {
		if ((ret=setOneRegInPage (file, REG_PAGE, page) < 0)) {
			fprintf(stderr,"Error setting the page number (0x%x).\n",page);
			return -ENODEV;
		}
	}
	return 0;
	
}
// I2c interface
int initI2cDevice (char * filename , int addr)
{
	
	int file;
    DEBUG_ALL("Init i2c device \"%s\"\n",filename);
	if ((file = open(filename,O_RDWR)) < 0) {
		fprintf(stderr,"Failed to open the bus:%s.\n",strerror (errno));
	}
	if (ioctl(file,I2C_SLAVE,addr) < 0) {
		fprintf(stderr,"Failed to acquire bus access and/or talk to slave:%s.\n",strerror (errno));
		return -ENODEV;
	}
	i2c_addr=addr;
	return file;
}

int closeI2cDevice (int file, int ret)
{
	DEBUG_ALL("Close i2c device \n");
	close (file);
	return ret;
}


int setReg(int file, int reg, uint8_t value) 
{
	uint8_t _page;
	uint8_t _reg;
	int ret;
	
	DEBUG ("SetReg 0x%x, value 0x%x\n",reg,value);
	if ((ret=addrAbsReg (reg, & _reg, & _page) ) < 0)
		return ret;
	if ((ret=setPage (file, _page)) < 0)
		return ret;
	return setOneRegInPage (file, _reg, value);
}

int getReg(int file, int reg, uint8_t * value) 
{
	uint8_t _page;
	uint8_t _reg;
	int ret;
	DEBUG ("GetReg 0x%x\n",reg);
	if ((ret=addrAbsReg (reg, &_reg, & _page) ) < 0)
		return ret;
	if ((ret=setPage (file, _page)) < 0)
		return ret;
	return getOneRegInPage (file, _reg, value);
}


int getPageReg(int file, uint8_t page , uint8_t reg, uint8_t * value, int len) 
{
	int ret;
	DEBUG ("GetPageReg page:0x%x reg:0x%x len:0x%x\n",reg,page,len);
	if ((ret=setPage (file, page)) < 0)
		return ret;
	return getRegInPage (file, 0 , value, len);
	
}

int getAllPageAllReg(int file,uint8_t * table, int len) 
{
	int ret;
	int i;
	DEBUG ("GetAllPageAllReg len:0x%x\n",len);
	if ( len > REG_AREA*PAGE_MAX) {
		fprintf(stderr,"Wrong lenght: %d.\n",len );
		return -ERANGE;
	}
        
	for (i=0; i< PAGE_MAX; i++) {
		if ((ret=getPageReg(file, i ,0 ,table+(i*REG_AREA), REG_AREA)) < 0)
			return ret;
	}
	return 0;
}


int setPageReg(int file, uint8_t page , uint8_t reg, uint8_t * value, int len) 
{
	int ret;
	
	DEBUG ("SetPageReg page:0x%x reg:0x%x len:0x%x\n",reg,page,len);
	if ((ret=setPage (file, page)) < 0)
		return ret;
	return setRegInPage (file, 0 , value, len);
	
}

int setAllPageAllReg(int file,uint8_t * table, int len) 
{
	int ret;
	int i;
	DEBUG ("SetAllPageAllReg len:0x%x\n",len);
    
	if ( len > REG_AREA*PAGE_MAX) {
		fprintf(stderr,"Wrong lenght: %d.\n",len );
		return -ERANGE;
	}
	for (i=0; i< PAGE_MAX; i++) {
		if ((ret=setPageReg(file, i ,0 ,table+(i*REG_AREA), REG_AREA)) < 0)
			return ret;
	}
	return 0;
}

int bootTu (int file, uint8_t * table, int len)
{
    
	DEBUG ("Boot TU len:0x%x\n",len);
	resetTU ();
	//Podpera prepisuji nastaveni i2c dle modemu
	*(table+0xf)=i2c_addr;
	return setAllPageAllReg(file,table,len);
}

int swReset (int file)
{
	DEBUG ("SW reset TU\n");
    return setReg(file, 0x381, 0x80);
}
