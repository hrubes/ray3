#ifndef _TU_DEBUG_H_
#define _TU_DEBUG_H_

#define DEBUG(args...)  { if(verbose>0) {printf ("Debug:  "); printf(args); } }
#define DEBUG_ALL(args...)  { if(verbose>1) {printf ("Debug:  "); printf(args);} }

#endif