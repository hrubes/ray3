#ifndef _TIMING_UNIT_INTERFACE_H_
#define _TIMING_UNIT_INTERFACE_H_

//interface
#define 	TU_DEV		"/dev/i2c-0"
#define		TU_ADDR		0x50
#define		REG_PAGE	0x7f
#define		REG_AREA	0x80
#define		REG_MAX		0x400
#define 	PAGE_MAX	8

//globalni promenna


//prototypy funkci
int initI2cDevice (char * filename , int addr);
int closeI2cDevice (int file, int ret);
int setReg(int file, int reg, uint8_t value);
int getReg(int file, int reg, uint8_t * value);
int getPageReg(int file, uint8_t page , uint8_t reg, uint8_t * value, int len);
int getAllPageAllReg(int file,uint8_t * table, int len);
int setPageReg(int file, uint8_t page , uint8_t reg, uint8_t * value, int len);
int setAllPageAllReg(int file,uint8_t * table, int len) ;
int bootTu (int file, uint8_t * table, int len);
int swReset (int file);
#endif
