#ifndef _TIMING_UNIT_REGISTERS_H_
#define _TIMING_UNIT_REGISTERS_H_

//Global Control Registers
#define REG_ID				0x00	//Identification Register
#define MPU_SEL_CNFG 			0x01	//MPU Selection Configuration Register
#define XO_FREQ_CNFG 			0x02	//XO Frequency Configuration Register
#define NOMINAL_FREQ_CNFG_7_0 		0x03	//Nominal Frequency Configuration Register
#define NOMINAL_FREQ_CNFG_15_8	 	0x04	//
#define NOMINAL_FREQ_CNFG_23_16 	0x05	//
#define INTERRUPT_CNFG 			0x06	//Interrupt Configuration Register
#define INTERRUPT_STS_7_0 		0x07	//Interrupt Status Register [7:0]
#define INTERRUPT_STS_15_8 		0x08	//Interrupt Status Register [15:8]
#define INTERRUPT_STS_23_16 		0x09	//Interrupt Status Register [23:16]
#define	INTERRUPT_STS_31_24 		0x0A 	//Interrupt Status Register [31:24]
#define	INTERRUPT_MASK_CNFG_7_0 	0x0B 	//Interrupt Mask Configuration Register [7:0]
#define INTERRUPT_MASK_CNFG_15_8	0x0C	 //Interrupt Mask Configuration Register [15:8]
#define INTERRUPT_MASK_CNFG_23_16 	0x0D	//Interrupt Mask Configuration Register [23:16]
#define INTERRUPT_MASK_CNFG_31_24 	0x0E	//Interrupt Mask Configuration Register [31:24]
#define I2C_SLAVE_ADDR_CNFG		0x0F	//Slave Address Configuration Register
//Configuration Registers
#define IN1_CNFG			0x11
#define IN2_CNFG			0x12
#define IN3_CNFG			0x13	//input1
#define IN4_CNFG			0x14	//input2
#define IN5_CNFG			0x15	//input3
#define IN6_CNFG			0x16	//input4
#define IN7_CNFG			0x17
#define IN8_CNFG			0x18
#define IN9_CNFG			0x19	//input5
#define IN10_CNFG			0x1a	//input6
#define IN11_CNFG			0x1b
#define IN12_CNFG			0x1c
#define IN13_CNFG			0x1d
#define IN14_CNFG			0x1e

#define HF_DIV_CNFG_7_0 		0x1F	//High Frequency Divider Configuration Register [7:0] for input {3:6}
#define HF_DIV_CNFG_11_8 		0x20 	//High Frequency Divider Configuration Register [11:8] for input {7:8}
#define IN_PDN_CNFG  			0x21 	//Input Power-Down Configuration Register for input {1:8}
#define IN3_FEC_DIVP_CNFG_7_0 		0x2C 	//Input 3 FEC Divider Numerator P Register
#define IN3_FEC_DIVP_CNFG_15_8	 	0x2D  
#define IN3_FEC_DIVQ_CNFG_7_0 		0x2E 	//Input 3 FEC Divider Denominator Q Register 
#define IN3_FEC_DIVQ_CNFG_15_8 		0x2F 
#define IN3_PRE_DIVN_CNFG_7_0 		0x30 	//Input 3 Pre-divider Denominator N Register
#define IN3_PRE_DIVN_CNFG_14_8 		0x31 	 
#define IN4_FEC_DIVP_CNFG_7_0 		0x32 	//Input 4 FEC Divider Numerator P Register
#define IN4_FEC_DIVP_CNFG_15_8 		0x33 
#define IN4_FEC_DIVQ_CNFG_7_0 		0x34	//Input 4 FEC Divider Numerator P Register 
#define IN4_FEC_DIVQ_CNFG_15_8 		0x35
#define IN4_PRE_DIVN_CNFG_7_0 		0x36 	//Input 4 Pre-divider Denominator N Register
#define IN4_PRE_DIVN_CNFG_14_8 		0x37 
#define IN5_FEC_DIVP_CNFG_7_0 		0x38 	//Input 5 FEC Divider Numerator P Register
#define IN5_FEC_DIVP_CNFG_15_8 		0x39 
#define IN5_FEC_DIVQ_CNFG_7_0 		0x3A	//Input 5 FEC Divider Numerator P Register 
#define IN5_FEC_DIVQ_CNFG_15_8 		0x3B
#define IN5_PRE_DIVN_CNFG_7_0 		0x3C 	//Input 5 Pre-divider Denominator N Register
#define IN5_PRE_DIVN_CNFG_14_8 		0x3D
#define IN6_FEC_DIVP_CNFG_7_0 		0x3E 	//Input 6 FEC Divider Numerator P Register
#define IN6_FEC_DIVP_CNFG_15_8 		0x3F 
#define IN6_FEC_DIVQ_CNFG_7_0 		0x40	//Input 6 FEC Divider Numerator P Register 
#define IN6_FEC_DIVQ_CNFG_15_8 		0x41
#define IN6_PRE_DIVN_CNFG_7_0 		0x42 	//Input 6 Pre-divider Denominator N Register
#define IN6_PRE_DIVN_CNFG_14_8 		0x43
#define IN7_FEC_DIVP_CNFG_7_0 		0x44 	//Input 7 FEC Divider Numerator P Register
#define IN7_FEC_DIVP_CNFG_15_8 		0x45 
#define IN7_FEC_DIVQ_CNFG_7_0 		0x46	//Input 7 FEC Divider Numerator P Register 
#define IN7_FEC_DIVQ_CNFG_15_8 		0x47
#define IN7_PRE_DIVN_CNFG_7_0 		0x48 	//Input 7 Pre-divider Denominator N Register
#define IN7_PRE_DIVN_CNFG_14_8 		0x49
#define IN8_FEC_DIVP_CNFG_7_0 		0x4A 	//Input 8 FEC Divider Numerator P Register
#define IN8_FEC_DIVP_CNFG_15_8 		0x4B 
#define IN8_FEC_DIVQ_CNFG_7_0 		0x4C	//Input 8 FEC Divider Numerator P Register 
#define IN8_FEC_DIVQ_CNFG_15_8 		0x4D
#define IN8_PRE_DIVN_CNFG_7_0 		0x4E 	//Input 8 Pre-divider Denominator N Register
#define IN8_PRE_DIVN_CNFG_14_8 		0x4F
#define IN9_FEC_DIVP_CNFG_7_0 		0x50 	//Input 9 FEC Divider Numerator P Register
#define IN9_FEC_DIVP_CNFG_15_8 		0x51 
#define IN9_FEC_DIVQ_CNFG_7_0 		0x52	//Input 9 FEC Divider Numerator P Register 
#define IN9_FEC_DIVQ_CNFG_15_8 		0x53
#define IN9_PRE_DIVN_CNFG_7_0 		0x54 	//Input 9 Pre-divider Denominator N Register
#define IN9_PRE_DIVN_CNFG_14_8 		0x55
#define IN10_FEC_DIVP_CNFG_7_0 		0x56 	//Input 10 FEC Divider Numerator P Register
#define IN10_FEC_DIVP_CNFG_15_8 	0x57 
#define IN10_FEC_DIVQ_CNFG_7_0 		0x58	//Input 10 FEC Divider Numerator P Register 
#define IN10_FEC_DIVQ_CNFG_15_8 	0x59
#define IN10_PRE_DIVN_CNFG_7_0 		0x5A 	//Input 10 Pre-divider Denominator N Register
#define IN10_PRE_DIVN_CNFG_14_8 	0x5B
#define IN11_FEC_DIVP_CNFG_7_0 		0x5C 	//Input 11 FEC Divider Numerator P Register
#define IN11_FEC_DIVP_CNFG_15_8 	0x5D 
#define IN11_FEC_DIVQ_CNFG_7_0 		0x5E	//Input 11 FEC Divider Numerator P Register 
#define IN11_FEC_DIVQ_CNFG_15_8 	0x5F
#define IN11_PRE_DIVN_CNFG_7_0 		0x60 	//Input 11 Pre-divider Denominator N Register
#define IN11_PRE_DIVN_CNFG_14_8 	0x61
#define IN12_FEC_DIVP_CNFG_7_0 		0x62 	//Input 12 FEC Divider Numerator P Register
#define IN12_FEC_DIVP_CNFG_15_8 	0x63 
#define IN12_FEC_DIVQ_CNFG_7_0 		0x64	//Input 12 FEC Divider Numerator P Register 
#define IN12_FEC_DIVQ_CNFG_15_8 	0x65
#define IN12_PRE_DIVN_CNFG_7_0 		0x66 	//Input 12 Pre-divider Denominator N Register
#define IN12_PRE_DIVN_CNFG_14_8 	0x67
#define IN13_FEC_DIVP_CNFG_7_0 		0x68 	//Input 13 FEC Divider Numerator P Register
#define IN13_FEC_DIVP_CNFG_15_8 	0x69 
#define IN13_FEC_DIVQ_CNFG_7_0 		0x6A	//Input 13 FEC Divider Numerator P Register 
#define IN13_FEC_DIVQ_CNFG_15_8 	0x6B
#define IN13_PRE_DIVN_CNFG_7_0 		0x6C 	//Input 13 Pre-divider Denominator N Register
#define IN13_PRE_DIVN_CNFG_14_8 	0x6D
#define IN14_FEC_DIVP_CNFG_7_0 		0x6E 	//Input 14 FEC Divider Numerator P Register
#define IN14_FEC_DIVP_CNFG_15_8 	0x6F 
#define IN14_FEC_DIVQ_CNFG_7_0 		0x70	//Input 14 FEC Divider Numerator P Register 
#define IN14_FEC_DIVQ_CNFG_15_8 	0x71
#define IN14_PRE_DIVN_CNFG_7_0 		0x72 	//Input 14 Pre-divider Denominator N Register
#define IN14_PRE_DIVN_CNFG_14_8 	0x73

#define FREQ_MON_FACTOR_CNFG 		0x80 	//Frequency Monitor Factor Configuration Register
#define HARD_FREQ_MON_THRESHOLD_CNFG 	0x81 	//Hard Frequency Monitor Threshold Configuration Register
#define SOFT_FREQ_MON_THRESHOLD_CNFG 	0x82 	//Soft Frequency Monitor Threshold Configuration Register
#define UPPER_THRESHOLD_0_CNFG 		0x83 	//Upper Threshold {0:3} Configuration Register
#define LOWER_THRESHOLD_0_CNFG 		0x84 	//Lower Threshold {0:3} Configuration Register
#define BUCKET_SIZE_0_CNFG 		0x85 	//Bucket Size {0:3} Configuration Register
#define DECAY_RATE_0_CNFG 		0x86 	//Decay Rate {0:3} Configuration Register
#define UPPER_THRESHOLD_1_CNFG		0x87 	//Starting address Upper Threshold {0:3} Configuration Register
#define LOWER_THRESHOLD_1_CNFG 		0x88
#define BUCKET_SIZE_1_CNFG 		0x89
#define DECAY_RATE_1_CNFG 		0x8A
#define UPPER_THRESHOLD_2_CNFG 		0x8B 	//– Starting address Upper Threshold {0:3} Configuration Register
#define LOWER_THRESHOLD_2_CNFG 		0x8C
#define BUCKET_SIZE_2_CNFG 		0x8D
#define DECAY_RATE_2_CNFG 		0x8E
#define UPPER_THRESHOLD_3_CNFG 		0x8F 	//– Starting address Upper Threshold {0:3} Configuration Register
#define LOWER_THRESHOLD_3_CNFG 		0x90
#define BUCKET_SIZE_3_CNFG 		0x91
#define DECAY_RATE_3_CNFG 		0x92
#define IN1_FREQ_READ_STS 		0x93	//Input {1:14} Frequency Read Status Register
#define IN2_FREQ_READ_STS 		0x94
#define IN3_FREQ_READ_STS 		0x95
#define IN4_FREQ_READ_STS 		0x96
#define IN5_FREQ_READ_STS 		0x97
#define IN6_FREQ_READ_STS 		0x98
#define IN7_FREQ_READ_STS 		0x99
#define IN8_FREQ_READ_STS 		0x9A
#define IN9_FREQ_READ_STS 		0x9B
#define IN10_FREQ_READ_STS 		0x9C
#define IN11_FREQ_READ_STS 		0x9D
#define IN12_FREQ_READ_STS 		0x9E
#define IN13_FREQ_READ_STS 		0x9F
#define IN14_FREQ_READ_STS 		0xA0
#define REMOTE_INPUT_VALID_CNFG_8_1 	0xA1 	//Remote Input Valid Configuration Register [8:1]
#define REMOTE_INPUT_VALID_CNFG_14_9 	0xA2 	//Remote Input Valid Configuration Register [14:9]
#define PHASE_ALARM_TIME_CNFG 		0xA3 	//Phase Alarm Time Configuration Register
#define LOS_STS 			0xA4 	//Loss of Signal Status Register
#define IN1_STS 			0xA5	//Input {1:14} Status Register
#define IN2_STS 			0xA6
#define IN3_STS 			0xA7
#define IN4_STS 			0xA8
#define IN5_STS 			0xA9
#define IN6_STS 			0xAA
#define IN7_STS 			0xAB
#define IN8_STS 			0xAC
#define IN9_STS 			0xAD
#define IN10_STS 			0xAE
#define IN11_STS 			0xAF
#define IN12_STS 			0xB0
#define IN13_STS 			0xB1
#define IN14_STS 			0xB2
#define	IN1_LOS_SYNC_CNFG 		0xB3 	//Input {1:2} LOS Sync Configuration Register
#define	IN2_LOS_SYNC_CNFG 		0xB4
#define	IN3_LOS_SYNC_CNFG 		0xB5 	//Input {3:14} LOS Sync Configuration Register
#define	IN4_LOS_SYNC_CNFG 		0xB6
#define	IN5_LOS_SYNC_CNFG 		0xB7
#define	IN6_LOS_SYNC_CNFG 		0xB8
#define	IN7_LOS_SYNC_CNFG 		0xB9
#define	IN8_LOS_SYNC_CNFG 		0xBA
#define	IN9_LOS_SYNC_CNFG 		0xBB
#define	IN10_LOS_SYNC_CNFG 		0xBC
#define	IN11_LOS_SYNC_CNFG 		0xBD
#define	IN12_LOS_SYNC_CNFG 		0xBE
#define	IN13_LOS_SYNC_CNFG 		0xBF
#define	IN14_LOS_SYNC_CNFG 		0xC0
#define IN_4_1_SYNC_PHASE_CNFG 		0xC1 	//Input 4–1 Sync Phase Configuration Register
#define IN_8_5_SYNC_PHASE_CNFG 		0xC2 	//Input 8–5 Sync Phase Configuration Register
#define IN_12_9_SYNC_PHASE_CNFG 	0xC3 	//Input 12–9 Sync Phase Configuration Register
#define IN_14_13_SYNC_PHASE_CNFG 	0xC4 	//Input 14–13 Sync Phase Configuration Register
#define	IN3_PHASE_OFFSET_CNFG 		0xC5	//Input {3:14} Phase Offset Configuration Register
#define	IN4_PHASE_OFFSET_CNFG 		0xC6	
#define	IN5_PHASE_OFFSET_CNFG 		0xC7	
#define	IN6_PHASE_OFFSET_CNFG 		0xC8	
#define	IN7_PHASE_OFFSET_CNFG 		0xC9	
#define	IN8_PHASE_OFFSET_CNFG 		0xCA	
#define	IN9_PHASE_OFFSET_CNFG 		0xCB	
#define	IN10_PHASE_OFFSET_CNFG 		0xCC	
#define	IN11_PHASE_OFFSET_CNFG 		0xCD	
#define	IN12_PHASE_OFFSET_CNFG 		0xCE	
#define	IN13_PHASE_OFFSET_CNFG 		0xCF	
#define	IN14_PHASE_OFFSET_CNFG 		0xDO	
//DPLL1 (DPLL2=DPLL1+0x80)
#define	DPLL1_PRIORITY_TABLE_STS_7_0 	0x100 	//DPLL{1:2} Priority Table Status Register [7:0]
#define	DPLL1_PRIORITY_TABLE_STS_15_8 	0x101 	//DPLL{1:2} Priority Table Status Register [15:8]
#define	DPLL1_OPERATING_STS 		0x102 	//DPLL{1:2} Operating Status Register
#define	DPLL1_CURRENT_DPLL_FREQ_STS_7_0 0x103 	//DPLL{1:2} Current DPLL Frequency Status Register [39:0]
#define	DPLL1_CURRENT_DPLL_FREQ_STS_15_8	0x104 
#define	DPLL1_CURRENT_DPLL_FREQ_STS_23_16 	0x105 
#define	DPLL1_CURRENT_DPLL_FREQ_STS_31_24 	0x106 
#define	DPLL1_CURRENT_DPLL_FREQ_STS_39_32 	0x107 
#define	DPLL1_CURRENT_DPLL_PHASE_STS_7_0 	0x108 
#define	DPLL1_CURRENT_DPLL_PHASE_STS_15_8 	0x109 
#define	DPLL1_CURRENT_DPLL_PHASE_STS_19_16 	0x10A 
#define DPLL1_TOD_NSEC_STS_7_0			0x10B
#define DPLL1_TOD_NSEC_STS_15_8			0x10C
#define DPLL1_TOD_NSEC_STS_23_16		0x10D
#define DPLL1_TOD_NSEC_STS_31_24		0x10E
#define DPLL1_TOD_SEC_STS_39_32			0x10F
#define DPLL1_TOD_SEC_STS_47_40			0x110
#define DPLL1_TOD_SEC_STS_55_48			0x111
#define DPLL1_TOD_SEC_STS_63_56			0x112
#define DPLL1_TOD_SEC_STS_71_64			0x113
#define DPLL1_TOD_SEC_STS_79_72			0x114
#define DPLL1_TOD_TRIGGER			0x115
#define	DPLL1_INPUT_MODE_CNFG 			0x116 	//DPLL{1:2} Input Mode Configuration Register
#define	DPLL1_MON_SW_PBO_CNFG 			0x117 	//DPLL{1:2} Monitor Software Register
#define	DPLL1_IN1_IN2_SEL_PRIORITY_CNFG 	0x118 	//DPLL{1:2} Input 1/2 Select Priority Configuration Register
#define	DPLL1_IN3_IN4_SEL_PRIORITY_CNFG 	0x119 	//DPLL{1:2} Input 3/4 Select Priority Configuration Register
#define	DPLL1_IN5_IN6_SEL_PRIORITY_CNFG 	0x11A 	//DPLL{1:2} Input 5/6 Select Priority Configuration Register
#define	DPLL1_IN7_IN8_SEL_PRIORITY_CNFG 	0x11B 	//DPLL{1:2} Input 7/8 Select Priority Configuration Register
#define	DPLL1_IN9_IN10_SEL_PRIORITY_CNFG 	0x11C 	//DPLL{1:2} Input 9/10 Select Priority Configuration Register
#define	DPLL1_IN11_IN12_SEL_PRIORITY_CNFG 	0x11D 	//DPLL{1:2} Input 11/12 Select Priority Configuration Register
#define	DPLL1_IN13_IN14_SEL_PRIORITY_CNFG 	0x11E 	//DPLL{1:2} Input 13/14 Select Priority Configuration Register
#define	DPLL1_INPUT_SEL_CNFG 			0x11F 	//DPLL{1:2} Input Select Configuration Register
#define	DPLL1_OPERATING_MODE_CNFG	 	0x120 	//DPLL{1:2} Operating Mode Configuration Register
#define	DPLL1_FB_SEL_CNFG 			0x121 	//DPLL{1:2} Feedback Select Configuration Register
#define	DPLL1_UPDATE_EVENT_CNFG		 	0x122 	//DPLL{1:2} Update Event Configuration Register
#define	DPLL1_DPLL_PATH_CNFG 			0x123 	//DPLL{1:2} DPLL Path Configuration Register
#define	DPLL1_DPLL_START_BW_DAMPING_CNFG	0x124 	//DPLL{1:2} DPLL Start Bandwidth Damping Configuration Register
#define	DPLL1_DPLL_ACQ_BW_DAMPING_CNFG 		0x125 	//DPLL{1:2} DPLL Acquired Bandwidth Damping Configuration Register
#define	DPLL1_DPLL_LOCKED_BW_DAMPING_CNFG 	0x126 	//DPLL{1:2} DPLL Locked Bandwidth Configuration Register
#define	DPLL1_BW_OVERSHOOT_CNFG 		0x127 	//DPLL{1:2} Bandwidth Overshoot Configuration Register
#define	DPLL1_PHASE_LOSS_COARSE_LIMIT_CNFG 	0x128 	//DPLL{1:2} Phase Loss Coarse Limit Configuration Register
#define	DPLL1_PHASE_LOSS_FINE_LIMIT_CNFG 	0x129 	//DPLL{1:2} Phase Loss Fine Limit Configuration Register
#define	DPLL1_HOLDOVER_MODE_CNFG_7_0 		0x12A 	//DPLL{1:2} Holdover Mode Configuration Register [7:0]
#define	DPLL1_HOLDOVER_MODE_CNFG_15_8 		0x12B 	//DPLL{1:2} Holdover Mode Configuration Register [15:8]
#define	DPLL1_HOLDOVER_FREQ_CNFG_7_0 		0x12C 	//DPLL{1:2} Holdover Frequency Configuration Register
#define	DPLL1_HOLDOVER_FREQ_CNFG_15_8 		0x12D 
#define DPLL1_HOLDOVER_FREQ_CNFG_23_16	 	0x12E 
#define	DPLL1_HOLDOVER_FREQ_CNFG_31_24 		0x12F 
#define	DPLL1_HOLDOVER_FREQ_CNFG_39_32	 	0x130 
#define	DPLL1_DPLL_FREQ_SOFT_LIMIT_CNFG 	0x131 	//DPLL{1:2} DPLL Frequency Soft Limit Configuration Register
#define	DPLL1_DPLL_FREQ_HARD_LIMIT_CNFG_7_0 	0x132 	//DPLL{1:2} DPLL Frequency Hard Limit Configuration Register [7:0]
#define	DPLL1_DPLL_FREQ_HARD_LIMIT_CNFG_15_8 	0x133 	//DPLL{1:2} DPLL Frequency Hard Limit Configuration Register [15:8]
#define	DPLL1_FR_MFR_SYNC_CNFG 			0x13E 	//DPLL{1:2} Frame/Multi-Frame Sync Configuration Register
#define	DPLL1_SYNC_MONITOR_CNFG 	 	0x13F 	//DPLL{1:2} Sync Monitor Configuration Register
#define	DPLL1_SYNC_EDGE_CNFG	 		0x140 	//DPLL{1:2} Sync Edge Configuration Register
#define	DPLL1_PHASE_OFFSET_CNFG_7_0 		0x143 	//DPLL{1:2} Phase Offset Configuration Register [28:0]
#define	DPLL1_PHASE_OFFSET_CNFG_15_8 		0x144 
#define	DPLL1_PHASE_OFFSET_CNFG_23_15 		0x145 
#define	DPLL1_PHASE_OFFSET_CNFG_28_24 		0x146 
#define	DPLL1_TIMER_INTERVAL_7_0 		0x147 	//DPLL{1:2} Timer Interval Register [7:0]
#define	DPLL1_TIMER_INTERVAL_13_8 		0x148 	//DPLL{1:2} Timer Interval Register [13:8]
#define	DPLL1_SYS_TIME_STS_7_0			0x149 	//DPLL{1:2} System Time Status Register
#define	DPLL1_SYS_TIME_STS_15_8 		0x14A 
#define	DPLL1_SYS_TIME_STS_23_15 		0x14B 
#define	DPLL1_SYS_TIME_STS_31_24		0x14C  
#define	DPLL1_BW_SW_TIME1_CNFG			0x14D 	// DPLL{1:2} Bandwidth Software Time 1 Configuration Register
#define	DPLL1_BW_SW_TIME2_CNFG 			0x14E 	//DPLL{1:2} Bandwidth Software Time 2 Configuration Register
#define	DPLL1_BW_SW_TIME3_CNFG 			0x14F 	//DPLL{1:2} Bandwidth Software Time 3 Configuration Register
#define	DPLL1_SLAVE_FORCE_REF_SEL_CNFG 		0x150 	//DPLL{1:2} Slave Force Reference Select Configuration Register
#define	DPLL1_PROG_PH_LIMIT_CNFG_7_0 		0x151 	//DPLL{1:2} Program Limit Configuration Register [23:0]
#define	DPLL1_PROG_PH_LIMIT_CNFG_15_8 		0x152 
#define	DPLL1_PROG_PH_LIMIT_CNFG_23_16		0x153 
//DPLL2
#define	DPLL2_PRIORITY_TABLE_STS_7_0 		0x200 	//DPLL2 Priority Table Status Register [7:0]
#define	DPLL2_PRIORITY_TABLE_STS_15_8 		0x201 	//DPLL2 Priority Table Status Register [15:8]
#define	DPLL2_OPERATING_STS 			0x202 	//DPLL2 Operating Status Register
#define	DPLL2_INPUT_MODE_CNFG 			0x216 	//DPLL2 Input Mode Configuration Register
#define	DPLL2_MON_CNFG 				0x217 	//DPLL2 Monitor Configuration Register
#define	DPLL2_IN1_IN2_SEL_PRIORITY_CNFG 	0x218 	//DPLL2 Input 1/2 Select Priority Configuration Register
#define	DPLL2_IN3_IN4_SEL_PRIORITY_CNFG 	0x219 	//DPLL2 Input 3/4 Select Priority Configuration Register
#define	DPLL2_IN5_IN6_SEL_PRIORITY_CNFG 	0x21A 	//DPLL2 Input 5/6 Select Priority Configuration Register
#define	DPLL2_IN7_IN8_SEL_PRIORITY_CNFG 	0x21B 	//DPLL2 Input 7/8 Select Priority Configuration Register
#define	DPLL2_IN9_IN10_SEL_PRIORITY_CNFG 	0x21C 	//DPLL2 Input 9/10 Select Priority Configuration Register
#define	DPLL2_IN11_IN12_SEL_PRIORITY_CNFG 	0x21D 	//DPLL2 Input 11/12 Select Priority Configuration Register
#define	DPLL2_IN13_IN14_SEL_PRIORITY_CNFG 	0x21E 	//DPLL2 Input 13/14 Select Priority Configuration Register
#define	DPLL2_INPUT_SEL_CNFG 			0x21F 	//DPLL2 Input Select Configuration Register
#define	DPLL2_OPERATING_MODE_CNFG 		0x220 	//DPLL2 Operating Mode Configuration Register
#define	DPLL2_DPLL_LOCKED_BW_DAMPING_CNFG 	0x226 	//DPLL2 DPLL Locked Bandwidth Damping Configuration Register
#define	DPLL2_PHASE_LOSS_COARSE_LIMIT_CNFG 	0x228 	//DPLL2 Phase Loss Coarse Limit Register
#define	DPLL2_PHASE_LOSS_FINE_LIMIT_CNFG 	0x229 	//DPLL2 Phase Loss Fine Limit Configuration Register0x200 dpll2_priority_table_sts[7:0] DPLL2 Priority Table Status Register [7:0]
#define	DPLL2_HOLDOVER_MODE_CNFG 		0x22B 	//DPLL2 Holdover Mode Configuration Register
#define	DPLL2_DPLL_FREQ_SOFT_LIMIT_CNFG 	0x231 	//DPLL2 DPLL Frequency Soft Limit Configuration Register
#define	DPLL2_DPLL_FREQ_HARD_LIMIT_CNFG_7_0 	0x232 	//DPLL2 DPLL Frequency Hard Limit Configuration Register [7:0]
#define	DPLL2_DPLL_FREQ_HARD_LIMIT_CNFG_15_8 	0x233 	//DPLL2 DPLL Frequency Hard Limit Configuration Register [15:8]
#define	DPLL2_FBDIV_CNFG_7_0 			0x250 	//DPLL2 Feedback Divisor Configuration Register [7:0]
#define	DPLL2_FBDIV_CNFG_13_8 			0x251 	//DPLL2 Feedback Divisor Configuration Register [13:8]
#define	DPLL2_DIVN_FRAC_L_CNFG 			0x252 	//DPLL2 Divisor N Fractional L Configuration Register
#define	DPLL2_DIVN_FRAC_M_CNFG 			0x253 	//DPLL2 Divisor N Fractional M Configuration Register
#define	DPLL2_DIVN_FRAC_H_CNFG 			0x254 	//DPLL2 Divisor N Fractional H Configuration Register
#define	DPLL2_DIVN_DEN_L_CNFG 			0x255 	//DPLL2 Divisor N Denominator L Configuration Register
#define	DPLL2_DIVN_DEN_H_CNFG 			0x256 	//DPLL2 Divisor N Denominator H Configuration Register
#define	DPLL2_DIVN_NUM_L_CNFG 			0x257 	//DPLL2 Divisor N Numerator L Configuration Register
#define	DPLL2_DIVN_NUM_H_CNFG 			0x258 	//DPLL2 Divisor N Numerator H Configuration Register
#define	DPLL2_DIVN_INT_CNFG 			0x259 	//DPLL2 Divisor N Interrupt Configuration Register
#define	DPLL2_DPLL_DSM_CNFG 			0x25c 	//DPLL2 DPLL DSM Configuration Register
//APLL1
#define	APLL1_ICP_CTRL_CNFG 			0x280 	//APLL1 Charge Pump Current Control Configuration Register
#define	APLL1_DIVISOR_FRAC_L_CNFG 		0x281 	//APLL1 Divisor Fractional L Configuration Register
#define	APLL1_DIVISOR_FRAC_M_CNFG 		0x282 	//APLL1 Divisor Fractional M Configuration Register
#define	APLL1_DIVISOR_FRAC_H_CNFG 		0x283 	//APLL1 Divisor Fractional H Configuration Register
#define	APLL1_DIVISOR_DEN_L_CNFG 		0x284 	//APLL1 Divisor Denominator L Configuration Register
#define	APLL1_DIVISOR_DEN_H_CNFG 		0x285 	//APLL1 Divisor Denominator H Configuration Register
#define	APLL1_DIVISOR_NUM_L_CNFG 		0x286 	//APLL1 Divisor Numerator L Configuration Register
#define	APLL1_DIVISOR_NUM_H_CNFG 		0x287 	//APLL1 Divisor Numerator H Configuration Register
#define	APLL1_DSM_CNFG 				0x288 	//APLL1 DSM Configuration Register
#define	APLL1_DIVISOR_INT_CNFG 			0x289 	//APLL1 Divisor Integer Configuration Register
#define	APLL1_FR_RATIO_CNFG_7_0 		0x28A 	//APLL1 Frame/Multi-Frame Ratio Configuration Register [7:0]
#define	APLL1_FR_RATIO_CNFG_15_8 		0x28B 	//APLL1 Frame/Multi-Frame Ratio Configuration Register [15:8]
#define	APLL1_FR_RATIO_CNFG_23_16 		0x28C 	//APLL1 Frame/Multi-Frame Ratio Configuration Register [23:16]
#define	APLL1_FR_RATIO_CNFG_28_24 		0x28D 	//APLL1 Frame/Multi-Frame Ratio Configuration Register [28:24]
//APPL2
#define	APLL2_ICP_CTRL_CNFG			0x28E 	//APLL2 Change Pump Current Control Configuration Register
#define	APLL2_DIVISOR_FRAC_L_CNFG 		0x28F 	//APLL2 Divisor Fractional L Configuration Register
#define	APLL2_DIVISOR_FRAC_M_CNFG 		0x290 	//APLL2 Divisor Fractional M Configuration Register
#define	APLL2_DIVISOR_FRAC_H_CNFG 		0x291 	//APLL2 Divisor Fractional H Configuration Register
#define	APLL2_DIVISOR_DEN_L_CNFG 		0x292 	//APLL2 Divisor Denominator L Configuration Register
#define	APLL2_DIVISOR_DEN_H_CNFG 		0x293 	//APLL2 Divisor Denominator H Configuration Register
#define	APLL2_DIVISOR_NUM_L_CNFG 		0x294 	//APLL2 Divisor Numerator L Configuration Register
#define	APLL2_DIVISOR_NUM_H_CNFG 		0x295 	//APLL2 Divisor Numerator H Configuration Register
#define	APLL2_DSM_CNFG 				0x296 	//APLL2 DSM Configuration Register
#define	APLL2_DIVISOR_INT_CNFG 			0x297 	//APLL2 Divisor Integer Configuration Register
#define	APLL2_FR_RATIO_CNFG_7_0 		0x298 	//APLL2 Frame/Multi-Frame Ratio Configuration Register [7:0]
#define	APLL2_FR_RATIO_CNFG_15_8 		0x299 	//APLL2 Frame/Multi-Frame Ratio Configuration Register [15:8]
#define	APLL2_FR_RATIO_CNFG_23_16 		0x29A 	//APLL2 Frame/Multi-Frame Ratio Configuration Register [23:16]
#define	APLL2_FR_RATIO_CNFG_28_24 		0x29B 	//APLL2 Frame/Multi-Frame Ratio Configuration Register [28:24]
//Output
#define	OUT1_MUX_CNFG 				0x300	//Output 1 Mux Configuration Register
#define	OUT1_DIV1_CNFG 				0x301 	//Output 1 Divisor 1 Configuration Register [4:0]
#define	OUT1_DIV2_CNFG_7_0 			0x302 	//Output 1 Divisor 2 Configuration Register [26:0]
#define	OUT1_DIV2_CNFG_15_8			0x303 
#define	OUT1_DIV2_CNFG_23_16 			0x304 
#define OUT1_DIV2_CNFG_26_24			0x305 
#define	OUT1_PH1_CNFG 				0x306 	//Output 1 Phase 1 Configuration Register [4:0]
#define	OUT1_PH2_CNFG_7_0 			0x307 	//Output 1 Phase 2 Configuration Register [26:0]
#define	OUT1_PH2_CNFG_15_8 			0x308 
#define	OUT1_PH2_CNFG_23_16 			0x309 
#define	OUT1_PH2_CNFG_26_24 			0x30A 
#define	OUT1_FINE_PH_CNFG 			0x30B 	//Output 1 Fine Phase Configuration Register
#define	OUT2_MUX_CNFG 				0x30C 	//Output 2 Mux Configuration Register
#define	OUT2_DIV1_CNFG 				0x30D 	//Output 2 Divisor 1 Configuration Register [4:0]
#define	OUT2_DIV2_CNFG_7_0 			0x30E 	//Output 2 Divisor 2 Configuration Register [26:0]
#define	OUT2_DIV2_CNFG_15_8 			0x30F 
#define	OUT2_DIV2_CNFG_23_16 			0x310 
#define	OUT2_DIV2_CNFG_26_24			0x311 
#define	OUT2_PH1_CNFG_4_0 			0x312 	//Output 2 Phase 1 Configuration Register [4:0]
#define	OUT2_PH2_CNFG_7_0 			0x313 	//Output 2 Phase 2 Configuration Register [7:0]
#define	OUT2_PH2_CNFG_15_8 			0x314 	//Output 2 Phase 2 Configuration Register [15:8]
#define	OUT2_PH2_CNFG_23_16 			0x315 	//Output 2 Phase 2 Configuration Register [23:16]
#define	OUT2_PH2_CNFG_26_24 			0x316 	//Output 2 Phase 2 Configuration Register [26:24]
#define	OUT2_FINE_PH_CNFG 			0x317 	//Output 2 Fine Phase Configuration Register
#define	OUT3_MUX_CNFG 				0x318 	//Output 3 Mux Configuration Register
#define	OUT3_DIV1_CNFG_4_0 			0x319 	//Output 3 Divisor 1 Configuration Register [4:0]
#define	OUT3_DIV2_CNFG_7_0 			0x31A 	//Output 3 Divisor 2 Configuration Register [7:0]
#define	OUT3_DIV2_CNFG_15_8 			0x31B 	//Output 3 Divisor 2 Configuration Register [15:8]
#define	OUT3_DIV2_CNFG_23_16 			0x31C 	//Output 3 Divisor 2 Configuration Register [23:16]
#define	OUT3_DIV2_CNFG_26_24 			0x31D 	//Output 3 Divisor 2 Configuration Register [26:24]
#define	OUT3_PH1_CNFG_4_0 			0x31E 	//Output 3 Phase 1 Configuration Register [4:0]
#define	OUT3_PH2_CNFG_7_0 			0x31F 	//Output 3 Phase 2 Configuration Register [7:0]
#define	OUT3_PH2_CNFG_15_8 			0x320 	//Output 3 Phase 2 Configuration Register [15:8]
#define	OUT3_PH2_CNFG_23_16 			0x321 	//Output 3 Phase 2 Configuration Register [23:16]
#define	OUT3_PH2_CNFG_26_24 			0x322 	//Output 3 Phase 2 Configuration Register [26:24]
#define	OUT3_PH_CNFG	 			0x323 	//Output 3 Fine Phase Configuration Register
#define	OUT4_MUX_CNFG 				0x324 	//Output 4 Mux Configuration Register
#define	OUT4_DIV1_CNFG	 			0x325 	//Output 4 Divisor 1 Configuration Register [4:0]
#define	OUT4_DIV2_CNFG_7_0 			0x326 	//Output 4 Divisor 2 Configuration Register [7:0]
#define	OUT4_DIV2_CNFG_15_8 			0x327 	//Output 4 Divisor 2 Configuration Register [15:8]
#define	OUT4_DIV2_CNFG_23_16 			0x328 	//Output 4 Divisor 2 Configuration Register [23:16]
#define	OUT4_DIV2_CNFG_26_24	 		0x329 	//Output 4 Divisor 2 Configuration Register [26:24]
#define	OUT4_PH1_CNFG_4_0			0x32A 	//Output 4 Phase 1 Configuration Register [4:0]
#define	OUT4_PH2_CNFG_7_0	 		0x32B 	//Output 4 Phase 2 Configuration Register [7:0]
#define	OUT4_PH2_CNFG_15_8 			0x32C 	//Output 4 Phase 2 Configuration Register [15:8]
#define	OUT4_PH2_CNFG_23_16 			0x32D 	//Output 4 Phase 2 Configuration Register [23:16]
#define	OUT4_PH2_CNFG_26_24 			0x32E 	//Output 4 Phase 2 Configuration Register [26:24]
#define	OUT4_FINE_PH_CNFG 			0x32F 	//Output 4 Fine Phase Configuration Register
#define	OUT5_MUX_CNFG 				0x330 	//Output 5 Mux Configuration Register
#define	OUT5_DIV1_CNFG_4_0 			0x331 	//Output 5 Divisor 1 Configuration Register [4:0]
#define	OUT5_DIV2_CNFG_7_0 			0x332 	//Output 5 Divisor 2 Configuration Register [7:0]
#define	OUT5_DIV2_CNFG_15_8 			0x333 	//Output 5 Divisor 2 Configuration Register [15:8]
#define	OUT5_DIV2_CNFG_23_16			0x334 	//Output 5 Divisor 2 Configuration Register [23:16]
#define	OUT5_DIV2_CNFG_26_24 			0x335 	//Output 5 Divisor 2 Configuration Register [26:24]
#define	OUT5_PH1_CNFG	 			0x336 	//Output 5 Phase 1 Configuration Register [4:0]
#define	OUT5_PH2_CNFG_7_0 			0x337 	//Output 5 Phase 2 Configuration Register [7:0]
#define	OUT5_PH2_CNFG_15_8 			0x338 	//Output 5 Phase 2 Configuration Register [15:8]
#define	OUT5_PH2_CNFG_23_16 			0x339 	//Output 5 Phase 2 Configuration Register [23:16]
#define	OUT5_PH2_CNFG_26_24 			0x33A 	//Output 5 Phase 2 Configuration Register [26:24]
#define	OUT5_FINE_PH_CNFG 			0x33B 	//Output 5 Fine Phase Configuration Register
#define	OUT6_MUX_CNFG 				0x33C 	//Output 6 Mux Configuration Register
#define	OUT6_DIV1_CNFG	 			0x33D 	//Output 6 Divisor 1 Configuration Register [7:0]
#define	OUT6_DIV2_CNFG_7_0 			0x33E 	//Output 6 Divisor 2 Configuration Register [7:0]
#define	OUT6_DIV2_CNFG_15_8 			0x33F 	//Output 6 Divisor 2 Configuration Register [15:8]
#define	OUT6_DIV2_CNFG_23_16 			0x340 	//Output 6 Divisor 2 Configuration Register [23:16]
#define	OUT6_DIV2_CNFG_26_24 			0x341 	//Output 6 Divisor 2 Configuration Register [26:24]
#define OUT6_PH1_CNFG 				0x342 	//Output 6 Phase 1 Configuration Register [4:0]
#define	OUT6_PH2_CNFG_7_0 			0x343 	//Output 6 Phase 2 Configuration Register [7:0]
#define	OUT6_PH2_CNFG_15_8 			0x344 	//Output 6 Phase 2 Configuration Register [15:8]
#define	OUT6_PH2_CNFG_23_16 			0x345 	//Output 6 Phase 2 Configuration Register [23:16]
#define	OUT6_PH2_CNFG_26_24 			0x346 	//Output 6 Phase 2 Configuration Register [26:24]
#define	OUT6_FINE_PH_CNFG 			0x347 	//Output 6 Fine Phase Configuration Register
#define	OUT7_MUX_CNFG 				0x348 	//Output 7 Mux Configuration Register
#define	OUT7_DIV1_CNFG	 			0x349 	//Output 7 Divisor 1 Configuration Register [7:0]
#define	OUT7_DIV2_CNFG_7_0 			0x34A 	//Output 7 Divisor 2 Configuration Register [7:0]
#define	OUT7_DIV2_CNFG_15_8 			0x34B 	//Output 7 Divisor 2 Configuration Register [15:8]
#define	OUT7_DIV2_CNFG_23_16 			0x34C 	//Output 7 Divisor 2 Configuration Register [23:16]
#define	OUT7_DIV2_CNFG_26_24 			0x34D 	//Output 7 Divisor 2 Configuration Register [26:24]
#define	OUT7_PH1_CNFG	 			0x34E 	//Output 7 Phase 1 Configuration Register [4:0]
#define	OUT7_PH2_CNFG_7_0 			0x34F 	//Output 7 Phase 2 Configuration Register [7:0]
#define	OUT7_PH2_CNFG_15_8 			0x350 	//Output 7 Phase 2 Configuration Register [15:8]
#define	OUT7_PH2_CNFG_23_16 			0x351 	//Output 7 Phase 2 Configuration Register [23:16]
#define	OUT7_PH2_CNFG_26_24 			0x352 	//Output 7 Phase 2 Configuration Register [26:24]
#define	OUT7_FINE_PH_CNFG 			0x353 	//Output 7 Fine Phase Configuration Register
#define	OUT8_MUX_CNFG 				0x354 	//Output 8 Mux Configuration Register
#define	OUT8_DIV1_CNFG_7_0 			0x355 	//Output 8 Divisor 1 Configuration Register [7:0]
#define	OUT8_DIV2_CNFG_7_0 			0x356 	//Output 8 Divisor 2 Configuration Register [7:0]
#define	OUT8_DIV2_CNFG_15_8 			0x357 	//Output 8 Divisor Configuration Register [15:8]
#define	OUT8_DIV2_CNFG_23_16 			0x358 	//Output 8 Divisor Configuration Register [23:16]
#define	OUT8_DIV2_CNFG_26_24 			0x359 	//Output 8 Divisor 2 Configuration Register [26:24]
#define	OUT8_PH1_CNFG	 			0x35A 	//Output 8 Phase 1 Configuration Register [4:0]
#define	OUT8_PH2_CNFG_7_0 			0x35B 	//Output 8 Phase 2 Configuration Register [7:0]
#define	OUT8_PH2_CNFG_15_8 			0x35C 	//Output 8 Phase 2 Configuration Register [15:8]
#define	OUT8_PH2_CNFG_23_16 			0x35D 	//Output 8 Phase 2 Configuration Register [23:16]
#define	OUT8_PH2_CNFG_26_24 			0x35E 	//Output 8 Phase 2 Configuration Register [26:24]
#define	OUT8_FINE_PH_CNFG 			0x35F 	//Output 8 Fine Phase Configuration Register
#define	OUT9_FREQ_CNFG 				0x360 	//Output 9 Frequency Configuration Register
#define	OUT10_CNFG 				0x36C 	//Output 10 Configuration Register
#define	OUT10_FREQ_CNFG_7_0 			0x36D 	//Output 10 Frequency Configuration Register [7:0]
#define	OUT10_FREQ_CNFG_14_8 			0x36E 	//Output 10 Frequency Configuration Register [14:8]
#define	OUT11_CNFG 				0x378 	//Output 11 Configuration Register
#define	OUT11_FREQ_CNFG_7_0 			0x379 	//Output 11 Frequency Configuration Register [7:0]
#define	OUT11_FREQ_CNFG_14_8 			0x37A 	//Output 11 Frequency Configuration Register [14:8]
#define	FR_MFR_PATH_CNFG 			0x37E 	//Output Frame/Multi-Frame Sync Configuration Register

#endif