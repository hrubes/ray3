/*
Za tak malo prace tolik muziky....
Jde o to usetrit pamet za extra shell a dovolit z tech scriptu volat exit a nemichat ty scripty navzajem...
Pruser nastane, kdyz se jeden script zablokuje navzdy - to by tady mohl bejt navic casovac, timeout a sriledlo.
bla.
*/
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/times.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <errno.h>
#include <stdarg.h>

//Preteceni navratove hodnoty funkce times zpusobi nefunkcnost behu rrcronpersec po cca pul roce behu
//2009-05-15 Lapka
#define RR_CPS_HALF_LIFE
#ifdef RR_CPS_HALF_LIFE
#include <sys/sysinfo.h>
#include <ctype.h>
#endif //RR_CPS_HALF_LIFE

#ifdef RR_CPS_HALF_LIFE

#define RR_ERR(a...) {int e=errno;fprintf(stderr,"rrCronPerSec: Error: ");errno=e;fprintf(stderr,a);fputc('\n',stderr);}
#define RR_DBG(a...) {int e=errno;if(rrTheVerbose>3){fprintf(stderr,"rrCronPerSec:");errno=e;fprintf(stderr,a);fputc('\n',stderr);}}

static int rrTheVerbose=0; //uroven vypisu
static int rrTheTimeout=0; //timeout spusteneho procesu

#define RRCP_MSGLEN 1024
static char rrTheTOMsg[RRCP_MSGLEN]; //buffer pro hlaseni z handleru timeoutu
static int rrTheTOMsgLen=0;
static pid_t rrThePid=0; //zapamatovany pid procesu

static volatile sig_atomic_t rrMustTerminate=0; //flag zadosti o ukonceni (signalu)
static volatile sig_atomic_t rrTimedOut=0;		//flag timeoutu podprocesu

struct s_rrCPrecord { //zaznam naplanovaneho prikazu
	long start_tim;		//cas zacatku behu casovace
	int interval;		//interval mezi volanimi prikazu
	char **arglist;		//predparsovany prikaz - rozlozeny na jednotlive argumenty, ukoncene NULL
	size_t arg_alloc;	//naalokovana velikost pole
	size_t arg_num;		//pocet argumentu/index terminatoru
	const char *cmd;	//ukazatel na argument, ze ktereho se prikaz parsoval
}; //rrCPrecord

typedef struct s_rrCPrecord rrCPrecord;

enum rrCPparse_state { //stav parseru prikazu
	rrCPps_blank,	//preskakuje mezery
	rrCPps_squote,	//retezec v apostrofech
	rrCPps_dquote,	//retezec v uvozovkach
	rrCPps_word,	//obecny retezec
	rrCPps_escquote,//escape sekvence v uvozovkach
	rrCPps_escseq	//escape sekvence
}; //rrCPparse_state

static rrCPrecord *rrTheCommandList=NULL; //pole naplanovanych prikazu, neserazene, ukoncene nulovym zaznamem

void free_cmdlist(rrCPrecord *clist)
{
	//uvolneni seznamu prikazu
	if (clist!=NULL)
	{
		rrCPrecord *p=clist;
		while (p->arglist!=NULL)
		{
			char **pa=p->arglist;
			while (*pa!=NULL)
				free(*pa++);
			free(p->arglist);
			++p;
		}
		free(clist);
		clist=NULL;
	}
}

static int add_argument(rrCPrecord *rec,const char *buf)
{
#define RRCP_ALLOC_QUANTUM	5
	//pridani dalsiho argumentu na konec seznamu
	if (rec->arg_num+1>=rec->arg_alloc)
	{
		//musi se prealokovat, neni misto na zaznam+terminator
		size_t nsize=rec->arg_alloc+RRCP_ALLOC_QUANTUM;
		char **narg=(char **)realloc(rec->arglist,nsize*sizeof(char *));
		if (narg==NULL) //selhani realokace, puvodni blok je platny
			return 1;
		rec->arglist=narg;
		rec->arg_alloc=nsize;
	}
	//vlozeni retezce
	char *sarg=strdup(buf);
	if (sarg==NULL) //selhani duplikace stringu
		return 1;
	rec->arglist[rec->arg_num]=sarg;
	++rec->arg_num;
	rec->arglist[rec->arg_num]=NULL; //terminator
	return 0;
}

static int parse_cmd(const char *cmd,rrCPrecord *rec)
{
	//rozparsovani retezce prikazu na seznam argumentu do zaznamu
#define RRCP_PARSE_BUFLEN	1024
	static char buf[RRCP_PARSE_BUFLEN];

	enum rrCPparse_state state=rrCPps_blank; //pocatecni stav
	size_t blen=0;
	rec->cmd=cmd; //ulozeni ukazatele na puvodni retezec
	while ((*cmd!='\0')&&(blen+1<RRCP_PARSE_BUFLEN)) 
	{ //do konce retezce
		switch (state) 
		{
			default:
			case rrCPps_blank: //preskakuje mezery
				if (*cmd=='\'')
				{
					state=rrCPps_squote;
				}
				else if (*cmd=='"')
				{
					state=rrCPps_dquote;
				}
				else if (*cmd=='\\')
				{
					state=rrCPps_escseq;
				}
				else if (!isspace(*cmd))
				{
					state=rrCPps_word;
					buf[blen++]=*cmd;
				}
				break;
			case rrCPps_squote: //retezec v apostrofech
				if (*cmd=='\'')
				{
					state=rrCPps_word;
				}
				else
				{
					buf[blen++]=*cmd;
				}
				break;
			case rrCPps_dquote: //retezec v uvozovkach
				if (*cmd=='"')
				{
					state=rrCPps_word;
				}
				else if (*cmd=='\\')
				{
					state=rrCPps_escquote;
				}
				else
				{
					buf[blen++]=*cmd;
				}
				break;
			case rrCPps_word: //obecny retezec
				if (isspace(*cmd))
				{
					//ulozeni argumentu
					buf[blen]='\0';
					if (add_argument(rec,buf))
					{
						RR_ERR("Error parsing command: memory allocation failed.")
						return 1;
					}
					blen=0;
					state=rrCPps_blank;
				}
				else if (*cmd=='\'')
				{
					state=rrCPps_squote;
				}
				else if (*cmd=='"')
				{
					state=rrCPps_dquote;
				}
				else if (*cmd=='\\')
				{
					state=rrCPps_escseq;
				}
				else
				{
					buf[blen++]=*cmd;
				}
				break;
			case rrCPps_escquote: //escape sekvence v uvozovkach
				if ((*cmd!='"')&&(*cmd!='\\')&&(*cmd!='\n'))
				{
					buf[blen++]='\\';
					if (blen+1>=RRCP_PARSE_BUFLEN) 
						break;
				}
				if (*cmd!='\n')
				{
					buf[blen++]=*cmd;
				}
				state=rrCPps_dquote;
				break;
			case rrCPps_escseq: //escape sekvence
				if (*cmd!='\n')
				{
					buf[blen++]=*cmd;
				}
				state=rrCPps_word;
				break;
		}
		++cmd;
	}
	if (blen>=RRCP_PARSE_BUFLEN-1)
	{
		RR_ERR("Error parsing command: buffer overrun.");
		return 1;
	} 
	else if (blen>0)
	{ //final argument
		buf[blen]='\0';
		if (add_argument(rec,buf))
		{
			RR_ERR("Error parsing command: memory allocation failed.")
			return 1;
		}
	}
	return 0;
}

static int parse_args(int argc,char *argv[])
{
	//zpracovani parametru
#define RR_ARG_OFFSET 3
	if(argc<RR_ARG_OFFSET)
	{
		fprintf(stderr,"Usage: rrcronpersec verboselvl timeout [sec cmd]\n");
		return 1;
	}
	if (sscanf(argv[1],"%i",&rrTheVerbose)!=1)
	{
		RR_ERR("Invalid verboselvl value, must be integer.");
		return 2;
	}
	if ((sscanf(argv[2],"%i",&rrTheTimeout)!=1)||(rrTheTimeout<=0))
	{
		RR_ERR("Invalid timeout value, must be positive integer.");
		return 3;
	}
	//prikazy
	int N=(argc-RR_ARG_OFFSET)/2;
	if (N==0)
	{
		RR_ERR("Zero number of items.");
		return 4;
	}
	if (argc-RR_ARG_OFFSET-2*N>0)
	{
		RR_ERR("The last argument (interval) is not paired with command. Invalid format.");
		return 4;
	}
	rrTheCommandList=(rrCPrecord *)malloc((N+1)*sizeof(rrCPrecord)); //naalokovani pole (+1 kvuli terminatoru)
	if (rrTheCommandList==NULL)
	{
		RR_ERR("Memory allocation error.");
		return 5;
	}
	int j;
	for (j=0;j<=N;++j)
	{
		rrTheCommandList[j].arglist=NULL; //zaslepeni pole - kvuli dealokaci a terminatoru
		rrTheCommandList[j].arg_alloc=0;
		rrTheCommandList[j].arg_num=0;
	}
	int i=RR_ARG_OFFSET; //index nasledujiciho argumentu
	rrCPrecord *p=rrTheCommandList; //aktualni prikaz 
	while (N>0)
	{
		//precte interval
		if ((sscanf(argv[i],"%i",&p->interval)!=1)||(p->interval<=0))
		{
			RR_ERR("Invalid interval for command %i, must be positive integer.",(i-RR_ARG_OFFSET)/2+1);
			free_cmdlist(rrTheCommandList);
			return 6;
		}
		++i;
		//precte a rozparsuje prikaz
		if (parse_cmd(argv[i],p))
		{
			free_cmdlist(rrTheCommandList);
			return 7;
		}
		++i;
		++p;
		--N;
	}
	//terminator uz je vyplneny
	return 0;
}

static void OnTerm(int sig)
{
	//zadost o ukonceni - nahodi se ukoncovaci flag
	rrMustTerminate=1;
	
}

static void OnTimeout(int sig)
{
	static const char killmsg[]="rrCronPerSec: Error: Kill failed.\n";
	//vyprseni timeoutu
	if (rrThePid>0)
	{
		rrTimedOut=1;
		write(STDERR_FILENO,rrTheTOMsg,rrTheTOMsgLen);
		//sestreleni procesu
		if (kill(rrThePid,SIGKILL))
		{
			//selhani killu - vypise hlaseni (bez \0 !)
			write(STDERR_FILENO,killmsg,sizeof(killmsg)-1);
		}
		rrThePid=0;
	}
}

static int get_uptime(long int *uptime)
{
	//cte ze systemu uptime - monotonni cas s rozlisenim v [s]
	static struct sysinfo s_info;
	if (sysinfo(&s_info)<0)
	{
		RR_ERR("Failed to get uptime: %s",strerror(errno));
		return 1;
	}
	*uptime=s_info.uptime;
	return 0;
}

static int loop(void)
{
	//hlavni smycka - cekani a spousteni programu

	long int now;
	long int sleep_time;
	rrCPrecord *p=rrTheCommandList;
	//nastartovani casovacu
	if (get_uptime(&now))
		return 1;
	while (p->arglist!=NULL) p++->start_tim=now;

	//maska pro blokovani casovace behem nastavovani jeho udalosti
	sigset_t waitmask,savemask;
	sigemptyset(&waitmask);
	sigaddset(&waitmask,SIGALRM);
	
	while (!rrMustTerminate)
	{
		if (get_uptime(&now))
			return 1;
		//kontrola vyprchani
		p=rrTheCommandList;
		while (!rrMustTerminate&&(p->arglist!=NULL))
		{
			//porovnani casu odolne proti preteceni
			if (now-p->start_tim>=p->interval)
			{
				//casovac dobehl
				if (rrTheVerbose>3)
				{
					fprintf(stderr,"Running command: ");
					char **pa=p->arglist;
					while (*pa!=NULL) fprintf(stderr," %s",*pa++);
					fputc('\n',stderr);
				}

				//vytvoreni podprocesu
				pid_t chpid = fork();
				if(chpid==-1){
					RR_ERR("Error forking new child: %s",strerror(errno));
					//chyba forku je nejaky hnus s nedostatkem pameti, radsi skoncim - uvolnim prostredky
					return 1;
				}
				if (chpid==0) {
					//potomek
					execvp(p->arglist[0],p->arglist);
					RR_ERR("Exec %s failed: %s",p->arglist[0],strerror(errno));
					exit(255);
				}
				else
				{
					//rodic - ceka na konec potomka, pokud dobehne timeout, sestreli ho
					//puvodne bylo reseno pres sigtimedwait na SIGCHLD, ale na Alchemy to nebylo spolehlive
					int ret;
					int status;
					//zablokovani signalu behem nastavovani
					if (sigprocmask(SIG_BLOCK,&waitmask,&savemask)<0)
					{
						RR_ERR("Failed to set signal mask: %s",strerror(errno));
					}
					//nastavani timeoutu
					rrThePid=chpid;
					snprintf(rrTheTOMsg,RRCP_MSGLEN,"rrCronPerSec: Command (%s) with pid %i has timed out - sending kill.\n",p->cmd,(int)chpid);
					rrTheTOMsg[RRCP_MSGLEN-1]='\0';
					rrTheTOMsgLen=strlen(rrTheTOMsg);
					rrTimedOut=0;
					alarm(rrTheTimeout);
					//odblokovani signalu, nastaveno
					if (sigprocmask(SIG_SETMASK,&savemask,NULL)<0)
					{
						RR_ERR("Failed to restore signal mask: %s",strerror(errno));
					}
					while (!rrMustTerminate) {
						//cekani na konec potomka/timeout
						//prevzeti navratove hodnoty potomka
						ret=waitpid(chpid,&status,0);
						if ((ret<0)&&(errno!=EINTR))
						{
							//selhani waitu - skonci cekani
							RR_ERR("Wait for child return value failed: %s",strerror(errno));
							break;
						}
						else if (ret==chpid)
						{
							//potomek dobehl
							if (WIFEXITED(status))
							{
								RR_DBG("Exit status: %d",WEXITSTATUS(status));
							}
							break;
						}
					}
					//zablokovani signalu behem nastavovani
					if (sigprocmask(SIG_BLOCK,&waitmask,&savemask)<0)
					{
						RR_ERR("Failed to set signal mask: %s",strerror(errno));
					}
					rrThePid=0;
					alarm(0); //vypnuti casovace
					//odblokovani signalu, nastaveno
					if (sigprocmask(SIG_SETMASK,&savemask,NULL)<0)
					{
						RR_ERR("Failed to restore signal mask: %s",strerror(errno));
					}
				}

				//restart casovace
				if (get_uptime(&now))
					return 1;
				p->start_tim=now;
			}
			++p;
		}

		if (rrMustTerminate)
			break;
		
		//dalsi udalost
		if (get_uptime(&now))
			return 1;
		sleep_time=-1; //invalidovana hodnota
		p=rrTheCommandList;
		//nalezeni nejmensiho zbyvajiciho casu
		while (p->arglist!=NULL)
		{
			long int rem_time=p->interval-(now-p->start_tim);
			if (rem_time<=0)
			{
				//vyprsely casovac ceka na obslouzeni
				sleep_time=0;
			} else {
				//pokud jeste nemame zadny interval nebo je aktualni kratsi nez ten drive nalezeny, ulozi se
				if ((sleep_time<0)||(sleep_time>rem_time)) sleep_time=rem_time;
			}
			++p;
		}
		if (sleep_time<=0)
		{
			RR_DBG("Zero or negative sleep: %li.",sleep_time);
		}
		else
		{
			//cekani na udalost
			if (!rrMustTerminate)
			{
				RR_DBG("Next event in %li s...",sleep_time);
				sleep(sleep_time);
			}
		}
	}
	return 0;
}

int main(int argc,char *argv[])
{
	//precteni parametru
	if (parse_args(argc,argv))
	{
		//chybne parametry
		return 1;
	}
#if 0
	//DEBUG
	rrCPrecord *p=rrTheCommandList;
	int i=0;
	while (p->arglist!=NULL)
	{
		printf("Command %i - interval %i (%i,%i):\n",i,p->interval,(int)p->arg_num,(int)p->arg_alloc);
		char **pa=p->arglist;
		while (*pa!=NULL)
			printf("%s\n",*pa++);
		++p;
		++i;
	}
#endif

	//nastaveni signalu
	struct sigaction sa;
	sa.sa_handler=OnTerm;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags=0;
	if ((sigaction(SIGTERM,&sa,NULL)<0)||(sigaction(SIGINT,&sa,NULL)<0)||(sigaction(SIGHUP,&sa,NULL)<0))
	{
		RR_ERR("Failed to set termination signal handler: %s",strerror(errno));
		free_cmdlist(rrTheCommandList);
		return 1;
	}
	sa.sa_handler=OnTimeout;
	if (sigaction(SIGALRM,&sa,NULL)<0)
	{
		RR_ERR("Failed to set timeout signal handler: %s",strerror(errno));
		free_cmdlist(rrTheCommandList);
		return 1;
	}

	//hlavni smycka
	int ret=loop();
	if (rrMustTerminate)
	{
		RR_DBG("Terminated by signal.");
	}
	free_cmdlist(rrTheCommandList);
	return ret;
}

#else //RR_CPS_HALF_LIFE

#define RR_ERR(a...) {fprintf(stderr,"rrCronPerSec:");fprintf(stderr,a);}
#define RR_DBG(a...) {if(rrTheVerbose>=8){fprintf(stderr,"rrCronPerSec:");fprintf(stderr,a);}}

static int rrTheVerbose =0;
static int rrTheTimeout =0;
int rrThePid=0;
char err_buf[200];
int err_len;

void timed_out(int p)
{
	if(rrThePid)
	{
		int sig;
		sig=SIGKILL;
		//Must be posix safe
		kill(rrThePid,sig);
		//Must be posix safe
		write(STDERR_FILENO,err_buf,err_len);
	}
} // timed out

void kill_signal(int p){
	timed_out(p);
	//Oprava po Jakubovi. bla.
	//Must be posix safe
	_exit(255);
}


struct arglist {
	char    **list;
	int     num;
	int     nalloc;
};

void	 addargs(struct arglist *, char *, ...);

struct record
{
	//time_t tim;
	clock_t tim;
	int interval;
	const char * cmd;
};

void *
xmalloc(size_t size)
{
	void *ptr;

	if (size == 0) {
		RR_ERR( "xmalloc: zero size\n");
		exit(EXIT_FAILURE);
	}
	ptr = malloc(size);
	if (ptr == NULL) {
		RR_ERR( "xmalloc: out of memory (allocating %lu bytes)\n", (u_long) size);
		exit(EXIT_FAILURE);
	}
	return ptr;
}

void *
xrealloc(void *ptr, size_t new_size)
{
	void *new_ptr;

	if (new_size == 0) {
		RR_ERR( "xrealloc: zero size\n");
		exit(EXIT_FAILURE);
	}
	new_ptr = realloc(ptr, new_size);
	if (new_ptr == NULL) {
		RR_ERR( "xrealloc: out of memory (new_size %lu bytes)\n", (u_long) new_size);
		exit(EXIT_FAILURE);
	}
	return new_ptr;
}

void
xfree(void *ptr)
{
	if (ptr == NULL) {
		RR_ERR( "xfree: NULL pointer given as argument\n");
		exit(EXIT_FAILURE);
	}
	free(ptr);
}

char *
xstrdup(const char *str)
{
	size_t len;
	char *cp;

	len = strlen(str) + 1;
	cp = xmalloc(len);
	strncpy(cp, str, len);
	return cp;
}

void addargs(struct arglist *args, char *fmt, ...)
{
	va_list ap;
	char buf[1024];
	int nalloc;

	va_start(ap, fmt);
	vsnprintf(buf, sizeof(buf), fmt, ap);
	va_end(ap);

	nalloc = args->nalloc;
	if (args->list == NULL) {
		nalloc = 32;
		args->num = 0;
	} else if (args->num+2 >= nalloc)
		nalloc *= 2;

	args->list = xrealloc(args->list, nalloc * sizeof(char *));
	args->nalloc = nalloc;
	args->list[args->num++] = xstrdup(buf);
	args->list[args->num] = NULL;
}

static void job(struct record * pr, int N)
{
	int i;
	//init
	//time_t tt=time(NULL);
	clock_t tt=times(NULL)/sysconf(_SC_CLK_TCK);
	for(i=0;i<N;i++)
	{
		pr[i].tim=tt+pr[i].interval;
	}
	for(;;)
	{
		//time_t tt1=time(NULL);
		clock_t tt1=times(NULL)/sysconf(_SC_CLK_TCK);
		//call
		for(i=0;i<N;i++)
		{
			if(pr[i].tim<=tt1)
			{
#if 1
				rrThePid = fork();
				if(rrThePid==-1){
					RR_DBG(("Error forking new child\n"));
					continue;
				}
				if(!rrThePid){
					/* Child */
					struct arglist args;
					args.list=NULL;
					char* b = pr[i].cmd;
					char* o;
					char* cmd;
					while(1){
						o = b;
						b = strchr(o,' ');
						if(b){
							cmd = xmalloc(b-o);
							memcpy(cmd,o,b-o);
							cmd[b-o]='\0';
							addargs(&args,cmd);
							xfree(cmd);
						}else{
							addargs(&args,o);
							break;
						}
						//skip the ' '
						b++;
					}
					//addargs(&args,pr[i].cmd);
					int i;
					for(i=0;i<args.num;i++){
						RR_DBG("%s\n",args.list[i]);
					}

					execvp(args.list[0],args.list);
					perror("exec()");
					exit(255);
				}else{
					int status;
					pid_t end;
					//Oprava po Jakubovi. bla.
					snprintf(err_buf,sizeof(err_buf),"Killing proces pid:%d cmd: %s\n",
							rrThePid, pr[i].cmd);
					//Oprava po Jakubovi. bla.
					err_buf[sizeof(err_buf)-1]=0;
					err_len = strlen(err_buf);

					alarm(rrTheTimeout);
					do{
						//Poprve to tady ceka na kill, pak se to odblokuje, z procesu se stane zombik a vrati to EINTR
						//Podruhe to uklidi zombika. bla.
						end = waitpid(rrThePid,&status,0);
					}while( (end==-1 && errno==EINTR));
					if(end < 0 || end != rrThePid)
					{
						//Dost fatalni pruser, nemelo by nikdy nastat. bla.
						RR_ERR("Wrong pid returned from: %d\n",end);
						perror("waitpid()");
					}
					else
					{
						if(WIFEXITED(status))
						{
							RR_DBG("Exit status: %d\n", WEXITSTATUS(status));
						}
					}
					rrThePid = 0;
					//Pouzivam sleep -> nutno vypnout. bla.
					alarm(0);
				}
#else
				alarm(rrTheTimeout);
				//Potrebuju pid: musim se forknout a spustit ten kram.
				//Musi se zablokovat SIGCHLD - potomek muze skoncit driv, nez se vzpamatuju (child na linuxu bezi nak moc brzo).
	 			//Zablokuju SIGCHLD, ulozim si pid, povolim SIGCHLD

				//Muze chvili trvat...
				system(pr[i].cmd);
#endif
				//time_t tt2=time(NULL);
				clock_t tt2=times(NULL)/sysconf(_SC_CLK_TCK);
				pr[i].tim=tt2+pr[i].interval;
			}
		}
		//next
		unsigned long next;
		next=0xffffffff;
		for(i=0;i<N;i++)
		{
			if((unsigned long)pr[i].tim<next)
				next=(unsigned long)pr[i].tim;
		}
		//sleep
		int sl;
		//time_t tt3=time(NULL);
		clock_t tt3=times(NULL)/sysconf(_SC_CLK_TCK);
		sl=(int)next-(int)tt3;
		if(sl<=0)
		{
			RR_DBG("Zero or negative sleep:%d.\n",sl);
		}
		else
		{
			sleep((unsigned int)sl);
		}
	}
}

int main(int argc,char *argv[])
{
#define RR_ARG_OFFSET 3
	if(argc<RR_ARG_OFFSET)
	{
		fprintf(stderr,"usage: rrcronpersec verboselvl timeout [sec cmd].\n");
		return 2;
	}
	rrTheVerbose=atoi(argv[1]);
	rrTheTimeout=atoi(argv[2]);
	//Parse cron parameters.
	int N=(argc-RR_ARG_OFFSET)/2;
	if(N==0)
	{
		RR_ERR("Zero number of items.\n");
		return 1;
	}

	signal(SIGALRM,timed_out);
	signal(SIGTERM,kill_signal);
	signal(SIGHUP,kill_signal);
	signal(SIGQUIT,kill_signal);
	signal(SIGINT,kill_signal);
	signal(SIGSTOP,kill_signal);
	signal(SIGSEGV,kill_signal);
	
	struct record * pr;
	pr=xmalloc(sizeof(struct record)*N);
	int i;
	for(i=0;i<N;i++)
	{
		int sec=0;
		sec=atoi(argv[RR_ARG_OFFSET+i*2]);
		if(!sec)
		{
			RR_ERR("Zero interval.\n");
			free(pr);
			return 1;
		}
		pr[i].interval=sec;
		const char * cmd;
		cmd=argv[RR_ARG_OFFSET+i*2+1];
		pr[i].cmd=cmd;
		RR_DBG("Add: interval:%d cmd:%s.\n",sec,cmd);
	}
	job(pr,N);
	free(pr);
	return 0;
}

#endif //RR_CPS_HALF_LIFE
