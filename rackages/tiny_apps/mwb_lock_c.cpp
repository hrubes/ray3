#include <cstring>
#include "mwb_lock_c.h"

MwbLock_C::MwbLock_C(const char* lock_name, unsigned int timeout, unsigned int expiration)
{
	strncpy(lockname, lock_name, EL_STRLEN_MAX);
	state = exp_lock(lockname, elTheCallerPid, timeout, expiration, elTheBackoffMin, elTheBackoffMax);
}

MwbLock_C::~MwbLock_C()
{
	if( state == 0 )
		exp_unlock(lockname);
}

