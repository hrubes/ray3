#ifndef __MWB_LOCK_C_H__
#define __MWB_LOCK_C_H__

#include "mwb_lock.h"

class MwbLock_C
{
	public:
		MwbLock_C(const char* lock_name, unsigned int timeout, unsigned int expiration);

		//vraci: 
		// =0 - zamceno, 
		// =1 - timeout, 
		// <0 - chyba	
		int getState(void) {return state;};
		~MwbLock_C();
	private:
		char lockname[EL_STRLEN_MAX];
		int state;
};

#endif //__MWB_LOCK_C_H__
