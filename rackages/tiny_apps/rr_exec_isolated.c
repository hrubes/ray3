/*
	rr_exec_isolated.c - run given command with its signal mask reset and extra (not std) file descriptors closed
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>

//exactly defined return value on error
#define E_FAILURE 255

static void usage()
{
	fprintf(stderr,
		"Usage: rr_exec_isolated <fullpath_cmd> [ <arg> ... ]\n"
		"       - run command with parameters, its signal mask will be reset and extra file descriptors (not stdin, stdout or stderr) will be closed\n"
	);
}

int main(int argc, char* argv[])
{
	if (argc<2)
	{
		usage();
		exit(E_FAILURE);
	}
	//reset process mask
	sigset_t newmask;
	sigemptyset(&newmask);
	if (sigprocmask(SIG_SETMASK,&newmask,NULL))
	{
		perror("sigprocmask");
		exit(E_FAILURE);
	}
#if 1
	//cut file descriptors
	DIR *d;
	if ((d=opendir("/proc/self/fd")))
	{
		struct dirent *de;
		while ((de=readdir(d)))
		{
			if (de->d_name[0]=='.')
				continue;
			long l;
			char *e=NULL;
			int fd;
			errno=0;
			l=strtol(de->d_name,&e,10);
			fd=(int)l;
			if ((errno!=0) || (*e!='\0') || ((long)fd!=l))
			{
				fprintf(stderr,"Error: Invalid fd (%s).\n",de->d_name);
				continue;
			}
			if (fd<3)
				continue;
			if (fd==dirfd(d))
				continue;
			if (close(fd)<0)
				fprintf(stderr,"Error: close %s: %s\n",de->d_name,strerror(errno));
		}
		closedir(d);
	} else {
		perror("opendir");
		exit(E_FAILURE);
	}
	//possible fallback is to close every fd up to getrlimit(RLIMIT_NOFILE, &rl)
#endif
	//run command
	execv(argv[1],argv+1);
	perror("execv");
	exit(E_FAILURE);
}

/* konec rr_exec_isolated.c */
