/*
	rr_exp_lock.c - implementace zamku s vyprchanim pro pouziti v Shellu
		- zamek je soubor, ktery existuje, dokud je zamceno, pri odemykani je odstranen
		- pri zamykani se pouziva pomocny zamek - soubor <zamek>.sublock, ktery je vytvoren pri prvnim zamceni a zustava v systemu
*/

//#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
//#include <errno.h>
#include <string.h>
//#include <signal.h>
//#include <time.h>
//#include <sys/types.h>
//#include <sys/stat.h>
//#include <fcntl.h>
//#include <sys/time.h>
#include "mwb_lock.h"

enum elMode { //mody programu
	elM_Lock, //zamceni
	elM_Unlock //odemceni
}; //elMode

//parametry
static enum elMode elTheMode=elM_Lock; //mod
static char elTheLockName[EL_STRLEN_MAX]=""; //jmeno zamku (soubor)
//static int elTheVerbosity=3; //uroven vypisu
//static pid_t elTheCallerPid=0; //PID volajiciho
//static unsigned int elTheTimeout=0; //timeout zamykani [s]
//static unsigned int elTheExpiration=0; //doba vyprchani zamku [s]
//static unsigned int elTheBackoffMin=500; //minimalni doba cyklu cekani [ms]
//static unsigned int elTheBackoffMax=1500; //maximalni doba cyklu cekani [ms]

/*
 * zobrazeni napovedy
 */
static void usage()
{
	fprintf(stderr,
		"Usage: rr_exp_lock <options>\n"
		"Options:\n"
		"\t-h         - show help\n"
		"\t-v <num>   - set verbosity level, default: %i\n"
		"\t-m <mode>  - mode:\n"
		"\t\tl        - lock%s\n"
		"\t\tu        - unlock%s\n"
		"\t-f <file>  - lock file\n"
		"\t-p <pid>   - caller PID (for locking)\n"
		"\t-t <num>   - locking timeout [s] (0=no waiting), default: %hus\n"
		"\t-e <num>   - lock expiration time [s], default: %hus\n"
		"\t-b <num>   - minimal backoff time [ms], default: %hums\n"
		"\t-B <num>   - maximal backoff time [ms], default: %hums\n"
		"Return value:\n"
		"\t0      - success\n"
		"\t1      - cannot lock (timeout, interrupted)\n"
		"\tother  - error\n"
		"Examples:\n"
		"\trr_exp_lock -m l -f /var/lock/test.lock -p $$ -t 10 -e 600\n"
		"\trr_exp_lock -m u -f /var/lock/test.lock\n"
		"\n"
		,elTheVerbosity
		,(elTheMode==elM_Lock)?" (default)":""
		,(elTheMode==elM_Unlock)?" (default)":""
		,elTheTimeout
		,elTheExpiration
		,elTheBackoffMin
		,elTheBackoffMax
		);
}

/*
 * zpracovani vstupnich parametru
 * parametry:
 *     argc, argv - vstupni parametry
 * vraci: 0 - OK, <0 - chyba
 */
static int parse_args(int argc,char *argv[])
{
	int o;
	char *r;
	while ((o=getopt(argc,argv,"hv:m:f:p:t:e:b:B:"))!=-1)
	{
		switch (o)
		{
			case 'h':
				usage();
				return -1;
			case 'v':
				elTheVerbosity=strtol(optarg,&r,10);
				if (*r!='\0')
				{
					EL_ERROR("Invalid value of verbosity (%s), must be an integer.",optarg);
					return -1;
				}
				break;
			case 'm':
				switch (optarg[0])
				{
					case 'l':elTheMode=elM_Lock;break;
					case 'u':elTheMode=elM_Unlock;break;
					default:
						EL_ERROR("Invalid mode (%s).",optarg);
						return -1;
				}
				break;
			case 'f':
				strncpy(elTheLockName,optarg,EL_STRLEN_MAX);
				if (*elTheLockName=='\0')
				{
					EL_ERROR("Lock file name must not be empty.");
					return -1;
				}
				if ((elTheLockName[EL_STRLEN_MAX-1]!='\0')||(strlen(elTheLockName)+sizeof(elSubLockSuffix)>EL_STRLEN_MAX))
				{
					EL_ERROR("Lock file name is too long.");
					return -1;
				}
				break;
			case 'p':
				elTheCallerPid=strtoul(optarg,&r,10);
				if ((*r!='\0')||(elTheCallerPid==0))
				{
					EL_ERROR("Invalid value of caller PID (%s), must be positive integer.",optarg);
					return -1;
				}
				break;
			case 't':
				elTheTimeout=strtoul(optarg,&r,10);
				if (*r!='\0')
				{
					EL_ERROR("Invalid value of locking timeout (%s), must be non-negative integer.",optarg);
					return -1;
				}
				break;
			case 'e':
				elTheExpiration=strtoul(optarg,&r,10);
				if ((*r!='\0')||(elTheExpiration==0))
				{
					EL_ERROR("Invalid value of lock expiration time (%s), must be positive integer.",optarg);
					return -1;
				}
				break;
			case 'b':
				elTheBackoffMin=strtoul(optarg,&r,10);
				if ((*r!='\0')||(elTheBackoffMin==0))
				{
					EL_ERROR("Invalid value of minimal backoff time (%s), must be positive integer.",optarg);
					return -1;
				}
				break;
			case 'B':
				elTheBackoffMax=strtoul(optarg,&r,10);
				if ((*r!='\0')||(elTheBackoffMax==0))
				{
					EL_ERROR("Invalid value of maximal backoff time (%s), must be positive integer.",optarg);
					return -1;
				}
				break;
			default:
				usage();
				return -1;
		}
	}
	int ret=0;
	if (elTheLockName[0]=='\0')
	{
		EL_ERROR("Lock file name must be set.");
		ret=-1;
	}
	switch (elTheMode)
	{
		case elM_Lock:
			//dodatecna kontrola parametru
			if (elTheCallerPid==0)
			{
				EL_WARNING("Caller PID was not set.");
			}
			if (elTheExpiration==0)
			{
				EL_ERROR("Expiration time must be set.");
				ret=-1;
			}
			if (elTheBackoffMin>elTheBackoffMax)
			{
				EL_ERROR("Minimal backoff time (%hums) must be smaller than maximal backoff time (%hums).",elTheBackoffMin,elTheBackoffMax);
				ret=-1;
			}
			else if (elTheTimeout>0)
			{
				if ((long int)elTheBackoffMax>=elTheTimeout*1000L)
				{
					EL_ERROR("Locking timeout (%hus) must be greater than maximal backoff time (%hums).",elTheTimeout,elTheBackoffMax);
					ret=-1;
				}
				if ((long int)elTheBackoffMax>=elTheExpiration*1000L)
				{
					EL_ERROR("Lock expiration timeout (%hus) must be greater than maximal backoff time (%hums).",elTheExpiration,elTheBackoffMax);
					ret=-1;
				}
			}
			break;
		case elM_Unlock:
			break;
	}
	return ret;
}

int main(int argc,char *argv[])
{
	elTheVerbosity=3;
	if (parse_args(argc,argv)<0)
		return -1;
	int ret=0;
	switch (elTheMode)
	{
		case elM_Lock:
			//zamykani
			ret=exp_lock(elTheLockName,elTheCallerPid,elTheTimeout,elTheExpiration,elTheBackoffMin,elTheBackoffMax);
			break;
		case elM_Unlock:
			//odemykani
			ret=exp_unlock(elTheLockName);
			break;
		default:
			EL_ERROR("Unknown mode encountered (%i).",elTheMode);
			ret=-1;
			break;
	}
	return ret;
}

/* konec rr_exp_lock.c */

