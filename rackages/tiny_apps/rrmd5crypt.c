/*
	rrmd5crypt.c - program pro prevod hesla na MD5 hash pouzivany v /etc/shadow
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <crypt.h>
#include <time.h>

/* delka perturbacniho retezce */
#define SALTLEN 8
/* maximalni delka hesla cteneho ze stdin */
#define MAX_INLEN 32

static char chset[]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789./";

void usage()
{
	fprintf(stderr,
		"Usage: rrmd5crypt [ <options> ] <password>\n"
		"       rrmd5crypt [ <options> ] < <file>\n"
		"Options:\n"
		"       -h        - show help\n"
		"       -s <str>  - convert to hash with given seed (empty=random seed (default)), 8 characters from [a-zA-Z0-9./] required\n"
	);
}

/* funkce pro kryptovani hesla, vraci MD5 hash */
const char *myCrypt(const char *pwd,const char *seed)
{
	int i;
	char salt[SALTLEN+5]="$1$";

	/* inicializace perturbacniho retezce */
	if (seed!=NULL)
	{
		/* use predefined salt */
		strncpy(salt+3,seed,SALTLEN);
	} else {
		/* random salt */
		for(i=0;i<SALTLEN;i++)
			salt[i+3]=chset[rand() % strlen(chset)];
	}
	salt[SALTLEN+3]='$';
	salt[SALTLEN+4]='\0';
	/* vypocet hashe */
	return crypt(pwd,salt);
}

int main(int argc, char *argv[])
{
	char passwd[MAX_INLEN];
	char *p_nl;
	char *p_seed=NULL;
	char *p_passwd=NULL;
	int o;
	srand(time(NULL));
	/* zpracovani parametru */
	while ((o=getopt(argc,argv,"hs:"))!=-1)
	{
		switch (o)
		{
			case 'h':
				/* napoveda */
				usage();
				return 1;
			case 's':
				/* seed - parametr hashovaci funkce */
				if (*optarg=='\0')
				{
					p_seed=NULL;
				} else {
					p_seed=optarg;
					if (strlen(p_seed)<8)
					{
						fprintf(stderr,"Error: Invalid seed length, 8 characters required.\n");
						return 2;
					}
					if (strspn(p_seed,chset)<8)
					{
						fprintf(stderr,"Error: Invalid characters in seed, must be from [a-zA-Z0-9./].\n");
						return 2;
					}
				}
				break;
			default:
				/* neznamy parametr */
				usage();
				return 2;
		}
	}
	if (optind<argc)
	{
		/* normalni mod - heslo predano jako parametr */
		p_passwd=argv[optind];
	} else {
		/* paranoidni mod - cte heslo ze stdin */
		if (fgets(passwd,MAX_INLEN,stdin)!=NULL)
		{
			/* odstraneni odradkovani */
			p_nl=strchr(passwd,'\n');
			if (p_nl!=NULL)
				*p_nl='\0';
			p_passwd=passwd;
		} else {
			fprintf(stderr,"Error: Failed to read password from input.\n");
			return 2;
		}
	}
	printf("%s\n",myCrypt(p_passwd,p_seed)); 
	return 0;
}

/* konec rrmd5crypt.c */

