/*
 *	rr_exec_timeout.c - vykona dany prikaz a hlida ho na zadany timeout
 *	                  - vraci navratovou hodnotu prikazu nebo 255 pri chybe/timeoutu
 *	                  - umoznuje hlidat i Shellovy prikaz (pomoci -s); ukonceni na timeout ale ukonci jen hlavni prikaz, ne pripadne spustene podprikazy (napr. ve skriptu)
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

static void usage()
{
	fprintf(stderr,
		"Usage: rr_exec_timeout <timeout> <command> [ <par> ... ]\n"
		"       rr_exec_timeout <timeout> -s <shell_command>\n"
		"\n"
		"Return value:\n"
		"	return value of command\n"
		"	255 - error/timeout\n"
		"\n"
	);
}

static void on_alarm(int sig)
{
}

int main(int argc,char *argv[])
{
	unsigned int timeout;
	int shell_cmd=0;
	char *eptr;
	struct sigaction sa;
	pid_t child;
	int status;
	int retv=0;
	char * const sh_argv[4]={
		"/bin/sh",
		"-c",
		argv[3],
		NULL
	};
	if (argc<3)
	{
		usage();
		return 255;
	}
	//precte timeout
	timeout=strtoul(argv[1],&eptr,10);
	if ((timeout==0)||(*eptr!='\0'))
	{
		fprintf(stderr,"%s: Error: Invalid timeout (%s). Must be positive integer.\n",argv[0],argv[1]);
		return 255;
	}
	//zkontroluje mod
	if (strcmp(argv[2],"-s")==0)
	{
		shell_cmd=1;
		if (argc!=4)
		{
			usage();
			return 255;
		}
	}
	sa.sa_handler=&on_alarm;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags=0;
	//pripravi se na zachycovani SIGALRM
	if (sigaction(SIGALRM,&sa,NULL)<0)
	{
		fprintf(stderr,"%s: Error: Failed to set signal handler: %s\n",argv[0],strerror(errno));
		return 255;
	}
	//odstepeni dcerinneho procesu
	if ((child=fork())<0)
	{
		fprintf(stderr,"%s: Error: Failed to fork: %s\n",argv[0],strerror(errno));
		return 255;
	}
	if (child==0)
	{
		//potomek
		//vykonat zadany prikaz
		if (shell_cmd)
			execvp(sh_argv[0],sh_argv);
		else
			execvp(argv[2],argv+2);
		fprintf(stderr,"%s: Error: Failed to exec command: %s\n",argv[0],strerror(errno));
		exit(255); //ukonci potomka
	}
	//rodic
	//zahaji timeout
	alarm(timeout);
	if (waitpid(child,&status,0)<0)
	{
		if (errno!=EINTR)
		{
			//chyba
			fprintf(stderr,"%s: Error: Failed to wait for child: %s\n",argv[0],strerror(errno));
			return 255;
		}
		//timeout
		fprintf(stderr,"%s: Error: Timeout.\n",argv[0]);
		if (kill(child,SIGKILL)<0)
		{
			fprintf(stderr,"%s: Error: Failed to kill child process %i: %s\n",argv[0],(int)child,strerror(errno));
			return 255;
		}
		retv=255; //timeout
		//odstrani zombie
		if (waitpid(child,&status,WNOHANG)<0)
		{
			fprintf(stderr,"%s: Error: Failed to collect zombie %i: %s\n",argv[0],(int)child,strerror(errno));
			return 255;
		}
	} else {
		//rozbalit navratovou hodnotu
		if (WIFEXITED(status))
			retv=WEXITSTATUS(status);
		else
			retv=255;
	}
	return retv;
}

/* konec rr_exec_timeout.c */

