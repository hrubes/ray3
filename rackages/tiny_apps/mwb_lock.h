#ifndef __MWB_LOCK_H__
#define __MWB_LOCK_H__

#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

#define EL_ERROR(args...) {fputs("rr_exp_lock: Error: ",stderr);fprintf(stderr,args);fputc('\n',stderr);}
#define EL_WARNING(args...) {if (elTheVerbosity>0) {fputs("mwb_lock: Warning: ",stderr);fprintf(stderr,args);fputc('\n',stderr);}}
#define EL_INFO(args...) {if (elTheVerbosity>2) {fputs("mwb_lock: ",stderr);fprintf(stderr,args);fputc('\n',stderr);}}
#define EL_DEBUG(args...) {if (elTheVerbosity>3) {fputs("mwb_lock: ",stderr);fprintf(stderr,args);fputc('\n',stderr);}}
//maximalni delka retezcu
#define EL_STRLEN_MAX 256

//pripona pomocnych souboru
static const char elSubLockSuffix[]=".sublock";

//parametry
static int elTheVerbosity=0; //uroven vypisu
static pid_t elTheCallerPid=0; //PID volajiciho
static unsigned int elTheTimeout=0; //timeout zamykani [s]
static unsigned int elTheExpiration=0; //doba vyprchani zamku [s]
static unsigned int elTheBackoffMin=500; //minimalni doba cyklu cekani [ms]
static unsigned int elTheBackoffMax=1500; //maximalni doba cyklu cekani [ms]

/*
 * zamykani s vyprchanim
 * parametry:
 *     lock_name - jmeno zamku
 *     pid - PID volajiciho procesu
 *     timeout - doba cekani na ziskani zamku [s]
 *     expiration - doba platnosti zamku [s]
 *     boff_min - minimalni doba cekani [ms]
 *     boff_max - maximalni doba cekani [ms]
 * vraci: =0 - zamceno, =1 - timeout, <0 - chyba
 */
int exp_lock(const char *lock_name,pid_t pid,unsigned int timeout,unsigned int expiration,unsigned int boff_min,unsigned int boff_max);
/*
 * odemceni
 * parametry:
 *     lock_name - jmeno zamku
 * vraci: =0 - odemceno, <0 - chyba
 */
int exp_unlock(const char * lock_name);

#ifdef __cplusplus
}
#endif

#endif //__MWB_LOCK_H__
