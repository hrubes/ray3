/*
		- zamek je soubor, ktery existuje, dokud je zamceno, pri odemykani je odstranen
		- pri zamykani se pouziva pomocny zamek - soubor <zamek>.sublock, ktery je vytvoren pri prvnim zamceni a zustava v systemu
*/

#include <stdlib.h>
#include <stdio.h>
//#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>

#include "mwb_lock.h"

//minimalni delka natazeni casovace [ms]
#define EL_QUANTUM 50

//ostatni
static volatile sig_atomic_t elSignal=0; //flag zachyceni signalu

/*
 * handler signalu - preruseni cekani
 */
static void on_signal(int sig)
{
	elSignal=1;
}

/*
 * kontrola vyprchani timeoutu
 * parametry:
 *     p_start - doba startu
 *     len - delka timeoutu [s]
 * vraci: =0 - bezi, 1 - vyprchal, <0 - chyba
 */
static int check_timeout(const struct timespec *p_start,unsigned int len)
{
	struct timespec tnow;
	if (clock_gettime(CLOCK_MONOTONIC,&tnow)<0)
	{
		EL_ERROR("Failed to get actual time: %s",strerror(errno));
		return -1;
	}
	//porovnava zatim jen sekundy - sekundove rozliseni
	//s ochranou proti zastarale casove znamce
	long int diff_s=tnow.tv_sec-(long int)p_start->tv_sec;
	return ((diff_s<0)||(diff_s>(long int)len))?1:0;
}

/*
 * vypocet zbyvajiciho casu
 * parametry:
 *     p_startt - zacatek intervalu
 *     timeout - delka intervalu [s]
 * vraci: zbyvajici cas [ms], <0 - chyba
 */
static long int get_rem_time(const struct timespec *p_startt,unsigned int timeout)
{
	struct timespec tnow;
	long int rem;
	if (clock_gettime(CLOCK_MONOTONIC,&tnow)<0)
	{
		EL_ERROR("Failed to get actual time: %s",strerror(errno));
		return -1;
	}
	rem=(tnow.tv_sec-(long int)p_startt->tv_sec)*1000L+(tnow.tv_nsec-p_startt->tv_nsec)/1000000L; //soucet rozdilu [s] a [ns] casti s prevodem na [ms] se zaokrouhlenim dolu
	if (rem<0)
	{
		EL_ERROR("Monotonic time is not monotonic (%lims).",rem);
		return 0;
	}
	rem=timeout*1000L-rem;
	return (rem>0)?rem:0;
}

/*
 * otevreni souboru odolne proti signalum
 * parametry:
 *     pathname - jmeno souboru
 *     flags - flagy
 *     mode - mod
 * vraci: deskriptor, <0 - chyba
 */
static int safe_open(const char *pathname,int flags,mode_t mode)
{
	int fd;
	do {
		fd=open(pathname,flags,mode);
	} while ((fd<0)&&(errno==EINTR));
	return fd;
}

/*
 * cteni ze souboru odolne proti signalum
 * parametry:
 *     fd - deskriptor
 *     buf - buffer
 *     count - maximalni mnozstvi dat
 * vraci: velikost prectenych dat, =0 - konec souboru, <0 - chyba
 */
static ssize_t safe_read(int fd,void *buf,size_t count)
{
	ssize_t ret;
	ssize_t tot=0;
	void *p=buf;
	do {
		ret=read(fd,p,count-tot);
		if (ret>0)
		{
			p+=ret;
			tot+=ret;
		}
	} while (((ret>0)&&(tot<count))||((ret<0)&&(errno==EINTR)));
	if (ret>=0)
		return tot;
	return ret;
}

/*
 * zapis do souboru odolny proti signalum
 * parametry:
 *     fd - deskriptor
 *     buf - buffer
 *     count - velikost dat
 * vraci: velikost zapsanych dat, <0 - chyba
 */
static ssize_t safe_write(int fd,void *buf,size_t count)
{
	ssize_t ret;
	ssize_t tot=0;
	void *p=buf;
	do {
		ret=write(fd,p,count-tot);
		if (ret>0)
		{
			p+=ret;
			tot+=ret;
		}
	} while (((ret>0)&&(tot<count))||((ret<0)&&(errno==EINTR)));
	if (ret>=0)
		return tot;
	return ret;
}

/*
 * uzavreni souboru odolne proti signalum
 * parametry:
 *     fd - deskriptor souboru
 * vraci: =0 - OK, <0 - chyba
 */
static int safe_close(int fd)
{
	int ret;
	do {
		ret=close(fd);
	} while ((ret<0)&&(errno==EINTR));
	return ret;
}

/*
 * zpracovani hlavniho zamku (chraneno pomocnym zamkem)
 * parametry:
 *     lock_name - jmeno zamku
 *     pid - PID volajiciho procesu
 *     expiration - doba platnosti zamku [s]
 * vraci: =0 - zamceno, =1 - blokovano, <0 - chyba
 */
static int get_main_lock(const char *lock_name,pid_t pid,unsigned int expiration)
{
	//v prubehu teto funkce se ignoruji signaly
	char data[EL_STRLEN_MAX];
	int lfd;
	//otevreni souboru zamku, pokud neni, vytvori se
	if ((lfd=safe_open(lock_name,O_RDWR|O_CREAT,S_IRWXU|S_IRWXG))<0)
	{
		EL_ERROR("Failed to open/create main lock file %s: %s",lock_name,strerror(errno));
		return -1;
	}
	//nacteni obsahu
	ssize_t dlen=safe_read(lfd,data,EL_STRLEN_MAX-1); //nechat misto na ukonceni retezce
	if (dlen<0)
	{
		EL_ERROR("Failed to read main lock file %s: %s",lock_name,strerror(errno));
		safe_close(lfd);
		return -1;
	}
	if (dlen>0)
	{
		//hlavni zamek ma obsah - je zamceny
		//analyza obsahu
		unsigned long int o_tstamp;
		unsigned long int o_pid;
		data[dlen]='\0'; //ukonceni retezce
		EL_DEBUG("Previous lock found.");
		if (sscanf(data,"%lu %lu",&o_tstamp,&o_pid)!=2)
		{
			//neplatny format -> neplatny zamek
			EL_WARNING("Main lock file %s is corrupted, taking over.",lock_name);
		} else {
			//platny format - kontrola, zda nevyprchal
			struct timespec testart;
			testart.tv_sec=o_tstamp;
			testart.tv_nsec=0; //[ns] se ignoruji
			switch (check_timeout(&testart,expiration))
			{
				case 0:
					//bezi - blokuje
					safe_close(lfd);
					return 1;
				case 1:
					//vyprchal
					EL_WARNING("Main lock file %s (owned by %lu) has expired. Taking over.",lock_name,o_pid);
					break;
				default:
					EL_ERROR("Failed to check lock expiration.");
					safe_close(lfd);
					return -1;
			}
		}
		//prebira se zamek - bude se prepisovat
		if (lseek(lfd,0,SEEK_SET)<0)
		{
			EL_ERROR("Failed to seek start of main lock file %s: %s",lock_name,strerror(errno));
			safe_close(lfd);
			return -1;
		}
	}
	//zamek ziskan - zapsat
	struct timespec tnow;
	if (clock_gettime(CLOCK_MONOTONIC,&tnow)<0)
	{
		EL_ERROR("Failed to get actual timestamp: %s",strerror(errno));
		safe_close(lfd);
		return -1;
	}
	int len=snprintf(data,EL_STRLEN_MAX,"%lu %lu\n",(unsigned long int)tnow.tv_sec,(unsigned long int)pid);
	if (len>=EL_STRLEN_MAX)
	{
		EL_ERROR("Failed to format lock data - too long.");
		safe_close(lfd);
		return -1;
	}
	int ret=safe_write(lfd,data,len); //bez '\0'
	if (ret<len)
	{
		if (ret<0)
		{
			EL_ERROR("Failed to write data to main lock file %s: %s",lock_name,strerror(errno));
		} else {
			EL_ERROR("Failed to write all data to main lock file: %s",strerror(errno));
		}
		ret=-1;
	} else {
		ret=0; //zamceno
	}
	//uzavreni
	if (safe_close(lfd)<0)
	{
		EL_ERROR("Failed to close main lock file %s: %s",lock_name,strerror(errno));
		ret=-1;
	}
	if (ret<0)
	{
		//zamek neni nejspis konzistentni -> pokusim se ho odstranit
		if (unlink(lock_name)<0)
		{
			EL_ERROR("Failed to clean main lock file %s: %s",lock_name,strerror(errno));
		}
	}
	return ret;
}

/*
 * kontrola a zamceni hlavniho zamku pomoci pomocneho zamku
 * parametry:
 *     lock_name - jmeno zamku
 *     sublock_name - jmeno pomocneho zamku
 *     pid - PID volajiciho procesu
 *     timeout - doba cekani na ziskani zamku [s]
 *     expiration - doba platnosti zamku [s]
 *     p_startt - cas zacatku cekani
 * vraci: =0 - hlavni zamek zamcen, =1 - hlavni zamek blokovan, =2 - timeout, <0 - chyba
 */
static int try_lock(const char *lock_name,const char *sublock_name,pid_t pid,unsigned int timeout,unsigned int expiration,const struct timespec *p_startt)
{
	int subl_fd;
	//otevrit pomocny zamek
	if ((subl_fd=open(sublock_name,O_RDWR|O_CREAT,S_IRWXU|S_IRWXG))<0)
	{
		if (errno==EINTR)
		{
			return 2;
		} else {
			EL_ERROR("Failed to create sub-lock %s: %s",sublock_name,strerror(errno));
			return -1;
		}
	}
	//zamceni pomocneho zamku
	int f_cmd;
	long int rem_time_ms=0;
	struct itimerval wait_len;
	memset(&wait_len, 0, sizeof(wait_len));
	struct flock fl;
	int ret=-1;
	memset(&fl,0,sizeof(fl));
	fl.l_type=F_WRLCK;
	fl.l_whence=SEEK_SET;
	fl.l_start=0;
	fl.l_len=0;
	if (timeout>0)
	{
		//vypocet timeoutu
		if ((rem_time_ms=get_rem_time(p_startt,timeout))<0)
		{
			EL_ERROR("Failed to get remaining time.");
			safe_close(subl_fd);
			return -1;
		}
		EL_DEBUG("Sub-lock waiting time: %lims",rem_time_ms);
	}
	if (rem_time_ms>0)
	{
		//zamyka s timeoutem
		if (rem_time_ms<EL_QUANTUM) rem_time_ms=EL_QUANTUM;
		wait_len.it_value.tv_sec=rem_time_ms/1000L;
		wait_len.it_value.tv_usec=(rem_time_ms%1000L)*1000L;
		wait_len.it_interval.tv_sec=0;
		wait_len.it_interval.tv_usec=0;
		f_cmd=F_SETLKW;
		//natazeni casovace
		if (setitimer(ITIMER_REAL,&wait_len,NULL))
		{
			EL_ERROR("Failed to start timeout: %s",strerror(errno));
			safe_close(subl_fd);
			return -1;
		}
	} else {
		//zamykani bez timeoutu
		f_cmd=F_SETLK;
	}
	if (fcntl(subl_fd,f_cmd,&fl))
	{
		//nezamcelo
		if (errno==EINTR)
		{
			EL_DEBUG("Timeout in waiting for sub-lock.\n");
			ret=2;
		} else {
			EL_ERROR("Failed to lock sub-lock %s: %s",sublock_name,strerror(errno));
			ret=-1;
		}
	} else {
		//zamceno
		//zastavi timeout
		wait_len.it_value.tv_sec=0;
		wait_len.it_value.tv_usec=0;
		if (setitimer(ITIMER_REAL,&wait_len,NULL))
		{
			EL_ERROR("Failed to stop timeout: %s",strerror(errno));
			ret=-1;
		} else {
			//kriticka sekce - kontrola hlavniho zamku
			ret=get_main_lock(lock_name,pid,expiration);
			//unlock provede close
		}
	}
	//uzavreni souboru (+odemceni, pokud bylo zamceno)
	if (safe_close(subl_fd))
	{
		EL_ERROR("Failed to close sub-lock file %s: %s",sublock_name,strerror(errno));
		ret=-1;
	}
	return ret;
}

/*
 * zamykani s vyprchanim
 * parametry:
 *     lock_name - jmeno zamku
 *     pid - PID volajiciho procesu
 *     timeout - doba cekani na ziskani zamku [s]
 *     expiration - doba platnosti zamku [s]
 *     boff_min - minimalni doba cekani [ms]
 *     boff_max - maximalni doba cekani [ms]
 * vraci: =0 - zamceno, =1 - timeout, <0 - chyba
 */
int exp_lock(const char *lock_name,pid_t pid,unsigned int timeout,unsigned int expiration,unsigned int boff_min,unsigned int boff_max)
{
	EL_DEBUG("Locking: %s",lock_name);
	//sestaveni jmena pomocneho zamku
	char sublock_name[EL_STRLEN_MAX];
	snprintf(sublock_name,EL_STRLEN_MAX,"%s%s",lock_name,elSubLockSuffix);
	sublock_name[EL_STRLEN_MAX-1]='\0';
	//zachyceni signalu HUP, TERM, INT a ALRM
	struct sigaction sig;
	sig.sa_handler=&on_signal;
	sigemptyset(&sig.sa_mask);
	sig.sa_flags=0;
	if ((sigaction(SIGHUP,&sig,NULL)<0)||(sigaction(SIGINT,&sig,NULL)<0)||(sigaction(SIGTERM,&sig,NULL)<0)||(sigaction(SIGALRM,&sig,NULL)<0))
	{
		EL_ERROR("Failed to set signal handler: %s",strerror(errno));
		return -1;
	}
	//zacatek mereni casu
	struct timespec tstart;
	if (clock_gettime(CLOCK_MONOTONIC,&tstart)<0)
	{
		EL_ERROR("Failed to get start time: %s",strerror(errno));
		return -1;
	}
	//inicializace pseudonahodneho generatoru
	srand(tstart.tv_sec^tstart.tv_nsec);
	//zkousi zamcit
	long int boff_time_ms;
	struct timespec boff_time;
	for (;;)
	{
		//pokus o zamceni
		switch (try_lock(lock_name,sublock_name,pid,timeout,expiration,&tstart))
		{
			case 0:
				//zamceno
				EL_DEBUG("Locked.");
				return 0;
			case 1:
				//blokovano
				break;
			case 2:
				//timeout
				return 1;
			default:
				//chyba
				EL_ERROR("Failed to lock.");
				return -1;
		}
		//pokud se nema cekat nebo je zjisten signal, tak je zablokovano
		if (timeout==0)
		{
			EL_DEBUG("No waiting.");
			return 1;
		}

		//cekani na uvolneni zamku
		//zmeri zbyvajici cas
		boff_time_ms=get_rem_time(&tstart,timeout);
		if (boff_time_ms<0)
		{
			EL_DEBUG("Failed to get remaining time for backoff.");
			return -1;
		}
		if (boff_time_ms==0)
		{
			EL_DEBUG("Timeout expired.");
			return 1;
		}
		if (boff_time_ms>(long int)boff_min)
		{
			//randomizovane pro snizeni poctu kolizi
			if (boff_time_ms>(long int)boff_max) boff_time_ms=boff_max;
			boff_time_ms=boff_min+rand()%(boff_time_ms-boff_min+1);
		}
		boff_time.tv_sec=boff_time_ms/1000L;
		boff_time.tv_nsec=(boff_time_ms%1000L)*1000000L;
		if (elSignal)
		{
			EL_DEBUG("Wait skipped.");
			return 1;
		}
		EL_DEBUG("Backoff: %lums",boff_time_ms);
		if (nanosleep(&boff_time,NULL)<0)
		{
			if (errno!=EINTR)
			{
				EL_ERROR("Failed to sleep for %lis %lins: %s",(long int)boff_time.tv_sec,boff_time.tv_nsec,strerror(errno));
				return -1;
			}
		}
		if (elSignal)
		{
			EL_DEBUG("Wait interrupted.");
			return 1;
		}
	}
}

/*
 * odemceni
 * parametry:
 *     lock_name - jmeno zamku
 * vraci: =0 - odemceno, <0 - chyba
 */
int exp_unlock(const char * lock_name)
{
	EL_DEBUG("Unlocking: %s",lock_name);
	if (unlink(lock_name)<0)
	{
		EL_ERROR("Failed to delete %s: %s",lock_name,strerror(errno));
		return -1;
	}
	EL_DEBUG("Unlocked.");
	return 0;
}

/*
int main(int argc,char *argv[])
{
	int ret=0;
	if( strcmp(argv[1], "z") == 0 )
	{
		ret=exp_lock("/tmp/fufifo",elTheCallerPid,elTheTimeout,elTheExpiration,elTheBackoffMin,elTheBackoffMax);
	}
	else if( strcmp(argv[1], "o") == 0 ) 
	{
		ret=exp_unlock("/tmp/fufifo");
	}
	else
		fprintf(stderr, "Nic nebude.\n");
	return ret;
}
*/

