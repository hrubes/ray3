#library of miscellaneous Shell functions

rt_dec2hex()
{
	#print number in hex form
	#parameters:
	#	dec number
	printf "%06X" "$1"
}

rt_echo_cmd()
{
	#print command and its parameters to stderr and execute it
	#parameters:
	#	command and its parameters
	#return value:
	#	return value of the command

	echo "$*" >&2
	"$@"
}

rt_echo_exec()
{
	#print command and its parameters and call exec
	#parameters:
	#	command and its parameters
	#return value:
	#	none, command will become the actual process

	echo "$*" >&2
	exec "$@"
}

rt_get_uptime()
{
	#store actual uptime [s] into variable
	#parameters:
	local RGU_VAR_UPTIME="$1" #name of variable
	#return value:
	#    0 - success
	#    other - failure

	eval $RGU_VAR_UPTIME'=$( rr_get_uptime )'
}

rt_get_dirname()
{
	#extract directory from file name into variable
	#parameters:
	local RGD_FILE="$1" #file name to be parsed
	local RGD_VAR_DNAME="$2" #name of target variable

	eval $RGD_VAR_DNAME'="${RGD_FILE%/*}"'
}

rt_get_basename()
{
	#extract simple file name from full file name
	#parameters:
	local RGB_FILE="$1" #file name to be parsed
	local RGB_VAR_FNAME="$2" #name of target variable

	eval $RGB_VAR_FNAME'="${RGB_FILE##*/}"'
}

rt_get_randnum()
{
	#store pseudorandom number into variable
	#parameters:
	local RGR_VAR_RAND="$1" #target variable

	local RGR_TMP=$( dd if=/dev/urandom count=8 bs=1 2>/dev/null )
	RGR_TMP=$( md5sum <<EOF
$RGR_TMP
EOF
	)
	RGR_TMP=$( cut -b '-4' <<EOF
$RGR_TMP
EOF
	)
	eval $RGR_VAR_RAND'=$(( 0x$RGR_TMP ))'
}

rt_cold_reboot()
{
	#perform cold reboot
	#no parameters
	#returns only on failure

	#terminate all processes refreshing watchdog
	killall rr_wdog 2>/dev/null
	svc -d /service/rrmbas 2>/dev/null
	svc -d /service/rrwdw 2>/dev/null
	#backoff before signalling PIC (necessary)
	#+ allow log messages to be processed and stored by syslog-ng
	# - it is part of rr_reset
	#attempt to perform power cycle
	powercycle
	#wait for reboot
	sleep 10
}

rt_request_recovery_restart()
{
	#ask for restart to recover from a hard error, unless limit of recovery restarts is reached
	#parameters:
	#	optional caller's name (if not present, it is read from $0)
	#return value:
	#	- if it returns, recovery restart was rejected
	#recovery restart is logged to /var/log/recovery_rst
	#configuration file /etc/rrconf/sysdefaults.cnf is presumed included
	#Note: requires root privileges

	local RRR_CALLER="$0"
	if [ $# -gt 0 ]; then
		RRR_CALLER="$1"
	fi
	#check if enough time has passed from boot
	local RRR_NOW
	rt_get_uptime RRR_NOW || RRR_NOW=0
	
	local RRR_CNT=0
	if [ "$RRR_NOW" -lt "$RR_RECOVERY_RST_PERIOD" ]; then
		#too soon after boot - check restart counter
		if [ -r "$RR_RECOVERY_RST_COUNTER" ]; then
			source "$RR_RECOVERY_RST_COUNTER"
			if ! [ "$RRR_CNT" -gt 0 ] 2>/dev/null; then
				#invalid counter - count 1 (there was a restart)
				RRR_CNT=1
			fi
		fi
		if [ "$RRR_CNT" -ge "$RR_RECOVERY_RST_NUM" ] 2>/dev/null; then
			#if the counter is filled, do not restart again
			return
		fi
	fi
	
	# # podpera: discard reboot request - aby se to furt neresetilo
	# echo "Reboot request catched and discarted - podpera" # podpera
	# logger -t RECOVERY_RST "Recovery restart (caller: $RRR_CALLER). Event catched and discarted - podpera" # podpera
	# exit 0 # podpera

	#perform recovery restart
	RRR_CNT=$(( RRR_CNT + 1 ))
	rr_remount rw #remount without locking
	rm -f "$RR_RECOVERY_RST_FLAG"
	echo "RRR_CNT=$RRR_CNT" >"$RR_RECOVERY_RST_COUNTER"
	rr_remount ro
	#log
	logger -t RECOVERY_RST "Recovery restart (caller: $RRR_CALLER)."
	
	#try cold reboot (power cycle)
	rt_cold_reboot
	#note failure to log
	logger -t RECOVERY_RST "Error: ARM has not performed power cycle, trying normal reboot (caller: $RRR_CALLER)."
	sleep 1
	reboot
	#wait for reboot
	while true; do
		sleep 10
	done
}

rt_start_logging()
{
	#Function initiating logging to selected log
	#Parameters:
	local RSL_TAG="$1" #log tag
	local RSL_ALL="$2" #0 - redirect stderr to log, other - redirect both stdout and stderr to log
	#Return value:
	#	0 - OK
	#	other - error

	local RSL_FIFO_HOLDER
	#create fifo placeholder to avoid name collision
	RSL_FIFO_HOLDER=$( mktemp "/tmp/rt_start_logging.tmp.XXXXXX" ) || return 2
	local RSL_FIFO="${RSL_FIFO_HOLDER}.fifo"
	#create fifo
	mkfifo "$RSL_FIFO"
	if [ $? -ne 0 ]; then
		rm -f "$RSL_FIFO_HOLDER"
		return 2
	fi
	#start parallel logging process
	logger -t "$RSL_TAG" <"$RSL_FIFO" >/dev/null 2>&1 &
	#reroute stderr to fifo
	exec 2>"$RSL_FIFO"
	if [ "$RSL_ALL" -ne 0 ]; then
		#reroute stdout to fifo
		exec 1>&2
	fi
	#release name of fifo
	rm -f "$RSL_FIFO"
	#release placeholder
	rm -f "$RSL_FIFO_HOLDER"
	return 0
}

rt_revive_logger()
{
	#Revive logger after FLASH congestion
	msvc -d syslog
	usleep 100000
	msvc syslog >/dev/null 2>&1
	if [ $? -eq 0 ]; then
		msvc -k syslog
	fi
	msvc -u syslog || return 2 #die "Failed to restart logger daemon."
	usleep 500000
	#info "Revive logger daemon."
	return 0
}


rt_start_logging_cnf()
{
	#Function initiating logging to rrconf log
	#Parameters:
	local RSLC_ALL="$1" #0 - redirect stderr to log, other - redirect both stdout and stderr to log
	#Return value:
	#	0 - OK
	#	other - error

	rt_start_logging RRCONF "$RSLC_ALL"
}

rt_log2evlog()
{
	#Function logging to Event Log
	#Parameters:
	RL2E_ORIGIN="$1" #identifier of message origin(subsystem)
	#			BOOT, LINK, CNFUPD, FWUPGR
	RL2E_MESSAGE="$2" #message to be sent to Event Log
	#Note: messages to Event Log are meant for user, they should be sparse and informative
	#Note: description of Event Log messages should be added to /etc/rrconf/evlog_description.txt

	logger -t RREVLOG "($RL2E_ORIGIN) $RL2E_MESSAGE"
}

rt_usrmsg_write()
{
	#Function for writing user messages to message file
	#Parameters:
	local RUMW_DIR="$1" #directory with message file
	local RUMW_MSG="$2" #message
	#configuration file /etc/rrconf/svcdefaults.cnf is presumed included

	echo "$RUMW_MSG" >>"$RUMW_DIR/$RR_USRCOMM_MSG_FILE"
}

