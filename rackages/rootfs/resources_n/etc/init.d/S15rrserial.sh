#!/bin/sh
#Recover serial number


#common functions and includes
source /etc/init.d/init_fn.sh "S15rrserial.sh"

recover_from_archive()
{
	#Unpack file from archive
	#Parameters:
	local RFA_ARCHIVE="$1" #archive
	local RFA_WORKDIR="$2" #working directory
	local RFA_FILE="$3" #archived file (full path)
	#Failure is detected by following steps

	local RFA_UNPACKDIR="$RFA_WORKDIR/unpack"
	rr_remount rw
	mkdir "$RFA_UNPACKDIR"
	tar xzf "$RFA_ARCHIVE" -C "$RFA_UNPACKDIR"
	mv "${RFA_UNPACKDIR}$RFA_FILE" "$RFA_FILE" #atomic replace
	rm -r "$RFA_UNPACKDIR"
	rr_remount ro
}

create_archive()
{
	#Create archive file
	#Parameters:
	local RFA_ARCHIVE="$1" #archive
	local RFA_FILE="$2" #archived file (full path)
	#Return value:
	#	0 - OK
	#	other - error

	rr_remount rw || return 2 #error
	tar czf "${RFA_ARCHIVE}.tmp" "$RFA_FILE" 2>/dev/null
	if [ $? -ne 0 ]; then
		rm -f "${RFA_ARCHIVE}.tmp"
		return 2 #error
	fi
	mv "${RFA_ARCHIVE}.tmp" "$RFA_ARCHIVE" #atomic replace
	if [ $? -ne 0 ]; then
		rm -f "${RFA_ARCHIVE}.tmp" "$RFA_ARCHIVE"
		return 2
	fi
	rr_remount ro || return 2 #error
	return 0 #OK
}

#recover serial number
if [ ! -f "$MWBR_FILE_SERIAL" ]; then
	if [ -f "$RR_CNF_SERIAL_ARCHIVE" ]; then
		info "Serial number file is missing, recovering from archive."
		recover_from_archive "$RR_CNF_SERIAL_ARCHIVE" "/etc/rrprd" "$MWBR_FILE_SERIAL"
	fi
fi
if [ ! -f "$MWBR_FILE_SERIAL" ]; then
	error "Serial number file is missing and cannot be recovered, entering failsafe mode."
	rt_log2evlog "BOOT" "Serial number is missing. Entering failsafe mode."
elif [ ! -f "$RR_CNF_SERIAL_ARCHIVE" ]; then
	info "Creating serial number archive."
	create_archive "$RR_CNF_SERIAL_ARCHIVE" "$MWBR_FILE_SERIAL" || error "Failed to create serial number archive."
fi

#recover MAC address
if [ ! -f "$RR_FILE_MAC_ADDRESS" ]; then
        if [ -f "$RR_CNF_MAC_ADDRESS_ARCHIVE" ]; then
                info "MAC address file is missing, recovering from archive."
                recover_from_archive "$RR_CNF_MAC_ADDRESS_ARCHIVE" "/etc/rrprd" "$RR_FILE_MAC_ADDRESS"
        fi
fi
