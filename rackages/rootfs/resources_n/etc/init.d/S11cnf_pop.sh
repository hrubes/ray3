#!/bin/sh
#Configuration recovery
#WARNING: Must be kept consistent with rrsh_cnf_push and rr_cnf_pop
#NOTE: syslog-ng is not running yet, logging from rr_remount is disabled

#common functions and includes
source /etc/init.d/init_fn.sh "S11cnf_pop.sh"

CP_BUDIR=/mnt/rwmnt/backup
CP_PACKAGES="rrkeydropb.tgz rrkeysshroot.tg rrprd_serial.tgz rrmacaddr.tgz rripaddr.tgz"

#recovery after interrupted firmware upgrade
if [ -d "$CP_BUDIR" ]; then
	rr_remount rw
	CP_FAIL=0
	#recover packages
	for CP_PACK in $CP_PACKAGES; do
		if [ ! -f "/$CP_PACK" ] && [ -f "$CP_BUDIR/$CP_PACK" ]; then
			info "Recovering: $CP_PACK"
			cp -a "$CP_BUDIR/$CP_PACK" /mnt/rootfs
			if [ $? -ne 0 ]; then
				error "Failed to recover $CP_PACK."
				CP_FAIL=1
			fi
		fi
	done
	rr_remount ro
	if [ "$CP_FAIL" -ne 0 ]; then
		error "Recovery failed."
	else
		#remove processed backup
		rm -r "$CP_BUDIR"
	fi
fi

