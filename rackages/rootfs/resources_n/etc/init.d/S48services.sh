#!/bin/sh
#Nastartovani vsech sluzeb ze /service, ktere neobsahuji soubor down (chovani simulujici supervise)

start()
{
	#spusteni sluzeb se /service a jejich logu
	for SERVICE in `ls /service/`; do
		if [ -d /service/$SERVICE ]; then
			if ! [ -e /service/$SERVICE/down ]; then
				msvc -u /service/$SERVICE
				if [ -d /service/$SERVICE/log ]; then
					#v podstate zbytecne, jen pri restartu
					msvc -u /service/$SERVICE/log
				fi
			fi
		fi
	done
}

stop()
{
	#zastaveni sluzeb ze /service, vcetne logu
	for SERVICE in `ls /service/`; do
		if [ -d /service/$SERVICE ]; then
			msvc -d /service/$SERVICE
			if [ -d /service/$SERVICE/log ]; then
				msvc -d /service/$SERVICE/log
			fi
		fi
	done
}

case $1 in
	start)
		start
		;;
	stop)
		stop
		;;
	restart)
		stop
		start
		;;
	*)
		start
		;;
esac

