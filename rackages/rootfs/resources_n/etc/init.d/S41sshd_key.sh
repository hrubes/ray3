#!/bin/sh
#Recover SSH keys

#common functions and includes
source /etc/init.d/init_fn.sh "S41sshd_key"


DROPBEARDIR="/etc/dropbear"
DROPBEARKEYDSS="$DROPBEARDIR/dropbear_dss_host_key"
DROPBEARKEYRSA="$DROPBEARDIR/dropbear_rsa_host_key"
DROPBEARARCH="/mnt/rootfs/rrkeydropb.tgz"

mkkeys()
{
	#create new dropbear keys
	/bin/dropbearkey -t dss -f "${DROPBEARKEYDSS}.tmp"
	if [ $? -ne 0 ]; then
		error "Failed to generate DSS key."
		rm -f "${DROPBEARKEYDSS}.tmp"
		return 42
	fi
	/bin/dropbearkey -t rsa -f "${DROPBEARKEYRSA}.tmp"
	if [ $? -ne 0 ]; then
		error "Failed to generate RSA key."
		rm -f "${DROPBEARKEYRSA}.tmp"
		return 42
	fi
	mv "${DROPBEARKEYDSS}.tmp" "$DROPBEARKEYDSS" #atomic replace
	if [ $? -ne 0 ]; then
		error "Failed to rename new DSS key."
		rm -f "${DROPBEARKEYDSS}.tmp"
		return 42
	fi
	mv "${DROPBEARKEYRSA}.tmp" "$DROPBEARKEYRSA" #atomic replace
	if [ $? -ne 0 ]; then
		error "Failed to rename new RSA key."
		rm -f "${DROPBEARKEYRSA}.tmp"
		return 42
	fi
	#force creation of new archive
	rm -f "$DROPBEARARCH"
	return 0
}


#no need to lock, no daemons started

#create/recover dropbear identity
if [ ! -f "$DROPBEARKEYDSS" ] || [ ! -f "$DROPBEARKEYRSA" ]; then
	rr_remount rw
	if [ -f "$DROPBEARARCH" ]; then
		#recover from archive
		info "SSH identity was lost - recovering from archive."
		UNPACK_DIR="$DROPBEARDIR/archive"
		mkdir "$UNPACK_DIR"
		tar zxf "$DROPBEARARCH" -C "$UNPACK_DIR"
		mv "${UNPACK_DIR}$DROPBEARKEYDSS" "$DROPBEARKEYDSS" #atomic replace
		mv "${UNPACK_DIR}$DROPBEARKEYRSA" "$DROPBEARKEYRSA" #atomic replace
		rm -r "$UNPACK_DIR"
		if [ ! -f "$DROPBEARKEYDSS" ] || [ ! -f "$DROPBEARKEYRSA" ]; then
			#archive was empty - create new keys
			error "Identity file missing from archive."
			#archive will be invalidated
		fi
	fi
	if [ ! -f "$DROPBEARKEYDSS" ] || [ ! -f "$DROPBEARKEYRSA" ]; then
		#no archive or invalid archive - make keys
		rt_log2evlog "BOOT" "SSH identity was lost. Generating new one."
		mkkeys
		if [ $? -ne 0 ]; then
			log_rrconf "Error: Failed to generate new identity."
			rr_remount ro
			recovery
		fi
	fi
	rr_remount ro
fi
#dropbear key is present
if [ ! -f "$DROPBEARARCH" ]; then
	#create archive
	rr_remount rw
	FAIL=0
	tar czf "${DROPBEARARCH}.tmp" "$DROPBEARKEYDSS" "$DROPBEARKEYRSA" 2>/dev/null || FAIL=1
	chmod 600 "${DROPBEARARCH}.tmp" || FAIL=1
	if [ $FAIL -eq 0 ]; then
		#atomic replace
		mv "${DROPBEARARCH}.tmp" "$DROPBEARARCH" || FAIL=1
	fi
	if [ $FAIL -ne 0 ]; then
		error "Failed to backup identity."
		#continue
	fi
	rr_remount ro
fi



#restore ssh root keys after firmware upgrade
if [ ! -f /root/.ssh/authorized_keys ]; then
	info "Recovering ssh keys..."
	if [ -f /mnt/rootfs/rrkeysshroot.tgz ]; then
		rr_remount rw
		tar -zxf /mnt/rootfs/rrkeysshroot.tgz -C /mnt/rootfs/
		if [ $? -ne 0 ]; then
			error "SSH keys archive corrupted. Cannot recover."
			log_rrconf "Error: Failed to recover ssh keys."
		fi
		rm /mnt/rootfs/rrkeysshroot.tgz
		rr_remount ro
	else
		error "SSH keys archive missing. Cannot recover."
		log_rrconf "Error: Failed to recover ssh keys."
	fi
fi

