#!/bin/sh
#Common functions and includes for /etc/init.d/S* scripts
#Parameters:
SCRIPT_NAME="$1" #name of caller

source /etc/rrconf/sysdefaults.cnf
source $RR_TOOLS_DIR/rrtools_misc.sh

info()
{
	echo "$SCRIPT_NAME: $*" >&2
}

error()
{
	echo "$SCRIPT_NAME: Error: $*" >&2
}

log_rrconf()
{
	logger -t RRCONF "$SCRIPT_NAME: $*"
}


save_bootlog()
{
	#save bootlog to FLASH before expected reboot
	cp /tmp/rrbootlog /mnt/rwmnt/.log/
	sync
}


reboot_cnf()
{

	info "Ordering reboot."
	save_bootlog
	reboot
}


