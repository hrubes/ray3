#!/bin/sh
#Start of logging system
#NOTE: syslog-ng is not running yet, logging from rr_remount is disabled

#common functions and includes
source /etc/init.d/init_fn.sh "S12logging.sh"



#check filesystem used for logging
#test file
TESTFILE=/mnt/rwmnt/.boot.congestion.watch
TESTSIZE=10240
#how many large files to remove in case of congestion
DELNUM=2
#length of watchdog supression during FLASH recovery
# 600s = 120 * 5s
WDOG_KEEP=120

check_logfs()
{
	#check logging FS
	local CL_RETV
	rm -f "$TESTFILE"
	dd if=/dev/urandom of="$TESTFILE" bs="$TESTSIZE" count=8 2>/dev/null
	CL_RETV=$?
	rm -f "$TESTFILE"
	return "$CL_RETV"
}


CHK_EVLOG_MESSAGE=""


#create logging directory
mkdir -p /mnt/rwmnt/.log
chmod 1777 /mnt/rwmnt/.log


#perform first log rotation (prevention in case of fast restarts)
rr_logrotate >/dev/null 2>&1

#start logging
msvc -u syslog
msvc -u logrotate
#msvc -u rotatehourly

#create link /dev/log to /tmp/log. This file is probably created by syslog-ng
ln -sf /tmp/log /dev/log

#backoff - allow syslog-ng to start
usleep 100000

#mark start in rr_remount log
logger -t "RR_REMOUNT" <<EOF
----------------------------
Start
----------------------------
EOF


#print delayed message to event log
if [ -n "$CHK_EVLOG_MESSAGE" ]; then
	rt_log2evlog "BOOT" "$CHK_EVLOG_MESSAGE"
fi
