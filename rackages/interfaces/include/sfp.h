#ifndef __SFP_H_
#define _SFP_H_


#define MAX_EEPROM_DATA 0xff

/* Offsets in decimal, for direct comparison with the SFF specs */

/* A0-based EEPROM offsets for DOM support checks */
#define SFF_A0_DOM                        92
#define SFF_A0_OPTIONS                    93
#define SFF_A0_COMP                       94

/* EEPROM bit values for various registers */
#define SFF_A0_DOM_EXTCAL                 (1 << 4)
#define SFF_A0_DOM_INTCAL                 (1 << 5)
#define SFF_A0_DOM_IMPL                   (1 << 6)
#define SFF_A0_DOM_PWRT                   (1 << 3)

#define SFF_A0_OPTIONS_AW                 (1 << 7)

/*
 * See ethtool.c comments about SFF-8472, this is the offset
 * at which the A2 page is in the EEPROM blob returned by the
 * kernel.
 */
#define SFF_A2_BASE                       0x100

/* A2-based offsets for DOM */
#define SFF_A2_TEMP                       96
#define SFF_A2_TEMP_HALRM                 0
#define SFF_A2_TEMP_LALRM                 2
#define SFF_A2_TEMP_HWARN                 4
#define SFF_A2_TEMP_LWARN                 6

#define SFF_A2_VCC                        98
#define SFF_A2_VCC_HALRM                  8
#define SFF_A2_VCC_LALRM                  10
#define SFF_A2_VCC_HWARN                  12
#define SFF_A2_VCC_LWARN                  14

#define SFF_A2_BIAS                       96
#define SFF_A2_BIAS_HALRM                 16
#define SFF_A2_BIAS_LALRM                 18
#define SFF_A2_BIAS_HWARN                 20
#define SFF_A2_BIAS_LWARN                 22

#define SFF_A2_TX_PWR                     102
#define SFF_A2_TX_PWR_HALRM               24
#define SFF_A2_TX_PWR_LALRM               26
#define SFF_A2_TX_PWR_HWARN               28
#define SFF_A2_TX_PWR_LWARN               30

#define SFF_A2_RX_PWR                     104
#define SFF_A2_RX_PWR_HALRM               32
#define SFF_A2_RX_PWR_LALRM               34
#define SFF_A2_RX_PWR_HWARN               36
#define SFF_A2_RX_PWR_LWARN               38

#define SFF_A2_ALRM_FLG                   112
#define SFF_A2_WARN_FLG                   116

/* 32-bit little-endian calibration constants */
#define SFF_A2_CAL_RXPWR4                 56
#define SFF_A2_CAL_RXPWR3                 60
#define SFF_A2_CAL_RXPWR2                 64
#define SFF_A2_CAL_RXPWR1                 68
#define SFF_A2_CAL_RXPWR0                 72

/* 16-bit little endian calibration constants */
#define SFF_A2_CAL_TXI_SLP                76
#define SFF_A2_CAL_TXI_OFF                78
#define SFF_A2_CAL_TXPWR_SLP              80
#define SFF_A2_CAL_TXPWR_OFF              82
#define SFF_A2_CAL_T_SLP                  84
#define SFF_A2_CAL_T_OFF                  86
#define SFF_A2_CAL_V_SLP                  88
#define SFF_A2_CAL_V_OFF                  90



//signal from GPIO
typedef enum {
	TX_ENABLE=0,
	TX_DISABLE
} SFP_TX_DISABLE_;

typedef enum {
	SIGNAL_NORMAL=0,
	SIGNAL_LOSS
} SFP_SIGNAL_LOSS_;

typedef enum {
	LASER_NORMAL=0,
	LASER_FAULT
} SFP_TX_FAULT_;

typedef enum {
	INSERTED=0,
	REMOVED
} SFP_PRESENT_;

typedef enum {
	VDD_ENABLE=0,
	VDD_DISABLE
} SFP_VDD_EN_;




//eeprom memory
typedef enum
{
	INVALID=0,
	VALID = 1
} SFP_EEPROM_STATUS;




struct STATUS_SFP_GPIO {
	SFP_PRESENT_ 		present;
	SFP_TX_FAULT_		fault;
	SFP_SIGNAL_LOSS_	loss;
	SFP_TX_DISABLE_		disable;
	SFP_VDD_EN_ 		vdd_en;
	
};


struct SFF8472DIAGS {

#define MCURR 0
#define LWARN 1
#define HWARN 2
#define LALRM 3
#define HALRM 4

	/* [5] tables are current, low/high warn, low/high alarm */
	__u8 supports_dom;      /* Supports DOM */
	__u8 supports_alarms;   /* Supports alarm/warning thold */
	__u8 calibrated_ext;    /* Is externally calibrated */
	__u16 bias_cur[5];      /* Measured bias current in 2uA units */
	__u16 tx_power[5];      /* Measured TX Power in 0.1uW units */
	__u16 rx_power[5];      /* Measured RX Power */
	__u8  rx_power_type;    /* 0 = OMA, 1 = Average power */
	__s16 sfp_temp[5];      /* SFP Temp in 16-bit signed 1/256 Celcius */
	__u16 sfp_voltage[5];   /* SFP voltage in 0.1mV units */

};

struct STATUS_SFP {
	STATUS_SFP_GPIO sfp_gpio;
	SFP_EEPROM_STATUS statusDataA0;
	SFP_EEPROM_STATUS statusDataA2;
	__u8 eepromData[2*MAX_EEPROM_DATA];
	SFF8472DIAGS diag8472;
};

int getSfpStatus (int sfp_num,STATUS_SFP * status);
int setSfpTxDisable (int sfp_num,SFP_TX_DISABLE_ disable);
int setSfpVdd (int sfp_num,SFP_VDD_EN_ enable);

#endif
