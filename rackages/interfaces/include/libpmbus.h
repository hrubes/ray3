#ifndef _LIB_PMBUS_H_
#define _LIB_PMBUSS_H
/*
 * GLOBAL PARAM
 */

#define I2C_DEV		"/dev/i2c-0"
#define MPBUS_ADDR	0x40

/*
 * COMMANDS
 */
#define PMBUS_PAGE						0x00
#define PMBUS_OPERATION					0x01
#define PMBUS_ON_OFF_CONFIG				0x02
#define PMBUS_CLEAR_FAULTS				0x03
#define PMBUS_PHASE						0x04

#define PMBUS_CAPABILITY				0x19
#define PMBUS_QUERY						0x1A

#define PMBUS_VOUT_MODE					0x20
#define PMBUS_VOUT_COMMAND				0x21
#define PMBUS_VOUT_TRIM					0x22
#define PMBUS_VOUT_CAL_OFFSET			0x23
#define PMBUS_VOUT_MAX					0x24
#define PMBUS_VOUT_MARGIN_HIGH			0x25
#define PMBUS_VOUT_MARGIN_LOW			0x26
#define PMBUS_VOUT_TRANSITION_RATE		0x27
#define PMBUS_VOUT_DROOP				0x28
#define PMBUS_VOUT_SCALE_LOOP			0x29
#define PMBUS_VOUT_SCALE_MONITOR		0x2A

#define PMBUS_COEFFICIENTS				0x30
#define PMBUS_POUT_MAX					0x31

#define PMBUS_FREQUENCY_SWITCH			0x33

#define	PMBUS_VIN_ON					0x35
#define	PMBUS_VIN_OFF					0x36

#define PMBUS_FAN_CONFIG_12				0x3A
#define PMBUS_FAN_COMMAND_1				0x3B
#define PMBUS_FAN_COMMAND_2				0x3C
#define PMBUS_FAN_CONFIG_34				0x3D
#define PMBUS_FAN_COMMAND_3				0x3E
#define PMBUS_FAN_COMMAND_4				0x3F

#define PMBUS_VOUT_OV_FAULT_LIMIT		0x40
#define PMBUS_VOUT_OV_FAULT_RESPONSE	0x41
#define PMBUS_VOUT_OV_WARN_LIMIT		0x42
#define PMBUS_VOUT_UV_WARN_LIMIT		0x43
#define PMBUS_VOUT_UV_FAULT_LIMIT		0x44
#define PMBUS_VOUT_UV_FAULT_RESPONSE	0x45
#define PMBUS_IOUT_OC_FAULT_LIMIT		0x46
#define PMBUS_IOUT_OC_FAULT_RESPONSE	0x47
#define PMBUS_IOUT_OC_LV_FAULT_LIMIT	0x48
#define PMBUS_IOUT_OC_LV_FAULT_RESPONSE	0x49
#define PMBUS_IOUT_OC_WARN_LIMIT		0x4A
#define PMBUS_IOUT_UC_FAULT_LIMIT		0x4B
#define PMBUS_IOUT_UC_FAULT_RESPONSE	0x4C

#define PMBUS_OT_FAULT_LIMIT			0x4F
#define PMBUS_OT_FAULT_RESPONSE			0x50
#define PMBUS_OT_WARN_LIMIT				0x51
#define PMBUS_UT_WARN_LIMIT				0x52
#define PMBUS_UT_FAULT_LIMIT			0x53
#define PMBUS_UT_FAULT_RESPONSE			0x54
#define PMBUS_VIN_OV_FAULT_LIMIT		0x55
#define PMBUS_VIN_OV_FAULT_RESPONSE		0x56
#define PMBUS_VIN_OV_WARN_LIMIT			0x57
#define PMBUS_VIN_UV_WARN_LIMIT			0x58
#define PMBUS_VIN_UV_FAULT_LIMIT		0x59

#define PMBUS_IIN_OC_FAULT_LIMIT		0x5B
#define PMBUS_IIN_OC_WARN_LIMIT			0x5D
#define PMBUS_POWER_GOOD_ON			0x5E
#define PMBUS_POWER_GOOD_OFF			0x5F
#define PMBUS_POUT_OP_FAULT_LIMIT		0x68
#define PMBUS_POUT_OP_WARN_LIMIT		0x6A
#define PMBUS_PIN_OP_WARN_LIMIT			0x6B

#define PMBUS_STATUS_BYTE				0x78
#define PMBUS_STATUS_WORD				0x79
#define PMBUS_STATUS_VOUT				0x7A
#define PMBUS_STATUS_IOUT				0x7B
#define PMBUS_STATUS_INPUT				0x7C
#define PMBUS_STATUS_TEMPERATURE		0x7D
#define PMBUS_STATUS_CML				0x7E
#define PMBUS_STATUS_OTHER				0x7F
#define PMBUS_STATUS_MFR_SPECIFIC		0x80
#define PMBUS_STATUS_FAN_12				0x81
#define PMBUS_STATUS_FAN_34				0x82

#define PMBUS_READ_VIN					0x88
#define PMBUS_READ_IIN					0x89
#define PMBUS_READ_VCAP					0x8A
#define PMBUS_READ_VOUT					0x8B
#define PMBUS_READ_IOUT					0x8C
#define PMBUS_READ_TEMPERATURE_1		0x8D
#define PMBUS_READ_TEMPERATURE_2		0x8E
#define PMBUS_READ_TEMPERATURE_3		0x8F
#define PMBUS_READ_FAN_SPEED_1			0x90
#define PMBUS_READ_FAN_SPEED_2			0x91
#define PMBUS_READ_FAN_SPEED_3			0x92
#define PMBUS_READ_FAN_SPEED_4			0x93
#define PMBUS_READ_DUTY_CYCLE			0x94
#define PMBUS_READ_FREQUENCY			0x95
#define PMBUS_READ_POUT					0x96
#define PMBUS_READ_PIN					0x97

#define PMBUS_REVISION					0x98
#define PMBUS_MFR_ID					0x99
#define PMBUS_MFR_MODEL					0x9A
#define PMBUS_MFR_REVISION				0x9B
#define PMBUS_MFR_LOCATION				0x9C
#define PMBUS_MFR_DATE					0x9D
#define PMBUS_MFR_SERIAL				0x9E


/*
 * STATUS_BYTE, STATUS_WORD (lower)
 */
#define PB_STATUS_NONE_ABOVE	(1<<0)
#define PB_STATUS_CML			(1<<1)
#define PB_STATUS_TEMPERATURE	(1<<2)
#define PB_STATUS_VIN_UV		(1<<3)
#define PB_STATUS_IOUT_OC		(1<<4)
#define PB_STATUS_VOUT_OV		(1<<5)
#define PB_STATUS_OFF			(1<<6)
#define PB_STATUS_BUSY			(1<<7)

/*
 * STATUS_WORD (upper)
 */
#define PB_STATUS_UNKNOWN		(1<<8)
#define PB_STATUS_OTHER			(1<<9)
#define PB_STATUS_FANS			(1<<10)
#define PB_STATUS_POWER_GOOD_N	(1<<11)
#define PB_STATUS_WORD_MFR		(1<<12)
#define PB_STATUS_INPUT			(1<<13)
#define PB_STATUS_IOUT_POUT		(1<<14)
#define PB_STATUS_VOUT			(1<<15)

/*
 * STATUS_IOUT
 */
#define PB_POUT_OP_WARNING		(1<<0)
#define PB_POUT_OP_FAULT		(1<<1)
#define PB_POWER_LIMITING		(1<<2)
#define PB_CURRENT_SHARE_FAULT	(1<<3)
#define PB_IOUT_UC_FAULT		(1<<4)
#define PB_IOUT_OC_WARNING		(1<<5)
#define PB_IOUT_OC_LV_FAULT		(1<<6)
#define PB_IOUT_OC_FAULT		(1<<7)

/*
 * STATUS_VOUT, STATUS_INPUT
 */
#define PB_VOLTAGE_UV_FAULT		(1<<4)
#define PB_VOLTAGE_UV_WARNING	(1<<5)
#define PB_VOLTAGE_OV_WARNING	(1<<6)
#define PB_VOLTAGE_OV_FAULT		(1<<7)

/*
 * STATUS_INPUT
 */
#define PB_PIN_OP_WARNING		(1<<0)
#define PB_IIN_OC_WARNING		(1<<1)
#define PB_IIN_OC_FAULT			(1<<2)

/*
 * STATUS_TEMPERATURE
 */
#define PB_TEMP_UT_FAULT		(1<<4)
#define PB_TEMP_UT_WARNING		(1<<5)
#define PB_TEMP_OT_WARNING		(1<<6)
#define PB_TEMP_OT_FAULT		(1<<7)


struct dev_status {
	__s8	validity;			// -1 invalid; 0 valid
	__u8	operation;
	__u8	on_off_config;
	__u16	vin;
	__u16	status_word;
	__u16	vout;
	__u16	iout;
	__u16	pout;
	__u16	temp;
};


int get_status (int address, dev_status * status);
int get_command_byte (int address, int command, __u8  * val);
int get_command_word (int address, int command, __u16  * val);
int set_command_byte (int address, int command, __u8  val);
int set_command_word (int address, int command, __u16  val);
int set_defalt (int address );
int enable_output (int address, int ena);
float convertL11 (__u16 value);
float convertL16 (__u16 value);
#endif
