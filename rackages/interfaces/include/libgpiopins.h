#ifndef _LIB_GPIOPINS_H_
#define _LIB_GPIOPINS_H

#include <stdbool.h>

typedef enum {
	GPIO_IN=0,
	GPIO_OUT
}GPIO_DIRECTION;


typedef enum {
	GPIO_L=0,		//log 0
	GPIO_H,			//log 1
	GPIO_NC			// nepouzito
}GPIO_VALUE;

#ifdef RR_PRODUCT_bma
typedef enum {
	SW_BUTTON,			//1- nestisknuto; 0 - stisknuto
	WDG_REFRESH,
	WDG_STATUS,
	PS_ALARM,
	RB_PWR_EN,
	I2C_EXP_INT,
	LED_SYS_GRN,		//1-nesviti ; 0-svitiset 
	LED_SYS_RED,		//1-nesviti ; 0-sviti
	LED_DIS,			// 1-LED AIR + SYS nesviti ; 0 -sviti
	USB_VDD_EN,	
	SWITCH_RESET,		//1- Reset (sekvence 1(100ms)0(20ms))
	BCM_RESET,
	TU_RESET,
	BCM_PS_EN,
	RB_MCU_RST,
	RB_MCU_ISP,
	LVM,
	LED_AIR_GRN,		//1-nesviti ; 0-sviti
	LED_AIR_RED,		//1-nesviti ; 0-sviti
	MUX_SEL0,
	MUX_SEL1,
	SWITCH_INT,
	TU_INT,
	BCM_PS_ALERT,
	USB_VDD_OC,	
	PS_PAIR0,
	BCM_SC0_AIRLOSS,
	BCM_SC0_ALARM1,
	BCM_SC0_ALARM2,
	BCM_SC1_AIRLOSS,
	BCM_SC1_ALARM1,
	BCM_SC1_ALARM2,
	SWITCH_P10_2500,
	EXP_SFP,
	TP36_OUT,
	TP37_OUT,
	TP40_OUT,
	TP44_IN,
	TP45_IN,
	TP46_IN,
	TP50_IN,
	TP51_IN,
	GPIO_PINS_END
} GPIO_PINS_ID;
#endif
#ifdef RR_PRODUCT_bk1
typedef enum {
	SW_BUTTON,			//1- nestisknuto; 0 - stisknuto
	WDG_STATUS,
	WDG_REFRESH,
	LED_SYS_GRN,		//1-nesviti ; 0-svitiset 
	LED_AIR_RED,		//1-nesviti ; 0-sviti
	LED_AIR_GRN,		//1-nesviti ; 0-sviti
	I2C_EXP_INT,
	PS_ALARM,
	LED_SYS_RED,		//1-nesviti ; 0-sviti
	TU_INT,
	PS_PAIR0,
	USB_VDD_OC,	
	SWITCH_INT,
	TP118_IN,
	TP119_IN,
	TP120_IN,
	TP121_IN,
	LED_DIS,			// 1-LED AIR + SYS nesviti ; 0 -sviti
	PHY_VSC_RESET,
	USB_VDD_EN,
	SWITCH_RESET,		//1- Reset (sekvence 1(100ms)0(20ms))
	BCM_RESET,
	TU_RESET,		
	TP116_OUT,
	TP117_OUT,
	GPIO_PINS_END
} GPIO_PINS_ID;
#endif
#ifdef RR_PRODUCT_bk2
typedef enum {
	SW_BUTTON,			//1- nestisknuto; 0 - stisknuto
	I2C_EXP_INT,
	SFP_INT,
	PS_ALARM,
	WDG_STATUS,
	WDG_REFRESH,
	LED_AIR_GRN,		//1-nesviti ; 0-sviti
	LED_SYS_GRN,		//1-nesviti ; 0-svitiset 
	LED_SYS_RED,		//1-nesviti ; 0-sviti
	TU_INT,
	PS_PAIR0,
	USB_VDD_OC,	
	SWITCH_INT,
	PHY_INT,
	TP68_IN,
	TP99_IN,
	TP100_IN,
	LED_DIS,	
	PHY_RESET,
	USB_VDD_EN,
	SWITCH_RESET,		//1- Reset (sekvence 1(100ms)0(20ms))
	TP63_OUT,
	TU_RESET,		
	SWITCH_P9_2500,
	SWITCH_P10_2500,
	GPIO_PINS_END
} GPIO_PINS_ID;
#endif
// pole se strukturama, obsahujici nazvy a cisla pinu
struct gpio_pins_ {
	GPIO_PINS_ID id;
	unsigned int pin;
	const char* name;
	GPIO_DIRECTION direction;
	GPIO_VALUE def_value;
};
extern const struct gpio_pins_ gpio_pins[];

// tohle se tyka pinu WDG_REFRESH
#define WDG_PULSE_DURATION 50000 // time in microseconds

// number of pulses to WDG_PIC to request powercycle, 
// timeout pro vyhodnoceni poctu pulsu od procesoru = 0.524s 
// viz mwm/mwm.c/RR_PWRCYCLE_TO
#define WDG_PULSES_TO_RESET 3 


//tato funkce je pouze pro puziti v preinitu a slouzi k vytvoreni /sys/class/gpio* adresaru
int gpioInit (); // nepouzivat !!!!
// finkce pro zapis a cteni GPIO pinu. Pri chybe vraci hodnotu < 0. OK =0
int gpioPinGet (GPIO_PINS_ID id , GPIO_VALUE* value);
int gpioPinSet (GPIO_PINS_ID id , GPIO_VALUE value);


// gpioPulse
// input arguments:
//    id - gpio id, see GPIO_PINS_ID
//    value - first written value (and finishes with 1-value)
//    t1 [us] - delay of value
//    t2=t1 [us] - delay of 1-value 
//    count=1 - number of pulses
//    t0=0 [us] - initial delay
// return values:
//    0 - all pulses written to gpio pin 
//   -1 - generating of pulses didn't finished and was interupted
// 	- you should check gpio pin value (and maybe time)
//   -EACCES - unable to open gpio pin for writing
//   -EINVAL - Wrong parameter
// implicitni hodnoty potrebuji c++
int gpioPulse (GPIO_PINS_ID id, GPIO_VALUE value, long t1, long t2=0, long count=1, long t0=0, bool last_t2_delay=false);
// najde id pinu dle nazvu . Vrati id , pri nalezeni jinak GPIO_PINS_END
GPIO_PINS_ID checkGpioPinsId (char *name);
#endif
