#ifndef __GPIOPINS_H
#define __GPIOPINS_H

int gpioCreate (unsigned int gpio, GPIO_DIRECTION direction, GPIO_VALUE def_value);
int gpioGetValue(unsigned int gpio, GPIO_VALUE *value);
int gpioSetValue(unsigned int gpio, GPIO_VALUE value);

int gpioSendPulse(unsigned int gpio, GPIO_VALUE value, long t1, long int t2=0, long count=1, long t0=0, bool last_t2_delay=false);

#endif
