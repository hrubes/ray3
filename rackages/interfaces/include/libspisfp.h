#ifndef __LIBSFPSPI_H__
#define __LIBSFPSPI_H__

#ifdef RR_PRODUCT_bma
	#define PIN_SFP_VDD_OC		0
	#define PIN_SFP_LOS			1
	#define PIN_SFP_PRESENT		2
	#define PIN_SFP_TX_FAULT	3
	#define PIN_SFP_VDD_EN		4
	#define PIN_SFP_TX_DISABLE	5
	#define EXP_RESET
#else
	#define PIN_SFP_TX_FAULT	0
	#define PIN_SFP_VDD_OC		1
	#define PIN_SFP_VDD_EN		2
	#define PIN_SFP_LOS			3
	#define PIN_SFP_PRESENT		4
	#define PIN_SFP_TX_DISABLE	5
#endif

int 	sfpInit (int sfp_num);
int 	sfpGetStatus (int sfp_num);
int 	sfpSetTxDisable (int sfp_num, SFP_TX_DISABLE_ val);
int 	sfpSetVddEn (int sfp_num, SFP_VDD_EN_ val);
int 	sfpGetEeprom( int sfp_num, uint8_t * table, uint8_t eepromAddr);
#ifdef CHECK_PRESENT
int 	sfpCheck (int sfp_num, uint8_t * old)
#endif

#endif /* __LIBSFPSPI_H__ */
