#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/types.h>

#include "sfp.h"
#include "libspisfp.h"

#ifdef RR_PRODUCT_bma
	#define SYSFS_GPIO_DIR 	"/sys/class/gpio"
	#define SFP_EXP_RESET 	501		//GPIO pin
	
static int gpioSetValue(unsigned int gpio, int value)
{
	int fd;
	char buf[64];

	if (value > 1)
		return -EINVAL;
	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);
	fd=open(buf, O_WRONLY);
	if (fd < 0) {
		fprintf(stderr,"Failed to set GPIO value: %s.\n",strerror (errno));
		return fd;
	}
	if (value == 1)
		write(fd, "1", 2);
	if  (value == 0)
		write(fd, "0", 2);
 
	close(fd);
	return 0;
}

static int resetExp (void)
{

	printf ("Perform HW reset\n");
	gpioSetValue(SFP_EXP_RESET, 1);
	usleep(20000);
	gpioSetValue(SFP_EXP_RESET, 0);
	usleep(20000);
	return 0;
}

#endif

#ifdef RR_PRODUCT_bk2
	#define SYSFS_GPIO_DIR 	"/sys/class/gpio"
	#define SFP_EXP_RESET 	508		//GPIO pin TP63
	
static int gpioSetValue(unsigned int gpio, int value)
{
	int fd;
	char buf[64];

	if (value > 1)
		return -EINVAL;
	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);
	fd=open(buf, O_WRONLY);
	if (fd < 0) {
		fprintf(stderr,"Failed to set GPIO value: %s.\n",strerror (errno));
		return fd;
	}
	if (value == 1)
		write(fd, "1", 2);
	if  (value == 0)
		write(fd, "0", 2);
 
	close(fd);
	return 0;
}

static int resetExp (void)
{

	printf ("Perform HW reset\n");
	gpioSetValue(SFP_EXP_RESET, 0);
	usleep(20000);
	gpioSetValue(SFP_EXP_RESET, 1);
	usleep(20000);
	return 0;
}

#endif

int main(int argc, char *argv[])
{
	int i, ret;
#ifdef RR_PRODUCT_bma
	#define NUMSFP 1
	int dev_num[NUMSFP] = {2};
#endif
#ifdef RR_PRODUCT_bk1
	#define NUMSFP 2
	int dev_num[NUMSFP] = {0,2};
#endif
#ifdef RR_PRODUCT_bk2
	#define NUMSFP 3
	int dev_num[NUMSFP] = {1,2,3};
#endif
	int dev_check[NUMSFP];
	int num = NUMSFP;
#if (defined RR_PRODUCT_bma) || (defined RR_PRODUCT_bk2)
	resetExp ();
#endif
	for (i=0; i < num; i++) {
		dev_check[i]=sfpGetStatus (dev_num[i]);
	}
	for (i=0; i < num; i++) {
		if (dev_check[i] >= 0) {
			printf ("Initialization SFP interface (dev:%d): ",dev_num[i]);
			if ((ret= sfpInit (dev_num[i])) < 0 ) {
				printf ("Error\n");
				return ret;
			} else {
				printf ("OK\n");
			}
		}
	}
	return 0;
}
