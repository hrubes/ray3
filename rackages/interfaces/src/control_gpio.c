#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>

#include "libgpiopins.h"

// 10 ms je dobry cas na vyvolani restartu pre WDG_REFRESH
const long int DEFAULT_DELAY=10000L;

enum MODE_E {
	MODE_NONE=0,
	MODE_HELP,
	MODE_GET,
	MODE_SET,
	MODE_INVALID
};

// parametry
GPIO_PINS_ID id = GPIO_PINS_END;
GPIO_VALUE val=GPIO_L;
long int pulse_count=-1;
long int pulse_t0=0;
long int pulse_t1=DEFAULT_DELAY;
long int pulse_t2=-1;
short int verbose=0;
bool debug=false;


void usage()
{
	fprintf(stdout,
		"control_gpio " RR_PRODUCT "-" RR_VERSION "\n" 
		"Examples: \n"
		"  control_gpio -g WDG_STATUS  # read a value\n"
		"  control_gpio --gpio=LED_SYS_GRN --set 0  # set a value\n"
		"  control_gpio -g LED_SYS_GRN -L -p 10 --t1=500000 -v -v  # 10 pulses, period 1 s\n"
		"\n"
		"Usage: control_gpio [options]\n"
		"Options are:\n"
		"  -v, --verbose\t\tIncrease verbosity level, you can use it two times\n"
		"  -d, --debug\t\tActivate debug mode with debug reports\n"
		"  \t\t\tUse it first to obtain full report of parsing parameters\n"
		"  -t, --table\t\tPrint table of gpio pins for product " RR_PRODUCT "\n"
		"  -h, --help\t\tShow this help\n"
		"\n"
		"Using -g activate READ mode\n"
		"  -g, --gpio=NAME\tDefine gpio pin to read/write (use -t to see table of valid names)\n"
		"\n"
		"Using -s, -L or -H activate WRITE mode\n"
		"  -s, --set=LEVEL\tDefine level (0/1) to be set on gpio pin (default 0)\n"
		"  \t\t\tIn PULSE mode is LEVEL used first and finish in (1-LEVEL)\n"
		"  -L\t\t\tSet low level, same as --set 0\n"
		"  -H\t\t\tSet hight level same as --set 1\n"
		"\n"
		"Using -p, --t0, --t1 or --t2 activate PULSE mode\n"
		"  -p, --pulse=VALUE\tDefine number of pulses (default 1)\n"
		"      --t0=VALUE\tInitial delay before start pulsing (default 0)\n"
		"      --t1=VALUE\tDuration of value specified with -s (default %ld)\n"
		"      --t2=VALUE\tDuration of (1-value) specified with -s (default same as t1)\n"
		"  \t\t\tAll duration values are in microseconds\n"
		"\n"
		"Mandatory arguments to long options are also mandatory for any corresponding\n"
		"short options.\n"
		"\n",
		DEFAULT_DELAY
		);

}


void table()
{
	int i;
	fprintf(stdout, "%4s\t%-15s\t%10s\n","pin","   name","direction");
	for (i=0; i<GPIO_PINS_END; ++i)
	{
		fprintf(stdout, "%4d\t%-15s\t%10s\n",
			gpio_pins[i].pin,
			gpio_pins[i].name,
			(gpio_pins[i].direction==GPIO_IN?"in   ":"out   ")
			);
	}
	fprintf(stdout, "\n");
}

MODE_E parse_params(int argc, char **argv)
{
	
	int opt;

	struct option longopts[] = {
		/* { name  has_arg  *flag  val } */
		{"t0",		required_argument, NULL, 0},
		{"t1",		required_argument, NULL, 0},
		{"t2",		required_argument, NULL, 0},
		{"gpio",	required_argument, NULL, 'g'},
		{"set",		required_argument, NULL, 's'},
		{"pulse",	required_argument, NULL, 'p'},
		{"verbose", 	no_argument, NULL, 'v'},
		{"debug", 	no_argument, NULL, 'd'},
		{"table", 	no_argument, NULL, 't'},
		{"help", 	no_argument, NULL, 'h'},
		{ 0, 0, NULL, 0 }
	};

	MODE_E mode=MODE_NONE;
	int option_index = 0;

	while (1) 
	{
		opt = getopt_long(argc, argv, "dg:s:HLp:vth", longopts, &option_index);
		if (opt==-1)
		{ // end of options
			break;
		}

		if ( debug || (opt=='d') )
		{
			if (opt) {
				fprintf(stdout, "option '%c' (%d)", opt, opt);
			} else {
				fprintf(stdout, "option \"%s\"", longopts[option_index].name);
			}
			if (optarg) {
				printf(" with arg \"%s\"", optarg);
			}
			fprintf(stdout, "\n");
		}

		switch (opt)
		{
			case 0:
				if ( (mode==MODE_NONE) || (mode==MODE_GET) )
				{
					mode=MODE_SET;
				}
				if (pulse_count<0)
				{
					pulse_count=1;
				}
				{
				long int t = atol(optarg);
				if ( (t<0) || ((t==0) && (option_index>0)) )
				{
					fprintf(stderr,"control_gpio Error: invalid duration \"%s\"\n",optarg);
					return(MODE_INVALID);
				}

				switch (option_index)
				{
					case 0:
						pulse_t0=t;
						break;
					case 1:
						pulse_t1=t;
						break;
					case 2:
						pulse_t2=t;
						break;
				}
				}
				break;
			case 'd':
				debug=true;
				break;
			case 'g':
				if (mode==MODE_NONE)
				{
					mode=MODE_GET;
				}
				if ( (id=checkGpioPinsId(optarg)) == GPIO_PINS_END)  
				{
					fprintf(stderr,"control_gpio Error: invalid gpio id \"%s\"\n",optarg);
					return(MODE_INVALID);
				}
				break;
			case 's':
				if ( (mode==MODE_NONE) || (mode==MODE_GET) )
				{
					mode=MODE_SET;
				}
				{
					int pom = atoi(optarg);
					if ((pom == GPIO_L ) || (pom == GPIO_H))
					{
						val=(GPIO_VALUE)pom;
					}
				}
				break;
			case 'L':
			case 'H':
				if ( (mode==MODE_NONE) || (mode==MODE_GET) )
				{
					mode=MODE_SET;
				}
				val = (opt=='L'?GPIO_L:GPIO_H);
				break;
			case 'p':
				if ( (mode==MODE_NONE) || (mode==MODE_GET) )
				{
					mode=MODE_SET;
				}
				pulse_count = atol(optarg);
				if (pulse_count==0)
				{
					fprintf(stderr,"control_gpio Error: invalid number of pulses \"%s\"\n",optarg);
					return(MODE_INVALID);
				}
				break;
			case 'v':
				++verbose;
				break;
			case 'h':
				usage();
				return MODE_HELP;
				break;
			case 't':
				table();
				return MODE_HELP;
				break;
			case '?':
				return(MODE_INVALID);
			default:
				fprintf(stderr, "control_gpio Error: unknown parameter: \"%c\", %d\n", opt, opt);
				return(MODE_INVALID);
		}
	}


	if (pulse_t2 < 0)
	{ // pokud neni zadan t2, tak se nastavi shodny s t1
		pulse_t2=pulse_t1;
	}
	if (mode==MODE_NONE) 
	{
		usage();
		mode=MODE_INVALID;
	} 
	else if ( ( (mode==MODE_GET) || (mode==MODE_SET) ) && (id == GPIO_PINS_END) )
	{
		fprintf(stderr,"control_gpio Error: missing gpio id \"%d\" (parameter -g)\n",id);
		mode=MODE_INVALID;
	} 
	return mode;
}


int main(int argc, char *argv[])
{
	int retval=0;
	int mode = 0;
	
	mode = parse_params(argc,argv);
	if (debug)
	{
		const char* modes[] = {"none", "help", "get", "set", "invalid", NULL};	
		fprintf(stdout, "----------------\n");
		fprintf(stdout, "mode: %s\n",modes[mode]);
		fprintf(stdout, "pin id: %d\n",id);
		fprintf(stdout, "value: %d\n",val);
		fprintf(stdout, "pulses: %ld\n",pulse_count);
		fprintf(stdout, "t0: %ld, t1: %ld, t2: %ld\n",pulse_t0, pulse_t1, pulse_t2);
		fprintf(stdout, "verbose: %d\n",verbose);
		fprintf(stdout, "----------------\n");
	}
	switch (mode)
	{
		case MODE_INVALID:
			retval=42;
			break;
		case MODE_HELP:
			break;
		case MODE_GET:
			if ( gpioPinGet(id,&val) < 0 ) 
			{
				fprintf(stderr,"control_gpio Error: get %s\n",gpio_pins[id].name);
				retval=33;
				break;
			}
			if (verbose>0)
			{
				fprintf(stdout,"get GPIO pins %s: value: %d\n",gpio_pins[id].name, val);
			}
			else
			{
				fprintf(stdout,"%d\n",val);
			}
			break;
		case MODE_SET:
			if (pulse_count > 0)
			{
				if ( gpioPulse(id, val, pulse_t1, pulse_t2, pulse_count, pulse_t0) < 0) 
				{
					fprintf(stderr,"control_gpio Error: pulse %s\n",gpio_pins[id].name);
					retval=33;
					break;
				}
				if (verbose>0)
				{
					fprintf(stdout,"pulse GPIO pins %s: final value: %d\n",gpio_pins[id].name, 1-val);
					fprintf(stdout,"t0 = %ld us, t1 = %ld us, t2 = %ld us, total pulses n = %ld\n",pulse_t0, pulse_t1, pulse_t2, pulse_count);
				}
				if (verbose>1)
				{
			fprintf(stdout," t0     t1  t2     t1\n");
					if (val==GPIO_L)
					{
			fprintf(stdout,"    ||.    ____.||    _\n");
			fprintf(stdout,"====||'____    '||____\n");
					}
					else
					{
			fprintf(stdout,"    ||.____    .||____\n");
			fprintf(stdout,"====||'    ____'||    _\n");
					}
			fprintf(stdout,"       %ld-times\n",pulse_count-1);
				}
			}
			else
			{
				if ( gpioPinSet(id,val) < 0 ) 
				{
					fprintf(stderr,"control_gpio Error: set %s\n",gpio_pins[id].name);
					retval=33;
					break;
				}
				if (verbose>0)
				{
					fprintf(stdout,"set GPIO pins %s: value: %d\n",gpio_pins[id].name, val);
				}
			}

			break;
		default:
			retval=42;
	}

	if (debug)
	{
		fprintf(stdout, "Returned value: %d\n", retval);
	}
	return(retval);
}

