/*
*  
*/
#include <unistd.h> 
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h> 
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <linux/types.h>
#include <arpa/inet.h>

#include "sfp.h"
#include "libspisfp.h"



/* Most common case: 16-bit unsigned integer in a certain unit */
#define A2_OFFSET_TO_U16(offset) \
	(eepromData[SFF_A2_BASE + (offset)] << 8 | eepromData[SFF_A2_BASE + (offset) + 1])

/* Calibration slope is a number between 0.0 included and 256.0 excluded. */
#define A2_OFFSET_TO_SLP(offset) \
	(eepromData[SFF_A2_BASE + (offset)] + eepromData[SFF_A2_BASE + (offset) + 1] / 256.)

/* Calibration offset is an integer from -32768 to 32767 */
#define A2_OFFSET_TO_OFF(offset) \
	((__s16)A2_OFFSET_TO_U16(offset))

/* RXPWR(x) are IEEE-754 floating point numbers in big-endian format */
#define A2_OFFSET_TO_RXPWRx(offset) \
	(befloattoh((__u32 *)((intptr_t)eepromData + SFF_A2_BASE + (offset))))

/*
 * 2-byte internal temperature conversions:
 * First byte is a signed 8-bit integer, which is the temp decimal part
 * Second byte are 1/256th of degree, which are added to the dec part.
 */
#define A2_OFFSET_TO_TEMP(offset) ((__s16)A2_OFFSET_TO_U16(offset))

#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))

//static int getSffDiagnostic (STATUS_SFP * status);
//static int getSfpGpio (STATUS_SFP_GPIO  *status);
static void sff8472ParseEeprom(const __u8 *eepromData, struct SFF8472DIAGS *diag);
static void sff8472Calibration(const __u8 *eepromData, struct SFF8472DIAGS *diag);
static void sff8472DomParse(const __u8 *eepromData, struct SFF8472DIAGS *diag);
static float befloattoh(const __u32 *source);


static int getSfpGpio (int sfp_num,STATUS_SFP_GPIO  *status)
{
	int ret;
	
	
	if ((ret=sfpGetStatus (sfp_num)) < 0)
		return ret;
	if ( CHECK_BIT(ret,PIN_SFP_PRESENT))
		status->present=REMOVED;
	else
		status->present=INSERTED;
	if ( CHECK_BIT(ret,PIN_SFP_TX_FAULT))
		status->fault=LASER_FAULT;
	else
		status->fault=LASER_NORMAL;
	if ( CHECK_BIT(ret,PIN_SFP_LOS))
		status->loss=SIGNAL_LOSS;
	else
		status->loss=SIGNAL_NORMAL;
	if ( CHECK_BIT(ret,PIN_SFP_TX_DISABLE))
		status->disable=TX_DISABLE;
	else
		status->disable=TX_ENABLE;
	if ( CHECK_BIT(ret,PIN_SFP_VDD_EN))
		status->vdd_en=VDD_DISABLE;
	else
		status->vdd_en=VDD_ENABLE;

	return ret;
}

static int getSffDiagnostic (int sfp_num,STATUS_SFP * status)
{
	memset (status->eepromData,0xff,2*MAX_EEPROM_DATA);
	if (status->sfp_gpio.present==REMOVED){
		status->statusDataA0=INVALID;
		status->statusDataA2=INVALID;
		return 0;
	}
	if (sfpGetEeprom (sfp_num,status->eepromData,0xa0) < 0) {
		status->statusDataA0=INVALID;
		return 0;
	}
	status->statusDataA0=VALID;
	if (sfpGetEeprom (sfp_num,0x100+status->eepromData,0xa2) < 0) {
		status->statusDataA2=INVALID;
		return 0;
	}
	status->statusDataA2=VALID;
	sff8472ParseEeprom(status->eepromData, &status->diag8472);
	return 0;
}

int getSfpStatus (int sfp_num,STATUS_SFP * status)
{
	int ret;
	if ((ret=getSfpGpio ( sfp_num,&status->sfp_gpio)) < 0) {
		return ret;
	}
	getSffDiagnostic (sfp_num,status);
	return 0;
}


static void sff8472DomParse(const __u8 *eepromData, struct SFF8472DIAGS *diag)
{
	diag->bias_cur[MCURR] = A2_OFFSET_TO_U16(SFF_A2_BIAS);
	diag->bias_cur[HALRM] = A2_OFFSET_TO_U16(SFF_A2_BIAS_HALRM);
	diag->bias_cur[LALRM] = A2_OFFSET_TO_U16(SFF_A2_BIAS_LALRM);
	diag->bias_cur[HWARN] = A2_OFFSET_TO_U16(SFF_A2_BIAS_HWARN);
	diag->bias_cur[LWARN] = A2_OFFSET_TO_U16(SFF_A2_BIAS_LWARN);

	diag->sfp_voltage[MCURR] = A2_OFFSET_TO_U16(SFF_A2_VCC);
	diag->sfp_voltage[HALRM] = A2_OFFSET_TO_U16(SFF_A2_VCC_HALRM);
	diag->sfp_voltage[LALRM] = A2_OFFSET_TO_U16(SFF_A2_VCC_LALRM);
	diag->sfp_voltage[HWARN] = A2_OFFSET_TO_U16(SFF_A2_VCC_HWARN);
	diag->sfp_voltage[LWARN] = A2_OFFSET_TO_U16(SFF_A2_VCC_LWARN);

	diag->tx_power[MCURR] = A2_OFFSET_TO_U16(SFF_A2_TX_PWR);
	diag->tx_power[HALRM] = A2_OFFSET_TO_U16(SFF_A2_TX_PWR_HALRM);
	diag->tx_power[LALRM] = A2_OFFSET_TO_U16(SFF_A2_TX_PWR_LALRM);
	diag->tx_power[HWARN] = A2_OFFSET_TO_U16(SFF_A2_TX_PWR_HWARN);
	diag->tx_power[LWARN] = A2_OFFSET_TO_U16(SFF_A2_TX_PWR_LWARN);

	diag->rx_power[MCURR] = A2_OFFSET_TO_U16(SFF_A2_RX_PWR);
	diag->rx_power[HALRM] = A2_OFFSET_TO_U16(SFF_A2_RX_PWR_HALRM);
	diag->rx_power[LALRM] = A2_OFFSET_TO_U16(SFF_A2_RX_PWR_LALRM);
	diag->rx_power[HWARN] = A2_OFFSET_TO_U16(SFF_A2_RX_PWR_HWARN);
	diag->rx_power[LWARN] = A2_OFFSET_TO_U16(SFF_A2_RX_PWR_LWARN);

	diag->sfp_temp[MCURR] = A2_OFFSET_TO_TEMP(SFF_A2_TEMP);
	diag->sfp_temp[HALRM] = A2_OFFSET_TO_TEMP(SFF_A2_TEMP_HALRM);
	diag->sfp_temp[LALRM] = A2_OFFSET_TO_TEMP(SFF_A2_TEMP_LALRM);
	diag->sfp_temp[HWARN] = A2_OFFSET_TO_TEMP(SFF_A2_TEMP_HWARN);
	diag->sfp_temp[LWARN] = A2_OFFSET_TO_TEMP(SFF_A2_TEMP_LWARN);
}

static void sff8472Calibration(const __u8 *eepromData, struct SFF8472DIAGS *diag)
{
	unsigned int i;
	__u16 rx_reading;
	/* Calibration should occur for all values (threshold and current) */
	for (i = 0; i < sizeof((unsigned int)diag->bias_cur); ++i) {
		/*
		 * Apply calibration formula 1 (Temp., Voltage, Bias, Tx Power)
		 */
		diag->bias_cur[i]    *= A2_OFFSET_TO_SLP(SFF_A2_CAL_TXI_SLP);
		diag->tx_power[i]    *= A2_OFFSET_TO_SLP(SFF_A2_CAL_TXPWR_SLP);
		diag->sfp_voltage[i] *= A2_OFFSET_TO_SLP(SFF_A2_CAL_V_SLP);
		diag->sfp_temp[i]    *= A2_OFFSET_TO_SLP(SFF_A2_CAL_T_SLP);

		diag->bias_cur[i]    += A2_OFFSET_TO_OFF(SFF_A2_CAL_TXI_OFF);
		diag->tx_power[i]    += A2_OFFSET_TO_OFF(SFF_A2_CAL_TXPWR_OFF);
		diag->sfp_voltage[i] += A2_OFFSET_TO_OFF(SFF_A2_CAL_V_OFF);
		diag->sfp_temp[i]    += A2_OFFSET_TO_OFF(SFF_A2_CAL_T_OFF);

		/*
		 * Apply calibration formula 2 (Rx Power only)
		 */
		rx_reading = diag->rx_power[i];
		diag->rx_power[i]    = A2_OFFSET_TO_RXPWRx(SFF_A2_CAL_RXPWR0);
		diag->rx_power[i]    += rx_reading * A2_OFFSET_TO_RXPWRx(SFF_A2_CAL_RXPWR1);
		diag->rx_power[i]    += rx_reading * A2_OFFSET_TO_RXPWRx(SFF_A2_CAL_RXPWR2);
		diag->rx_power[i]    += rx_reading * A2_OFFSET_TO_RXPWRx(SFF_A2_CAL_RXPWR3);
	}
}
static void sff8472ParseEeprom(const __u8 *eepromData, struct SFF8472DIAGS *diag)
{
	diag->supports_dom = eepromData[SFF_A0_DOM] & SFF_A0_DOM_IMPL;
	diag->supports_alarms = eepromData[SFF_A0_OPTIONS] & SFF_A0_OPTIONS_AW;
	diag->calibrated_ext = eepromData[SFF_A0_DOM] & SFF_A0_DOM_EXTCAL;
	diag->rx_power_type = eepromData[SFF_A0_DOM] & SFF_A0_DOM_PWRT;

	sff8472DomParse(eepromData, diag);

	/*
	 * If the SFP is externally calibrated, we need to read calibration data
	 * and compensate the already stored readings.
	 */
	if (diag->calibrated_ext)
		sff8472Calibration(eepromData, diag);
}

/* Converts to a float from a big-endian 4-byte source buffer. */
static float befloattoh(const __u32 *source)
{
	union {
		__u32 src;
		float dst;
	} converter;

	converter.src = ntohl(*source);
	return converter.dst;
}


int setSfpTxDisable (int sfp_num,SFP_TX_DISABLE_ disable)
{
	if ( disable == TX_DISABLE ) {
		return sfpSetTxDisable (sfp_num,TX_DISABLE);
	}
	
	if ( disable == TX_ENABLE ) {
		return sfpSetTxDisable (sfp_num,TX_ENABLE);
	}
	return -EINVAL;
}

int setSfpVdd (int sfp_num,SFP_VDD_EN_ enable)
{
	if ( enable == VDD_DISABLE ) {
		return sfpSetVddEn (sfp_num,VDD_DISABLE);
	}
	
	if ( enable == VDD_ENABLE ) {
		return sfpSetVddEn (sfp_num,VDD_ENABLE);
	}
	return -EINVAL;
}
