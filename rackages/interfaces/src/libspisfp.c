/***************************************************************************
    knihovna pro komunikaci s SFP prostrednictvim SPI zbernice
        
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

//#define DEBUG

#include "sfp.h"
#include "libspisfp.h"

#define SPI_SPEED			600000						
#define I2C_TIMEOUT			50
#define I2C_SPEED			19
#define I2C_EEPROM_STEP		64
#define I2C_ADDR			0x77

/* Expander SC18IS600 */
/*  Register offsets  */
#define SC18IS600_IOCONFIG		0x00
#define SC18IS600_IOSTATE		0x01
#define SC18IS600_I2CCLOCK		0x02
#define SC18IS600_I2CTO			0x03
#define SC18IS600_I2CSTAT		0x04
#define SC18IS600_I2CADR		0x05

/* Pin configuration */
#define SC18IS600_PIN_BIDIR		0x00
#define SC18IS600_PIN_INPUT		0x01
#define SC18IS600_PIN_PUSHPULL	0x02
#define SC18IS600_PIN_OPENDRAIN	0x03
#ifdef RR_PRODUCT_bma
	#define SC18IS600_PIN_DEF	0x55 //GPIO 0,1,2,3 IN
#else
	//#define SC18IS600_PIN_DEF	0x65 //GPIO 2 push pull
	#define SC18IS600_PIN_DEF	0x75 //GPIO 2 open drain
#endif
/* I2C Status */
#define SC18IS600_STAT_OK		0xF0 // Transmission successfull
#define SC18IS600_STAT_ANACK	0xF1 // Device address not acknowledged
#define SC18IS600_STAT_DNACK	0xF2
#define SC18IS600_STAT_BUSY		0xF3 // I2C-bus busy
#define SC18IS600_STAT_TO		0xF4 // I2C-bus time-out
#define SC18IS600_STAT_INVCNT	0xF5 // I2C-bus invalid data count

/* Commands */
#define SC18IS600_CMD_WRBLK		0x00 // Write N bytes to I2C-bus slave device
#define SC18IS600_CMD_RDBLK		0x01 // Read N bytes from I2C-bus slave device
#define SC18IS600_CMD_RDAWR		0x02 // I2C-bus read after write
#define SC18IS600_CMD_WRAWR		0x03 // I2C-bus write after write
#define SC18IS600_CMD_RDBUF		0x06 // Read buffer
#define SC18IS600_CMD_SPICON	0x18 // SPI configuration
#define SC18IS600_CMD_WRREG		0x20 // Write to internal register
#define SC18IS600_CMD_RDREG		0x21 // Read from internal register
#define SC18IS600_CMD_PWRDWN	0x30 // Power-down mode


char dev_name[32]={""};

struct spi_ioc_transfer xfer[2];
 
 
/********************************************************************

interni funkce pro knihovnu 
 
********************************************************************/
static int spiOpen (int sfp_num)
{
	int file;

#ifdef RR_PRODUCT_bk2
	if  ((sfp_num > 0) && (sfp_num < 4)) {
#else
	if  ((sfp_num == 0) || (sfp_num == 2)) {
#endif
		sprintf (dev_name,"/dev/spidev0.%d",sfp_num);
		if ((file = open(dev_name,O_RDWR)) < 0)
			printf("Failed to open the SPI bus (\"%s\").\n",dev_name);
		return file;
	}
	printf("Failed to open the SPI bus  (no existing devices).\n");
	return -ENODEV;
}

static int spiClose (int file)
{
	return close (file);
}

#ifdef DEBUG
static int spiInitGet(int file)
{
	int ret;
	__u8    mode, lsb, bits;
	__u32 speed;
	
	if ((ret=ioctl(file, SPI_IOC_RD_MODE, &mode)) < 0) {
		perror("SPI rd_mode");
		return ret;
	}
	
	if ((ret=ioctl(file, SPI_IOC_RD_LSB_FIRST, &lsb)) < 0) {
		perror("SPI rd_lsb_fist");
		return ret;
	}
	
	if ((ret=ioctl(file, SPI_IOC_RD_BITS_PER_WORD, &bits)) < 0) {
		perror("SPI bits_per_word");
		return ret;
	}
	
	if ((ret=ioctl(file, SPI_IOC_RD_MAX_SPEED_HZ, &speed)) < 0) {
		perror("SPI max_speed_hz");
		return ret;
	}

	printf("%s:  spi mode %d, %d bits %sper word, %d Hz max\n", dev_name, mode, bits, lsb ? "(lsb first) " : "", speed);
	return 0;
	    
}
#endif

static int spiInitSet(int file)
{
	int ret;
	__u8    mode, lsb, bits;
	__u32 speed=SPI_SPEED;
 

	mode = SPI_MODE_3;
	
	if ((ret=ioctl(file, SPI_IOC_WR_MODE, &mode)) <0 )   {
		perror("can't set spi mode");
		return ret;
	}
	lsb = 0; 
	if ((ret=ioctl(file, SPI_IOC_WR_LSB_FIRST, &lsb)) < 0 ) {
		perror("SPI rd_lsb_fist");
		return ret;
	}
	bits=8;
	if ((ret=ioctl(file, SPI_IOC_WR_BITS_PER_WORD, &bits)) <0 )  {
		perror("can't set bits per word");
		return ret;
	}
           
	if ((ret=ioctl(file, SPI_IOC_WR_MAX_SPEED_HZ, &speed)) <0 )  {
		perror("can't set max speed hz");
		return ret;
	}
#ifdef DEBUG
	printf("%s: spi mode %d, %d bits %sper word, %d Hz max\n",dev_name, mode, bits, lsb ? "(lsb first) " : "", speed);
#endif
	memset(xfer, 0, sizeof xfer);
	xfer[0].tx_buf= NULL;
	xfer[0].len = 3; 
	xfer[0].cs_change = 0; 
	xfer[0].delay_usecs = 1000;
	xfer[0].speed_hz = speed;
	xfer[0].bits_per_word = bits;
 
	xfer[1].tx_buf=NULL;
	xfer[1].len = 1;
	xfer[1].cs_change = 0;
	xfer[1].delay_usecs = 1000;
	xfer[1].speed_hz = speed;
	xfer[1].bits_per_word = bits;
 
	return 0;
}
 
 
 
static int spiSetInternalReg (int file, uint8_t value, uint8_t reg)
{
	int ret;
	static uint8_t bufTx[10];
	static uint8_t bufRx[10];
	
	memset(bufTx, 0, sizeof bufTx);
	memset(bufRx, 0, sizeof bufRx);
	bufTx[0] = SC18IS600_CMD_WRREG;
	bufTx[1] = reg;
	bufTx[2] = value;
	xfer[0].tx_buf = (unsigned long)bufTx;
	xfer[0].len = 3;
	xfer[1].rx_buf = (unsigned long) bufRx;
	xfer[1].len = 0;
	if ((ret = ioctl(file, SPI_IOC_MESSAGE(1), xfer)) < 0 ) {
		perror("SPI_IOC_MESSAGE");
		return ret;
	}
#ifdef DEBUG
	printf ("set (tx): CMD:0x%02x reg:0x%02x val:0x%02x \n",bufTx[0],bufTx[1],bufTx[2]);
#endif
	return 0;
} 
 
static int spiGetInternalReg (int file, uint8_t *value, uint8_t reg)
{
	int ret;
	static uint8_t bufTx[10];
	static uint8_t bufRx[10];
	
	memset(bufTx, 0, sizeof bufTx);
	memset(bufRx, 0, sizeof bufRx);
	bufTx[0] = SC18IS600_CMD_RDREG;
	bufTx[1] = reg;
	xfer[0].tx_buf = (unsigned long)bufTx;
	xfer[0].len = 2;
	xfer[1].rx_buf = (unsigned long) bufRx;
	xfer[1].len = 1;
	if ((ret = ioctl(file, SPI_IOC_MESSAGE(2), xfer)) < 0 ) {
		perror("SPI_IOC_MESSAGE");
		return ret;
	}
#ifdef DEBUG
	printf ("get (tx): CMD:0x%02x reg:0x%02x\n",bufTx[0],bufTx[1]);
	printf ("get (rx): val:0x%02x\n",bufRx[0]);
#endif
	*value =bufRx[0]; 
	return 0;
} 

static int spiCheckI2cBusyN (int file)
{
	uint8_t i2cStatus;
	int timeout;
	int ret;
	
	for (timeout=0;; timeout ++ ) {
		if ((ret =spiGetInternalReg (file, &i2cStatus, SC18IS600_I2CSTAT)) < 0)
			return ret;
		if (i2cStatus != SC18IS600_STAT_BUSY)
			break;
		if (timeout == I2C_TIMEOUT)
			return -ENODEV;
	}
	return 0;
}

static int spiCheckI2cOk (int file)
{
	uint8_t i2cStatus;
	int timeout;
	int ret; 
	
	for (timeout=0;; timeout ++ ) {
		if ((ret =spiGetInternalReg (file, &i2cStatus, SC18IS600_I2CSTAT)) < 0)
			return ret;
		if (i2cStatus == SC18IS600_STAT_OK)
			break;
		if (i2cStatus != SC18IS600_STAT_BUSY)
			return -ENODEV;
		if (timeout == I2C_TIMEOUT)
			return -ENODEV;
		usleep (1000);
	}
	return 0;
}


static int spiSetI2cEepromOffset (int file, uint8_t offset, uint8_t addr)
{
	static uint8_t bufTx[5];
	static uint8_t bufRx[5];
	int ret;
	
	memset(bufTx, 0, sizeof bufTx);
	memset(bufRx, 0, sizeof bufRx);
	bufTx[0] = SC18IS600_CMD_WRBLK;
	bufTx[1] = 1; 
	bufTx[2] = addr; 
	bufTx[3] = offset; 
	xfer[0].tx_buf = (unsigned long)bufTx;
	xfer[0].len = 4;
	xfer[1].rx_buf = (unsigned long) bufRx;
	xfer[1].len = 0;
	
	if ((ret = ioctl(file, SPI_IOC_MESSAGE(1), xfer)) < 0 ) {
		perror("SPI_IOC_MESSAGE");
		return ret;
	}
#ifdef DEBUG
	printf ("set i2c data offset 0x%x \n",offset);
	printf ("get after set (tx): 0x%02x 0x%02x 0x%02x 0x%02x\n",bufTx[0],bufTx[1],bufTx[2],bufTx[3]);
#endif
	return spiCheckI2cOk (file);



}  


static int spiGetI2cEepromBlock (int file, uint8_t * data, uint8_t len, uint8_t addr)
{
	static uint8_t bufTx[100];
	static uint8_t bufRx[100];
	int ret;
	
	if (len > 96 ) 
		return -EINVAL;
		
	memset(bufTx, 0, sizeof bufTx);
	memset(bufRx, 0, sizeof bufRx);
	bufTx[0] = SC18IS600_CMD_RDBLK;
	bufTx[1] = len+1;
	bufTx[2] = addr;
	xfer[0].tx_buf = (unsigned long)bufTx;
	xfer[0].len = 3;
	xfer[1].rx_buf = (unsigned long) bufRx;
	xfer[1].len = 0;
	
	if ((ret = ioctl(file, SPI_IOC_MESSAGE(1), xfer)) < 0 ) {
		perror("SPI_IOC_MESSAGE");
		return ret;
	}
#ifdef DEBUG
	printf ("get i2c data from addr 0x%x len:0x%x\n",addr,len);
	printf ("get after set (tx): 0x%02x 0x%02x 0x%02x\n",bufTx[0],bufTx[1],bufTx[2]);
#endif
	if ((ret=spiCheckI2cOk (file)) < 0)
		return ret;
	memset(bufTx, 0, sizeof bufTx);
	memset(bufRx, 0, sizeof bufRx);
	bufTx[0] = SC18IS600_CMD_RDBUF;
	xfer[0].tx_buf = (unsigned long)bufTx;
	xfer[0].len = 1;
	xfer[1].rx_buf = (unsigned long) bufRx;
	xfer[1].len = len+1;
	if ((ret = ioctl(file, SPI_IOC_MESSAGE(2), xfer)) < 0 ) {
		perror("SPI_IOC_MESSAGE");
		return ret;
	}
#ifdef DEBUG
	printf ("read buff (tx): 0x%02x 0x%02x\n",bufTx[0],bufTx[1]);
	printf ("read buff (rx): [1]:0x%02x [2];0x%02x [3]:0x%02x [4]:0x%02x ....[%d]:0x%02x [%d]:0x%02x [%d]:0x%02x \n",bufRx[0],bufRx[1],bufRx[2],bufRx[3],len-2,bufRx[len-2],len-1,bufRx[len-1],len,bufRx[len]);
#endif
	memcpy (data, &bufRx[1], len);
	return 0;
}  


static int spiGetI2cEeprom (int file, uint8_t * data, uint8_t addr)
{
	int ret;
	int i;
	uint8_t * ptr;
	uint8_t offset;
	int num=256/I2C_EEPROM_STEP;
	
	if ((ret=spiCheckI2cBusyN (file)) < 0)
		return ret;
	

	for ( i=0; i< num; i++) {
		ptr=data + i*I2C_EEPROM_STEP;
		if (i == 0)
			offset = 0xff;
		else
			offset = i*I2C_EEPROM_STEP -1;
			
		if ((ret=spiSetI2cEepromOffset (file, offset , addr)) < 0)
			return ret;
		if ((ret=spiGetI2cEepromBlock (file, ptr, I2C_EEPROM_STEP, addr)) < 0 )
			return ret;
	}
	
	return 0;
}  

static int _sfpSetTxDisable (int file, SFP_TX_DISABLE_ val)
{
	int ret;
	uint8_t value;
#ifdef DEBUG
	printf("sfpSetTx\n");
#endif
	if ((ret=spiGetInternalReg (file, &value, SC18IS600_IOSTATE)) < 0 )
		return ret;
	//printf("getvalue:0x%x\n",value);
	if (val == TX_DISABLE)
		value=value & ~(1<<PIN_SFP_TX_DISABLE);
	else
		value=value | 1<<PIN_SFP_TX_DISABLE;
#ifdef RR_PRODUCT_bk2
	value=value | 1<<PIN_SFP_PRESENT;
#endif
	//printf("setvalue:0x%x\n",value);
	return spiSetInternalReg (file, value, SC18IS600_IOSTATE);
} 

static int _sfpSetVdd (int file, SFP_VDD_EN_ val)
{
	int ret;
	uint8_t value;
#ifdef DEBUG
	printf("sfpSetVdd\n");
#endif
	if ((ret=spiGetInternalReg (file, &value, SC18IS600_IOSTATE)) < 0 ) {
		spiClose (file);
		return ret;
	}
	if (val == VDD_ENABLE)
		value=value & ~(1<<PIN_SFP_VDD_EN);
	else
		value=value | 1<<PIN_SFP_VDD_EN;
#ifdef RR_PRODUCT_bk2
	value=value | 1<<PIN_SFP_PRESENT;
#endif
	return spiSetInternalReg (file, value, SC18IS600_IOSTATE);
}

/**************************************************************************************
				LIB FCE
  
**************************************************************************************/
int 	sfpInit (int sfp_num)
{
	int file;
	int ret=0;
	uint8_t value;
	
	if ((file=spiOpen (sfp_num)) < 0 ) {
		return ret;
	}
	if ((ret=spiInitSet(file)) < 0 )
		goto err_end;
	if ((ret=spiSetInternalReg (file, I2C_SPEED, SC18IS600_I2CCLOCK)) < 0)
		goto err_end;

	if ((ret=spiSetInternalReg (file, SC18IS600_PIN_DEF, SC18IS600_IOCONFIG)) < 0)
		goto err_end;
#ifdef DEBUG
	spiInitGet(file);
#endif

	if ((ret=spiGetInternalReg (file, &value, SC18IS600_IOSTATE)) < 0 )
		goto err_end;
#ifdef CHECK_PRESENT
	present = (value >> PIN_SFP_PRESENT) & 0x1;
	if (present) {
#endif
		value=value & ~(1<<PIN_SFP_VDD_EN | 1<<PIN_SFP_TX_DISABLE);
#ifdef CHECK_PRESENT
	} else {
		value=value | 1<<PIN_SFP_VDD_EN | 1<<PIN_SFP_TX_DISABLE;
	}
#endif
#ifdef RR_PRODUCT_bk2
	value=value | 1<<PIN_SFP_PRESENT;
#endif
	ret=spiSetInternalReg (file, value, SC18IS600_IOSTATE);

err_end:
	spiClose (file);
	return ret;
}

int 	sfpGetStatus (int sfp_num)
{
	int ret;
	int file;
	uint8_t value;
	
	if ((file=spiOpen (sfp_num)) < 0) {
		return file;
	}
	if ((ret=spiInitSet(file)) < 0 ) {
		spiClose (file);
		return ret;
	}
	ret=spiGetInternalReg (file, &value, SC18IS600_IOSTATE);
	spiClose (file);
	if (ret < 0 )
		return ret;
	return value;
}

int 	sfpSetTxDisable (int sfp_num, SFP_TX_DISABLE_ val)
{
	int ret;
	int file;
	
	if ((file=spiOpen (sfp_num)) < 0) {
		return file;
	}
	if ((ret=spiInitSet(file)) < 0 ) {
		spiClose (file);
		return ret;
	}
	ret = _sfpSetTxDisable (file, val);
	spiClose (file);
	return ret;
}

int 	sfpSetVddEn (int sfp_num, SFP_VDD_EN_ val)
{
	int ret;
	int file;
	if ((file=spiOpen (sfp_num)) < 0) {
		return file;
	}
	if ((ret=spiInitSet(file)) < 0 ) {
		spiClose (file);
		return ret;
	}
	ret = _sfpSetVdd (file, val);
	spiClose (file);
	return ret;
}

int 	sfpGetEeprom( int sfp_num, uint8_t * table, uint8_t eepromAddr)
{
	int ret;
	int file;
#ifdef DEBUG
	int i, j;
	uint8_t * ptr;
#endif
	if ((file=spiOpen (sfp_num)) < 0) {
		return file;
	}
	if ((ret=spiInitSet(file)) < 0 ) {
		spiClose (file);
		return ret;
	}
	
	if ((ret=spiGetI2cEeprom (file, table, eepromAddr)) < 0 ) {
		spiClose (file);
		return ret;
	}
	spiClose (file);
#ifdef DEBUG	
	ptr=table;
	printf ("Table\n");
	for (j=0; j<16; j++) {
		for (i=0; i < 16; i++,ptr++)
			printf (" %02x",*(ptr));
		printf ("\n");
	}
#endif
	return 0;
}

#ifdef CHECK_PRESENT
int 	sfpCheck (int sfp_num, uint8_t * old)
{
	int file;
	int ret=0;
	uint8_t value;
	uint8_t present;
	
	if ((file=spiOpen (sfp_num)) < 0 ) {
		return ret;
	}
	if ((ret=spiInitSet(file)) < 0 )
		goto err_end;
	if ((ret=spiGetInternalReg (file, &value, SC18IS600_IOSTATE)) < 0 )
		goto err_end;
	present = (value >> PIN_SFP_PRESENT) & 0x1;
	if (present) {
		if (present != *old) {
			value=value & ~(1<<PIN_SFP_VDD_EN);
			value=value | 1<<PIN_SFP_TX_DISABLE;
		}
	} else {
		if (present != *old) {
			value=value & ~(1<<PIN_SFP_TX_DISABLE);
			value=value | 1<<PIN_SFP_VDD_EN;
		}
	}
	ret=spiSetInternalReg (file, value, SC18IS600_IOSTATE);
err_end:
	spiClose (file);
	return ret;
}
#endif
