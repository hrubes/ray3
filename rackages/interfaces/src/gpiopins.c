#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

#include "libgpiopins.h"

#define SYSFS_GPIO_DIR "/sys/class/gpio"
#define MAX_BUF 64


/****************************************************************
*/
static int gpioExport(unsigned int gpio)
{
	int fd, len;
	char buf[MAX_BUF];
 
	fd = open(SYSFS_GPIO_DIR "/export", O_WRONLY);
	if (fd < 0) {
		fprintf(stderr,"Failed to export GPIO: %s.\n",strerror (errno));
		return fd;
	}
	len = snprintf(buf, sizeof(buf), "%d", gpio);
	write(fd, buf, len+1);
	close(fd);
	return 0;
}

/****************************************************************
*/
static int gpioSetDirection(unsigned int gpio, GPIO_DIRECTION direction)
{
	int fd;
	char buf[MAX_BUF];
 
	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR  "/gpio%d/direction", gpio);
	fd=open(buf, O_WRONLY);
	if (fd < 0) {
		fprintf(stderr,"Failed to set GPIO direction: %s.\n",strerror (errno));
		return fd;
	}
	if (direction == GPIO_OUT)
		write(fd, "out", 4);
	if (direction == GPIO_IN)
		write(fd, "in", 3);
	close(fd);
	return 0;
}

/****************************************************************
*/
int gpioSetValue(unsigned int gpio, GPIO_VALUE value)
{
	int fd;
	char buf[MAX_BUF];
	
	if (value > 1)
		return -EINVAL;
	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);
	fd=open(buf, O_WRONLY);
	if (fd < 0) {
		fprintf(stderr,"Failed to set GPIO value: %s.\n",strerror (errno));
		return fd;
	}
	if (value == GPIO_H)
		write(fd, "1", 2);
	if  (value == GPIO_L)
		write(fd, "0", 2);
 
	close(fd);
	return 0;
}

/****************************************************************
*/
int gpioGetValue(unsigned int gpio, GPIO_VALUE *value)
{
	int fd;
	char buf[MAX_BUF];
	char val;

	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);
	fd=open(buf, O_RDONLY);
	if (fd < 0) {
		fprintf(stderr,"Failed to get GPIO value: %s.\n",strerror (errno));
		return fd;
	}
	read(fd, &val, 1);
	if (val == '0')
		*value = (GPIO_VALUE)0;
	else
		*value = (GPIO_VALUE)1;
	close(fd);
	return 0;
}
/****************************************************************
*/
int gpioCreate (unsigned int gpio, GPIO_DIRECTION direction, GPIO_VALUE def_value)
{
	int ret;
	if ((ret=gpioExport(gpio)) < 0 )
		return ret;
	if ((ret=gpioSetDirection(gpio, direction)) < 0)
		return ret;
	if (direction == GPIO_IN)
		return 0;
	if (def_value < GPIO_NC)
		return gpioSetValue(gpio, def_value);
	else
		return 0;
	
}	




// /****************************************************************
// */

#include <time.h>

struct timespec us2timespec(long t)
{
	// COEF urcuje kolik ma jedna vterina podjednotek
	//const long COEF=1000; 	// 1 s =       1000 ms (1e3)
	const long COEF=1000000;	// 1 s =    1000000 us (1e6)
	const long NSTOS=1000000000;	// 1 s = 1000000000 ns (1e9)
	struct timespec ts;
	ts.tv_sec = time_t(t/COEF); // to seconds
	ts.tv_nsec = (t%COEF)*(NSTOS/COEF); // and reminder to nanoseconds
	//fprintf(stderr, "input: %dus -> %ds, %dns  (milion: %d)\n", t, ts.tv_sec, ts.tv_nsec, MILION);
	return ts;
}


int gpioSendPulse(unsigned int gpio, GPIO_VALUE value, long t1, long t2, long count, long t0, bool last_t2_delay)
// gpio - cislo gpio pinu
// value - prvni zapisovana uroven (a skonci v 1-value)
// t1 - [us] trvani urovne value
// t2=t1 - [us] trvani urovne 1-value 
// count=1 - pocet pulzu
// t0=0 - [us] uvodni prodleva
// last_t2_delay - true  -> fce skonci az po poslednim cekani
//               - false -> fce skonci po posledni zmene hodnoty
//
// RETVAL
//    0 - all pulses written to gpio pin 
//   -1 - generating of pulses didn't finished and was interupted
// 	- you should check gpio pin value (and maybe time)
//   -EACCES - unable to open gpio pin for writing
//   -EINVAL - Wrong parameter
{
	long int i;
	int fd;
	int retv=0;
	char buf[MAX_BUF];
	char L1[]="1", L2[]="0";
	struct timespec ts0,ts1,ts2;
	
	if (count <= 0) // nebo zpusobit nekonecnou smycku
		return -EINVAL;

	if (value == GPIO_H)
	{ // prvni zmena budo do urovne H a skoncime v L
		// nic, hodnoty jsou preddefinovany z deklarace
	}
	else if (value == GPIO_L)
	{ // prvni zmena bude do urovne L a skoncime v H
		L1[0] = '0';
		L2[0] = '1';
	} 
	else
	{
		return -EINVAL;
	}
	if (t0>0)
	{ // cekani pred zacatkem pulsu
		ts0 = us2timespec(t0);
		retv = nanosleep(&ts0,NULL);
		//fprintf(stderr,"sleep 0: %dus (%ds, %dns) with retv %d\n", t0, ts0.tv_sec, ts0.tv_nsec, retv);
		if (retv!=0)
		{
			return -1;
		}
	}

	if (t2==0) // nastaveni stridy 1:1
		t2=t1; 

	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);
	fd=open(buf, O_WRONLY);
	if (fd < 0) {
		fprintf(stderr,"Failed to set GPIO value: %s.\n",strerror (errno));
		return -EACCES;
	}

	// konverze us na timespec
	ts1 = us2timespec(t1);
	ts2 = us2timespec(t2);

	for (i=0; i<count; ++i)
	{
		retv = write(fd, L1, 2);
		//fprintf(stderr,"write L1=%s with retv %d\n", L1, retv);
		if (retv<0)
			break;
		retv = nanosleep(&ts1,NULL);
		//fprintf(stderr,"sleep 1: %dus (%ds, %dns) with retv %d\n", t1, ts1.tv_sec, ts1.tv_nsec, retv);
		if (retv!=0)
			break;
		retv = write(fd, L2, 2);
		//fprintf(stderr,"write L2=%s with retv %d\n", L2, retv);
		if (retv<0)
			break;
		if (last_t2_delay || i<count-1)
		{ // po posledni zmene urovne uz se nespi
			retv = nanosleep(&ts2,NULL);
			//fprintf(stderr,"sleep 2: %dus (%ds, %dns) with retv %d\n", t2, ts2.tv_sec, ts2.tv_nsec, retv);
			if (retv!=0)
				break;
		}
	}
	//fprintf(stderr,"closing with retv %d\n",retv);
	close(fd);
	return retv;
}


