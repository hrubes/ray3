
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include <linux/types.h>
#include "sfp.h"
#include <math.h>

#define PRINT_BIAS(string, index) printf("\t%-43s %.3f mA\n", (string), (double)(diag->bias_cur[(index)] / 500.))

#define PRINT_xX_PWR(string, var, index) printf("\t%-43s %.4f mW / %.2f dBm\n", (string), (double)((var)[(index)] / 10000.), convert_mw_to_dbm((double)((var)[(index)] / 10000.)))

#define PRINT_TEMP(string, index) printf("\t%-43s %.2f degrees C \n", (string), (double)(diag->sfp_temp[(index)] / 256.))

#define PRINT_VCC(string, index)  printf("\t%-43s %.4f V\n", (string), (double)(diag->sfp_voltage[(index)] / 10000.))

#define PRINTCNT32(a,b) { \
	{ \
		if (a < b) \
			fprintf(stderr,"\t%20u",a-b + 0xffffffff); \
		else \
			fprintf(stderr,"\t%20u",a-b); \
	}\
}

#define PRINTCNT32SAVE(a,b,c) { \
	{ \
		if (a < b) {\
			*c+=a-b + 0xffffffff;\
			fprintf(stderr,"\t%20u",a-b + 0xffffffff); \
		} else {\
			*c+=a-b;\
			fprintf(stderr,"\t%20u",a-b); \
		}\
	}\
}

static struct sff8472_aw_flags {
	const char *str;        /* Human-readable string, null at the end */
	int offset;             /* A2-relative adress offset */
	__u8 value;             /* Alarm is on if (offset & value) != 0. */
} sff8472_aw_flags[] = {
	{ "Laser bias current high alarm:",   SFF_A2_ALRM_FLG, (1 << 3) },
	{ "Laser bias current low alarm:",    SFF_A2_ALRM_FLG, (1 << 2) },
	{ "Laser bias current high warning:", SFF_A2_WARN_FLG, (1 << 3) },
	{ "Laser bias current low warning:",  SFF_A2_WARN_FLG, (1 << 2) },

	{ "Laser output power high alarm:",   SFF_A2_ALRM_FLG, (1 << 1) },
	{ "Laser output power low alarm:",    SFF_A2_ALRM_FLG, (1 << 0) },
	{ "Laser output power high warning:", SFF_A2_WARN_FLG, (1 << 1) },
	{ "Laser output power low warning:",  SFF_A2_WARN_FLG, (1 << 0) },

	{ "Module temperature high alarm:",   SFF_A2_ALRM_FLG, (1 << 7) },
	{ "Module temperature low alarm:",    SFF_A2_ALRM_FLG, (1 << 6) },
	{ "Module temperature high warning:", SFF_A2_WARN_FLG, (1 << 7) },
	{ "Module temperature low warning:",  SFF_A2_WARN_FLG, (1 << 6) },

	{ "Module voltage high alarm:",   SFF_A2_ALRM_FLG, (1 << 5) },
	{ "Module voltage low alarm:",    SFF_A2_ALRM_FLG, (1 << 4) },
	{ "Module voltage high warning:", SFF_A2_WARN_FLG, (1 << 5) },
	{ "Module voltage low warning:",  SFF_A2_WARN_FLG, (1 << 4) },

	{ "Laser rx power high alarm:",   SFF_A2_ALRM_FLG + 1, (1 << 7) },
	{ "Laser rx power low alarm:",    SFF_A2_ALRM_FLG + 1, (1 << 6) },
	{ "Laser rx power high warning:", SFF_A2_WARN_FLG + 1, (1 << 7) },
	{ "Laser rx power low warning:",  SFF_A2_WARN_FLG + 1, (1 << 6) },

	{ NULL, 0, 0 },
};

static double convert_mw_to_dbm(double mw)
{
	return (10. * log10(mw / 1000.)) + 30.;
}






/*--------------------------------------------------------------------*/

// vypise se status SFP 
static void printSFPgpio  (STATUS_SFP_GPIO  *status)
{
	printf("SFP status:\n");
	
	if (status->present == INSERTED)
		printf("\t%-43s %s\n","Present:","inserted");
	else
		printf("\t%-43s %s\n","Present:","removed");
	if (status->disable == TX_ENABLE)
		printf("\t%-43s %s\n","TX:","enable");
	else
		printf("\t%-43s %s\n","TX:","disable");
	if (status->fault == LASER_NORMAL)
		printf("\t%-43s %s\n","Laser:","normal");
	else
		printf("\t%-43s %s\n","Laser:","fault");
	if (status->loss == SIGNAL_NORMAL)
		printf("\t%-43s %s\n","Signal:","normal");
	else
		printf("\t%-43s %s\n","Signal:","loss");

	printf("SFP VDD:\n");
	if (status->vdd_en == VDD_ENABLE)
		printf("\t%-43s %s\n","En:","enable");
	else
		printf("\t%-43s %s\n","En:","disable");


}


static void sff8079PrintIdentifier(const __u8 *eepromData)
{
	printf("\t%-43s ", "Identifier:");
	switch (eepromData[0]) {
	case 0x00:
		printf("no module present, unknown, or unspecified (0x%02x)\n",eepromData[0]);
		break;
	case 0x01:
		printf("GBIC\n");
		break;
	case 0x02:
		printf("module soldered to motherboard\n");
		break;
	case 0x03:
		printf("SFP\n");
		break;
	default:
		 printf("reserved or unknown (0x%02x\n",eepromData[0]);
		break;
	}
}

static void sff8079PrintExtIdentifier(const __u8 *eepromData)
{
	printf("\t%-43s ", "Extended identifier:");
	if (eepromData[1] == 0x00)
		printf("GBIC not specified / not MOD_DEF compliant\n");
	else if (eepromData[1] == 0x04)
		printf("GBIC/SFP defined by 2-wire interface ID\n");
	else if (eepromData[1] <= 0x07)
		printf("GBIC compliant with MOD_DEF %u\n", eepromData[1]);
	else
		printf("unknown (0x%02x)\n", eepromData[1]);
}

static void sff8079PrintConnector(const __u8 *eepromData)
{
	printf("\t%-43s ", "Connector:");
	switch (eepromData[2]) {
	case 0x00:
		printf("unknown or unspecified\n");
		break;
	case 0x01:
		printf("SC\n");
		break;
	case 0x02:
		printf("Fibre Channel Style 1 copper\n");
		break;
	case 0x03:
		printf("Fibre Channel Style 2 copper\n");
		break;
	case 0x04:
		printf("BNC/TNC\n");
		break;
	case 0x05:
		printf("Fibre Channel coaxial headers\n");
		break;
	case 0x06:
		printf("FibreJack\n");
		break;
	case 0x07:
		printf("LC\n");
		break;
	case 0x08:
		printf("MT-RJ\n");
		break;
	case 0x09:
		printf("MU\n");
		break;
	case 0x0a:
		printf("SG\n");
		break;
	case 0x0b:
		printf("Optical pigtail\n");
		break;
	case 0x0c:
		printf("MPO Parallel Optic\n");
		break;
	case 0x20:
		printf("HSSDC II\n");
		break;
	case 0x21:
		printf("Copper pigtail\n");
		break;
	case 0x22:
		printf("RJ45\n");
		break;
	default:
		printf("reserved or unknown (0x%02x)\n",eepromData[2]);
		break;
	}
}

static void sff8079PrintTransceiver(const __u8 *eepromData)
{
	static const char *pfx ="Transceiver type:";

	/* 10G Ethernet Compliance Codes */
	if (eepromData[3] & (1 << 7))
		printf("\t%-43s 10G Ethernet: 10G Base-ER" \
		       " [SFF-8472 rev10.4 only]\n", pfx);
	if (eepromData[3] & (1 << 6))
		printf("\t%-43s 10G Ethernet: 10G Base-LRM\n", pfx);
	if (eepromData[3] & (1 << 5))
		printf("\t%-43s 10G Ethernet: 10G Base-LR\n", pfx);
	if (eepromData[3] & (1 << 4))
		printf("\t%-43s 10G Ethernet: 10G Base-SR\n", pfx);
	/* Infiniband Compliance Codes */
	if (eepromData[3] & (1 << 3))
		printf("\t%-43s Infiniband: 1X SX\n", pfx);
	if (eepromData[3] & (1 << 2))
		printf("\t%-43s Infiniband: 1X LX\n", pfx);
	if (eepromData[3] & (1 << 1))
		printf("\t%-43s Infiniband: 1X Copper Active\n", pfx);
	if (eepromData[3] & (1 << 0))
		printf("\t%-43s Infiniband: 1X Copper Passive\n", pfx);
	/* ESCON Compliance Codes */
	if (eepromData[4] & (1 << 7))
		printf("\t%-43s ESCON: ESCON MMF, 1310nm LED\n", pfx);
	if (eepromData[4] & (1 << 6))
		printf("\t%-43s ESCON: ESCON SMF, 1310nm Laser\n", pfx);
	/* SONET Compliance Codes */
	if (eepromData[4] & (1 << 5))
		printf("\t%-43s SONET: OC-192, short reach\n", pfx);
	if (eepromData[4] & (1 << 4))
		printf("\t%-43s SONET: SONET reach specifier bit 1\n", pfx);
	if (eepromData[4] & (1 << 3))
		printf("\t%-43s SONET: SONET reach specifier bit 2\n", pfx);
	if (eepromData[4] & (1 << 2))
		printf("\t%-43s SONET: OC-48, long reach\n", pfx);
	if (eepromData[4] & (1 << 1))
		printf("\t%-43s SONET: OC-48, intermediate reach\n", pfx);
	if (eepromData[4] & (1 << 0))
		printf("\t%-43s SONET: OC-48, short reach\n", pfx);
	if (eepromData[5] & (1 << 6))
		printf("\t%-43s SONET: OC-12, single mode, long reach\n", pfx);
	if (eepromData[5] & (1 << 5))
		printf("\t%-43s SONET: OC-12, single mode, inter. reach\n", pfx);
	if (eepromData[5] & (1 << 4))
		printf("\t%-43s SONET: OC-12, short reach\n", pfx);
	if (eepromData[5] & (1 << 2))
		printf("\t%-43s SONET: OC-3, single mode, long reach\n", pfx);
	if (eepromData[5] & (1 << 1))
		printf("\t%-43s SONET: OC-3, single mode, inter. reach\n", pfx);
	if (eepromData[5] & (1 << 0))
		printf("\t%-43s SONET: OC-3, short reach\n", pfx);
	/* Ethernet Compliance Codes */
	if (eepromData[6] & (1 << 7))
		printf("\t%-43s Ethernet: BASE-PX\n", pfx);
	if (eepromData[6] & (1 << 6))
		printf("\t%-43s Ethernet: BASE-BX10\n", pfx);
	if (eepromData[6] & (1 << 5))
		printf("\t%-43s Ethernet: 100BASE-FX\n", pfx);
	if (eepromData[6] & (1 << 4))
		printf("\t%-43s Ethernet: 100BASE-LX/LX10\n", pfx);
	if (eepromData[6] & (1 << 3))
		printf("\t%-43s Ethernet: 1000BASE-T\n", pfx);
	if (eepromData[6] & (1 << 2))
		printf("\t%-43s Ethernet: 1000BASE-CX\n", pfx);
	if (eepromData[6] & (1 << 1))
		printf("\t%-43s Ethernet: 1000BASE-LX\n", pfx);
	if (eepromData[6] & (1 << 0))
		printf("\t%-43s Ethernet: 1000BASE-SX\n", pfx);
	/* Fibre Channel link length */
	if (eepromData[7] & (1 << 7))
		printf("\t%-43s FC: very long distance (V)\n", pfx);
	if (eepromData[7] & (1 << 6))
		printf("\t%-43s FC: short distance (S)\n", pfx);
	if (eepromData[7] & (1 << 5))
		printf("\t%-43s FC: intermediate distance (I)\n", pfx);
	if (eepromData[7] & (1 << 4))
		printf("\t%-43s FC: long distance (L)\n", pfx);
	if (eepromData[7] & (1 << 3))
		printf("\t%-43s FC: medium distance (M)\n", pfx);
	/* Fibre Channel transmitter technology */
	if (eepromData[7] & (1 << 2))
		printf("\t%-43s FC: Shortwave laser, linear Rx (SA)\n", pfx);
	if (eepromData[7] & (1 << 1))
		printf("\t%-43s FC: Longwave laser (LC)\n", pfx);
	if (eepromData[7] & (1 << 0))
		printf("\t%-43s FC: Electrical inter-enclosure (EL)\n", pfx);
	if (eepromData[8] & (1 << 7))
		printf("\t%-43s FC: Electrical intra-enclosure (EL)\n", pfx);
	if (eepromData[8] & (1 << 6))
		printf("\t%-43s FC: Shortwave laser w/o OFC (SN)\n", pfx);
	if (eepromData[8] & (1 << 5))
		printf("\t%-43s FC: Shortwave laser with OFC (SL)\n", pfx);
	if (eepromData[8] & (1 << 4))
		printf("\t%-43s FC: Longwave laser (LL)\n", pfx);
	if (eepromData[8] & (1 << 3))
		printf("\t%-43s FC: Copper Active\n", pfx);
	if (eepromData[8] & (1 << 2))
		printf("\t%-43s FC: Copper Passive\n", pfx);
	if (eepromData[8] & (1 << 1))
		printf("\t%-43s FC: Copper FC-BaseT\n", pfx);
	/* Fibre Channel transmission media */
	if (eepromData[9] & (1 << 7))
		printf("\t%-43s FC: Twin Axial Pair (TW)\n", pfx);
	if (eepromData[9] & (1 << 6))
		printf("\t%-43s FC: Twisted Pair (TP)\n", pfx);
	if (eepromData[9] & (1 << 5))
		printf("\t%-43s FC: Miniature Coax (MI)\n", pfx);
	if (eepromData[9] & (1 << 4))
		printf("\t%-43s FC: Video Coax (TV)\n", pfx);
	if (eepromData[9] & (1 << 3))
		printf("\t%-43s FC: Multimode, 62.5um (M6)\n", pfx);
	if (eepromData[9] & (1 << 2))
		printf("\t%-43s FC: Multimode, 50um (M5)\n", pfx);
	if (eepromData[9] & (1 << 0))
		printf("\t%-43s FC: Single Mode (SM)\n", pfx);
	/* Fibre Channel speed */
	if (eepromData[10] & (1 << 7))
		printf("\t%-43s FC: 1200 MBytes/sec\n", pfx);
	if (eepromData[10] & (1 << 6))
		printf("\t%-43s FC: 800 MBytes/sec\n", pfx);
	if (eepromData[10] & (1 << 4))
		printf("\t%-43s FC: 400 MBytes/sec\n", pfx);
	if (eepromData[10] & (1 << 2))
		printf("\t%-43s FC: 200 MBytes/sec\n", pfx);
	if (eepromData[10] & (1 << 0))
		printf("\t%-43s FC: 100 MBytes/sec\n", pfx);
}

static void sff8079PrintEncoding(const __u8 *eepromData)
{
	printf("\t%-43s ", "Encoding:");
	switch (eepromData[11]) {
	case 0x00:
		printf("unspecified\n");
		break;
	case 0x01:
		printf("8B/10B\n");
		break;
	case 0x02:
		printf("4B/5B\n");
		break;
	case 0x03:
		printf("NRZ\n");
		break;
	case 0x04:
		printf("Manchester\n");
		break;
	case 0x05:
		printf("SONET Scrambled\n");
		break;
	case 0x06:
		printf("64B/66B\n");
		break;
	default:
		printf("reserved or unknown (0x%02x)\n", eepromData[11]);
		break;
	}
}

static void sff8079PrintRateIdentifier(const __u8 *eepromData)
{
	printf("\t%-43s ", "Rate identifier:");
	switch (eepromData[13]) {
	case 0x00:
		printf("unspecified\n");
		break;
	case 0x01:
		printf("4/2/1G Rate_Select & AS0/AS1\n");
		break;
	case 0x02:
		printf("8/4/2G Rx Rate_Select only\n");
		break;
	case 0x03:
		printf("8/4/2G Independent Rx & Tx Rate_Select\n");
		break;
	case 0x04:
		printf("8/4/2G Tx Rate_Select only\n");
		break;
	default:
		printf("reserved or unknown (0x%02x)\n", eepromData[13]);
		break;
	}
}

static void sff8079PrintOui(const __u8 *eepromData)
{
	printf("\t%-43s %02x:%02x:%02x\n", "Vendor OUI:", eepromData[37], eepromData[38], eepromData[39]);
}

static void sff8079PrintWavelength(const __u8 *eepromData)
{
	if (eepromData[8] & (1 << 2)) {
		printf("\t%-43s ", "Passive Cu cmplnce:");
		switch (eepromData[60]) {
		case 0x00:
			printf("unspecified");
			break;
		case 0x01:
			printf("SFF-8431 appendix E");
			break;
		default:
			printf("unknown 0x%02x", eepromData[60]);
			break;
		}
		printf(" [SFF-8472 rev10.4 only]\n");
	} else if (eepromData[8] & (1 << 3)) {
		printf("\t%-43s ", "Active Cu cmplnce:");
		switch (eepromData[60]) {
		case 0x00:
			printf("unspecified");
			break;
		case 0x01:
			printf("SFF-8431 appendix E");
			break;
		case 0x04:
			printf("SFF-8431 limiting");
			break;
		default:
			printf("unknown (0x%02x",eepromData[60]);
			break;
		}
		printf(" [SFF-8472 rev10.4 only]\n");
	} else {
		printf("\t%-43s %unm\n", "Laser wavelength:",(eepromData[60] << 8) | eepromData[61]);
	}
}

static void sff8079PrintValue(const __u8 *eepromData, unsigned int reg, const char *name, unsigned int mult, const char *unit)
{
	unsigned int val = eepromData[reg];

	printf("\t%-43s %u%s\n", name, val * mult, unit);
}

static void sff8079PrintAscii(const __u8 *eepromData, unsigned int first_reg, unsigned int last_reg, const char *name)
{
	unsigned int reg;
	unsigned char val;
	
	printf("\t%-43s ", name);
	for (reg = first_reg; reg <= last_reg; reg++) {
		val = eepromData[reg];
		putchar(((val >= 32) && (val <= 126)) ? val : '_');
	}
	printf("\n");
}

static void sff8079PrintAll(const __u8 *eepromData)
{
	printf ("\nEEprom SFP:\n");
	sff8079PrintIdentifier(eepromData);
	if (((eepromData[0] == 0x01) || (eepromData[0] == 0x03))&& (eepromData[1] == 0x04)) {
		sff8079PrintExtIdentifier(eepromData);
		sff8079PrintConnector(eepromData);
		sff8079PrintTransceiver(eepromData);
		sff8079PrintEncoding(eepromData);
		sff8079PrintValue(eepromData, 12, "BR, Nominal:", 100, "MBd");
		sff8079PrintRateIdentifier(eepromData);
		sff8079PrintValue(eepromData, 14, "Length (SMF,km):", 1, "km");
		sff8079PrintValue(eepromData, 15, "Length (SMF):", 100, "m");
		sff8079PrintValue(eepromData, 16, "Length (50um):", 10, "m");
		sff8079PrintValue(eepromData, 17, "Length (62.5um):", 10, "m");
		sff8079PrintValue(eepromData, 18, "Length (Copper):", 1, "m");
		sff8079PrintValue(eepromData, 19, "Length (OM3):", 10, "m");
		sff8079PrintWavelength(eepromData);
		sff8079PrintAscii(eepromData, 20, 35, "Vendor name:");
		sff8079PrintOui(eepromData);
		sff8079PrintAscii(eepromData, 40, 55, "Vendor PN:");
		sff8079PrintAscii(eepromData, 56, 59, "Vendor rev:");
	}
}

static void sff8472PrintAll(const __u8 *eepromData, struct SFF8472DIAGS *diag)
{
	
	int i;

	if (!diag->supports_dom) {
		printf("\t%-43s No\n", "Optical diagnostics support:");
		return ;
	}
	printf("\t%-43s Yes\n", "Optical diagnostics support:");
	PRINT_BIAS("Laser bias current:", MCURR);
	PRINT_xX_PWR("Laser output power:", diag->tx_power, MCURR);
	if (!diag->rx_power_type)
		PRINT_xX_PWR("Receiver signal OMA:", diag->rx_power, MCURR);
	else
		PRINT_xX_PWR("Receiver signal average optical power:", diag->rx_power, MCURR);
	PRINT_TEMP("Module temperature:", MCURR);
	PRINT_VCC("Module voltage:", MCURR);
	printf("\t%-43s %s\n", "Alarm/warning flags implemented:", (diag->supports_alarms ? "Yes" : "No"));
	if (diag->supports_alarms) {
		for (i = 0; sff8472_aw_flags[i].str; ++i) {
			printf("\t%-43s %s\n", sff8472_aw_flags[i].str, eepromData[SFF_A2_BASE + sff8472_aw_flags[i].offset] & sff8472_aw_flags[i].value ? "On" : "Off");
		}
		PRINT_BIAS("Laser bias current high alarm threshold:",   HALRM);
		PRINT_BIAS("Laser bias current low alarm threshold:",    LALRM);
		PRINT_BIAS("Laser bias current high warning threshold:", HWARN);
		PRINT_BIAS("Laser bias current low warning threshold:",  LWARN);

		PRINT_xX_PWR("Laser output power high alarm threshold:",diag->tx_power, HALRM);
		PRINT_xX_PWR("Laser output power low alarm threshold:",diag->tx_power, LALRM);
		PRINT_xX_PWR("Laser output power high warning threshold:",diag->tx_power, HWARN);
		PRINT_xX_PWR("Laser output power low warning threshold:",diag->tx_power, LWARN);

		PRINT_TEMP("Module temperature high alarm threshold:",   HALRM);
		PRINT_TEMP("Module temperature low alarm threshold:",    LALRM);
		PRINT_TEMP("Module temperature high warning threshold:", HWARN);
		PRINT_TEMP("Module temperature low warning threshold:",  LWARN);

		PRINT_VCC("Module voltage high alarm threshold:",   HALRM);
		PRINT_VCC("Module voltage low alarm threshold:",    LALRM);
		PRINT_VCC("Module voltage high warning threshold:", HWARN);
		PRINT_VCC("Module voltage low warning threshold:",  LWARN);

		PRINT_xX_PWR("Laser rx power high alarm threshold:",diag->rx_power, HALRM);
		PRINT_xX_PWR("Laser rx power low alarm threshold:",diag->rx_power, LALRM);
		PRINT_xX_PWR("Laser rx power high warning threshold:",diag->rx_power, HWARN);
		PRINT_xX_PWR("Laser rx power low warning threshold:",diag->rx_power, LWARN);
	}

}

void printSFPStatus  (STATUS_SFP  *status)
{
	printSFPgpio  (&status->sfp_gpio);
	if (status->statusDataA0 == VALID) {
		 sff8079PrintAll(status->eepromData);
	}
	if (status->statusDataA2 == VALID) {
		 sff8472PrintAll(status->eepromData, &status->diag8472);
	}
}


