#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include "libgpiopins.h"
#include "gpiopins.h"


#ifdef RR_PRODUCT_bma
const struct gpio_pins_ gpio_pins[]={
	{SW_BUTTON,6,"SW_BUTTON",GPIO_IN,GPIO_NC},
	{WDG_STATUS,33,"WDG_STATUS",GPIO_IN,GPIO_NC},
	{WDG_REFRESH,34,"WDG_REFRESH",GPIO_OUT,GPIO_L},
	{PS_ALARM,35,"PS_ALARM",GPIO_IN,GPIO_NC},
	{RB_PWR_EN,42,"RB_PWR_EN",GPIO_OUT,GPIO_L},
	{I2C_EXP_INT,43,"I2C_EXP_INT",GPIO_IN,GPIO_NC},
	{RB_MCU_ISP,44,"RB_MCU_ISP",GPIO_OUT,GPIO_L},
	{RB_MCU_RST,47,"RB_MCU_RST",GPIO_OUT,GPIO_H},
	{LED_SYS_RED,51,"LED_SYS_RED",GPIO_OUT,GPIO_H},
	{LED_SYS_GRN,56,"LED_SYS_GRN",GPIO_OUT,GPIO_H},
	{BCM_SC0_AIRLOSS,480,"BCM_SC0_AIRLOSS",GPIO_IN,GPIO_NC},
	{BCM_SC0_ALARM1,481,"BCM_SC0_ALARM1",GPIO_IN,GPIO_NC},
	{BCM_SC0_ALARM2,482,"BCM_SC0_ALARM2",GPIO_IN,GPIO_NC},
	{BCM_SC1_AIRLOSS,483,"BCM_SC1_AIRLOSS",GPIO_IN,GPIO_NC},
	{BCM_SC1_ALARM1,484,"BCM_SC1_ALARM1",GPIO_IN,GPIO_NC},
	{BCM_SC1_ALARM2,485,"BCM_SC1_ALARM2",GPIO_IN,GPIO_NC},
	{TP50_IN,486,"TP50_IN",GPIO_IN,GPIO_NC},
	{TP51_IN,487,"TP51_IN",GPIO_IN,GPIO_NC},
	{SWITCH_INT,488,"SWITCH_INT",GPIO_IN,GPIO_NC},
	{TU_INT,489,"TU_INT",GPIO_IN,GPIO_NC},
	{BCM_PS_ALERT,490,"BCM_PS_ALERT",GPIO_IN,GPIO_NC},
	{USB_VDD_OC,491,"USB_VDD_OC",GPIO_IN,GPIO_NC},
	{PS_PAIR0,492,"PS_PAIR0",GPIO_IN,GPIO_NC},
	{TP44_IN,493,"TP44_IN",GPIO_IN,GPIO_NC},
	{TP45_IN,494,"TP45_IN",GPIO_IN,GPIO_NC},
	{TP46_IN,495,"TP46_IN",GPIO_IN,GPIO_NC},
	{LED_AIR_GRN,496,"LED_AIR_GRN",GPIO_OUT,GPIO_H},
	{LED_AIR_RED,497,"LED_AIR_RED",GPIO_OUT,GPIO_H},
	{MUX_SEL0,498,"MUX_SEL0",GPIO_OUT,GPIO_L},
	{MUX_SEL1,499,"MUX_SEL1",GPIO_OUT,GPIO_L},
	{TP40_OUT,500,"TP40_OUT",GPIO_OUT,GPIO_H},
	{EXP_SFP,501,"EXP_SFP",GPIO_OUT,GPIO_H},
	{TP36_OUT,502,"TP36_OUT",GPIO_OUT,GPIO_L},
	{TP37_OUT,503,"TP37_OUT",GPIO_OUT,GPIO_H},
	{LED_DIS,504,"LED_DIS",GPIO_OUT,GPIO_L},
	{USB_VDD_EN,505,"USB_VDD_EN",GPIO_OUT,GPIO_L},
	{SWITCH_RESET,506,"SWITCH_RESET",GPIO_OUT,GPIO_L},
	{BCM_RESET,507,"BCM_RESET",GPIO_OUT,GPIO_H},
	{TU_RESET,508,"TU_RESET",GPIO_OUT,GPIO_L},
	{BCM_PS_EN,509,"BCM_PS_EN",GPIO_OUT,GPIO_L},
	{LVM,510,"LVM",GPIO_OUT,GPIO_L},
	{SWITCH_P10_2500,511,"SWITCH_P10_2500",GPIO_OUT,GPIO_H}
};
#endif
#ifdef RR_PRODUCT_bk1
const struct gpio_pins_ gpio_pins[]={
	{SW_BUTTON,6,"SW_BUTTON",GPIO_IN,GPIO_NC},
	{WDG_STATUS,33,"WDG_STATUS",GPIO_IN,GPIO_NC},
	{WDG_REFRESH,34,"WDG_REFRESH",GPIO_OUT,GPIO_L},
	{LED_SYS_GRN,35,"LED_SYS_GRN",GPIO_OUT,GPIO_H},
	{LED_AIR_RED,36,"LED_AIR_RED",GPIO_OUT,GPIO_L},
	{LED_AIR_GRN,42,"LED_AIR_GRN",GPIO_OUT,GPIO_H},
	{I2C_EXP_INT,43,"I2C_EXP_INT",GPIO_IN,GPIO_NC},
	{PS_ALARM,51,"PS_ALARM",GPIO_IN,GPIO_NC},
	{LED_SYS_RED,56,"LED_SYS_RED",GPIO_OUT,GPIO_L},
	{TU_INT,496,"TU_INT",GPIO_IN,GPIO_NC},
	{PS_PAIR0,497,"PS_PAIR0",GPIO_IN,GPIO_NC},
	{USB_VDD_OC,498,"USB_VDD_OC",GPIO_IN,GPIO_NC},
	{SWITCH_INT,499,"SWITCH_INT",GPIO_IN,GPIO_NC},
	{TP118_IN,500,"TP118_IN",GPIO_IN,GPIO_NC},
	{TP119_IN,501,"TP119_IN",GPIO_IN,GPIO_NC},
	{TP120_IN,502,"TP120_IN",GPIO_IN,GPIO_NC},
	{TP121_IN,503,"TP121_IN",GPIO_IN,GPIO_NC},
	{LED_DIS,504,"LED_DIS",GPIO_OUT,GPIO_L},
	{PHY_VSC_RESET,505,"PHY_VSC_RESET",GPIO_OUT,GPIO_L},
	{USB_VDD_EN,506,"USB_VDD_EN",GPIO_OUT,GPIO_L},
	{SWITCH_RESET,507,"SWITCH_RESET",GPIO_OUT,GPIO_L},
	{BCM_RESET,508,"BCM_RESET",GPIO_OUT,GPIO_L},
	{TU_RESET,509,"TU_RESET",GPIO_OUT,GPIO_L},
	{TP116_OUT,510,"TP116_OUT",GPIO_OUT,GPIO_L},
	{TP117_OUT,511,"TP117_OUT",GPIO_OUT,GPIO_L},

};
#endif

#ifdef RR_PRODUCT_bk2
const struct gpio_pins_ gpio_pins[]={
	{SW_BUTTON,6,"SW_BUTTON",GPIO_IN,GPIO_NC},
	{I2C_EXP_INT,37,"I2C_EXP_INT",GPIO_IN,GPIO_NC},
	{SFP_INT,38,"SFP_INT",GPIO_IN,GPIO_NC},
	{PS_ALARM,39,"PS_ALARM",GPIO_IN,GPIO_NC},
	{WDG_STATUS,40,"WDG_STATUS",GPIO_IN,GPIO_NC},
	{WDG_REFRESH,41,"WDG_REFRESH",GPIO_OUT,GPIO_L},
	{LED_AIR_GRN,42,"LED_AIR_GRN",GPIO_OUT,GPIO_H},
	{LED_SYS_GRN,51,"LED_SYS_GRN",GPIO_OUT,GPIO_H},
	{LED_SYS_RED,56,"LED_SYS_RED",GPIO_OUT,GPIO_L},
	{TU_INT,496,"TU_INT",GPIO_IN,GPIO_NC},
	{PS_PAIR0,497,"PS_PAIR0",GPIO_IN,GPIO_NC},
	{USB_VDD_OC,498,"USB_VDD_OC",GPIO_IN,GPIO_NC},
	{SWITCH_INT,499,"SWITCH_INT",GPIO_IN,GPIO_NC},
	{PHY_INT,500,"PHY_INT",GPIO_IN,GPIO_NC},
	{TP68_IN,501,"TP68_IN",GPIO_IN,GPIO_NC},
	{TP99_IN,502,"TP99_IN",GPIO_IN,GPIO_NC},
	{TP100_IN,503,"TP100_IN",GPIO_IN,GPIO_NC},
	{LED_DIS,504,"LED_DIS",GPIO_OUT,GPIO_L},
	{PHY_RESET,505,"PHY_RESET",GPIO_OUT,GPIO_L},
	{USB_VDD_EN,506,"USB_VDD_EN",GPIO_OUT,GPIO_L},
	{SWITCH_RESET,507,"SWITCH_RESET",GPIO_OUT,GPIO_L},
	{TP63_OUT,508,"TP63_OUT",GPIO_OUT,GPIO_L},
	{TU_RESET,509,"TU_RESET",GPIO_OUT,GPIO_L},
	{SWITCH_P9_2500,510,"SWITCH_P9_2500",GPIO_OUT,GPIO_H},
	{SWITCH_P10_2500,511,"SWITCH_P10_2500",GPIO_OUT,GPIO_L},
};
#endif
//tato funkce je pouze pro puziti v preinitu a slouzi k vytvoreni /sys/class/gpio* adresaru
// nepouzivat !!!!
int gpioInit ()
{
	int ret;
	int i;
	//GPIO_PINS_ID i;
	for (i=0; i < GPIO_PINS_END; i++) {
		printf ("Init pin GPIO_%d\n",gpio_pins[i].pin);
		if ((ret= gpioCreate (gpio_pins[i].pin, gpio_pins[i].direction,gpio_pins[i].def_value)) < 0) {
			fprintf (stderr, "Error init GPIO pins (rows %d)\n",i);
			return ret;
		}
	}
	return 0;
};

int gpioPinGet (GPIO_PINS_ID id , GPIO_VALUE* value)
{
	if (id >=  GPIO_PINS_END)
		return -EINVAL;
	return gpioGetValue(gpio_pins[id].pin, value);
		
}		

int gpioPinSet (GPIO_PINS_ID id , GPIO_VALUE value)
{
	if (id >=  GPIO_PINS_END)
		return -EINVAL;
	if (gpio_pins[id].direction != GPIO_OUT)
		return -EINVAL;
	return gpioSetValue(gpio_pins[id].pin,value);
}

int gpioPulse(GPIO_PINS_ID id, GPIO_VALUE value, long t1, long t2, long count, long t0, bool last_t2_delay)
{
	if (id >=  GPIO_PINS_END)
		return -EINVAL;
	if (gpio_pins[id].direction != GPIO_OUT)
		return -EINVAL;
	return gpioSendPulse(gpio_pins[id].pin, value, t1, t2, count, t0, last_t2_delay);
}

GPIO_PINS_ID checkGpioPinsId (char *name)
{

//	GPIO_PINS_ID i;
	int i;
	for (i=0; i < GPIO_PINS_END; i++) {
		if (strcmp(name,gpio_pins[i].name) == 0)
			return (GPIO_PINS_ID)i;
	}
	return GPIO_PINS_END;
}
