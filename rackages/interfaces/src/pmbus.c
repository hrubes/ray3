#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include <linux/types.h>
#include <stdio.h>


#include "libpmbus.h"

typedef enum
{
	CMD_INVALID=0,
	CMD_SET,
	CMD_GET,
	CMD_INIT,
	CMD_STATUS,
	CMD_ENA
} CMD;

typedef enum
{
	FORMAT_INVALID=0,
	FORMAT_BYTE,
	FORMAT_WORD
} FORMAT;

typedef enum
{
	COMMAND_INVALID=0,
	COMMAND_VALID
} COMMAND;

struct cmd_ {
	CMD cmd;
	FORMAT data_format;
	int	data;
	COMMAND command_format;
	int	command;
};



struct option longopts[] = {
 /* { name  has_arg  *flag  val } */
    {"set",		1, NULL, 's'},	
    {"get",		1, NULL, 'g'},
	{"enable",		1, NULL, 'g'},
    {"byte",		0, NULL, 'b'},	
    {"word",		0, NULL, 'w'},
    {"init",		0, NULL, 'i'},
    {"status",		0, NULL, 't'},
	{"pmbusaddr",		1, NULL, 'p'},
    {"help", 		0, NULL, 'h'},	
    { 0, 0, NULL, 0 }
};

const char *usage =
"usage: pmbus [-hit] | [-e param] | [sg param [ -w word data | -b byte data]\n"
"	-s, --set	 		set command [ must be set -b or -w ]\n" 
"		PARAM:			PMBUS_command:data\n"
"	-g, --get			get command [ must be set -b or -w ]\n"
"		PARAM:			PMBUS_command:data\n"
"	-e, -enable			enable/disable Vout\n"
"		PARAM:			0 - disable; 1 enable\n"
"	-b, --byte			Byte data\n"
"	-w, --word			Word data\n"
"	-i  --init			Initialize the IR38060\n"
"	-t  --status			Get status IR38060\n"
"	-p  --pmbusaddr			PMBUS address (default is 0x40)\n"
"	-h  --help			help\n";

int main(int argc, char **argv)
{
	int ret=0;
	struct cmd_ command ={CMD_INVALID,FORMAT_INVALID,0,COMMAND_INVALID,0};
	struct dev_status status;
	int c=0;
	int address=MPBUS_ADDR;
	
	if (argc == 1) {
		printf ("%s",usage);
		return 0;
	}
	while ((c = getopt_long(argc, argv, "p:s:g:e:tiwb", longopts, 0)) != EOF) {
		switch (c) {
			case 's': 
				command.cmd=CMD_SET;
				if (sscanf(optarg,"%i:%i",&command.command,&command.data) == 2);
						command.command_format=COMMAND_VALID;
				break;
			case 'g': 
				command.cmd=CMD_GET;
				if (sscanf(optarg,"%i",&command.command) == 1);
						command.command_format=COMMAND_VALID;
				break;
			case 'e': 
				command.cmd=CMD_ENA;
				if (sscanf(optarg,"%i",&command.command) == 1);
						if ((command.command == 0) || (command.command ==1))
							command.command_format=COMMAND_VALID;
				break;
			case 'i': 
				command.cmd=CMD_INIT;
				break;
			case 't': 
				command.cmd=CMD_STATUS;
				break;
			case 'b': 
				command.data_format=FORMAT_BYTE;
				break;
			case 'w': 
				command.data_format=FORMAT_WORD;
				break;
			case 'p': 
				sscanf(optarg,"%i",&address);
				break;
			case 'h': 
				printf ("%s",usage);
				return 0;
				break;
		}
	}
	switch (command.cmd) {
		case CMD_STATUS:
			ret=get_status (address, &status);
			if (status.validity < 0) {
				printf ("GET_STATUS: ERROR\n");
			} else { 
				printf ("OPERATION=0x%02x\n",status.operation);
				switch (status.operation) {
					case 0x80:
						printf ("          ON\n");
						break;
					case 0x40:
						printf ("          Soft OFF\n");
						break;
					case 0x00:
						printf ("          Immediate OFF\n");
						break;
				}
				printf ("ON_OFF_CONFIG=0x%02x\n",status.on_off_config);
				switch (status.on_off_config) {
					case 0x1f:
						printf ("          Respond to Operation on/off and EN pin\n");
						break;
					case 0x1b:
						printf ("          Respond to Operation on/off only\n");
						break;
					case 0x17:
						printf ("          Respond to EN pin only\n");
						break;
					case 0x0:
						printf ("          Start when Power present\n");
						break;
				}
				printf ("STATUS_WORD=0x%04x\n",status.status_word);
					if (status.status_word & PB_STATUS_CML)
						printf ("          Communication/Memory/Logic fault\n");
					if (status.status_word & PB_STATUS_TEMPERATURE)
						printf ("          Temperature fault\n");
					if (status.status_word & PB_STATUS_VIN_UV)
						printf ("          Input Under-voltage fault\n");
					if (status.status_word & PB_STATUS_IOUT_OC)
						printf ("          Output over-current fault\n");
					if (status.status_word & PB_STATUS_VOUT_OV)
						printf ("          Output over-voltage fault\n");
					if (status.status_word & PB_STATUS_OFF)
						printf ("          Output off (due to fault or enable)\n");
					if (status.status_word & PB_STATUS_BUSY)
						printf ("          Device busy fault\n");
					if (status.status_word & PB_STATUS_POWER_GOOD_N)
						printf ("          Output power not good\n");
					if (status.status_word & PB_STATUS_INPUT)
						printf ("          Input under-voltage fault\n");
					if (status.status_word & PB_STATUS_IOUT_POUT)
						printf ("          Output over-current fault\n");
					if (status.status_word & PB_STATUS_VOUT)
						printf ("          Output high or low fault\n");
					if ((status.status_word & ~(PB_STATUS_WORD_MFR |  PB_STATUS_FANS | PB_STATUS_OTHER| PB_STATUS_UNKNOWN | PB_STATUS_NONE_ABOVE)) == 0)
						printf ("          OK\n");
				printf ("Vin =%.3fV\n",convertL11 (status.vin));
				printf ("Vout=%.3fV\n",convertL16 (status.vout));
				printf ("Iout=%.3fA\n",convertL11 (status.iout));
				printf ("Pout=%.3fW\n",convertL11 (status.pout));
				printf ("Temp=%.1fC\n",convertL11 (status.temp));
			}
			break;
		case CMD_INIT:
			printf ("Init IR38060: ");
			if ((ret=set_defalt (address)) < 0)
				printf ("ERROR\n");
			else
				printf ("OK\n");
			break;
		case CMD_GET:
			if ( command.command_format == COMMAND_INVALID) {
				printf ("Command has not been specified\n");
				break;
			}
			if ( command.data_format == FORMAT_INVALID) {
				printf ("Data has not been specified\n");
				break;
			} else if ( command.data_format == FORMAT_BYTE) {
				printf ("Get command 0x%02x  ",(__u8)command.command);
				if ((ret=get_command_byte (address,(__u8)command.command,(__u8 *) & command.data)) < 0 )
					printf (": ERROR\n");
				else
					printf (" data 0x%02x : OK\n", (__u8) 0xff & command.data);
						
			} else {
				printf ("Get command 0x%02x  ",(__u8)command.command);
				if ((ret=get_command_word (address,(__u8)command.command, (__u16 *) & command.data)) < 0 )
					printf (": ERROR\n");
				else
					printf (" data 0x%04x : OK\n", (__u16) 0xffff & command.data);
			}
			break;	
		case CMD_SET:
			if ( command.command_format == COMMAND_INVALID) {
				printf ("Command has not been specified\n");
				break;
			}
			if ( command.data_format == FORMAT_INVALID) {
				printf ("Data has not been specified\n");
				break;
			} else if ( command.data_format == FORMAT_BYTE) {
				printf ("Set command 0x%02x data 0x%02x : ",(__u8)command.command, (__u8) 0xff& command.data);
				if ((ret=set_command_byte (address,(__u8)command.command, (__u8) 0xff & command.data)) < 0 )
					printf ("ERROR\n");
				else
					printf ("OK\n");
						
			} else {
				printf ("Set command 0x%02x data 0x%04x : ",(__u8)command.command, (__u16) 0xffff& command.data);
				if ((ret=set_command_word (address,(__u8)command.command, (__u16) 0xffff & command.data)) < 0 )
					printf ("ERROR\n");
				else
					printf ("OK\n");
			}
			break;	
		case CMD_ENA:
			if ( command.command_format == COMMAND_INVALID) {
				printf ("Command has not been specified\n");
				break;
			}
			if (command.command == 0)
				printf ("Disable Vout ");
			else 
				printf ("Enable Vout ");
			if ((ret=enable_output (address, (int) command.command)) < 0)
				printf ("ERROR\n");
			else
				printf ("OK\n");
			break;
		default:
			printf ("%s",usage);
			break;
			
	}
	
	return ret;

}
