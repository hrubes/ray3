#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/types.h>
#include <stdio.h>


#include "sfp.h"
#include "libspisfp.h"
#include "sfp-print.h"

static void usage ()
{
#ifdef RR_PRODUCT_bma
	printf ("sfp param [value]\n");
	printf ("param 			status , tx, vdd \n");
	printf ("value 			enable/disable (only for tx and vdd) \n");
#else
	printf ("sfp device param [value]\n");
#ifdef RR_PRODUCT_bk2
	printf ("device 		SFP No (1 | 2 | 3)\n");
#else
	printf ("device 		SFP No (0 or 2)\n");
#endif
	printf ("param 			status , tx, vdd \n");
	printf ("value 			enable/disable (only for tx and vdd) \n");
#endif
}

int main(int argc, char *argv[])
{
	int ret;
	int dev_num;
	int enable = -1;
	STATUS_SFP status; 
#ifdef RR_PRODUCT_bma
	if ((argc < 2) || (argc > 3)) {
#else
	if ((argc < 3) || (argc > 4)) {
#endif
		usage();
		return -EINVAL;
	}
#ifdef RR_PRODUCT_bma
	dev_num =2;
#endif
#ifdef RR_PRODUCT_bk1
	dev_num = atoi(argv[1]);
	if (!((dev_num == 0) || (dev_num == 2))) {
		usage();
		return -EINVAL;
	}
#endif
#ifdef RR_PRODUCT_bk2
	dev_num = atoi(argv[1]);
	if ((dev_num < 1) || (dev_num > 3)) {
		usage();
		return -EINVAL;
	}
#endif
#ifdef RR_PRODUCT_bma
	if (strcmp ("status",argv[1]) == 0) {
#else
	if (strcmp ("status",argv[2]) == 0) {
#endif
		ret= getSfpStatus (dev_num, &status);
		if (ret < 0) {
			printf ("Error\n");
			return ret;
		}
		printSFPStatus  (&status);
		return 0;
	}
#ifdef RR_PRODUCT_bma
	if (argc == 3) {
		if (strcmp ("enable",argv[2]) == 0) { 
			enable=1;
		} else 	if (strcmp ("disable",argv[2]) == 0) {
			enable=0;
		}
		if (enable >= 0) {
			if (strcmp ("tx",argv[1]) == 0) {
				if (enable > 0)
					return setSfpTxDisable (dev_num,TX_DISABLE);
				else 
					return setSfpTxDisable (dev_num,TX_ENABLE);
			}
			if (strcmp ("vdd",argv[1]) == 0) {
#else
	if (argc == 4) {
		if (strcmp ("enable",argv[3]) == 0) { 
			enable=1;
		} else 	if (strcmp ("disable",argv[3]) == 0) {
			enable=0;
		}
		if (enable >= 0) {
			if (strcmp ("tx",argv[2]) == 0) {
				if (enable > 0)
					return setSfpTxDisable (dev_num,TX_DISABLE);
				else 
					return setSfpTxDisable (dev_num,TX_ENABLE);
			}
			if (strcmp ("vdd",argv[2]) == 0) {
#endif
				if (enable > 0)
					return setSfpVdd (dev_num,VDD_ENABLE);
				else 
					return setSfpVdd (dev_num,VDD_DISABLE);
			}
		}
	}
	usage();
	return -EINVAL;
}

