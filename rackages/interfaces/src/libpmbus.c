/***************************************************************************
    knihovna pro komunikaci s IR38060 prostrednictvim PMBUS zbernice
        
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <math.h> 

#include "i2c-dev.h"
#include "libpmbus.h"




static int open_device (int address)
{
		int ret = 0;
		int fd;
		if ((ret = open(I2C_DEV, O_RDWR))  < 0) {
				perror("Unable to open device node.");
				return ret;
		}
		fd=ret;
		if ((ret = ioctl(fd,I2C_SLAVE,address )) < 0) {
				perror("Could not set I2C_SLAVE.");
				return ret;
		}
		if( (ret = ioctl( fd, I2C_PEC, 0)) < 0) { 
				perror("Failed to enable PEC"); 
				return ret; 
		} 
		return fd;
}


static int pmbus_read_byte (int device, int command, __u8 * val)
{
		__s32 value = 0;

		if ((value = i2c_smbus_read_byte_data(device, command)) < 0) {
				perror("I2C Read byte operation failed.");
				return -EIO;
		}
		*val = value & 0xff;
		return 0;
}

static int pmbus_write_byte (int device, int command, __u8 val)
{
		__s32 value = 0;

		if ((value = i2c_smbus_write_byte_data(device, command,val)) < 0) {
				perror("I2C Write byte operation failed.");
				return -EIO;
		}
		return 0;
}

static int pmbus_read_word (int device, int command, __u16 * val)
{
		__s32 value = 0;

		if ((value = i2c_smbus_read_word_data(device, command)) < 0) {
				perror("I2C Read word operation failed.");
				return -EIO;
		}
		*val = value & 0xffff;
		return 0;
}

static int pmbus_write_word (int device, int command, __u16 val)
{
		__s32 value = 0;

		if ((value = i2c_smbus_write_word_data(device, command,val)) < 0) {
				perror("I2C Write word operation failed.");
				return -EIO;
		}
		return 0;
} 

static int close_device (int device, int ret)
{
	int ret1;
	if ((ret = close(device))  < 0) {
				perror("Unable to close device node.");
				return ret1;
	}
	return ret;
}

/*******************************************************************************/
int get_status (int address, dev_status * status)
{
		int ret ;
		int device;
		status->validity = -1;
		if ((ret=open_device (address)) < 0)
			return ret;
		device=ret;
		if ((ret=pmbus_read_byte (device,PMBUS_OPERATION , &status->operation)) < 0)
			return close_device (device, ret);
		if ((ret=pmbus_read_byte (device,PMBUS_ON_OFF_CONFIG , &status->on_off_config)) < 0)
			return close_device (device, ret);
		if ((ret=pmbus_read_word (device,PMBUS_STATUS_WORD, &status->status_word)) < 0)
			return close_device (device, ret);
		if ((ret=pmbus_read_word (device,PMBUS_READ_VIN , &status->vin)) < 0)
			return close_device (device, ret);
		if ((ret=pmbus_read_word (device,PMBUS_READ_VOUT , &status->vout)) < 0)
			return close_device (device, ret);
		if ((ret=pmbus_read_word (device,PMBUS_READ_IOUT , &status->iout)) < 0)
			return close_device (device, ret);
		if ((ret=pmbus_read_word (device,PMBUS_READ_POUT , &status->pout)) < 0)
			return close_device (device, ret);
		if ((ret=pmbus_read_word (device,PMBUS_READ_TEMPERATURE_1 , &status->temp)) < 0)
			return close_device (device, ret);
		status->validity = 0;
		return close_device (device, ret);
}

int get_command_byte (int address, int command, __u8  * val)
{
		int ret ;
		int device;
		
		if ((ret=open_device (address)) < 0)
			return ret;
		device=ret;
		ret=pmbus_read_byte (device,command , val);
		return close_device (device, ret);
}

int get_command_word (int address, int command, __u16  * val)
{
		int ret ;
		int device;
		
		if ((ret=open_device (address)) < 0)
			return ret;
		device=ret;
		ret=pmbus_read_word (device,command , val);
		return close_device (device, ret);
}

int set_command_byte (int address, int command, __u8  val)
{
		int ret ;
		int device;
		
		if ((ret=open_device (address)) < 0)
			return ret;
		device=ret;
		ret=pmbus_write_byte (device,command , val);
		return close_device (device, ret);
}

int set_command_word (int address, int command, __u16  val)
{
		int ret ;
		int device;
		
		if ((ret=open_device (address)) < 0)
			return ret;
		device=ret;
		ret=pmbus_write_word (device,command , val);
		return close_device (device, ret);
}

int set_defalt (int address)
{
		int ret ;
		int device;
		
		if ((ret=open_device (address)) < 0)
			return ret;
		device=ret;
		// set VOUT_COMMAND 0,9V
//		if ((ret=pmbus_write_word (device,PMBUS_VOUT_COMMAND , 0x00E6)) < 0 )
		if ((ret=pmbus_write_word (device,PMBUS_VOUT_COMMAND , 0x00F1)) < 0 )
				return close_device (device, ret);
		//on off SW
		if ((ret=pmbus_write_byte (device,PMBUS_ON_OFF_CONFIG , 0x1b)) < 0 )
				return close_device (device, ret);
		//set VOUT_MAX 1,5V
		if ((ret=pmbus_write_word (device,PMBUS_VOUT_MAX,0x0180)) < 0 )
				return close_device (device, ret);
		//set VOUT_TRANSITION_RATE 0V/us
		if ((ret=pmbus_write_word (device,PMBUS_VOUT_TRANSITION_RATE,0)) < 0 )
				return close_device (device, ret);
		//set VOUT_SCALE_LOOP=1
		if ((ret=pmbus_write_word (device,PMBUS_VOUT_SCALE_LOOP,0xe808)) < 0 )
				return close_device (device, ret);
		//set FREQUENCY_SWITCH 607MHz
 		if ((ret=pmbus_write_word (device,PMBUS_FREQUENCY_SWITCH,0x25f)) < 0 )
				return close_device (device, ret);
		//set VIN_ON 5V
		if ((ret=pmbus_write_word (device,PMBUS_VIN_ON,0xf80a)) < 0 )
				return close_device (device, ret);
		//set VIN_OFF0,5V
		if ((ret=pmbus_write_word (device,PMBUS_VIN_OFF,0xf80a)) < 0 )
				return close_device (device, ret);
		//set OV_FAULT_LIMIT 1,5V
		if ((ret=pmbus_write_word (device,PMBUS_VOUT_OV_FAULT_LIMIT,0x180)) < 0 )
				return close_device (device, ret);
		//set VOUT_OV_WARN_LIMIT 1,45V
		if ((ret=pmbus_write_word (device,PMBUS_VOUT_OV_WARN_LIMIT,0x173)) < 0 )
				return close_device (device, ret);
		//set VOUT_UV_WARN_LIMIT 1,35V
		if ((ret=pmbus_write_word (device,PMBUS_VOUT_UV_WARN_LIMIT,0x15a)) < 0 )
				return close_device (device, ret);
		//set VOUT_UV_FAULT_LIMIT 1,4V
		if ((ret=pmbus_write_word (device,PMBUS_VOUT_UV_FAULT_LIMIT,0x166)) < 0 )
				return close_device (device, ret);
		//set IOUT_OC_FAULT_LIMIT	9A
		if ((ret=pmbus_write_word (device,PMBUS_IOUT_OC_FAULT_LIMIT,0xf812)) < 0 )
				return close_device (device, ret);
		//set OT_FAULT_LIMIT 145C
		if ((ret=pmbus_write_word (device,PMBUS_OT_FAULT_LIMIT,0x91)) < 0 )
				return close_device (device, ret);
		//set POWER_GOOD_ON 0,7V
		if ((ret=pmbus_write_word (device,PMBUS_POWER_GOOD_ON,0x00b3)) < 0 )
				return close_device (device, ret);
		//set POWER_GOOD_OFF 0,65V
		if ((ret=pmbus_write_word (device,PMBUS_POWER_GOOD_OFF,0x00a6)) < 0 )
				return close_device (device, ret);
		//on/off (0x80/0x00
		ret=pmbus_write_byte (device,PMBUS_OPERATION , 0x00); 
		return close_device (device, ret);
		
}

int enable_output (int address, int ena)
{
	__u8 val= 0;
	int ret ;
	int device;
		
	if ((ret=open_device (address)) < 0)
		return ret;
	device=ret;
	if (ena != 0)
		val=0x80;
	//on/off (0x80/0x00
	ret=pmbus_write_byte (device,PMBUS_OPERATION , val); 
	return close_device (device, ret);
}

float convertL11 (__u16 value)
{
	__s8 exponent = (value>>11) & 0x1f;
	__s16 mantisa = value &  0x7ff;
	
	if (exponent > 0x0f)
			exponent |= 0xE0;
	
	if (mantisa > 0x03ff)
			mantisa |=0xf800;
	
	return mantisa * pow(2,exponent);
}


float convertL16 (__u16 value)
{
	
	__s8 exponent = -8;
	
	return value * pow(2,exponent);
}
