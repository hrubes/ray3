

#ifndef RRAIR_LIB_H_
#define RRAIR_LIB_H_

#include <stdbool.h>
#include <stdint.h>


#define MIISERVER_PORT 30000
#define MIISERVER_IP "172.16.1.100"
#define RR_IP "192.168.1.100"
#define SOM_PORT 5001
#define SOM_IP "10.190.5.41"



typedef enum {
	FAIL_E      = 0,
	SUCCESS_E   = 1
} RETURN_ENUM;

typedef enum {
	MEDIA_MODE_RR,
	MEDIA_MODE_DIRECT,
	MEDIA_MODE_SOM,
} MEDIA_MODE_E;



bool str_to_uint16(const char *str, uint16_t *res);
bool isValidIpAddress(const char *ipAddress);


bool isReadyWait(uint8_t device, uint32_t max_time_ms);

RETURN_ENUM RR_API_VersionGet(uint8_t device);
RETURN_ENUM RR_API_ComConfigure(uint8_t device, MEDIA_MODE_E mode, const char* host, uint16_t port);


#endif // RRAIR_LIB_H_
