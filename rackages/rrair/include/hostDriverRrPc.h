
#ifndef HOST_DRIVER_RR_PC_H
#define HOST_DRIVER_RR_PC_H

#include "hostDriver.h"

#define MAX_IP_LEN 16

int setIpAndPortOf650(uint8_t deviceIndex, const char* ip, uint16_t port);


#endif // HOST_DRIVER_RR_PC_H
