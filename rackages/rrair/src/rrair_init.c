// aplikace na inicializaci 85650 a 85820


#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <getopt.h>
//#include <errno.h>
#include <bcm_uw_api.h>

#include "hostDriverRrPc.h"
#include "rrair_lib.h"

#define PROGNAME "rrair_init"

// typedef enum {
//     FAIL_E      = 0,
//     SUCCESS_E   = 1
// } RETURN_ENUM;

// =======================================================================

//const size_t fn_size=100;
#define FN_SIZE 100
#define BOOTBUFF_SIZE (1024*1024)
const uint32_t max_wait_ms = 1000;

typedef enum {
    MULTI_E = 0,
    VER_E,
    HELP_E
} ACTION_ENUM;

struct Options_S {
	//ACTION_ENUM action;
	bool verbose;
	bool help;
	MEDIA_MODE_E media_mode;
        char host[MAX_IP_LEN];
	uint16_t port;
        char filename_fw[FN_SIZE];
        char filename_dc[FN_SIZE];
        char filename_ini[FN_SIZE];
        char filename_inirx[FN_SIZE];
        char path_agc[FN_SIZE];
	bool txrxstart;
	bool rfinit;
	bool modem_status;
	//bool bcmver;
};

void initOptions(struct Options_S* options)
{
	//options->action=VER_E;
	options->verbose=0;
	options->help=false;
	options->media_mode=MEDIA_MODE_RR;
	options->host[0]='\0';
	options->port=0;
	options->filename_fw[0]='\0';
	options->filename_dc[0]='\0';
	options->filename_ini[0]='\0';
	options->filename_inirx[0]='\0';
	options->path_agc[0]='\0';
	options->txrxstart = false;
	options->rfinit = false;
	options->modem_status = false;
	//options->bcmver = true;
}



int help(void)
{
	printf("Usage: " PROGNAME " [OPTIONS]\n");
	printf("Basic program for initializing 85650\n\n");
	printf("Without any parameter prints version of firmware in 85650\n");
	printf("  -m, --mode=MODE - selects connection mode\n");
	printf("                    MODE can be \"rr\" (default), \"direct\" or \"som\"\n");
	printf("                    note: som mode uses extra 4B header in communication protocol\n");
	printf("      --host <host_ip> - default rr: %s, direct: %s, som: %s\n", RR_IP, MIISERVER_IP, SOM_IP);
	printf("      --port <host_port> - default rr,direct: %d, som: %d\n", MIISERVER_PORT, SOM_PORT);
	printf("      --fw=bcm85650.fw - loads firmware (1)\n");
	printf("      --dc=<file>.dc - loads dc file (2)\n");
	printf("      --agc=<path> - path to AGC files (3)\n");
	printf("      --bin=<file>.bin - loads binary ini file (4)\n");
	printf("      --binrx=<file>.bin - loads binary ini file for assymetric rx channel (4)\n");
	printf("      --txrxstart - TxRxOperationStart (5)\n");
	printf("      --rfinit - RF_AllInitialize (6)\n");
	printf("      --modem_status - print modem status\n");
	
	//printf("  -v, --verbose\n");
	printf("  -h, --help - print this help\n");
	printf("Numbers in brackets shows proper ordering of the operations.\n\n");

	return SUCCESS_E;
}


// bool str_to_uint16(const char *str, uint16_t *res) {
// 	char *end;
// 	errno = 0;
// 	long val = strtol(str, &end, 10);
// 	if (errno || end == str || *end != '\0' || val < 0 || val >= UINT16_MAX) {
// 		return false;
// 	}
// 	*res = (uint16_t)val;
// 	return true;
// }


int parseParams(struct Options_S* options, int argc, char* const argv[])
{
	if (options==NULL) 
	{
		return FAIL_E;
	}

	// if (argc==2) {
	// 	options->fwload = true;
	// 	strncpy(options->filename, argv[1], FN_SIZE);
	// 	options->filename[FN_SIZE-1]=0;
	// }
	static struct option long_options_[] =
	{
		{"help", no_argument, 0, 'h'},
		//        {"debug", no_argument, 0, 'd'},
		//        {"version", no_argument, 0, 'v'},
		//        {"test", no_argument, 0, 't'},
		{"fw", required_argument, 0, 1},
		{"dc", required_argument, 0, 2},
		{"agc", required_argument, 0, 3},
		{"bin", required_argument, 0, 4},
		{"binrx", required_argument, 0, 5},
		{"txrxstart", no_argument, 0, 6},
		{"rfinit", no_argument, 0, 7},
		{"modem_status", no_argument, 0, 8},
		{"host", required_argument, 0, 9},
		{"port", required_argument, 0, 10},
		{"mode", required_argument, 0, 'm'},
		{0, 0, 0, 0}
	};
	static char short_options_[]="hm:";

	int option_index = 0;
	int opt;
	optind=0;

	while((opt = getopt_long(argc, argv, short_options_, long_options_, &option_index)) != -1) {
		switch (opt) {
			case 0: {
					//"getopt_long" nastavil primo promennou
					if (long_options_[option_index].flag != 0)
						break;
					switch (option_index)
					{
						default: {
								 fprintf(stderr, "Option parsing - strange option.\n"); //k tomu by nemelo dojit
								 return FAIL_E;
							 }
					}
					break;
				}
			case 1: {
					strncpy(options->filename_fw, optarg, FN_SIZE);
					options->filename_fw[FN_SIZE-1]='\0';
					break;
				}
			case 2: {
					strncpy(options->filename_dc, optarg, FN_SIZE);
					options->filename_dc[FN_SIZE-1]='\0';
					//options->bcmver = false;
					break;
				}
			case 3: {
					strncpy(options->path_agc, optarg, FN_SIZE);
					options->path_agc[FN_SIZE-1]='\0';
					//options->bcmver = false;
					break;
				}
			case 4: {
					strncpy(options->filename_ini, optarg, FN_SIZE);
					options->filename_ini[FN_SIZE-1]='\0';
					//options->bcmver = false;
					break;
				}
			case 5: {
					strncpy(options->filename_inirx, optarg, FN_SIZE);
					options->filename_inirx[FN_SIZE-1]='\0';
					//options->bcmver = false;
					break;
				}
			case 6: {
					options->txrxstart = true;
					//options->bcmver = false;
					break;
				}
			case 7: {
					options->rfinit = true;
					//options->bcmver = false;
					break;
				}
			case 8: {
					options->modem_status = true;
					break;
				}
			case 9: { // host ip
					strncpy(options->host, optarg, MAX_IP_LEN);
					options->host[MAX_IP_LEN-1]='\0';
					if (!isValidIpAddress(options->host)) {
						fprintf(stderr, "Option parsing: parameter --host has invalid value: %s\n", optarg);
						options->host[0]='\0';
						return FAIL_E;
					}
					break;
				}
			case 10: { // host port
					 if (!str_to_uint16(optarg, &options->port)) {
						fprintf(stderr, "Option parsing: parameter --port has invalid value: %s\n", optarg);
						return FAIL_E;
					 }
					 break;
				 }
			case 'm':
				if (optarg[0]=='d') {
					options->media_mode=true;
				} 
				break;
			case 'h':
				options->help = true;
				break;
			default:
				options->help = true;
				return FAIL_E;

		}
	}


	if ((options->filename_inirx[0]) && (!options->filename_ini[0]))
	{
		fprintf(stderr, "Option parsing: missing --ini parameter\n");
		return FAIL_E;
	}

	return SUCCESS_E;
}


size_t fileToBuff(const char* filename, BYTE* buffer, size_t buffer_size)
{
	FILE *fd;
	size_t len;
	fd = fopen(filename, "rb");
	if (fd == NULL)
	{
		fprintf(stderr, "File \"%s\" not found\n", filename);
		return(0);
	}

	fseek(fd, 0, SEEK_END); // seek to end of file
	len = ftell(fd); // get current file pointer
	fseek(fd, 0, SEEK_SET);  // go back to begin
	
	if (len>buffer_size) {
		fprintf(stderr, "File \"%s\" is too large\n", filename);
		fclose(fd);
		return(0);
	}

	len = fread(buffer, sizeof(BYTE), buffer_size, fd);
	fclose(fd);
	return len;
}


// bool isReadyWait(uint8_t device, uint32_t max_time_ms)
// {
// 	BCM_UW_ERROR_MSG_ENUM retVal;
// 	BCM_UW_MODEM_VERSION_STRUCT pVer;
// 
// 	uint32_t sleep_time = 100*1000;
// 	uint32_t total_time = 0;
// 	uint32_t max_time = max_time_ms*1000;
// 
// 	for (total_time=0; total_time<=max_time; total_time+=sleep_time)
// 	{
// 		retVal = BCM_UW_API_VersionGet (device, &pVer);
// 		if (retVal == BCM_UW_SUCCESS_MSG_E)
// 		{
// 			if (total_time>0) {
// 				fprintf(stderr, "Waited %f s\n", (double)total_time/(1000*1000));
// 			}
// 			return true;
// 		}
// 		bcmUwHostSleep(sleep_time); 
// 	}
// 
// 	fprintf(stderr, "Device %d is not ready\n", device);
// 	return false;
// }

// =======================================================================
// =======================================================================
// ===== BCM_UW_API ======================================================
// =======================================================================
// =======================================================================


// RETURN_ENUM RR_API_VersionGet(uint8_t device)
// {
// 	BCM_UW_ERROR_MSG_ENUM retVal;
// 	BCM_UW_MODEM_VERSION_STRUCT pVer;
// 
// 	retVal = BCM_UW_API_VersionGet (device, &pVer);
// 	if (retVal != BCM_UW_SUCCESS_MSG_E)
// 	{
// 		fprintf(stderr, "API_VersionGet - Err no: %d\n", retVal);
// 		return(FAIL_E);
// 	}
// 	printf("650 fw version (device=%d) = %d.%d.%d\n", device, pVer.majorVersion, pVer.minorVersion, pVer.buildVersion);
// 
// 	return(SUCCESS_E);
// }

RETURN_ENUM RR_API_FirmwareFromHostBoot(uint8_t device, const char* mcfwfile)
{
	BCM_UW_ERROR_MSG_ENUM       status;
	UINT32                      len;
	BYTE bootBuff[ BOOTBUFF_SIZE ]={0};

	len = fileToBuff(mcfwfile, bootBuff, BOOTBUFF_SIZE);
	if (len==0)
	{
		return FAIL_E;
	}

	status = BCM_UW_API_FirmwareFromHostBoot(device, bootBuff, len, BCM_UW_DEVICE_RESET_ALL_E);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_FirmwareFromHostBoot - Error no: %d\n", status);
		return(FAIL_E);
	}

	printf("API_FirmwareFromHostBoot - Done\n");
	return(SUCCESS_E);
}

RETURN_ENUM RR_API_DeviceConfigure(uint8_t device, const char* dcfile)
{
	BCM_UW_ERROR_MSG_ENUM       status;
	UINT32                      len;
	BYTE pBuff[ BOOTBUFF_SIZE ]={0};
	BCM_UW_DEVCONF_STATUS_STRUCT dc_ret;
	int ii;

	len = fileToBuff(dcfile, pBuff, BOOTBUFF_SIZE);
	if (len==0)
	{
		return FAIL_E;
	}

	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	status = BCM_UW_API_DeviceConfigure(device, pBuff, len, &dc_ret);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_DeviceConfigure - Error no: %d\n", status);
		return FAIL_E;
	}

    //errors
	fprintf(stderr,"pllSysIsErr: %s\n", (dc_ret.pllSysIsErr==0)?"OK":"Error");
	fprintf(stderr,"pllCpuIsErr: %s\n", (dc_ret.pllCpuIsErr==0)?"OK":"Error");
	fprintf(stderr,"pllEdbIsErr: %s\n", (dc_ret.pllEdbIsErr==0)?"OK":"Error");
	fprintf(stderr,"pllAuxIsErr: %s\n", (dc_ret.pllAuxIsErr==0)?"OK":"Error");
	for (ii=0; ii<3; ++ii) {
		fprintf(stderr,"pllDacIsErr[%d]: %s\n", ii, (dc_ret.pllDacIsErr[ii]==0)?"OK":"Error");
	}
	for (ii=0; ii<6; ++ii) {
		fprintf(stderr,"pllAdcIsErr[%d]: %s\n", ii, (dc_ret.pllAdcIsErr[ii]==0)?"OK":"Error");
	}
	fprintf(stderr,"versionMismatchErr: %s\n", (dc_ret.versionMismatchErr==0)?"OK":"Error");
	fprintf(stderr,"pllSys: %d MHz\n", dc_ret.pllSys);
	fprintf(stderr,"pllCpu: %d MHz\n", dc_ret.pllCpu);

	printf("API_DeviceConfigure - Done\n");
	return(SUCCESS_E);
}

RETURN_ENUM RR_API_ConfigFileFromHostLoad(uint8_t device, uint8_t scIndex, const char* inifile, const char* inirxfile)
{
	BCM_UW_ERROR_MSG_ENUM       status;
	UINT32                      len=0, lenRx=0;
	BYTE pBuff[ BOOTBUFF_SIZE ]={0};
	BYTE pBuffRx[ BOOTBUFF_SIZE ]={0};

	len = fileToBuff(inifile, pBuff, BOOTBUFF_SIZE);
	if (len==0)
	{
		return FAIL_E;
	}
	if (inirxfile[0])
	{
		lenRx = fileToBuff(inirxfile, pBuffRx, BOOTBUFF_SIZE);
		if (lenRx==0)
		{
			return FAIL_E;
		}
	}

	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}


	if (inirxfile[0])
	{ // assymetric
		//DLLEXPORT BCM_UW_ERROR_MSG_ENUM BCM_UW_API_ConfigFileExFromHostLoad (BYTE deviceIndex, BYTE scIndex, BYTE *pTxBin, UINT32 lenTx, BYTE *pRxBin, UINT32 lenRx);
		status = BCM_UW_API_ConfigFileExFromHostLoad(device, scIndex, pBuff, len, pBuffRx, lenRx);
	} else {
		//DLLEXPORT BCM_UW_ERROR_MSG_ENUM BCM_UW_API_ConfigFileFromHostLoad (BYTE deviceIndex, BYTE scIndex, BYTE *pBuffer, UINT32 len);
		status = BCM_UW_API_ConfigFileFromHostLoad(device, scIndex, pBuff, len);
	}
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_ConfigFileFromHostLoad - Error no: %d\n", status);
		return(FAIL_E);
	}

	printf("API_ConfigFileFromHostLoad - Done\n");
	return(SUCCESS_E);
}

RETURN_ENUM RR_API_TxRxOperationStart(uint8_t device, uint8_t scIndex)
{
	BCM_UW_ERROR_MSG_ENUM       status;

	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	status = BCM_UW_API_TxRxOperationStart(device, scIndex);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_TxRxOperationStart - Error no: %d\n", status);
		return(FAIL_E);
	}

	printf("API_TxRxOperationStart - Done\n");
	return(SUCCESS_E);
}

RETURN_ENUM RR_API_RF_AllInitialize(uint8_t device)
{
	BCM_UW_ERROR_MSG_ENUM       status;
	BCM_UW_RF_ALL_INITIALIZE_STRUCT initData;
	//initData.rxFrequency0Khz=17100000;
	initData.rxFrequency0Khz=17700000;
	initData.rxFrequency1Khz=0;
	//initData.txFrequency0Khz=17100000;
	initData.txFrequency0Khz=17700000;
	initData.txFrequency1Khz=0;
	initData.targetPowerDbmTenths=0;

	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	fprintf(stderr, "rxFrequency0Khz: %d\n", initData.rxFrequency0Khz);
	fprintf(stderr, "rxFrequency1Khz: %d\n", initData.rxFrequency1Khz);
	fprintf(stderr, "txFrequency0Khz: %d\n", initData.txFrequency0Khz);
	fprintf(stderr, "txFrequency1Khz: %d\n", initData.txFrequency1Khz);
	// BCM_UW_API_RF_AllInitialize
	status = BCM_UW_API_RF_AllInitialize(device, &initData);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_RF_AllInitialize - Error no: %d\n", status);
		return(FAIL_E);
	}

	printf("API_RF_AllInitialize - Done\n");
	return(SUCCESS_E);
}

RETURN_ENUM RR_API_WbRxFreqSet(uint8_t device)
{
	BCM_UW_ERROR_MSG_ENUM       status;
	BCM_UW_WB_RX_NAME_ENUM wbRxIndex = BCM_UW_WB_RX_0_E;
	INT32 ncoFreqKHz = -18000;

	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	// BCM_UW_API_WbRxFreqSet
	status = BCM_UW_API_WbRxFreqSet(device, wbRxIndex, ncoFreqKHz);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_WbRxFreqSet - Error no: %d\n", status);
		return(FAIL_E);
	}

	printf("API_WbRxFreqSet - Done\n");
	return(SUCCESS_E);
}

RETURN_ENUM RR_API_WbTxTxCalibration(uint8_t device)
{
	BCM_UW_ERROR_MSG_ENUM status;
	BCM_UW_CALIB_STATUS_ENUM calibStatus;
	BCM_UW_WB_TX_NAME_ENUM wbTxIndex = BCM_UW_WB_TX_0_E;
	int timeElapsed = 0;
	const int timeWait = 1; //s

	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	// BCM_UW_API_WbTxTxCalibrationStart
	status = BCM_UW_API_WbTxTxCalibrationStart (device, wbTxIndex);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_WbTxTxCalibrationStart - Error no: %d\n", status);
		return(FAIL_E);
	}

	for (timeElapsed=0; timeElapsed<=30; timeElapsed+=timeWait)
	{
		// BCM_UW_API_WbTxTxCalibrationStatusGet
		status = BCM_UW_API_WbTxTxCalibrationStatusGet (device, wbTxIndex, &calibStatus);
		if (status != BCM_UW_SUCCESS_MSG_E)
		{
			fprintf(stderr, "API_WbTxTxCalibrationStatusGet - Error no: %d\n", status);
			return(FAIL_E);
		}

		switch (calibStatus) {
			case BCM_UW_CALIB_DISABLED_E: //0
                        	fprintf (stderr, "TxTx%d calibration disabled\n", wbTxIndex);
				return FAIL_E;
			case BCM_UW_CALIB_IN_PROCESS_E: //1
                        	fprintf (stderr, "TxTx%d calibration still in process\n", wbTxIndex);
				break;
			case BCM_UW_CALIB_SUCCESS_CALIB_E: //2
                        	fprintf (stderr, "TxTx%d calibration success\n", wbTxIndex);
				return SUCCESS_E;
			case BCM_UW_CALIB_NO_CORRELATION_FAILURE_E: //3
                        	fprintf (stderr, "TxTx%d calibration No Correlation Failure\n", wbTxIndex);
				return FAIL_E;
			case BCM_UW_CALIB_TIME_OUT_FAILURE_E: //4
                        	fprintf (stderr, "TxTx%d calibration time out\n", wbTxIndex);
				return FAIL_E;
			case BCM_UW_CALIB_WAITING_TO_START_E: //5
                        	fprintf (stderr, "TxTx%d calibration Waiting to Start\n", wbTxIndex);
				return FAIL_E; // Broadcom, to v tomto pripade ukoncuje
		}

		bcmUwHostSleep(timeWait*1000*1000); // 1s
	}
        fprintf (stderr, "TxTx%d calibration time out\n", wbTxIndex);
	return(FAIL_E);
}


RETURN_ENUM RR_API_AdpdCalibration(uint8_t device, uint8_t scIndex)
{
	BCM_UW_ERROR_MSG_ENUM status;
	BCM_UW_CALIB_STATUS_ENUM calibStatus;
	BCM_UW_ADPD_MODE_ENUM adpdMode = BCM_UW_ADPD_MODE_TRACKING_E;
	int timeElapsed = 0;
	const int timeWait = 5; //s

	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	// BCM_UW_API_AdpdCalibrationStart
	status = BCM_UW_API_AdpdCalibrationStart (device, scIndex);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_AdpdCalibrationStart - Error no: %d\n", status);
		return(FAIL_E);
	}

	// timout 4 minutes
	for (timeElapsed=0; timeElapsed<=240; timeElapsed+=timeWait)
	{
		// BCM_UW_API_AdpdCalibrationStatusGet
		status = BCM_UW_API_AdpdCalibrationStatusGet (device, scIndex, &calibStatus);
		if (status != BCM_UW_SUCCESS_MSG_E)
		{
			fprintf(stderr, "API_AdpdCalibrationStatusGet - Error no: %d\n", status);
			return(FAIL_E);
		}

		switch (calibStatus) {
			case BCM_UW_CALIB_DISABLED_E: //0
                        	fprintf (stderr, "ADPD%d calibration disabled\n", scIndex);
				return FAIL_E;
			case BCM_UW_CALIB_IN_PROCESS_E: //1
                        	fprintf (stderr, "ADPD%d calibration still in process\n", scIndex);
				break;
			case BCM_UW_CALIB_SUCCESS_CALIB_E: //2
                        	fprintf (stderr, "ADPD%d calibration success\n", scIndex);
				status = BCM_UW_API_AdpdModeSet(device, scIndex, adpdMode);
				if (status != BCM_UW_SUCCESS_MSG_E)
				{
					fprintf(stderr, "API_AdpdModeSet - Error no: %d\n", status);
					return(FAIL_E);
				}
				fprintf(stderr, "API_AdpdModeSet - tracking mode - Done\n");
				return SUCCESS_E;
			case BCM_UW_CALIB_NO_CORRELATION_FAILURE_E: //3
                        	fprintf (stderr, "ADPD%d calibration No Correlation Failure\n", scIndex);
				return FAIL_E;
			case BCM_UW_CALIB_TIME_OUT_FAILURE_E: //4
                        	fprintf (stderr, "ADPD%d calibration time out\n", scIndex);
				return FAIL_E;
			case BCM_UW_CALIB_WAITING_TO_START_E: //5
                        	fprintf (stderr, "ADPD%d calibration Waiting to Start\n", scIndex);
				return FAIL_E; // Broadcom, to v tomto pripade ukoncuje
		}

		bcmUwHostSleep(timeWait*1000*1000); // 5s
	}
        fprintf (stderr, "ADPD%d calibration time out\n", scIndex);
	return(FAIL_E);
}



RETURN_ENUM RR_API_ModemStatusGet(uint8_t device, uint8_t scIndex)
{
	BCM_UW_ERROR_MSG_ENUM       status;
	BCM_UW_MODEM_STATUS_STRUCT pMs;	

	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	status = BCM_UW_API_ModemStatusGet(device, scIndex, &pMs);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_ModemStatusGet - Error no: %d\n", status);
		return FAIL_E;
	}

	const static char* BCM_UW_ACQUIRE_STATUS_ENUM_str[] = {
		"ACQUIRE_IN_PROGRESS_E",
		"ACQUIRE_LOCKED_E",
		"ACQUIRE_FAILED_E",
		"ACQUIRE_NOT_ACTIVATED_E",
		"invalid",
		0
	};
	const static char* BCM_UW_ACQUIRE_LAST_ERR_ENUM_str[] = {
		"BCM_UW_ACQUIRE_SUCCESS_E",
		"BCM_UW_ACQUIRE_ERR_INT_PREAMBLE_LOSS_E",
		"BCM_UW_ACQUIRE_ERR_INT_BIT_LOSS_E",
		"BCM_UW_ACQUIRE_ERR_FEC_ULOCKED_TIMOUT_E",
		"BCM_UW_ACQUIRE_ERR_AT_FRAME_0_SYNC_E",
		"BCM_UW_ACQUIRE_ERR_TIMEOUT_E",
		"BCM_UW_ACQUIRE_ERR_AT_FRAME_2_SYNC_E",
		"BCM_UW_ACQUIRE_ERR_FATAL_E",
		"invalid",
		0
	};
	const static char* BCM_UW_IND_PREAMBLE_STATE_BIT_E_str[] = {"unlocked", "verify", "locked", "invalid", 0};
	const static char* networkStatus_str[] =  {"Not Locked", "Locked", "N/R = PRBS mode", "invalid", 0};

	fprintf(stderr,"acquireStatus: %s\n", BCM_UW_ACQUIRE_STATUS_ENUM_str[(pMs.acquireStatus<4)?pMs.acquireStatus:4]);
	fprintf(stderr,"lastAcquireError: %s\n", BCM_UW_ACQUIRE_LAST_ERR_ENUM_str[(pMs.lastAcquireError<8)?pMs.lastAcquireError:8]);
	fprintf(stderr,"absoluteMseTenths: %d [dB/10]\n", pMs.absoluteMseTenths);
	fprintf(stderr,"normalizedMseTenths: %d [dB/10]\n", pMs.normalizedMseTenths);
	fprintf(stderr,"radialMseTenths: %d [dB/10]\n", pMs.radialMseTenths);
	fprintf(stderr,"internalAgc: %d [dB]\n", pMs.internalAgc);
	fprintf(stderr,"externalAgcRegister: %d\n", pMs.externalAgcRegister);
	fprintf(stderr,"carrierOffset: %d [Hz]\n", pMs.carrierOffset);
	fprintf(stderr,"rxSymbolRate: %d [Baud]\n", pMs.rxSymbolRate);
	fprintf(stderr,"txSymbolRate: %d [Baud]\n", pMs.txSymbolRate);
	fprintf(stderr,"ldpcDecoderStress: %d (fraction in range [0,1])\n", pMs.ldpcDecoderStress);
	fprintf(stderr,"txAcmProfile: ACM_PROFILE_%d_E\n", pMs.txAcmProfile);
	fprintf(stderr,"rxAcmProfile: ACM_PROFILE_%d_E\n", pMs.rxAcmProfile);
	fprintf(stderr,"acmEngineRxSensorsEnabled: %s\n", (pMs.acmEngineRxSensorsEnabled==TRUE)?"enabled":"disabled");
	fprintf(stderr,"acmEngineTxSwitchEnabled: %s\n", (pMs.acmEngineTxSwitchEnabled==TRUE)?"enabled":"disabled");
	BYTE val=pMs.debugIndications&1;
	fprintf(stderr,"debugIndications.RX_TIMING: %d=%s\n", val, val?"lock":"unlock");
	val = (pMs.debugIndications>>BCM_UW_IND_PREAMBLE_STATE_BIT_E)&3;
	fprintf(stderr,"debugIndications.PREAMBLE_STATE: %d=%s\n", val, BCM_UW_IND_PREAMBLE_STATE_BIT_E_str[val]);
	val = (pMs.debugIndications>>BCM_UW_IND_LDPC_EN_BIT_E)&1;
	fprintf(stderr,"debugIndications.LDPC_EN: %d=%s\n", val, val?"enable":"RS only mode");
	val = (pMs.debugIndications>>BCM_UW_IND_RS_UNLOCK_BIT_E)&1;
	fprintf(stderr,"debugIndications.RS_UNLOCK: %d=%s\n", val, val?"lock":"unlock");
	val = (pMs.debugIndications>>BCM_UW_IND_LDPC_UNLOCK_BIT_E)&1;
	fprintf(stderr,"debugIndications.LDPC_UNLOCK: %d=%s\n", val, val?"lock":"unlock");
	val = (pMs.debugIndications>>BCM_UW_IND_LDPC_UNCOR_BIT_E)&1;
	fprintf(stderr,"debugIndications.LDPC_UNCOR: %d=%s\n", val, val?"corrected":"uncorrected");
	val = (pMs.debugIndications>>BCM_UW_IND_RS_UNCOR_BIT_E)&1;
	fprintf(stderr,"debugIndications.RS_UNCOR: %d=%s\n", val, val?"corrected":"uncorrected");
	fprintf(stderr,"resPhNoiseVal: 0x%08x\n", pMs.resPhNoiseVal);
	fprintf(stderr,"aafGain: %d [dB/10] (N/A 650)\n", pMs.aafGain);
	fprintf(stderr,"timingSnr: %d [dB/10]\n", pMs.timingSnr);
	fprintf(stderr,"networkStatus: %s\n", networkStatus_str[(pMs.networkStatus<3)?pMs.networkStatus:3]);
	fprintf(stderr,"txCarrierOffset: %d [Hz]\n", pMs.txCarrierOffset);

	printf("API_ModemStatusGet - Done\n");
	return(SUCCESS_E);
}

RETURN_ENUM RR_GeneralInit(uint8_t device, uint8_t scIndex)
{
	BCM_UW_ERROR_MSG_ENUM retVal;
	CPU_TYPE_ENUM cpu = MC_E; // nejspis se ignoruje

	fprintf(stderr,"Some messing with registers for bcm650 kit (rx,tx voltage)\n");
	//DLLEXPORT BCM_UW_ERROR_MSG_ENUM BCM_UW_API_RegisterWrite (BYTE deviceIndex, BYTE scIndex, UINT32 offset, CPU_TYPE_ENUM cpu, UINT16 val);


        // ===============    RX Configuration   ============================
        // RX LDO 5v - set nominal voltage "bcm85650_auxafe_sdc_shdw6_reg"
	retVal = BCM_UW_API_RegisterWrite (device, scIndex, 16654, cpu, 0x202);

        // RX LDO 3.3v - set nominal voltage "bcm85650_auxafe_sdc_shdw5_reg"
	retVal = BCM_UW_API_RegisterWrite (device, scIndex, 16653, cpu, 0x17b);

        //===============    TX Configuration   =============================
        // TX LDO 3.3v - set nominal voltage "bcm85650_auxafe_sdc_shdw0_reg"
	retVal = BCM_UW_API_RegisterWrite (device, scIndex, 16648, cpu, 0x13d);

        // TX LDO 5v - set nominal voltage "bcm85650_auxafe_sdc_shdw1_reg"
	retVal = BCM_UW_API_RegisterWrite (device, scIndex, 16649, cpu, 0x21e);

        // TX LDO 2.5v - set nominal voltage "bcm85650_auxafe_sdc_shdw2_reg"
	retVal = BCM_UW_API_RegisterWrite (device, scIndex, 16650, cpu, 0x13b);

        // TX LDO 4.3v - set nominal voltage "bcm85650_auxafe_sdc_shdw3_reg"
	retVal = BCM_UW_API_RegisterWrite (device, scIndex, 16651, cpu, 0x124);

	return SUCCESS_E;
}


RETURN_ENUM RR_AGC_load(uint8_t device, const char* agc_path)
{
	// cteno z dc souboru
	int adc_mode[] = {1,6,1,6,1,1}; //adcX_mode, kde X=0..5
	int adcToCh7 = 1; // wb_rx_env_0_adc // Chan_adc(device,6)
	int adcToCh8 = 3; // wb_rx_env_1_adc // Chan_adc(device,7)
	int X1;
	UINT32 len;
	BCM_UW_ERROR_MSG_ENUM       status;
	BCM_UW_WB_RX_SOURCE_NAME_ENUM adcIndex;
	BCM_UW_WB_RXAGC_GAIN_DIST_TABLE_STRUCT rxAgcGDTable;
	BCM_UW_WB_AGC_DB_TO_DAC_TABLE_STRUCT rxAgcDB2DACTable;

	char filename[FN_SIZE];

	snprintf(filename, FN_SIZE, "%s/%s", agc_path, "RX_IQ_policy_table.bin");
	filename[FN_SIZE-1]=0;
	len = fileToBuff(filename, (BYTE*)&rxAgcGDTable, sizeof(rxAgcGDTable));
	if (len<sizeof(rxAgcGDTable))
	{
		fprintf(stderr, "RR_AGC_load - Error reading file \"%s\", readed %d B, needed %u B\n", filename, len, sizeof(rxAgcGDTable));
		return FAIL_E;
	}

	snprintf(filename, FN_SIZE, "%s/%s", agc_path, "RX_IQ_dBtoDac.bin");
	filename[FN_SIZE-1]=0;
	len = fileToBuff(filename, (BYTE*)&rxAgcDB2DACTable, sizeof(rxAgcDB2DACTable));
	if (len<sizeof(rxAgcDB2DACTable)) 
	{
		fprintf(stderr, "RR_AGC_load - Error reading file \"%s\", readed %d B, needed %u B\n", filename, len, sizeof(rxAgcDB2DACTable));
		return FAIL_E;
	}

	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	int ii;
	for (ii=0; ii<6; ++ii)
	{
		if ((adcToCh7 == ii) || (adcToCh8 == ii))
			{continue;}
		X1 = adc_mode[ii];//ADC_mode(device, ii);
		if ( (X1==1) || (X1==5) ) 
		{
			adcIndex = (BCM_UW_WB_RX_SOURCE_NAME_ENUM)(ii*3+0);
			//GainDistributionTable="RX_IQ_policy_table.bin"
			//DbToDacTable="RX_IQ_dBtoDac.bin"
			status = BCM_UW_API_WbRxAgcGainDistributionTableSet (device, adcIndex, &rxAgcGDTable);
			if (status != BCM_UW_SUCCESS_MSG_E)
			{
				fprintf(stderr, "API_WbRxAgcGainDistributionTableSet - Error no: %d, index %d\n", status, adcIndex);
				return FAIL_E;
			}
			status =  BCM_UW_API_WbRxAgcDbToDacTableSet (device, adcIndex, &rxAgcDB2DACTable);
			if (status != BCM_UW_SUCCESS_MSG_E)
			{
				fprintf(stderr, "API_WbRxAgcDbToDacTableSet - Error no: %d, index %d\n", status, adcIndex);
				return FAIL_E;
			}
			fprintf(stderr, "RR_AGC_load - IQ tables loaded at index %d\n", adcIndex);
		}
		if ( (X1==2) || (X1==3) || (X1==4)) 
		{
			adcIndex = (BCM_UW_WB_RX_SOURCE_NAME_ENUM)(ii*3+1);
			//GainDistributionTable="RX_IF_policy_table.bin"
			//DbToDacTable="RX_IF_dBtoDac.bin"
			fprintf(stderr, "RR_AGC_load - IF tables not implemented (index %d)\n", adcIndex);
			return FAIL_E;
		}
		if ( (X1==6) || (X1==3) || (X1==4)) 
		{
			adcIndex = (BCM_UW_WB_RX_SOURCE_NAME_ENUM)(ii*3+2);

			//GainDistributionTable="RX_IF_policy_table.bin"
			//DbToDacTable="RX_IF_dBtoDac.bin"
			fprintf(stderr, "RR_AGC_load - IF tables not implemented (index %d)\n", adcIndex);
			return FAIL_E;
		}
	}
	return SUCCESS_E;
}

// RETURN_ENUM RR_API_ComConfigure(uint8_t device, const struct Options_S* options)
// {
// 	BCM_UW_ERROR_MSG_ENUM retVal;
// 	BCM_UW_COM_MEDIA_ENUM media = BCM_UW_COM_MEDIA_MII_DEVICE_E;
// 	BCM_UW_COM_MEDIA_ENUM boot_media = BCM_UW_COM_MEDIA_NULL_E;
// 	uint16_t port=0;
//         char host[MAX_IP_LEN];
//         host[0]='\0';
// 	
// 	if (options->media_mode) {
// 		media = BCM_UW_COM_MEDIA_MII_DEVICE_E;
// 		//setIpAndPortOf650(device, MIISERVER_IP, MIISERVER_PORT);
// 		strncpy(host, MIISERVER_IP, MAX_IP_LEN);
// 		port=MIISERVER_PORT;
// 	} else {
// 		media = BCM_UW_COM_MEDIA_IP_CLIENT_E;
// 		//setIpAndPortOf650(device, SOM_IP, SOM_PORT);
// 		strncpy(host, SOM_IP, MAX_IP_LEN);
// 		port=SOM_PORT;
// 	}
// 
// 	if (options->port) {
// 		port=options->port;
// 	}
// 	if (options->host[0]) {
// 		strncpy(host, options->host, MAX_IP_LEN);
// 	}
// 	host[MAX_IP_LEN-1]='\0';
// 
// 	setIpAndPortOf650(device, host, port);
// 
// 	//call API
// 	retVal = BCM_UW_API_ComConfigure(device, media, boot_media, 50L * 1000000L);
// 	if (retVal != BCM_UW_SUCCESS_MSG_E)
// 	{
// 		fprintf(stderr, "API_ComConfigure - Error no: %d\n", retVal);
// 		return(FAIL_E);
// 	}
// 	printf("API_ComConfigure - Done\n");
// 	return(SUCCESS_E);
// }

// =======================================================================

int main(int argc, char *argv[])
{
	int retVal;
	uint8_t device = 0;
	uint8_t scIndex = 0;
	bool api_open = false;
	struct Options_S options;

	initOptions(&options);
	retVal = parseParams(&options, argc, argv);
	if (retVal != SUCCESS_E) {
		return retVal;
	}

	if (options.help)
	{
		help();
		return retVal;
	}

	// call init API
	retVal = BCM_UW_API_Open();
	if (retVal != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_Open - Err no: %d\n", retVal);
		return(1);
	}
	api_open=true;
	printf("API_Open - Done\n");

	retVal = RR_API_ComConfigure(device, options.media_mode, options.host, options.port);
	if (retVal != SUCCESS_E)
	{
		fprintf(stderr, "RR_API_ComConfigure failed\n");
		if (api_open) {BCM_UW_API_Close();}
		return(1);
	}
	// // udi: why do we need to sleep? was it related to threads?
	// Who the hell is udi? Someone from Broadcom/Maxlinear
	bcmUwHostSleep(100*1000); 


	if (options.filename_fw[0]) {
		if (options.media_mode==MEDIA_MODE_SOM)
		{
			fprintf(stderr, "Booting not supported in som mode.\n");
			if (api_open) {BCM_UW_API_Close();}
			return(1);
		}
		//BCM_UW_API_FirmwareFromHostBoot, PG111/p57
		retVal = RR_API_FirmwareFromHostBoot(device, options.filename_fw);
		if (retVal != SUCCESS_E)
		{
			if (api_open) {BCM_UW_API_Close();}
			return(1);
		}

		// wait for boot
		bcmUwHostSleep(4*1000*1000); //4s
	}

	//if (options.bcmver || options.filename_fw[0]) {
	RR_API_VersionGet(device);
	if (retVal != SUCCESS_E)
	{
		if (api_open) {BCM_UW_API_Close();}
		return(1);
	}
	//}

	if (options.filename_dc[0]) {
		// BCM_UW_API_DeviceConfigure, PG111/p58
		retVal = RR_API_DeviceConfigure(device, options.filename_dc);
		if (retVal != SUCCESS_E)
		{
			if (api_open) {BCM_UW_API_Close();}
			return(1);
		}
		//bcmUwHostSleep(500*1000); 
	}

	if (options.path_agc[0]) {
		// BCM_UW_API_WbRxAgcGainDistributionTableSet, PG111/p245
		// BCM_UW_API_WbRxAgcDbToDacTableSet, PG111/p246
		retVal = RR_AGC_load(device, options.path_agc);
		if (retVal != SUCCESS_E)
		{
			if (api_open) {BCM_UW_API_Close();}
			return(1);
		}
		//bcmUwHostSleep(500*1000); 
	}

	if (options.filename_ini[0]) {
		// BCM_UW_API_ConfigFileFromHostLoad, PG111/p63
		retVal = RR_API_ConfigFileFromHostLoad(device, scIndex, options.filename_ini, options.filename_inirx);
		if (retVal != SUCCESS_E)
		{
			if (api_open) {BCM_UW_API_Close();}
			return(1);
		}
		// additional settings
		//    BCM_UW_API_PlaModeSet
		//    BCM_UW_API_ProtectionModeSet
		//    BCM_UW_API_PreambleSet
		//    BCM_UW_API_EthPort...
		//    BCM_UW_API_RxFreqOffsetSet
		//bcmUwHostSleep(500*1000); 
	}

	if (options.txrxstart) {
		// BCM_UW_API_TxRxOperationStart
		retVal = RR_API_TxRxOperationStart(device, scIndex);
		if (retVal != SUCCESS_E)
		{
			if (api_open) {BCM_UW_API_Close();}
			return(1);
		}
		//bcmUwHostSleep(500*1000); 
	}

	if (options.modem_status) {
		// BCM_UW_API_ModemStatusGet
		retVal = RR_API_ModemStatusGet(device, scIndex);
		if (retVal != SUCCESS_E)
		{
			if (api_open) {BCM_UW_API_Close();}
			return(1);
		}
		//bcmUwHostSleep(500*1000); 
	}

	if (options.rfinit) {
		// RF_Config.Initialize()

		//GeneralInit - nastaveni zdroju
		retVal = RR_GeneralInit(device, scIndex);
		if (retVal != SUCCESS_E)
		{
			if (api_open) {BCM_UW_API_Close();}
			return(1);
		}
		

		// BCM_UW_API_RF_AllInitialize
		retVal = RR_API_RF_AllInitialize(device);
		if (retVal != SUCCESS_E)
		{
			if (api_open) {BCM_UW_API_Close();}
			return(1);
		}

		// ===================
		// TODO: zapnuti LOx2
		// ===================
		// #RX0
		// HostDll.DLL_RF_RegWrite(device, 0, 0, 18, 16352) # 0x12, 0x3fe0
		// HostDll.DLL_RF_RegWrite(device, 0, 0, 19, 4040) # 0x13, 0x0fc8
		// #TX0
		// HostDll.DLL_RF_RegWrite(device, 0, 2, 27, 33160) # 0x1b, 0x8188
		// HostDll.DLL_RF_RegWrite(device, 0, 2 ,28, 16320) # 0x1c, 0x3fc0
		// ===================
		printf("Setup LOx2, then press Enter to continue.\n");
		getchar();



		// BCM_UW_API_WbTxTxCalibrationStart, PG111/p235
		retVal = RR_API_WbTxTxCalibration(device);
		if (retVal != SUCCESS_E)
		{
			if (api_open) {BCM_UW_API_Close();}
			return(1);
		}

		retVal = RR_API_AdpdCalibration(device, scIndex);
		if (retVal != SUCCESS_E)
		{
			if (api_open) {BCM_UW_API_Close();}
			return(1);
		}

		// nastaveni ncoFreqKHz = -18000;
		retVal =  RR_API_WbRxFreqSet(device);
		if (retVal != SUCCESS_E)
		{
			if (api_open) {BCM_UW_API_Close();}
			return(1);
		}
	}

	if (api_open) {BCM_UW_API_Close();}

	return(0);
}
