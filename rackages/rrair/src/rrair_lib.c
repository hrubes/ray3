// odkladiste na spolecne veci

#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <bcm_uw_api.h>

#include "rrair_lib.h"
#include "hostDriverRrPc.h"


// pomocne funkce

bool str_to_uint16(const char *str, uint16_t *res) {
	char *end;
	errno = 0;
	long val = strtol(str, &end, 10);
	if (errno || end == str || *end != '\0' || val < 0 || val >= UINT16_MAX) {
		return false;
	}
	*res = (uint16_t)val;
	return true;
}

bool isValidIpAddress(const char *ipAddress)
{
	struct sockaddr_in sa;
	int result = inet_pton(AF_INET, ipAddress, &(sa.sin_addr));
	return (result == 1);
}



// funkce zavisle na bcm_uw_api

bool isReadyWait(uint8_t device, uint32_t max_time_ms)
{
	BCM_UW_ERROR_MSG_ENUM retVal;
	BCM_UW_MODEM_VERSION_STRUCT pVer;

	uint32_t sleep_time = 100*1000;
	uint32_t total_time = 0;
	uint32_t max_time = max_time_ms*1000;

	for (total_time=0; total_time<=max_time; total_time+=sleep_time)
	{
		retVal = BCM_UW_API_VersionGet (device, &pVer);
		if (retVal == BCM_UW_SUCCESS_MSG_E)
		{
			if (total_time>0) {
				fprintf(stderr, "Waited %f s\n", (double)total_time/(1000*1000));
			}
			return true;
		}
		bcmUwHostSleep(sleep_time); 
	}

	fprintf(stderr, "Device %d is not ready\n", device);
	return false;
}

// =======================================================================
// =======================================================================
// ===== BCM_UW_API ======================================================
// =======================================================================
// =======================================================================


RETURN_ENUM RR_API_VersionGet(uint8_t device)
{
	BCM_UW_ERROR_MSG_ENUM retVal;
	BCM_UW_MODEM_VERSION_STRUCT pVer;

	retVal = BCM_UW_API_VersionGet (device, &pVer);
	if (retVal != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_VersionGet - Err no: %d\n", retVal);
		return(FAIL_E);
	}
	printf("650 fw version (device=%d) = %d.%d.%d\n", device, pVer.majorVersion, pVer.minorVersion, pVer.buildVersion);

	return(SUCCESS_E);
}

//RETURN_ENUM RR_API_ComConfigure(uint8_t device, const struct Options_S* options)
RETURN_ENUM RR_API_ComConfigure(uint8_t device, MEDIA_MODE_E mode, const char* host, uint16_t port)
{
	BCM_UW_ERROR_MSG_ENUM retVal;
	BCM_UW_COM_MEDIA_ENUM media = BCM_UW_COM_MEDIA_MII_DEVICE_E;
	BCM_UW_COM_MEDIA_ENUM boot_media = BCM_UW_COM_MEDIA_NULL_E;
	uint16_t iport=MIISERVER_PORT;
        char ihost[MAX_IP_LEN];
        ihost[0]='\0';

	if (mode==MEDIA_MODE_RR) {
		strncpy(ihost, RR_IP, MAX_IP_LEN);
	} else if (mode==MEDIA_MODE_DIRECT) {
		strncpy(ihost, MIISERVER_IP, MAX_IP_LEN);
	} else {
		strncpy(ihost, SOM_IP, MAX_IP_LEN);
		media = BCM_UW_COM_MEDIA_IP_CLIENT_E;
		iport=SOM_PORT;
	}

	if (port) {
		iport=port;
	}
	if (host[0]) {
		strncpy(ihost, host, MAX_IP_LEN);
	}
	ihost[MAX_IP_LEN-1]='\0';

	setIpAndPortOf650(device, ihost, iport);

	//call API
	retVal = BCM_UW_API_ComConfigure(device, media, boot_media, 50L * 1000000L);
	if (retVal != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_ComConfigure - Error no: %d\n", retVal);
		return(FAIL_E);
	}
	printf("API_ComConfigure - Done\n");
	return(SUCCESS_E);
}
