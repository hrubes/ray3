/* Copyright 2014 Broadcom Corporation
*
 * This program is the proprietary software of Broadcom Corporation and/or
* its licensors, and may only be used, duplicated, modified or distributed
* pursuant to the terms and conditions of a separate, written license
* agreement executed between you and Broadcom (an "Authorized License").
* Except as set forth in an Authorized License, Broadcom grants no license
* (express or implied), right to use, or waiver of any kind with respect to
* the Software, and Broadcom expressly reserves all rights in and to the
* Software and all intellectual property rights therein.  IF YOU HAVE NO
* AUTHORIZED LICENSE, THEN YOU HAVE NO RIGHT TO USE THIS SOFTWARE IN ANY
* WAY, AND SHOULD IMMEDIATELY NOTIFY BROADCOM AND DISCONTINUE ALL USE OF
* THE SOFTWARE.
 *
* Except as expressly set forth in the Authorized License,
*
* 1. This program, including its structure, sequence and organization,
* constitutes the valuable trade secrets of Broadcom, and you shall use
* all reasonable efforts to protect the confidentiality thereof, and to
* use this information only in connection with your use of Broadcom
* integrated circuit products.
*
* 2. TO THE MAXIMUM EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED
* "AS IS" AND WITH ALL FAULTS AND BROADCOM MAKES NO PROMISES,
* REPRESENTATIONS OR WARRANTIES, EITHER EXPRESS, IMPLIED, STATUTORY, OR
* OTHERWISE, WITH RESPECT TO THE SOFTWARE.  BROADCOM SPECIFICALLY
* DISCLAIMS ANY AND ALL IMPLIED WARRANTIES OF TITLE, MERCHANTABILITY,
* NONINFRINGEMENT, FITNESS FOR A PARTICULAR PURPOSE, LACK OF VIRUSES,
* ACCURACY OR COMPLETENESS, QUIET ENJOYMENT, QUIET POSSESSION OR
* CORRESPONDENCE TO DESCRIPTION. YOU ASSUME THE ENTIRE RISK ARISING
* OUT OF USE OR PERFORMANCE OF THE SOFTWARE.
*
 * 3. TO THE MAXIMUM EXTENT PERMITTED BY LAW, IN NO EVENT SHALL
* BROADCOM OR ITS LICENSORS BE LIABLE FOR (i) CONSEQUENTIAL, INCIDENTAL,
* SPECIAL, INDIRECT, OR EXEMPLARY DAMAGES WHATSOEVER ARISING OUT OF OR
* IN ANY WAY RELATING TO YOUR USE OF OR INABILITY TO USE THE SOFTWARE EVEN
* IF BROADCOM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; OR (ii)
* ANY AMOUNT IN EXCESS OF THE AMOUNT ACTUALLY PAID FOR THE SOFTWARE ITSELF
* OR U.S. $1, WHICHEVER IS GREATER. THESE LIMITATIONS SHALL APPLY
* NOTWITHSTANDING ANY FAILURE OF ESSENTIAL PURPOSE OF ANY LIMITED REMEDY.
*/


//#include <string.h>
//#include <sys/types.h>
//#include <sys/ioctl.h>
//#include <sys/mman.h>
//#include <sys/stat.h>

//#include <sys/user.h>

//#include <unistd.h>
//#include <poll.h>
//#include <sys/time.h>
//#include <sched.h>

//#include <fcntl.h>
//#define __USE_UNIX98
#include <pthread.h>
//#include <semaphore.h>
//#include <syslog.h>
#include <stdio.h> 
#include <stdlib.h>
//#include <time.h>
//#include <errno.h>

#include <sys/socket.h>
#include <netinet/in.h>
//#include <arpa/inet.h>
//#include <linux/if_ether.h>
//#include <net/if.h>
//#include <linux/if_packet.h>

//#include "defs_uw.h"
#include "hostDriver.h"
//hu #include "board_ctl.h"
//hu #include "hostUtils.h"

//#define MAX_UART_TRANSACTION_SIZE_BYTES (1024 * 8) //due to buffer size limitation
//#define MIISERVER_PORT 30000
#define SOM_PORT 5001
//const uint16_t MIISERVER_PORT=30000;
//const uint16_t SOM_PORT=5001;


//UINT32 gSpiMaxRateHz[BCM_UW_DEVICES_NUM];
//UINT32 gSpiUserRateHz[BCM_UW_DEVICES_NUM];
//SPI_MODE_ENUM gSpiMode[BCM_UW_DEVICES_NUM] = { SPI_MODE_3_E, SPI_MODE_3_E};
//int gSpiDrdyIntFd = -1; // file descriptor for SPI data ready interrupts
int gMiiFd[BCM_UW_DEVICES_NUM] = {  -1,  -1, -1, -1}; // file descriptor for UDP socket to MII-A/P
//char *gMiiServerIP[BCM_UW_DEVICES_NUM] = { "172.16.1.100", "172.16.2.100", "0.0.0.0", "0.0.0.0"};
char *gMiiServerIP[BCM_UW_DEVICES_NUM] = { "10.190.5.40", "172.16.2.100", "0.0.0.0", "0.0.0.0"};
//extern BOOLEAN gMiipEnable;


// hostDriver.h
bcmUwHostMutexHandle apiMutex[BCM_UW_DEVICES_NUM];


#ifdef __cplusplus
extern "C" {
#endif

//BOOLEAN bcmUwHostSpiDataReadyInit();

/////////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostOpen / hostDriver.h                     //
//  PARAMETERS:                                                //
//  DESCRIPTION: all the things we want to do when we open the application //
//  RETURNS: TRUE on failure, FALSE on success                 //
/////////////////////////////////////////////////////////////////
BOOLEAN bcmUwHostOpen(void)
{
    int ix;
    BOOLEAN ret;

    for (ix=0; ix<BCM_UW_DEVICES_NUM; ix++)
    {
        //gSpiMaxRateHz[ix] = 8000000;
        //gSpiUserRateHz[ix] = 8000000;
        apiMutex[ix] = bcmUwHostMutexCreate();
        if (apiMutex[ix] == NULL)
        {
            return(TRUE);
        }
    }

    //ret = bcmUwHostSpiDataReadyInit();
    ret = FALSE;
    return(ret);
}

//////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostClose / hostDriver.h                 //
//  PARAMETERS:                                             //
//  DESCRIPTION: all the things we want to do when we close the application //
//  RETURNS: TRUE on failure, FALSE on success              //
//////////////////////////////////////////////////////////////
BOOLEAN bcmUwHostClose(void)
{
    int i;
    for (i = 0; i < BCM_UW_DEVICES_NUM; i++)
    {
        if (gMiiFd[i] != -1)
        {
            close(gMiiFd[i]);
            gMiiFd[i] = -1;
        }
    }
    return(FALSE);
}

/////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostDeviceReset / hostDriver.h          // 
//  PARAMETERS:                                            //
//  DESCRIPTION:                                           //
//  RETURNS:                                               //
/////////////////////////////////////////////////////////////
void bcmUwHostDeviceReset(BYTE deviceIndex)
{
    // BOARD_TYPE_ENUM boardType = bdctl_type_get();
    // UINT32 startTime = bcmUwHostTickGet();
    // UINT32 elapsedTime;
    // int ret;
    // UINT8 u8val = 0;

    // //----------------------------------------------------
    // //reset
    // //----------------------------------------------------
    // if (boardType == BOARD_TYPE_985650_E)
    // {
    //     bdctl_cpld_write(CPLD9650P_650_RESET_REG, CPLD9650P_650_NRESET_VALUE);
    // }
    // else if ((boardType == BOARD_TYPE_985820D_820_E) || (boardType == BOARD_TYPE_985820D_ANALOG_E))
    // {
    //     bdctl_lptm_fpga_write(lptm9820d_reset1_reg, 0x3e);
    // }
    // else
    // {
    //     // TODO: how to reset a chip if we are on a carrier board?
    // }

    // //----------------------------------------------------
    // //wait for power stabilized indication
    // //----------------------------------------------------
    // while (1)
    // {
    //     // sleep 100 msec between polling attempts
    //     bcmUwHostSleep(100000);

    //     if (boardType == BOARD_TYPE_985650_E)
    //     {
    //         // poll on bit 6
    //         ret = bdctl_lptm_fpga_read(lptm9650p_reset2_reg, &u8val);
    //         //fprintf(stderr,"985650 u8val = %x\n", u8val);
    //         if (u8val & 0x40)
    //         {
    //             //fprintf(stderr,"985650 power indication OK\n");
    //             // sleep 100 msec after reset done
    //             bcmUwHostSleep(100000);
    //             return;
    //         }
    //     }
    //     else if ((boardType == BOARD_TYPE_985820D_820_E) || (boardType == BOARD_TYPE_985820D_ANALOG_E))
    //     {
    //         // poll on bit 6
    //         ret = bdctl_lptm_fpga_read(lptm9820d_reset1_reg, &u8val);
    //         //fprintf(stderr,"985820d u8val = %x\n", u8val);
    //         if (u8val & 0x40)
    //         {
    //             //fprintf(stderr,"985820d power indication OK\n");
    //             // sleep 100 msec after reset done
    //             bcmUwHostSleep(100000);
    //             return;
    //         }
    //     }
    //     else
    //     {
    //         // no idea - abort
    //         //fprintf(stderr,"no board\n");
    //         return;
    //     }

    //     // more than 3 seconds? Old LPTM? Error? return anyway
    //     elapsedTime = bcmUwHostTickGet() - startTime;
    //     if (elapsedTime > 3000)
    //     {
    //         //fprintf(stderr,"breaking on timeout\n");
    //         return;
    //     }
    // }
    printf("Use 985650 reset button, then press Enter to continue.\n");
    getchar();
}

///////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostSleep / hostDriver.h                  //
//  PARAMETERS:                                              //
//  DESCRIPTION:                                             //
//  RETURNS:                                                 //
///////////////////////////////////////////////////////////////
void bcmUwHostSleep(UINT32 sleepTimeMicro)
{
    struct timespec ts;
    ts.tv_sec = sleepTimeMicro / 1000000;
    ts.tv_nsec = (sleepTimeMicro - (ts.tv_sec * 1000000)) * 1000;
    nanosleep(&ts, NULL);
}

////////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostTickGet / hostDriver.h                 //
//  PARAMETERS:                                               //
//  DESCRIPTION:                                              //
//  RETURNS:                                                  //
////////////////////////////////////////////////////////////////
UINT32 bcmUwHostTickGet(void)
{
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return((ts.tv_sec * 1000) + (ts.tv_nsec/1000000));
}

////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostApiMutexGet / hostDriver.h         //
//  PARAMETERS:                                           //
//  DESCRIPTION:                                          //
//  RETURNS:                                              //
////////////////////////////////////////////////////////////
void bcmUwHostApiMutexGet(BYTE deviceIndex)
{
    bcmUwHostMutexLock(apiMutex[deviceIndex]);
}

/////////////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostApiMutexRelease / hostDriver.h              //
//  PARAMETERS:                                                    //
//  DESCRIPTION:                                                   //
//  RETURNS:                                                       //
/////////////////////////////////////////////////////////////////////
void bcmUwHostApiMutexRelease(BYTE deviceIndex)
{
    bcmUwHostMutexUnlock(apiMutex[deviceIndex]);
}

//////////////////////////////////////////////////////////////////////////

// hostDriver.h
bcmUwHostMutexHandle bcmUwHostMutexCreate(void)
{
    pthread_mutex_t *pMutex;         // Mutex
    pthread_mutexattr_t mutexattr;   // Mutex attribute variable
    int rc;

    // allocate new mutex element
    pMutex = malloc(sizeof(pthread_mutex_t));
    if (pMutex == NULL)
    {
        return(NULL);
    }
    
    pthread_mutexattr_init(&mutexattr);

    // Set the mutex as a recursive mutex
    pthread_mutexattr_settype(&mutexattr, PTHREAD_MUTEX_RECURSIVE_NP);

    // create the mutex with the attributes set
    rc = pthread_mutex_init(pMutex, &mutexattr);

    // After initializing the mutex, the mutex attribute should be destroyed
    // opravdu?
    pthread_mutexattr_destroy(&mutexattr);

    // handle the result
    if (rc)
    {
        //printf("mutex init failed\n");
        free(pMutex);
        return(NULL);
    }
    return((bcmUwHostMutexHandle)pMutex);
}

// hostDriver.h
void bcmUwHostMutexDelete(bcmUwHostMutexHandle mutex)
{
    if (mutex != NULL)
    {
        // no point in calling pthread_mutex_destroy() as we cannot know if it is not locked now
        free(mutex);
    }
}

// hostDriver.h
int bcmUwHostMutexLock(bcmUwHostMutexHandle mutex)
{
    int rc;
    rc = pthread_mutex_lock((pthread_mutex_t *)mutex);
    if (rc)
    {
        fprintf(stderr,"ERROR: lock failed\n");
        return(1);
    }
    return(0);
}

// hostDriver.h
int bcmUwHostMutexUnlock(bcmUwHostMutexHandle mutex)
{
    int rc;
    rc = pthread_mutex_unlock((pthread_mutex_t *)mutex);
    if (rc)
    {
        fprintf(stderr,"ERROR: unlock failed\n");
        return(1);
    }
    return(0);
}

/////////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostUartDataSend / hostDriver.h             //
//  PARAMETERS:                                                //
//  DESCRIPTION: writes a buffer to the uart                   //
//  RETURNS:                                                   //
/////////////////////////////////////////////////////////////////
void bcmUwHostUartDataSend(BYTE deviceIndex, BYTE *pBuff, UINT32 buffLen)
{
    // UINT32      chunkLen;
    // int         uart_fd = bdctl_get_uart_fd(deviceIndex);
    // //int         ix;

    // if (uart_fd == -1) { fprintf(stderr,"UART - Error at DataSend \n"); }

    // while (buffLen)
    // {
    //     chunkLen = MIN(buffLen, MAX_UART_TRANSACTION_SIZE_BYTES);
    //     (void)write(uart_fd, pBuff, chunkLen);

    //     //debug print
    //     // printf("Send(%d): \n", (int)chunkLen);
    //     // for (ix=0; ix<chunkLen; ix++)
    //     // { printf("%02X ", pBuff[ix]); } printf("\n");

    //     pBuff += chunkLen;
    //     buffLen -= chunkLen;
    // }
    return;
}

////////////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostUartDataReceive / hostDriver.h             //
//  PARAMETERS:                                                   //
//  DESCRIPTION: reads a buffer from the uart                     //
//  RETURNS:                                                      //
////////////////////////////////////////////////////////////////////
void bcmUwHostUartDataReceive(BYTE deviceIndex, BYTE *pBuff, UINT32 buffLen, UINT32 timeoutMilli, UINT32 *pReadBytes)
{
    // int         uart_fd = bdctl_get_uart_fd(deviceIndex);
    // struct      pollfd pfd = {uart_fd, POLLIN};
    // ssize_t     rx_bytes;
    // // todo - replace UART timeout calculation with bcmUwHostTickGet()
    // struct      timeval end_time, current_time;
    // struct      timeval timeout = {0, timeoutMilli*1000};
    // int         ret;
    // //int         ix;

    // if (uart_fd == -1) { fprintf(stderr,"UART - Error at DataReceive \n"); }

    // *pReadBytes = 0;

    // if (gettimeofday(&current_time, NULL) < 0)
    // {
    //     return;
    // }

    // timeradd(&current_time, &timeout, &end_time);

    // while (timercmp(&current_time, &end_time, <) && buffLen > 0)
    // {
    //     if ((ret = poll(&pfd, 1, timeoutMilli)) > 0)
    //     {
    //         rx_bytes = read(uart_fd, pBuff, buffLen);
    //         if (rx_bytes > 0)
    //         {
    //              //debug print
    //              // printf("Recv(%d): \n", (int)rx_bytes);
    //              // for (ix=0; ix<rx_bytes; ix++)
    //              // { printf("%02X ", pBuff[ix]); } printf("\n");

    //             *pReadBytes += rx_bytes;
    //             pBuff       += rx_bytes;
    //             buffLen     -= rx_bytes;
    //         }
    //         else
    //         {
    //             return;
    //         }
    //     }
    //     gettimeofday(&current_time, NULL);
    //     timersub(&end_time, &current_time, &timeout);
    //     timeoutMilli = (timeout.tv_sec*1000) + (timeout.tv_usec/1000);
    // }
    return;
}

///////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostUartReceiverFlush / hostDriver.h      //
//  PARAMETERS:                                              //
//  DESCRIPTION: flushing the uart rx                        //
//  RETURNS:                                                 //
///////////////////////////////////////////////////////////////
void bcmUwHostUartReceiverFlush(BYTE deviceIndex)
{
    //bdctl_flush_uart(bdctl_get_uart_fd(deviceIndex));
    return;
}

////////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostSpiDataSend / hostDriver.h             //
//  PARAMETERS:                                               //
//  DESCRIPTION: writes a buffer to the spi                   //
//  RETURNS:                                                  //
////////////////////////////////////////////////////////////////
void bcmUwHostSpiDataSend(BYTE deviceIndex, BYTE *pTxBuff, UINT32 buffLen)
{
    //--------------------------------------------
    // SEND COMMAND - pRX=NULL, len=TX_LEN
    //--------------------------------------------
    ////////////////////////////////////////////////////
    // SPI:          | TX null | TX data              //
    //       --------|---------|---------             //
    //       RX null | !used   | send    (Rx=null)    //
    //       RX data | !used   | recieve (TX=0x7E)    //
    ////////////////////////////////////////////////////

    int ret;

    // spi_transaction_t spi;
    // spi.spi_tx_len     = buffLen;
    // spi.spi_rx_len     = 0;
    // spi.spi_speed_hz   = MIN(gSpiUserRateHz[deviceIndex], gSpiMaxRateHz[deviceIndex]); //read spi speed from board-control
    // spi.spi_frame_size = 8;
    // spi.spi_mode       = gSpiMode[deviceIndex];
    // spi.spi_dev_minor  = (deviceIndex == 0 ? SPI_DEV_CS_650_A_S_E : SPI_DEV_CS_650_B_S_E);
    // spi.spi_tx_buf     = pTxBuff;
    // spi.spi_rx_buf     = 0;

    // //debug print
    // // int ix;
    // // printf("Send(%d): ", (int)buffLen);
    // // for (ix=0; ix<buffLen; ix++)
    // // { printf("%02X ", pTxBuff[ix]); } printf("\n");

    // ret = bdctl_spi_transaction(&spi, NULL);

    ret = -1;
    if (ret < 0) { fprintf(stderr,"SPI - Error at SEND_IOCTL)\n"); }
}

//////////////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostSpiDataReceive / hostDriver.h                //
//  PARAMETERS:                                                     //
//  DESCRIPTION: reads a buffer from the spi                        //
//  RETURNS:                                                        //
//////////////////////////////////////////////////////////////////////
void bcmUwHostSpiDataReceive(BYTE deviceIndex, BYTE* pTxBuff, BYTE* pRxBuff, UINT32 buffLen)
{
    //--------------------------------------------
    // RECIEVE COMMAND - TX=0x7E, len=RX_EST_LEN
    //--------------------------------------------

    int ret;

    // spi_transaction_t spi;
    // spi.spi_tx_len     = 0;
    // spi.spi_rx_len     = buffLen;               //(no, this is not a bug..)
    // spi.spi_speed_hz   = MIN(gSpiUserRateHz[deviceIndex], gSpiMaxRateHz[deviceIndex]); //read spi speed from board-control
    // spi.spi_frame_size = 8;
    // spi.spi_mode       = gSpiMode[deviceIndex];
    // spi.spi_dev_minor  = (deviceIndex == 0 ? SPI_DEV_CS_650_A_S_E : SPI_DEV_CS_650_B_S_E);
    // spi.spi_tx_buf     = pTxBuff;
    // spi.spi_rx_buf     = pRxBuff;

    // //debug print
    // // int ix;
    // // printf("Recv(%d): ", (int)buffLen);
    // // for (ix=0; ix<buffLen; ix++)
    // // { printf("%02X ", pRxBuff[ix]); } printf("\n");

    // ret = bdctl_spi_transaction(&spi, NULL);

    ret = -1;
    if (ret < 0) { fprintf(stderr,"SPI - Error at RECV_IOCTL\n"); }
}

///////////////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostSpiModeSet / hostDriver.h                     //
//  PARAMETERS:                                                      //
//  DESCRIPTION: sets SPI mode (four possible modes: 00,01,10,11)    //
//  RETURNS:                                                         //
///////////////////////////////////////////////////////////////////////
void bcmUwHostSpiModeSet(BYTE deviceIndex, int mode)
{
    // switch (mode)
    // {
    //     case 0:
    //         gSpiMode[deviceIndex] = SPI_MODE_0_E;
    //         break;
    //     case 1:
    //         gSpiMode[deviceIndex] = SPI_MODE_1_E;
    //         break;
    //     case 2:
    //         gSpiMode[deviceIndex] = SPI_MODE_2_E;
    //         break;
    //     case 3:
    //         gSpiMode[deviceIndex] = SPI_MODE_3_E;
    //         break;
    //     default:
    //         fprintf(stderr,"SPI - Error at MODE_SET\n");
    //         return;
    // }
    return;
}

////////////////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostSpiMaxRateSet / hostDriver.h                   //
//  PARAMETERS:                                                       //
//  DESCRIPTION: limits future SPI transactions to rate <= maxRateHz  //
//  RETURNS:                                                          //
////////////////////////////////////////////////////////////////////////
void bcmUwHostSpiMaxRateSet(BYTE deviceIndex, UINT32 maxRateHz)
{
    // fprintf(stderr,"setting max rate to: req=%ld\n", maxRateHz);

    // gSpiMaxRateHz[deviceIndex] = maxRateHz;
}

///////////////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostSpiIsDataReady / hostDriver.h                 //
//  PARAMETERS:                                                      //
//  DESCRIPTION: Return current value of SPI data ready signal       //
//  RETURNS: TRUE if "1", FALSE if "0"                               //
///////////////////////////////////////////////////////////////////////
BOOLEAN bcmUwHostSpiIsDataReady(BYTE deviceIndex)
{
    // gpio_transaction_t gpio;
    // int ret;

    // if (deviceIndex == 1)
    // {
    //     gpio.gpio_addr = GPIO_IN_PIN_EXT_SPI_DRDY_E;
    // }
    // else
    // {
    //     fprintf(stderr,"%s Only device 1 is busy-wait over GPIO driven Device %d ambiguous\n", __FUNCTION__, deviceIndex);
    //     return(TRUE);
    // }

    // gpio.gpio_opcode = BDCTL_OP_READ;

    // ret = bdctl_gpio_transaction(&gpio, NULL);
    // if (ret == 1)
    // {
    //     return(TRUE);
    // }
    // else
    // {
    //     return(FALSE);
    // }
    return(TRUE);
}

////////////////////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostSpiDataReadyWait / hostDriver.h                    //
//  PARAMETERS:                                                           //
//  DESCRIPTION: Wait for SPI data ready to become "1" for this device    //
//  RETURNS: TRUE if reached timeout, FALSE if received interrupt         //
////////////////////////////////////////////////////////////////////////////
BOOLEAN bcmUwHostSpiDataReadyWait(BYTE deviceIndex, UINT32 timeoutMilli)
{
    // int ret;
    // UINT32 startTime = bcmUwHostTickGet();

    // if (deviceIndex == 0)   // device 0 is interrupt driven
    // {
    //     int num_fds = 1;
    //     struct pollfd ufds;

    //     while(1)
    //     {
    //         //reset the event flags
    //         ufds.fd         = gSpiDrdyIntFd;
    //         ufds.events     = POLLIN | POLLOUT;
    //         ufds.revents    = 0;
    // //      fprintf(stderr,"calling poll:\n");

    //         ret = poll(&ufds, num_fds, timeoutMilli/* -1 if Don't timeout */);
    //         if (ret == -1)
    //         {
    //             fprintf(stderr,"%s ERROR: ret = %d\n", __FUNCTION__, ret);
    //             return(TRUE);
    //         }

    //         if (ret == 0) // Timeout occurred
    //         {
    //             fprintf(stderr,"%s Timeout: ret = %d\n", __FUNCTION__, ret);
    //             return(TRUE);
    //         }

    //         if (ufds.revents)
    //         {
    //             fprintf(stderr,"%s Got interrupt from spi-int-dev %d :\n", __FUNCTION__, deviceIndex);
    //             return(FALSE);//_host_interrupt_handle();
    //         }
    //     }
    // }
    // else if (deviceIndex == 1)// device 1 is busy-wait driven
    // {
    //     while (1)
    //     {
    //         // check signal (must poll on PCI w/ USB/SPI)
    //         if (bcmUwHostSpiIsDataReady(deviceIndex))
    //         {
    //             return(FALSE);
    //         }

    //         // timeout?
    //         // this cose assumes timeoutMilli >> tick resolution
    //         if (bcmUwHostTickGet() - startTime > timeoutMilli)
    //         {
    //             sched_yield();
    //             return(TRUE);
    //         }
    //     }
    // }

    return(TRUE);
}

/////////////////////////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostSpiDataReadyInit                                        //
//                                                                             //
//  PARAMETERS:                                                                //
//                                                                             //
//  DESCRIPTION: init the SPI data ready irq device,                           //
//      this function should be invoked before any other SPI data ready related//
//                                                                             //
//  RETURNS: TRUE if reached timeout, FALSE if received interrupt              //
/////////////////////////////////////////////////////////////////////////////////
// BOOLEAN bcmUwHostSpiDataReadyInit()
// {
//     const char *dev_name = {"/dev/gpiodev985650.spi-drdy-int"}; // for the 650 device
// 
//     /* Open SPI-int devices */
//     if ( gSpiDrdyIntFd == -1 )
//     {
//         gSpiDrdyIntFd = open(dev_name, O_RDWR);
//         if (gSpiDrdyIntFd < 0)
//         {
//             fprintf(stderr,"%s - ERROR: cannot open %s\n", __FUNCTION__, dev_name);
//             return (TRUE);
//         }
// 
//         fprintf(stderr,"%s Opened /dev/gpiodev985650.spi-drdy-int\n", __FUNCTION__);
//     }
//     
//     return(FALSE);
// }


//////////////////////////////////////////////////////////////////
//  FUNCTION: _bcmUwHostMIISetSockTimeout                       //
//  PARAMETERS:                                                 //
//  DESCRIPTION: sets socket timeout                            //
//  RETURNS:                                                    //
//////////////////////////////////////////////////////////////////
static int _bcmUwHostMIISetSockTimeout(int fd, int timeoutMilli)
{
    struct timeval tv;

    tv.tv_sec = timeoutMilli / 1000 ;
    tv.tv_usec = ( timeoutMilli % 1000) * 1000;

    return setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(tv));
}

//////////////////////////////////////////////////////////////////
//  FUNCTION: _bcmUwHostMIIConnect                              //
//  PARAMETERS:                                                 //
//  DESCRIPTION: "Connects" and binds DGRAM socket              //
//  RETURNS:                                                    //
//////////////////////////////////////////////////////////////////
static int _bcmUwHostMIIConnect(BYTE deviceIndex)
{
    struct sockaddr_in server;
    int rc;

    if (deviceIndex >= BCM_UW_DEVICES_NUM)
    {
        return -1;
    }
    else if (gMiiFd[deviceIndex] > 0)
    {
        return 0;
    }
    else do
    {
        gMiiFd[deviceIndex] = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        if (gMiiFd[deviceIndex] < 0)
        {
            fprintf(stderr,"ERROR: MII - at %s():%d\n", __FUNCTION__, __LINE__);
            break;
        }
        server.sin_family = AF_INET;
        // server.sin_port = htons(MIISERVER_PORT);
        server.sin_port = htons(SOM_PORT);

        // // support for MIIP (ETH2)
        rc = inet_aton(gMiiServerIP[deviceIndex], &server.sin_addr);
        // if(gMiipEnable == FALSE)
        // {
        //     rc = inet_aton(gMiiServerIP[0], &server.sin_addr);
        // }
        // else
        // {
        //     rc = inet_aton(gMiiServerIP[1], &server.sin_addr);
        // }

        if (rc == 0)
        {
            fprintf(stderr,"ERROR: MII - at %s():%d\n", __FUNCTION__, __LINE__);
            break;
        }
        rc = connect(gMiiFd[deviceIndex], (const struct sockaddr *)&server, sizeof(server));
        if (rc < 0)
        {
            fprintf(stderr,"ERROR: MII - at %s():%d\n", __FUNCTION__, __LINE__);
            break;
        }

        _bcmUwHostMIISetSockTimeout(gMiiFd[deviceIndex], 1000);
	//printf("socket fd=%d pripojen na adresu %s:%d\n", gMiiFd[deviceIndex], gMiiServerIP[deviceIndex],MIISERVER_PORT);
	printf("socket fd=%d pripojen na adresu %s:%d\n", gMiiFd[deviceIndex], gMiiServerIP[deviceIndex],SOM_PORT);

        return 0;

    } while (0);

    if (gMiiFd[deviceIndex] > 0)
    {
        close(gMiiFd[deviceIndex]);
    }
    gMiiFd[deviceIndex] = -1;
    return -1;
}

/////////////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostMIIDataWrite / hostDriver.h                 //
//  PARAMETERS:                                                    //
//  DESCRIPTION: writes a buffer to the MII                        //
//  RETURNS:                                                       //
/////////////////////////////////////////////////////////////////////
void bcmUwHostMIIDataWrite(BYTE deviceIndex, BYTE *pBuff, UINT32 buffLen)
{
    size_t numOfBytesSent;

    if (_bcmUwHostMIIConnect(deviceIndex) != 0)
    {
        return;
    }
    numOfBytesSent = send(gMiiFd[deviceIndex], (const char *)pBuff, buffLen, 0 );
    if (numOfBytesSent != (int)buffLen)
    {
        fprintf(stderr,"ERROR: MII - at %s():%d (%d != %d)\n", __FUNCTION__, __LINE__, (int)numOfBytesSent, (int)buffLen);
    }

    return;
}

////////////////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostMIIDataRead / hostDriver.h                     //
//  PARAMETERS:                                                       //
//  DESCRIPTION: reads a buffer from the MII                          //
//  RETURNS: TRUE if reached timeout, FALSE if received interrupt     //
////////////////////////////////////////////////////////////////////////
BOOLEAN bcmUwHostMIIDataRead(BYTE deviceIndex, BYTE *pBuff, UINT32 maxLen, UINT32 *pBuffLen, UINT32 timeoutMilli)
{
    int numOfBytesReceived;
    if (_bcmUwHostMIIConnect(deviceIndex) != 0)
    {
        return(FALSE);
    }

    _bcmUwHostMIISetSockTimeout(gMiiFd[deviceIndex], timeoutMilli);
    numOfBytesReceived = recv(gMiiFd[deviceIndex], (char *)pBuff, maxLen, 0);
    if (numOfBytesReceived <= 0)
    {
        *pBuffLen = 0;
        return(TRUE);
    }
    else
    {
        *pBuffLen = numOfBytesReceived;
    }
    return(FALSE);
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

