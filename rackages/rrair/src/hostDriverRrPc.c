#include <pthread.h>
#include <stdio.h> 
#include <stdlib.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <string.h>
#include "hostDriverRrPc.h"


int gMiiFd[BCM_UW_DEVICES_NUM] = {  -1,  -1, -1, -1}; // file descriptor for UDP socket to MII-A/P
//char *gMiiServerIP[BCM_UW_DEVICES_NUM] = { "172.16.1.100", "172.16.2.100", "0.0.0.0", "0.0.0.0"};
//char *gMiiServerIP[BCM_UW_DEVICES_NUM] = { "10.190.5.40", "172.16.2.100", "0.0.0.0", "0.0.0.0"};
//char *gMiiServerIP[BCM_UW_DEVICES_NUM] = { "192.168.141.226", "172.16.2.100", "0.0.0.0", "0.0.0.0"};
char gMiiServerIP[BCM_UW_DEVICES_NUM][MAX_IP_LEN] = {{0}};
uint16_t gMiiServerPort[BCM_UW_DEVICES_NUM] = {0};//{SOM_PORT, SOM_PORT, SOM_PORT, SOM_PORT};

//extern BOOLEAN gMiipEnable;


// hostDriver.h
bcmUwHostMutexHandle apiMutex[BCM_UW_DEVICES_NUM];


#ifdef __cplusplus
extern "C" {
#endif

// helper function for ip and port setting
int setIpAndPortOf650(uint8_t deviceIndex, const char* ip, uint16_t port)
{
	if (deviceIndex>=BCM_UW_DEVICES_NUM) {
		return -1;
	}
	gMiiServerPort[deviceIndex] = port;
	strncpy(gMiiServerIP[deviceIndex], ip, MAX_IP_LEN);
	gMiiServerIP[deviceIndex][MAX_IP_LEN-1]='\0';
	return 0;
}



/////////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostOpen / hostDriver.h                     //
//  PARAMETERS:                                                //
//  DESCRIPTION: all the things we want to do when we open the application //
//  RETURNS: TRUE on failure, FALSE on success                 //
/////////////////////////////////////////////////////////////////
BOOLEAN bcmUwHostOpen(void)
{
    int ix;
    BOOLEAN ret;

    for (ix=0; ix<BCM_UW_DEVICES_NUM; ix++)
    {
        //gSpiMaxRateHz[ix] = 8000000;
        //gSpiUserRateHz[ix] = 8000000;
        apiMutex[ix] = bcmUwHostMutexCreate();
        if (apiMutex[ix] == NULL)
        {
            return(TRUE);
        }
    }

    //ret = bcmUwHostSpiDataReadyInit();
    ret = FALSE;
    return(ret);
}

//////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostClose / hostDriver.h                 //
//  PARAMETERS:                                             //
//  DESCRIPTION: all the things we want to do when we close the application //
//  RETURNS: TRUE on failure, FALSE on success              //
//////////////////////////////////////////////////////////////
BOOLEAN bcmUwHostClose(void)
{
    int i;
    for (i = 0; i < BCM_UW_DEVICES_NUM; i++)
    {
        if (gMiiFd[i] != -1)
        {
            close(gMiiFd[i]);
            gMiiFd[i] = -1;
        }
    }
    return(FALSE);
}

/////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostDeviceReset / hostDriver.h          // 
//  PARAMETERS:                                            //
//  DESCRIPTION:                                           //
//  RETURNS:                                               //
/////////////////////////////////////////////////////////////
void bcmUwHostDeviceReset(BYTE deviceIndex)
{
    printf("Use 985650 reset button, then press Enter to continue.\n");
    getchar();
}

///////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostSleep / hostDriver.h                  //
//  PARAMETERS:                                              //
//  DESCRIPTION:                                             //
//  RETURNS:                                                 //
///////////////////////////////////////////////////////////////
void bcmUwHostSleep(UINT32 sleepTimeMicro)
{
    struct timespec ts;
    ts.tv_sec = sleepTimeMicro / 1000000;
    ts.tv_nsec = (sleepTimeMicro - (ts.tv_sec * 1000000)) * 1000;
    nanosleep(&ts, NULL);
}

////////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostTickGet / hostDriver.h                 //
//  PARAMETERS:                                               //
//  DESCRIPTION:                                              //
//  RETURNS:                                                  //
////////////////////////////////////////////////////////////////
UINT32 bcmUwHostTickGet(void)
{
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return((ts.tv_sec * 1000) + (ts.tv_nsec/1000000));
}

////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostApiMutexGet / hostDriver.h         //
//  PARAMETERS:                                           //
//  DESCRIPTION:                                          //
//  RETURNS:                                              //
////////////////////////////////////////////////////////////
void bcmUwHostApiMutexGet(BYTE deviceIndex)
{
    bcmUwHostMutexLock(apiMutex[deviceIndex]);
}

/////////////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostApiMutexRelease / hostDriver.h              //
//  PARAMETERS:                                                    //
//  DESCRIPTION:                                                   //
//  RETURNS:                                                       //
/////////////////////////////////////////////////////////////////////
void bcmUwHostApiMutexRelease(BYTE deviceIndex)
{
    bcmUwHostMutexUnlock(apiMutex[deviceIndex]);
}


// hostDriver.h
bcmUwHostMutexHandle bcmUwHostMutexCreate(void)
{
    pthread_mutex_t *pMutex;         // Mutex
    pthread_mutexattr_t mutexattr;   // Mutex attribute variable
    int rc;

    // allocate new mutex element
    pMutex = malloc(sizeof(pthread_mutex_t));
    if (pMutex == NULL)
    {
        return(NULL);
    }
    
    pthread_mutexattr_init(&mutexattr);

    // Set the mutex as a recursive mutex
    pthread_mutexattr_settype(&mutexattr, PTHREAD_MUTEX_RECURSIVE_NP);

    // create the mutex with the attributes set
    rc = pthread_mutex_init(pMutex, &mutexattr);

    // After initializing the mutex, the mutex attribute should be destroyed
    // opravdu?
    pthread_mutexattr_destroy(&mutexattr);

    // handle the result
    if (rc)
    {
        //printf("mutex init failed\n");
        free(pMutex);
        return(NULL);
    }
    return((bcmUwHostMutexHandle)pMutex);
}

// hostDriver.h
void bcmUwHostMutexDelete(bcmUwHostMutexHandle mutex)
{
    if (mutex != NULL)
    {
        // no point in calling pthread_mutex_destroy() as we cannot know if it is not locked now
        free(mutex);
    }
}

// hostDriver.h
int bcmUwHostMutexLock(bcmUwHostMutexHandle mutex)
{
    int rc;
    rc = pthread_mutex_lock((pthread_mutex_t *)mutex);
    if (rc)
    {
        fprintf(stderr,"ERROR: lock failed\n");
        return(1);
    }
    return(0);
}

// hostDriver.h
int bcmUwHostMutexUnlock(bcmUwHostMutexHandle mutex)
{
    int rc;
    rc = pthread_mutex_unlock((pthread_mutex_t *)mutex);
    if (rc)
    {
        fprintf(stderr,"ERROR: unlock failed\n");
        return(1);
    }
    return(0);
}

//////////////////////////////////////////////////////////////////////////

//  FUNCTION: bcmUwHostUartDataSend / hostDriver.h             //
void bcmUwHostUartDataSend(BYTE deviceIndex, BYTE *pBuff, UINT32 buffLen) { return; }

//  FUNCTION: bcmUwHostUartDataReceive / hostDriver.h             //
void bcmUwHostUartDataReceive(BYTE deviceIndex, BYTE *pBuff, UINT32 buffLen, UINT32 timeoutMilli, UINT32 *pReadBytes) { return; }

//  FUNCTION: bcmUwHostUartReceiverFlush / hostDriver.h      //
void bcmUwHostUartReceiverFlush(BYTE deviceIndex) { return; }

//  FUNCTION: bcmUwHostSpiDataSend / hostDriver.h             //
void bcmUwHostSpiDataSend(BYTE deviceIndex, BYTE *pTxBuff, UINT32 buffLen) { fprintf(stderr,"SPI - Error at SEND_IOCTL)\n"); }

//  FUNCTION: bcmUwHostSpiDataReceive / hostDriver.h                //
void bcmUwHostSpiDataReceive(BYTE deviceIndex, BYTE* pTxBuff, BYTE* pRxBuff, UINT32 buffLen) { fprintf(stderr,"SPI - Error at RECV_IOCTL\n"); }

//  FUNCTION: bcmUwHostSpiModeSet / hostDriver.h                     //
void bcmUwHostSpiModeSet(BYTE deviceIndex, int mode) { return; }

//  FUNCTION: bcmUwHostSpiMaxRateSet / hostDriver.h                   //
void bcmUwHostSpiMaxRateSet(BYTE deviceIndex, UINT32 maxRateHz) { }

//  FUNCTION: bcmUwHostSpiIsDataReady / hostDriver.h                 //
BOOLEAN bcmUwHostSpiIsDataReady(BYTE deviceIndex) { return(TRUE); }

//  FUNCTION: bcmUwHostSpiDataReadyWait / hostDriver.h                    //
BOOLEAN bcmUwHostSpiDataReadyWait(BYTE deviceIndex, UINT32 timeoutMilli) { return(TRUE); }

//////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////
//  FUNCTION: _bcmUwHostMIISetSockTimeout                       //
//  PARAMETERS:                                                 //
//  DESCRIPTION: sets socket timeout                            //
//  RETURNS:                                                    //
//////////////////////////////////////////////////////////////////
static int _bcmUwHostMIISetSockTimeout(int fd, int timeoutMilli)
{
    struct timeval tv;

    tv.tv_sec = timeoutMilli / 1000 ;
    tv.tv_usec = ( timeoutMilli % 1000) * 1000;

    return setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(tv));
}

//////////////////////////////////////////////////////////////////
//  FUNCTION: _bcmUwHostMIIConnect                              //
//  PARAMETERS:                                                 //
//  DESCRIPTION: "Connects" and binds DGRAM socket              //
//  RETURNS:                                                    //
//////////////////////////////////////////////////////////////////
static int _bcmUwHostMIIConnect(BYTE deviceIndex)
{
    struct sockaddr_in server;
    int rc;

    if (deviceIndex >= BCM_UW_DEVICES_NUM)
    {
        return -1;
    }
    else if (gMiiFd[deviceIndex] > 0)
    {
        return 0;
    }
    else do
    {
        gMiiFd[deviceIndex] = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        if (gMiiFd[deviceIndex] < 0)
        {
            fprintf(stderr,"ERROR: MII - at %s():%d\n", __FUNCTION__, __LINE__);
            break;
        }
        server.sin_family = AF_INET;
        // server.sin_port = htons(MIISERVER_PORT);
        server.sin_port = htons(gMiiServerPort[deviceIndex]);

        // // support for MIIP (ETH2)
        rc = inet_aton(gMiiServerIP[deviceIndex], &server.sin_addr);
        // if(gMiipEnable == FALSE)
        // {
        //     rc = inet_aton(gMiiServerIP[0], &server.sin_addr);
        // }
        // else
        // {
        //     rc = inet_aton(gMiiServerIP[1], &server.sin_addr);
        // }

        if (rc == 0)
        {
            fprintf(stderr,"ERROR: MII - at %s():%d\n", __FUNCTION__, __LINE__);
            break;
        }
        rc = connect(gMiiFd[deviceIndex], (const struct sockaddr *)&server, sizeof(server));
        if (rc < 0)
        {
            fprintf(stderr,"ERROR: MII - at %s():%d\n", __FUNCTION__, __LINE__);
            break;
        }

        _bcmUwHostMIISetSockTimeout(gMiiFd[deviceIndex], 1000);
	//printf("socket fd=%d pripojen na adresu %s:%d\n", gMiiFd[deviceIndex], gMiiServerIP[deviceIndex],MIISERVER_PORT);
	fprintf(stderr, "Socket fd=%d bound to address %s:%d\n", gMiiFd[deviceIndex], gMiiServerIP[deviceIndex], gMiiServerPort[deviceIndex]);

        return 0;

    } while (0);

    if (gMiiFd[deviceIndex] > 0)
    {
        close(gMiiFd[deviceIndex]);
    }
    gMiiFd[deviceIndex] = -1;
    return -1;
}

/////////////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostMIIDataWrite / hostDriver.h                 //
//  PARAMETERS:                                                    //
//  DESCRIPTION: writes a buffer to the MII                        //
//  RETURNS:                                                       //
/////////////////////////////////////////////////////////////////////
void bcmUwHostMIIDataWrite(BYTE deviceIndex, BYTE *pBuff, UINT32 buffLen)
{
    size_t numOfBytesSent;

    if (_bcmUwHostMIIConnect(deviceIndex) != 0)
    {
        return;
    }
    numOfBytesSent = send(gMiiFd[deviceIndex], (const char *)pBuff, buffLen, 0 );
    if (numOfBytesSent != (int)buffLen)
    {
        fprintf(stderr,"ERROR: MII - at %s():%d (%d != %d)\n", __FUNCTION__, __LINE__, (int)numOfBytesSent, (int)buffLen);
    }

    return;
}

////////////////////////////////////////////////////////////////////////
//  FUNCTION: bcmUwHostMIIDataRead / hostDriver.h                     //
//  PARAMETERS:                                                       //
//  DESCRIPTION: reads a buffer from the MII                          //
//  RETURNS: TRUE if reached timeout, FALSE if received interrupt     //
////////////////////////////////////////////////////////////////////////
BOOLEAN bcmUwHostMIIDataRead(BYTE deviceIndex, BYTE *pBuff, UINT32 maxLen, UINT32 *pBuffLen, UINT32 timeoutMilli)
{
    int numOfBytesReceived;
    if (_bcmUwHostMIIConnect(deviceIndex) != 0)
    {
        return(FALSE);
    }

    _bcmUwHostMIISetSockTimeout(gMiiFd[deviceIndex], timeoutMilli);
    numOfBytesReceived = recv(gMiiFd[deviceIndex], (char *)pBuff, maxLen, 0);
    if (numOfBytesReceived <= 0)
    {
        *pBuffLen = 0;
        return(TRUE);
    }
    else
    {
        *pBuffLen = numOfBytesReceived;
    }
    return(FALSE);
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

