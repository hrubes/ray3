// aplikace na vypis stavovych informaci 85650 a 85820

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <sys/time.h>
#include <getopt.h>
//#include <errno.h>
#include <bcm_uw_api.h>

#include "hostDriverRrPc.h"
#include "rrair_lib.h"

#define PROGNAME "rrair_status"

#define STATUS_OUT stdout

// typedef enum {
//     FAIL_E      = 0,
//     SUCCESS_E   = 1
// } RETURN_ENUM;
// 
#define SC_INDEX_COUNT 2

// =======================================================================


const char* BCM_UW_WB_TX_AGC_MODE_ENUM_str[] = {"IDLE", "SINGLE", "DUAL_WB", "DUAL_DCC",0};
const char* BCM_UW_WB_RX_AGC_MODE_ENUM_str[] = {
	"IDLE",
	"ONE_TO_ONE",
	"ONE_TO_TWO",
	"TWO_TO_TWO",
	"TWO_TO_FOUR",
	"ADPD",
	0
};
const char* BCM_UW_WB_AGC_LOCK_STATUS_ENUM_str[] = { "IDLE", "UNLOCKED", "SEMI_LOCKED", "LOCKED", 0};
const char* BCM_UW_ACQUIRE_STATUS_ENUM_str[] = {
	"IN_PROGRESS",
	"LOCKED",
	"FAILED",
	"NOT_ACTIVATED",
	"invalid",
	0
};
const char* BCM_UW_ACQUIRE_LAST_ERR_ENUM_str[] = {
	"SUCCESS",
	"ERR_INT_PREAMBLE_LOSS",
	"ERR_INT_BIT_LOSS",
	"ERR_FEC_ULOCKED_TIMOUT",
	"ERR_AT_FRAME_0_SYNC",
	"ERR_TIMEOUT",
	"ERR_AT_FRAME_2_SYNC",
	"ERR_FATAL",
	"invalid",
	0
};

const char* BCM_UW_WB_TX_NAME_ENUM_str[] = { "0", "1", "XC_0", "XC_1", 0 };

const char* BCM_UW_WB_RX_NAME_ENUM_str[] = { "0", "1", "2", "3", "ADPD0", "ADPD1", "invalid" "ENV0", "ENV1", 0 };

const char* BCM_UW_WB_TX_DEST_NAME_ENUM_str[] = { "0", "0_I", "0_Q", "1", "1_I", "1_Q", "MAX", 0 };

const char* BCM_UW_WB_RX_SOURCE_NAME_ENUM_str[] = { "0", "0_I", "0_Q", "1", "1_I", "1_Q", "2", "2_I", "2_Q", "3", "3_I", "3_Q", "4", "4_I", "4_Q", "5", "5_I", "5_Q", "MAX", 0 };

const char* BCM_UW_MODULATION_TYPE_ENUM_str[] = {
	"QPSK",
	"8QAM",
	"16QAM",
	"32QAM",
	"64QAM",
	"128QAM",
	"256QAM",
	"512QAM",
	"1024QAM",
	"2048QAM",
	"4096QAM",
	0
};




//const size_t fn_size=100;
//#define FN_SIZE 100
//#define BOOTBUFF_SIZE (1024*1024)
const uint32_t max_wait_ms = 100;

typedef enum {
    MULTI_E = 0,
    VER_E,
    HELP_E
} ACTION_ENUM;




struct Options_S {
	bool verbose;
	bool help;
	bool media_mode;
	char host[MAX_IP_LEN];
	uint16_t port;
	long int count;
	long int time;
	uint16_t modem;
	uint16_t wbtx;
	uint16_t wbrx;
	uint16_t wbtxagc;
	uint16_t wbrxagc;
	uint16_t rftx;
	uint16_t rfrx;
	uint16_t rfrxrsl;
	uint16_t rss;
	uint16_t ethInit;
	uint16_t ethConfig;
	uint16_t ethStatus;
	uint16_t ethRxCount;
	uint16_t ethTxCount;
	uint16_t ethCountClear;
	uint16_t acmprofile;
	uint16_t acmConfig;
	uint16_t acmProfileEnable;
	uint16_t acmEngineEnable;
	uint16_t acmSwitch;
};

void initOptions(struct Options_S* options)
{
	options->verbose=0;
	options->help=false;
	options->media_mode=MEDIA_MODE_RR;
	options->host[0]='\0';
	options->port=0;
	options->count=1;
	options->time=1;
	options->modem = 0;
	options->wbtx = 0;
	options->wbrx = 0;
	options->wbtxagc = 0;
	options->wbrxagc = 0;
	options->rftx = 0;
	options->rfrx = 0;
	options->rfrxrsl = 0;
	options->rss = 0;
	options->ethInit = 0;
	options->ethConfig = 0;
	options->ethStatus = 0;
	options->ethRxCount = 0;
	options->ethTxCount = 0;
	options->ethCountClear = 0;
	options->acmprofile = 0;
	options->acmConfig = 0;
	options->acmProfileEnable = 0;
	options->acmEngineEnable = 0;
	options->acmSwitch = 0;
}


struct AcmParams_S {
	uint16_t profileMask;
	uint8_t enableRxSensors;
	uint8_t enableTxSwitch;
	BCM_UW_ACM_PROFILE_ENUM acmProfile;
	uint8_t isLocal;
};

void initAcmParams(struct AcmParams_S* params)
{
	params->profileMask = 0;
	params->enableRxSensors = 1;
	params->enableTxSwitch = 1;
	params->acmProfile = BCM_UW_ACM_PROFILE_0_E;
	params->isLocal = 1;
};



int help(void)
{
	printf("Usage: " PROGNAME " [OPTIONS]\n");
	printf("Basic program for 85650 status listing\n\n");
	printf("Without any parameter prints version of firmware in 85650\n");
	printf("  -m, --mode=MODE - selects connection mode\n");
        printf("                    MODE can be \"rr\" (default), \"direct\" or \"som\"\n");
        printf("                    note: som mode uses extra 4B header in communication protocol\n");
	printf("      --host <host_ip> - default rr: %s, direct: %s, som: %s\n", RR_IP, MIISERVER_IP, SOM_IP);
	printf("      --port <host_port> - default rr,direct: %d, som: %d\n", MIISERVER_PORT, SOM_PORT);
	printf("  -c, --count=COUNT - number of repetitions\n");
	printf("  -t, --time=MILISECONDS - delay between repetitions\n");
	printf("      --modem=SC - BCM_UW_API_ModemStatusGet\n");
	printf("             where SC can be 0 or 1\n");
	printf("      --acmprofile=SC - BCM_UW_API_ModemAcmProfileMaskGet\n");
	printf("      --wbtx=WBTX - BCM_UW_API_WbTxStatusGet\n");
	printf("             where WBTX can be 0, 1, 0X, 1X (BCM_UW_WB_TX_NAME_ENUM)\n");
	printf("      --wbrx=WBRX - BCM_UW_API_WbRxStatusGet\n");
	printf("             where WBRX can be 0, 1, 2, 3, 0A, 1A, 0E, 1E (BCM_UW_WB_RX_NAME_ENUM)\n");
	printf("      --wbtxagc=DAC - BCM_UW_API_WbTxAgcStatusGet\n");
	printf("             where DAC can be 0, 0I, 0Q, 1, 1I, 1Q (BCM_UW_WB_TX_DEST_NAME_ENUM)\n");
	printf("      --wbrxagc=ADC - BCM_UW_API_WbRxAgcStatusGet\n");
	printf("             where ADC can be 0, 0I, 0Q, 1, 1I, ... , 5, 5I, 5Q(BCM_UW_WB_RX_SOURCE_NAME_ENUM)\n");
	printf("      --rftx=RFTX - BCM_UW_API_RF_TxStatusGet\n");
	printf("             where RFTX can be 0 or 1 (BCM_UW_RF_TX_DEV_IDX_ENUM)\n");
	printf("      --rfrx=RFRX - BCM_UW_API_RF_RxStatusGet\n");
	printf("             where RFRX can be 0, 1, 2, 3 (BCM_UW_RF_RX_DEV_IDX_ENUM)\n");
	printf("      --rfrxrsl=RFRX - BCM_UW_API_RF_RxRslGet\n");
	printf("      --rss=RFRX - BCM_UW_API_RF_RxStatusGet\n");
	printf(" ETHERNET\n");
	printf("      --ethInit=SC - BCM_UW_API_EthPortInitialize\n");
	printf("             where SC can be 0 or 1\n");
	printf("      --ethConfig=SC - BCM_UW_API_EthPortConfigure\n");
	printf("             where SC can be 0 or 1\n");
	printf("      --ethStatus=SC - BCM_UW_API_EthPortStatusGet\n");
	printf("             where SC can be 0 or 1\n");
	printf("      --ethRxCount=SC - BCM_UW_API_EthPortRxCountersGet\n");
	printf("             where SC can be 0 or 1\n");
	printf("      --ethTxCount=SC - BCM_UW_API_EthPortTxCountersGet\n");
	printf("             where SC can be 0 or 1\n");
	printf("      --ethCountClear=SC - BCM_UW_API_EthPortCountersClear\n");
	printf("             where SC can be 0 or 1\n");
	printf(" ACM\n");
	printf("      --acmConfig=SC - BCM_UW_API_AcmConfigurationGet\n");
	printf("             where SC can be 0 or 1\n");
	printf("      --acmProfile=SC - BCM_UW_API_ModemAcmProfileMaskGet\n");
	printf("             where SC can be 0 or 1\n");
	printf("      --acmProfileEnable=SC profileMask - BCM_UW_API_ModemAcmProfileEnable\n");
	printf("             where SC can be 0 or 1\n");
	printf("             	profileMask is UINT16 hex number\n");
	printf("      --acmEngineEnable=SC enableRxSensors enableTxSwitch - BCM_UW_API_ModemAcmEngineEnable\n");
	printf("             where SC can be 0 or 1\n");
	printf("             	enableRxSensors is 0(off) or 1(on)\n");
	printf("             	enableTxSwitch is 0(off) or 1(on)\n");
	printf("      --acmSwitch=SC acmProfile isLocal - BCM_UW_API_ModemAcmSwitch\n");
	printf("             where SC can be 0 or 1\n");
	printf("             	acmProfile is number 0 - 15\n");
	printf("             	isLocal is 0(remote) or 1(local)\n");
	
	//printf("  -v, --verbose\n");
	printf("  -h, --help - print this help\n");
	printf("All status options can be used multiple and together.\n\n");
	printf("Example:\n");
	printf(PROGNAME " -t 1000 -c 3 --wbtx --wbtxagc=0 --wbtxagc=1\n\n");

	return SUCCESS_E;
}


// bool str_to_uint16(const char *str, uint16_t *res) {
// 	char *end;
// 	errno = 0;
// 	long val = strtol(str, &end, 10);
// 	if (errno || end == str || *end != '\0' || val < 0 || val >= UINT16_MAX) {
// 		return false;
// 	}
// 	*res = (uint16_t)val;
// 	return true;
// }


int parseParams(struct Options_S* options, struct AcmParams_S* acmParams, int argc, char* const argv[])
{
	if (options==NULL) 
	{
		return FAIL_E;
	}

	int num=0;

	// if (argc==2) {
	// 	options->fwload = true;
	// 	strncpy(options->filename, argv[1], FN_SIZE);
	// 	options->filename[FN_SIZE-1]=0;
	// }
	static struct option long_options_[] =
	{
		{"help", no_argument, 0, 'h'},
		//        {"debug", no_argument, 0, 'd'},
		//        {"version", no_argument, 0, 'v'},
		//        {"test", no_argument, 0, 't'},
		{"modem", required_argument, 0, 1},
		{"wbtx", required_argument, 0, 3},
		{"wbrx", required_argument, 0, 4},
		{"wbtxagc", required_argument, 0, 5},
		{"wbrxagc", required_argument, 0, 6},
		{"rftx", required_argument, 0, 10},
		{"rfrx", required_argument, 0, 11},
		{"rfrxrsl", required_argument, 0, 12},
		{"rss", required_argument, 0, 13},
		{"ethInit", required_argument, 0, 14},
		{"ethConfig", required_argument, 0, 15},
		{"ethStatus", required_argument, 0, 16},
		{"ethRxCount", required_argument, 0, 17},
		{"ethTxCount", required_argument, 0, 18},
		{"ethCountClear", required_argument, 0, 19},
		{"acmConfigGet", required_argument, 0, 20},
		{"acmProfile", required_argument, 0, 2},
		{"acmProfileEnable", required_argument, 0, 21},
		{"acmEngineEnable", required_argument, 0, 22},
		{"acmSwitch", required_argument, 0, 23},
		{"mode", required_argument, 0, 'm'},
		{"host", required_argument, 0, 101},
		{"port", required_argument, 0, 102},
		{"count", required_argument, 0, 'c'},
		{"time", required_argument, 0, 't'},
		{0, 0, 0, 0}
	};
	static char short_options_[]="hm:c:t:";

	int option_index = 0;
	int opt;
	optind=0;

	while((opt = getopt_long(argc, argv, short_options_, long_options_, &option_index)) != -1) {
		switch (opt) {
			case 0: {
					//"getopt_long" nastavil primo promennou
					if (long_options_[option_index].flag != 0)
						break;
					switch (option_index)
					{
						default: {
								 fprintf(stderr, "Option parsing - strange option.\n"); //k tomu by nemelo dojit
								 return FAIL_E;
							 }
					}
					break;
				}
			case 1: 
				num = optarg[0]-'0';
				if ((num<0) || (num>=SC_INDEX_COUNT))
				{
					fprintf(stderr, "Error: Parameter modem has invalid value \"%s\"\n", optarg);
					return FAIL_E;
				}

				options->modem = options->modem | (1<<num);
				break;
			case 3: 
				num = optarg[0]-'0';
				if (optarg[1]==0) {num += 0;}
				else if (optarg[1]=='X') {num += 2;}
				else 
				{
					fprintf(stderr, "Error: Parameter wbtx has invalid value \"%s\"\n", optarg);
					return FAIL_E;
				}
				if ((num<0) || (num>BCM_UW_WB_TX_XC_1_E))
				{
					fprintf(stderr, "Error: Parameter wbtx has invalid value \"%s\" (index=%d)\n", optarg, num);
					return FAIL_E;
				}
				options->wbtx = options->wbtx | (1<<num);
				break;
			case 4: 
				num = optarg[0]-'0';
				if (optarg[1]==0) {num += 0;}
				else if (optarg[1]=='A') {num += 4;}
				else if (optarg[1]=='E') {num += 7;}
				else 
				{
					fprintf(stderr, "Error: Parameter wbrx has invalid value \"%s\"\n", optarg);
					return FAIL_E;
				}
				if ((num<0) || (num>BCM_UW_WB_RX_ENV1_E) || (num==6))
				{
					fprintf(stderr, "Error: Parameter wbrx has invalid value \"%s\" (index=%d)\n", optarg, num);
					return FAIL_E;
				}
				options->wbrx = options->wbtx | (1<<num);
				break;
			case 5: 
				num = (optarg[0]-'0')*3;
				if (optarg[1]==0) {num += 0;}
				else if (optarg[1]=='I') {num += 1;}
				else if (optarg[1]=='Q') {num += 2;}
				else 
				{
					fprintf(stderr, "Error: Parameter wbtxagc has invalid value \"%s\"\n", optarg);
					return FAIL_E;
				}
				if ((num<0) || (num>=BCM_UW_WB_TX_DEST_DAC_MAX_E))
				{
					fprintf(stderr, "Error: Parameter wbtxagc has invalid value \"%s\" (index=%d)\n", optarg, num);
					return FAIL_E;
				}
					
				options->wbtxagc = options->wbtxagc | (1<<num);
				break;
			case 6: 
				num = (optarg[0]-'0')*3;
				if (optarg[1]==0) {num += 0;}
				else if (optarg[1]=='I') {num += 1;}
				else if (optarg[1]=='Q') {num += 2;}
				else 
				{
					fprintf(stderr, "Error: Parameter wbrxagc has invalid value \"%s\"\n", optarg);
					return FAIL_E;
				}
				if ((num<0) || (num>=BCM_UW_WB_RX_SOURCE_ADC_MAX_E))
				{
					fprintf(stderr, "Error: Parameter wbrxagc has invalid value \"%s\" (index=%d)\n", optarg, num);
					return FAIL_E;
				}
				//fprintf(stderr, "wbrxagc: num %d\n", num);
				options->wbrxagc = options->wbrxagc | (1<<num);
				break;
			case 10: 
				num = optarg[0]-'0';
				if ((num<0) || (num>=BCM_UW_RF_TX_DEV_MAX_E))
				{
					fprintf(stderr, "Error: Parameter rftx has invalid value \"%s\"\n", optarg);
					return FAIL_E;
				}
				options->rftx = options->rftx | (1<<num);
			// BCM_UW_RF_TX_DEV_IDX_ENUM // 0-2
				break;
			case 11: 
				num = optarg[0]-'0';
				if ((num<0) || (num>=BCM_UW_RF_RX_DEV_MAX_E))
				{
					fprintf(stderr, "Error: Parameter rfrx has invalid value \"%s\"\n", optarg);
					return FAIL_E;
				}
				options->rfrx = options->rfrx | (1<<num);
			// BCM_UW_RF_RX_DEV_IDX_ENUM // 0-4
				break;
			case 12: 
				num = optarg[0]-'0';
				if ((num<0) || (num>=BCM_UW_RF_RX_DEV_MAX_E))
				{
					fprintf(stderr, "Error: Parameter rfrxrsl has invalid value \"%s\"\n", optarg);
					return FAIL_E;
				}
				options->rfrxrsl = options->rfrxrsl | (1<<num);
			// BCM_UW_RF_RX_DEV_IDX_ENUM // 0-4
				break;
			case 13: 
				num = optarg[0]-'0';
				if ((num<0) || (num>=BCM_UW_RF_RX_DEV_MAX_E))
				{
					fprintf(stderr, "Error: Parameter rss has invalid value \"%s\"\n", optarg);
					return FAIL_E;
				}
				options->rss = options->rss | (1<<num);
			// BCM_UW_RF_RX_DEV_IDX_ENUM // 0-4
				break;
			case 14: 
				num = optarg[0]-'0';
				if ((num<0) || (num>=SC_INDEX_COUNT))
				{
					fprintf(stderr, "Error: Parameter ethInit has invalid value \"%s\"\n", optarg);
					return FAIL_E;
				}
				options->ethInit = options->ethInit | (1<<num);
			// BCM_UW_RF_RX_DEV_IDX_ENUM // 0-4
				break;
			case 15: 
				num = optarg[0]-'0';
				if ((num<0) || (num>=SC_INDEX_COUNT))
				{
					fprintf(stderr, "Error: Parameter ethConfig has invalid value \"%s\"\n", optarg);
					return FAIL_E;
				}
				options->ethConfig = options->ethConfig | (1<<num);
			// BCM_UW_RF_RX_DEV_IDX_ENUM // 0-4
				break;
			case 16: 
				num = optarg[0]-'0';
				if ((num<0) || (num>=SC_INDEX_COUNT))
				{
					fprintf(stderr, "Error: Parameter ethStatus has invalid value \"%s\"\n", optarg);
					return FAIL_E;
				}
				options->ethStatus = options->ethStatus | (1<<num);
			// BCM_UW_RF_RX_DEV_IDX_ENUM // 0-4
				break;
			case 17: 
				num = optarg[0]-'0';
				if ((num<0) || (num>=SC_INDEX_COUNT))
				{
					fprintf(stderr, "Error: Parameter ethRxCount has invalid value \"%s\"\n", optarg);
					return FAIL_E;
				}
				options->ethRxCount = options->ethRxCount | (1<<num);
			// BCM_UW_RF_RX_DEV_IDX_ENUM // 0-4
				break;
			case 18: 
				num = optarg[0]-'0';
				if ((num<0) || (num>=SC_INDEX_COUNT))
				{
					fprintf(stderr, "Error: Parameter ethTxCount has invalid value \"%s\"\n", optarg);
					return FAIL_E;
				}
				options->ethTxCount = options->ethTxCount | (1<<num);
			// BCM_UW_RF_RX_DEV_IDX_ENUM // 0-4
				break;
			case 19: 
				num = optarg[0]-'0';
				if ((num<0) || (num>=SC_INDEX_COUNT))
				{
					fprintf(stderr, "Error: Parameter ethCountClear has invalid value \"%s\"\n", optarg);
					return FAIL_E;
				}
				options->ethCountClear = options->ethCountClear | (1<<num);
			// BCM_UW_RF_RX_DEV_IDX_ENUM // 0-4
				break;
			case 20:
				num = optarg[0] - '0';
				if ((num<0) || (num>=SC_INDEX_COUNT))
				{
					fprintf(stderr, "Error: Parameter acmConfigGet has invalid value \"%s\"\n", optarg);
					return FAIL_E;
				}
				options->acmConfig = options->acmConfig | (1<<num);
				break;

			case 2: 
				num = optarg[0]-'0';
				if ((num<0) || (num>=SC_INDEX_COUNT))
				{
					fprintf(stderr, "Error: Parameter acmProfile has invalid value \"%s\"\n", optarg);
					return FAIL_E;
				}

				options->acmprofile = options->acmprofile | (1<<num);
				break;
			case 21: 
				num = optarg[0]-'0';
				if ((num<0) || (num>=SC_INDEX_COUNT))
				{
					fprintf(stderr, "Error: Parameter acmProfileEnable has invalid value \"%s\"\n", optarg);
					return FAIL_E;
				}
				//dalsi noname parametry
				if( argv[optind] == NULL )
				{
					fprintf(stderr, "Error: acmProfileEnable missing parameter profileMask\n");
					return FAIL_E;
				}
				acmParams->profileMask = (uint16_t)strtoul(argv[optind], NULL, 16);
				optind ++;

				options->acmProfileEnable = options->acmProfileEnable | (1<<num);
				break;
			case 22: 
				num = optarg[0]-'0';
				if ((num<0) || (num>=SC_INDEX_COUNT))
				{
					fprintf(stderr, "Error: Parameter acmEngineEnable has invalid value \"%s\"\n", optarg);
					return FAIL_E;
				}
				//dalsi noname parametry
				if( argv[optind] == NULL )
				{
					fprintf(stderr, "Error: acmEngineEnable missing parameter enableRxSensors\n");
					return FAIL_E;
				}
				acmParams->enableRxSensors = (uint8_t)strtoul(argv[optind], NULL, 10);
				optind ++;
				if( argv[optind] == NULL )
				{
					fprintf(stderr, "Error: acmEngineEnable missing parameter enableTxSwitch\n");
					return FAIL_E;
				}
				acmParams->enableTxSwitch = (uint8_t)strtoul(argv[optind], NULL, 10);
				optind ++;

				options->acmEngineEnable = options->acmEngineEnable | (1<<num);
				break;
			case 23: 
				num = optarg[0]-'0';
				if ((num<0) || (num>=SC_INDEX_COUNT))
				{
					fprintf(stderr, "Error: Parameter acmSwitch has invalid value \"%s\"\n", optarg);
					return FAIL_E;
				}
				//dalsi noname parametry
				if( argv[optind] == NULL )
				{
					fprintf(stderr, "Error: acmSwitch missing parameter acmProfile\n");
					return FAIL_E;
				}
				acmParams->acmProfile = (BCM_UW_ACM_PROFILE_ENUM)strtoul(argv[optind], NULL, 10);
				optind ++;
				if( argv[optind] == NULL )
				{
					fprintf(stderr, "Error: acmswitch missing parameter isLocal\n");
					return FAIL_E;
				}
				acmParams->isLocal = (uint8_t)strtoul(argv[optind], NULL, 10);
				optind ++;

				options->acmSwitch = options->acmSwitch | (1<<num);
				break;
			case 101: { // host ip
					strncpy(options->host, optarg, MAX_IP_LEN);
					options->host[MAX_IP_LEN-1]='\0';
					if (!isValidIpAddress(options->host)) {
						fprintf(stderr, "Option parsing: parameter --host has invalid value: %s\n", optarg);
						options->host[0]='\0';
						return FAIL_E;
					}
					break;
				}
			case 102: { // host port
					 if (!str_to_uint16(optarg, &options->port)) {
						fprintf(stderr, "Option parsing: parameter --port has invalid value: %s\n", optarg);
						return FAIL_E;
					 }
					 break;
				 }
			case 'm':
				if (optarg[0]=='d') {
					options->media_mode=true;
				} 
				break;
			case 'c':
				options->count = atol(optarg);
				if (options->count<1)
				{
					fprintf(stderr, "Error: Parameter count has invalid value \"%ld\"\n", options->count);
					return FAIL_E;
				}
				break;
			case 't':
				options->time = atol(optarg);
				if (options->time<0)
				{
					fprintf(stderr, "Error: Parameter time has invalid value \"%ld\"\n", options->time);
					return FAIL_E;
				}
				break;
			case 'h':
				options->help = true;
				break;
			default:
				options->help = true;
				return FAIL_E;

		}
	}

	return SUCCESS_E;
}



// bool isReadyWait(uint8_t device, uint32_t max_time_ms)
// {
// 	BCM_UW_ERROR_MSG_ENUM retVal;
// 	BCM_UW_MODEM_VERSION_STRUCT pVer;
// 
// 	uint32_t sleep_time = 100*1000;
// 	uint32_t total_time = 0;
// 	uint32_t max_time = max_time_ms*1000;
// 
// 	for (total_time=0; total_time<=max_time; total_time+=sleep_time)
// 	{
// 		retVal = BCM_UW_API_VersionGet (device, &pVer);
// 		if (retVal == BCM_UW_SUCCESS_MSG_E)
// 		{
// 			if (total_time>0) {
// 				fprintf(stderr, "Waited %f s\n", (double)total_time/(1000*1000));
// 			}
// 			return true;
// 		}
// 		bcmUwHostSleep(sleep_time); 
// 	}
// 
// 	fprintf(stderr, "Device %d is not ready\n", device);
// 	return false;
// }
// 

void printSyntStatusStruct(const BCM_UW_RF_SYNT_STATUS_STRUCT const *syntStatus)
{
	fprintf(STATUS_OUT, "syntStatus.chipType: %c\n", syntStatus->chipType);// E.g. "S"/"T"/"R"/"?"
	fprintf(STATUS_OUT, "syntStatus.chipBand: %c\n", syntStatus->chipBand);// E.g. "U"/"K"/"X"...
	fprintf(STATUS_OUT, "syntStatus.chipVersion: 0x%02X\n", syntStatus->chipVersion); // E.g. 0xA0
	fprintf(STATUS_OUT, "syntStatus.isLocked: %s\n", (syntStatus->isLocked==TRUE)?"true":"false");
	fprintf(STATUS_OUT, "syntStatus.temperatureAdcReading: %d\n", syntStatus->temperatureAdcReading); //UINT16
	fprintf(STATUS_OUT, "syntStatus.temperatureDegrees: %d [deg] (only if calibrated)\n", syntStatus->temperatureDegrees); //INT8
	fprintf(STATUS_OUT, "syntStatus.frequency: %d [??Hz]\n", syntStatus->frequency); //UINT32
	fprintf(STATUS_OUT, "syntStatus.isValid: %s\n", (syntStatus->isValid==TRUE)?"true":"false");
}

void printChipFilesPropertiesStruct(const BCM_UW_RF_CHIP_FILES_PROPERTIES_STRUCT const *filesProperties)
{
	int ii; 
   	//BCM_UW_RF_CHIP_FILES_PROPERTIES_STRUCT filesProperties;
	fprintf(STATUS_OUT, "filesProperties.mctVersion: %d.%d.%d\n", filesProperties->mctVersion, filesProperties->mctRevision, filesProperties->mctBuild); 
	fprintf(STATUS_OUT, "filesProperties.xmlVersion: %d.%d.%d\n", filesProperties->xmlVersion, filesProperties->xmlRevision, filesProperties->xmlBuild);
	fprintf(STATUS_OUT, "filesProperties.xmlChipType: %d\n", filesProperties->xmlChipType);
	fprintf(STATUS_OUT, "filesProperties.xmlChipSN: ");
	for (ii=0;ii<11;++ii)
	{
		fprintf(STATUS_OUT, "%02x ", filesProperties->xmlChipSN[ii]);
	}
	fputc('\n',STATUS_OUT);
}


// =======================================================================
// =======================================================================
// ===== BCM_UW_API ======================================================
// =======================================================================
// =======================================================================


// RETURN_ENUM RR_API_VersionGet(uint8_t device)
// {
// 	BCM_UW_ERROR_MSG_ENUM retVal;
// 	BCM_UW_MODEM_VERSION_STRUCT pVer;
// 
// 	retVal = BCM_UW_API_VersionGet (device, &pVer);
// 	if (retVal != BCM_UW_SUCCESS_MSG_E)
// 	{
// 		fprintf(stderr, "API_VersionGet - Err no: %d\n", retVal);
// 		return(FAIL_E);
// 	}
// 	printf("650 fw version (device=%d) = %d.%d.%d\n", device, pVer.majorVersion, pVer.minorVersion, pVer.buildVersion);
// 
// 	return(SUCCESS_E);
// }


// // modem functions
RETURN_ENUM RR_API_ModemStatusGet(uint8_t device, uint8_t scIndex)
{
	BCM_UW_ERROR_MSG_ENUM       status;
	BCM_UW_MODEM_STATUS_STRUCT pMs;	

	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	status = BCM_UW_API_ModemStatusGet(device, scIndex, &pMs);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_ModemStatusGet - Error no: %d, SC %d\n", status, scIndex);
		return FAIL_E;
	}

	const static char* BCM_UW_IND_PREAMBLE_STATE_BIT_E_str[] = {"unlocked", "verify", "locked", "invalid", 0};
	const static char* networkStatus_str[] =  {"Not Locked", "Locked", "N/R = PRBS mode", "invalid", 0};

	fprintf(STATUS_OUT,"    === ModemStatusGet (SC %d) ===\n", scIndex);
	fprintf(STATUS_OUT,"acquireStatus: %s\n", BCM_UW_ACQUIRE_STATUS_ENUM_str[(pMs.acquireStatus<4)?pMs.acquireStatus:4]);
	fprintf(STATUS_OUT,"lastAcquireError: %s\n", BCM_UW_ACQUIRE_LAST_ERR_ENUM_str[(pMs.lastAcquireError<8)?pMs.lastAcquireError:8]);
	fprintf(STATUS_OUT,"absoluteMseTenths: %d [dB/10]\n", pMs.absoluteMseTenths);
	fprintf(STATUS_OUT,"normalizedMseTenths: %d [dB/10]\n", pMs.normalizedMseTenths);
	fprintf(STATUS_OUT,"radialMseTenths: %d [dB/10]\n", pMs.radialMseTenths);
	fprintf(STATUS_OUT,"internalAgc: %d [dB]\n", pMs.internalAgc);
	fprintf(STATUS_OUT,"externalAgcRegister: %d\n", pMs.externalAgcRegister);
	fprintf(STATUS_OUT,"carrierOffset: %d [Hz]\n", pMs.carrierOffset);
	fprintf(STATUS_OUT,"rxSymbolRate: %d [Baud]\n", pMs.rxSymbolRate);
	fprintf(STATUS_OUT,"txSymbolRate: %d [Baud]\n", pMs.txSymbolRate);
	fprintf(STATUS_OUT,"ldpcDecoderStress: %d (fraction in range [0,1])\n", pMs.ldpcDecoderStress);
	fprintf(STATUS_OUT,"txAcmProfile: ACM_PROFILE_%d_E\n", pMs.txAcmProfile);
	fprintf(STATUS_OUT,"rxAcmProfile: ACM_PROFILE_%d_E\n", pMs.rxAcmProfile);
	fprintf(STATUS_OUT,"acmEngineRxSensorsEnabled: %s\n", (pMs.acmEngineRxSensorsEnabled==TRUE)?"enabled":"disabled");
	fprintf(STATUS_OUT,"acmEngineTxSwitchEnabled: %s\n", (pMs.acmEngineTxSwitchEnabled==TRUE)?"enabled":"disabled");
	BYTE val=pMs.debugIndications&1;
	fprintf(STATUS_OUT,"debugIndications.RX_TIMING: %d=%s\n", val, val?"lock":"unlock");
	val = (pMs.debugIndications>>BCM_UW_IND_PREAMBLE_STATE_BIT_E)&3;
	fprintf(STATUS_OUT,"debugIndications.PREAMBLE_STATE: %d=%s\n", val, BCM_UW_IND_PREAMBLE_STATE_BIT_E_str[val]);
	val = (pMs.debugIndications>>BCM_UW_IND_LDPC_EN_BIT_E)&1;
	fprintf(STATUS_OUT,"debugIndications.LDPC_EN: %d=%s\n", val, val?"enable":"RS only mode");
	val = (pMs.debugIndications>>BCM_UW_IND_RS_UNLOCK_BIT_E)&1;
	fprintf(STATUS_OUT,"debugIndications.RS_UNLOCK: %d=%s\n", val, val?"lock":"unlock");
	val = (pMs.debugIndications>>BCM_UW_IND_LDPC_UNLOCK_BIT_E)&1;
	fprintf(STATUS_OUT,"debugIndications.LDPC_UNLOCK: %d=%s\n", val, val?"lock":"unlock");
	val = (pMs.debugIndications>>BCM_UW_IND_LDPC_UNCOR_BIT_E)&1;
	fprintf(STATUS_OUT,"debugIndications.LDPC_UNCOR: %d=%s\n", val, val?"corrected":"uncorrected");
	val = (pMs.debugIndications>>BCM_UW_IND_RS_UNCOR_BIT_E)&1;
	fprintf(STATUS_OUT,"debugIndications.RS_UNCOR: %d=%s\n", val, val?"corrected":"uncorrected");
	fprintf(STATUS_OUT,"resPhNoiseVal: 0x%08x\n", pMs.resPhNoiseVal);
	fprintf(STATUS_OUT,"aafGain: %d [dB/10] (N/A 650)\n", pMs.aafGain);
	fprintf(STATUS_OUT,"timingSnr: %d [dB/10]\n", pMs.timingSnr);
	fprintf(STATUS_OUT,"networkStatus: %s\n", networkStatus_str[(pMs.networkStatus<3)?pMs.networkStatus:3]);
	fprintf(STATUS_OUT,"txCarrierOffset: %d [Hz]\n", pMs.txCarrierOffset);
	fputc('\n',STATUS_OUT);

	return(SUCCESS_E);
}

// //DLLEXPORT BCM_UW_ERROR_MSG_ENUM BCM_UW_API_ModemAcquireCountersGet              (BYTE deviceIndex, BYTE scIndex, BCM_UW_MODEM_ACQUIRE_COUNTERS_STRUCT *pAc);
// //DLLEXPORT BCM_UW_ERROR_MSG_ENUM BCM_UW_API_ModemFecCountersGet                  (BYTE deviceIndex, BYTE scIndex, BCM_UW_MODEM_FEC_COUNTERS_STRUCT *pFc);
// //DLLEXPORT BCM_UW_ERROR_MSG_ENUM BCM_UW_API_ModemIqImbalanceStatusGet            (BYTE deviceIndex, BYTE scIndex, BCM_UW_MODEM_IQ_IMBALANCE_STATUS_STRUCT *pIs);
// //DLLEXPORT BCM_UW_ERROR_MSG_ENUM BCM_UW_API_ModemEqualizerStatusGet              (BYTE deviceIndex, BYTE scIndex, BOOLEAN xpicSlave, BCM_UW_MODEM_EQU_STATUS_STRUCT *pEs);
// //DLLEXPORT BCM_UW_ERROR_MSG_ENUM BCM_UW_API_ModemEqualizerExStatusGet            (BYTE deviceIndex, BYTE scIndex, BCM_UW_MODEM_CHAIN_ENUM chain, BCM_UW_MODEM_EQU_STATUS_STRUCT *pEs);
// //DLLEXPORT BCM_UW_ERROR_MSG_ENUM BCM_UW_API_ModemXpicStatusGet                   (BYTE deviceIndex, BYTE scIndex, BCM_UW_MODEM_XPIC_STATUS_STRUCT *pXs);
// //DLLEXPORT BCM_UW_ERROR_MSG_ENUM BCM_UW_API_ProtectionStatusGet                  (BYTE deviceIndex, BYTE scIndex, BCM_UW_PROTECTION_STATUS_STRUCT *pPs);
// 
// // modem modes
// //DLLEXPORT BCM_UW_ERROR_MSG_ENUM BCM_UW_API_ModemMrcStatusGet                    (BYTE deviceIndex, BYTE scIndex, BCM_UW_MODEM_MRC_STATUS_STRUCT *pStat);
// //DLLEXPORT BCM_UW_ERROR_MSG_ENUM BCM_UW_API_ModemMimoStatusGet                   (BYTE deviceIndex, BYTE scIndex, BCM_UW_MODEM_MIMO_STATUS_STRUCT *pStat);

// ACM =====
RETURN_ENUM RR_API_ModemAcmProfileMaskGet(uint8_t device, int scIndex)
{
        BCM_UW_ERROR_MSG_ENUM       status;
        UINT16 profilesMask;

        if (!isReadyWait(device, max_wait_ms))
        {
                return FAIL_E;
        }

        status = BCM_UW_API_ModemAcmProfileMaskGet (device, scIndex, &profilesMask);
        if (status != BCM_UW_SUCCESS_MSG_E)
        {
                fprintf(stderr, "API_ModemAcmProfileMaskGet - Error no: %d, SC %d\n", status, scIndex);
                return(FAIL_E);
        }

	fprintf(STATUS_OUT, "    === ModemAcmProfileMaskGet (SC %d) ===\n", scIndex);
	fprintf(STATUS_OUT, "profilesMask: %d 0x%04x\n", profilesMask, profilesMask); //UINT16
	fputc('\n',STATUS_OUT);

	return SUCCESS_E;
}

// // wideband
RETURN_ENUM RR_API_WbTxStatusGet(uint8_t device, int wbTxIndex)
{
	BCM_UW_ERROR_MSG_ENUM       status;
	//BCM_UW_WB_TX_NAME_ENUM wbTxIndex = BCM_UW_WB_TX_0_E;
	BCM_UW_WB_TX_STATUS_STRUCT wbTxStat;

	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	status = BCM_UW_API_WbTxStatusGet (device, wbTxIndex, &wbTxStat);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_WbTxStatusGet - Error no: %d, WbTx %s\n", status, BCM_UW_WB_TX_NAME_ENUM_str[wbTxIndex]);
		return(FAIL_E);
	}

	fprintf(STATUS_OUT, "    === WbTxStatusGet (WbTx %s) ===\n", BCM_UW_WB_TX_NAME_ENUM_str[wbTxIndex]);
	fprintf(STATUS_OUT, "ncoFreqKhz: %d [kHz]\n", wbTxStat.ncoFreqKhz); //INT32
	fprintf(STATUS_OUT, "txTxFracDelay: %d [kHz]\n", wbTxStat.txTxFracDelay); //UINT16
	fprintf(STATUS_OUT, "txTx.diffPhaseDeg: %d [deg/10]\n", wbTxStat.txTx.diffPhaseDeg); //INT16
	fprintf(STATUS_OUT, "txTx.diffGainDB: %d [dB/10]\n", wbTxStat.txTx.diffGainDB); //INT16
	fprintf(STATUS_OUT, "txTx.dcLeakageIPercent: %d [%%/10]\n", wbTxStat.txTx.dcLeakageIPercent); //INT16
	fprintf(STATUS_OUT, "txTx.dcLeakageQPercent: %d [%%/10]\n", wbTxStat.txTx.dcLeakageQPercent); //INT16
	fputc('\n',STATUS_OUT);

	return SUCCESS_E;
}

RETURN_ENUM RR_API_WbRxStatusGet(uint8_t device, int wbRxIndex)
{
	BCM_UW_ERROR_MSG_ENUM       status;
	//BCM_UW_WB_RX_NAME_ENUM wbRxIndex = BCM_UW_WB_RX_0_E;
	BCM_UW_WB_RX_STATUS_STRUCT wbRxStat;

	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	
	status = BCM_UW_API_WbRxStatusGet (device, wbRxIndex, &wbRxStat);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_WbRxStatusGet - Error no: %d, WbRx %s\n", status, BCM_UW_WB_RX_NAME_ENUM_str[wbRxIndex]);
		return(FAIL_E);
	}

	fprintf(STATUS_OUT, "    === WbRxStatusGet (WbRx %s) ===\n", BCM_UW_WB_RX_NAME_ENUM_str[wbRxIndex]);
	fprintf(STATUS_OUT, "ncoFreqKhz: %d [kHz]\n", wbRxStat.ncoFreqKhz); //INT32
	fprintf(STATUS_OUT, "rxRx.diffPhaseTenthsDeg: %d [deg/10]\n", wbRxStat.rxRx.diffPhaseTenthsDeg); //INT16
	fprintf(STATUS_OUT, "rxRx.diffGainTenthsDB: %d [dB/10]\n", wbRxStat.rxRx.diffGainTenthsDB); //INT16
	fprintf(STATUS_OUT, "rxRx.dcLeakageITenthsPercent: %d [%%/10]\n", wbRxStat.rxRx.dcLeakageITenthsPercent); //INT16
	fprintf(STATUS_OUT, "rxRx.dcLeakageQTenthsPercent: %d [%%/10]\n", wbRxStat.rxRx.dcLeakageQTenthsPercent); //INT16
	fputc('\n',STATUS_OUT);

	return SUCCESS_E;
}



RETURN_ENUM RR_API_WbTxAgcStatusGet(uint8_t device, int dacIndex)
{
	BCM_UW_ERROR_MSG_ENUM       status;
	//BCM_UW_WB_TX_DEST_NAME_ENUM dacIndex = BCM_UW_WB_TX_DEST_DAC_0_E; // for
	int ii;
	BCM_UW_WB_TXAGC_STATUS_STRUCT txagc;

	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	status = BCM_UW_API_WbTxAgcStatusGet (device, dacIndex, &txagc);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_WbTxAgcStatusGet - Error no: %d, DAC %s, dacIndex %d\n", status, BCM_UW_WB_TX_DEST_NAME_ENUM_str[dacIndex], dacIndex);
		return(FAIL_E);
	}


	fprintf(STATUS_OUT, "    === WbTxAgcStatusGet (DAC %s, dacIndex %d) ===\n", BCM_UW_WB_TX_DEST_NAME_ENUM_str[dacIndex], dacIndex);
	fprintf(STATUS_OUT, "mode: %s\n", BCM_UW_WB_TX_AGC_MODE_ENUM_str[txagc.mode]); 
	fprintf(STATUS_OUT, "lockStatus: %s\n", BCM_UW_WB_AGC_LOCK_STATUS_ENUM_str[txagc.lockStatus]); 
	fprintf(STATUS_OUT, "currentTargetPowerDbmHundredths: %d, %d [dBm/100]\n", 
			txagc.currentTargetPowerDbmHundredths[0], 
			txagc.currentTargetPowerDbmHundredths[1] 
	       ); //INT16
	fprintf(STATUS_OUT, "digitalGainsDbHundredths: %d, %d, %d [dB/100]\n",
			txagc.digitalGainsDbHundredths[0],
			txagc.digitalGainsDbHundredths[1],
			txagc.digitalGainsDbHundredths[2]
	       ); 
	fprintf(STATUS_OUT, "analogGainsDbHundredths: "); 
	for (ii=0; ii<4; ++ii)
	{
		if (ii>0) {fprintf(STATUS_OUT, ", ");}
		fprintf(STATUS_OUT, "%d", txagc.analogGainsDbHundredths[ii]);// INT16
	}
	fprintf(STATUS_OUT, "[dB/100]\n"); 
	fprintf(STATUS_OUT, "analogGainWords: "); 
	for (ii=0; ii<4; ++ii)
	{
		if (ii>0) {fprintf(STATUS_OUT, ", ");}
		fprintf(STATUS_OUT, "%d", txagc.analogGainWords[ii]); // UINT32
	}
	fputc('\n',STATUS_OUT);
	fprintf(STATUS_OUT, "analogPowerMeterWord: %d\n", txagc.analogPowerMeterWord); 
	fprintf(STATUS_OUT, "analogPowerMeterDbm: %d [dBm]\n", txagc.analogPowerMeterDbm); 
	fprintf(STATUS_OUT, "isOpenLoop: %s\n", (txagc.isOpenLoop==TRUE)?"true":"false");
	fputc('\n',STATUS_OUT);

	return SUCCESS_E;
}


RETURN_ENUM RR_API_WbRxAgcStatusGet(uint8_t device, int adcIndex)
{
	BCM_UW_ERROR_MSG_ENUM       status;
	//BCM_UW_WB_RX_SOURCE_NAME_ENUM adcIndex = BCM_UW_WB_RX_SOURCE_ADC_0_E; //for [0,6,12,15] = [BCM_UW_WB_RX_SOURCE_ADC_0_E, BCM_UW_WB_RX_SOURCE_ADC_2_E, BCM_UW_WB_RX_SOURCE_ADC_4_E, BCM_UW_WB_RX_SOURCE_ADC_5_E]
	BCM_UW_WB_RXAGC_STATUS_STRUCT rxAgcStat;
	int ii;

	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	status = BCM_UW_API_WbRxAgcStatusGet (device, adcIndex, &rxAgcStat);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_WbRxAgcStatusGet - Error no: %d, ADC %s, adcIndex %d\n", status, BCM_UW_WB_RX_SOURCE_NAME_ENUM_str[adcIndex], adcIndex);
		return(FAIL_E);
	}

	fprintf(STATUS_OUT, "    === WbRxAgcStatusGet (ADC %s, adcIndex %d) ===\n", BCM_UW_WB_RX_SOURCE_NAME_ENUM_str[adcIndex], adcIndex);
	fprintf(STATUS_OUT, "mode: %s\n", BCM_UW_WB_RX_AGC_MODE_ENUM_str[rxAgcStat.mode]); 
	fprintf(STATUS_OUT, "lockStatus: %s\n", BCM_UW_WB_AGC_LOCK_STATUS_ENUM_str[rxAgcStat.lockStatus]); 
	fprintf(STATUS_OUT, "peakDetectorLockStatus: %s\n", BCM_UW_WB_AGC_LOCK_STATUS_ENUM_str[rxAgcStat.peakDetectorLockStatus]); 
	fprintf(STATUS_OUT, "innerAgcLockStatus: ");
	for (ii=0; ii<4; ++ii)
	{
		if (ii>0) {fprintf(STATUS_OUT, ", ");}
		fprintf(STATUS_OUT, "%s", BCM_UW_WB_AGC_LOCK_STATUS_ENUM_str[rxAgcStat.innerAgcLockStatus[ii]]);
	}
	fputc('\n',STATUS_OUT);
	fprintf(STATUS_OUT, "rslDbHundredths: %d, %d [dB/100]\n", 
			rxAgcStat.rslDbHundredths[0], 
			rxAgcStat.rslDbHundredths[1] 
	       ); //INT16
	fprintf(STATUS_OUT, "peakToAvgDbHundredths: %d [dBm]\n", rxAgcStat.peakToAvgDbHundredths); // INT16 
	fprintf(STATUS_OUT, "innerAgcDigitalGainsDbHundredths: %d, %d [dB/100]\n", 
			rxAgcStat.innerAgcDigitalGainsDbHundredths[0], 
			rxAgcStat.innerAgcDigitalGainsDbHundredths[1] 
	       ); //INT16
	fprintf(STATUS_OUT, "digitalGainsDbHundredths: %d, %d, %d [dB/100]\n", 
			rxAgcStat.digitalGainsDbHundredths[0], 
			rxAgcStat.digitalGainsDbHundredths[1], 
			rxAgcStat.digitalGainsDbHundredths[2] 
	       ); //INT16
	fprintf(STATUS_OUT, "analogGainsDbHundredths: %d, %d, %d, %d [dB/100]\n", 
			rxAgcStat.analogGainsDbHundredths[0], 
			rxAgcStat.analogGainsDbHundredths[1], 
			rxAgcStat.analogGainsDbHundredths[2], 
			rxAgcStat.analogGainsDbHundredths[3] 
	       ); //INT16
	fprintf(STATUS_OUT, "analogGainWords: %d, %d, %d, %d\n", 
			rxAgcStat.analogGainWords[0], 
			rxAgcStat.analogGainWords[1], 
			rxAgcStat.analogGainWords[2], 
			rxAgcStat.analogGainWords[3] 
	       ); //UINT32
	fprintf(STATUS_OUT, "wbPowerDetectorDbHundredths: %d, %d [dB/100]\n", 
			rxAgcStat.wbPowerDetectorDbHundredths[0], 
			rxAgcStat.wbPowerDetectorDbHundredths[1] 
	       ); //INT16
	fprintf(STATUS_OUT, "bbAafPowerDetectorDbHundredths: %d, %d, %d, %d [dB/100]\n", 
			rxAgcStat.bbAafPowerDetectorDbHundredths[0], 
			rxAgcStat.bbAafPowerDetectorDbHundredths[1], 
			rxAgcStat.bbAafPowerDetectorDbHundredths[2], 
			rxAgcStat.bbAafPowerDetectorDbHundredths[3] 
	       ); //INT16
	fprintf(STATUS_OUT, "bbRrcPowerDetectorDbHundredths: %d, %d, %d, %d [dB/100]\n", 
			rxAgcStat.bbRrcPowerDetectorDbHundredths[0], 
			rxAgcStat.bbRrcPowerDetectorDbHundredths[1], 
			rxAgcStat.bbRrcPowerDetectorDbHundredths[2], 
			rxAgcStat.bbRrcPowerDetectorDbHundredths[3] 
	       ); //INT16
	fprintf(STATUS_OUT, "disturberDetected: %d\n", rxAgcStat.disturberDetected); // BYTE
	fputc('\n',STATUS_OUT);

	return SUCCESS_E;
}

// // rf
RETURN_ENUM RR_API_RF_TxStatusGet(uint8_t device, int rfTxIdx)
{
	BCM_UW_ERROR_MSG_ENUM       status;
	//BYTE rfTxIdx = 0; //BCM_UW_RF_TX_DEV_IDX_ENUM
	BCM_UW_RF_TX_STATUS_STRUCT txStat;

	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	status = BCM_UW_API_RF_TxStatusGet (device, rfTxIdx, &txStat);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_RF_TxStatusGet - Error no: %d, RfTx %d\n", status, rfTxIdx);
		return(FAIL_E);
	}

	fprintf(STATUS_OUT, "    === RF_TxStatusGet (RfTx %d) ===\n", rfTxIdx);
	fprintf(STATUS_OUT, "chipType: %c\n", txStat.chipType);// E.g. "S"/"T"/"R"/"?"
	fprintf(STATUS_OUT, "chipBand: %c\n", txStat.chipBand);// E.g. "U"/"K"/"X"...
	fprintf(STATUS_OUT, "chipVersion: 0x%02X\n", txStat.chipVersion); // E.g. 0xA0
	fprintf(STATUS_OUT, "isCalibrated: %s\n", (txStat.isCalibrated==TRUE)?"true":"false");
	fprintf(STATUS_OUT, "temperatureAdcReading: %d\n", txStat.temperatureAdcReading); //UINT16
	fprintf(STATUS_OUT, "temperatureDegrees: %d [deg] (only if calibrated)\n", txStat.temperatureDegrees); //INT8
	fprintf(STATUS_OUT, "powerOutAdcReading: %d\n", txStat.powerOutAdcReading); //UINT16
	fprintf(STATUS_OUT, "powerOutDbmTenths: %d [dB/10] (only if calibrated)\n", txStat.temperatureDegrees); //INT16
	printSyntStatusStruct(&txStat.syntStatus);
	printChipFilesPropertiesStruct(&txStat.filesProperties);
	fputc('\n',STATUS_OUT);

	return SUCCESS_E;
}

RETURN_ENUM RR_API_RF_RxStatusGet(uint8_t device, int rfRxIdx)
{
	BCM_UW_ERROR_MSG_ENUM       status;
	//BYTE rfRxIdx = 0;
	BCM_UW_RF_RX_STATUS_STRUCT rxStat;

	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	status = BCM_UW_API_RF_RxStatusGet (device, rfRxIdx, &rxStat);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_RF_RxStatusGet - Error no: %d, RfRx %d\n", status, rfRxIdx);
		return(FAIL_E);
	}

	fprintf(STATUS_OUT, "    === RF_RxStatusGet (RfRx %d) ===\n", rfRxIdx);
	fprintf(STATUS_OUT, "chipType: %c\n", rxStat.chipType);// E.g. "S"/"T"/"R"/"?"
	fprintf(STATUS_OUT, "chipBand: %c\n", rxStat.chipBand);// E.g. "U"/"K"/"X"...
	fprintf(STATUS_OUT, "chipVersion: 0x%02X\n", rxStat.chipVersion); // E.g. 0xA0
	fprintf(STATUS_OUT, "isCalibrated: %s\n", (rxStat.isCalibrated==TRUE)?"true":"false");
	fprintf(STATUS_OUT, "temperatureAdcReading: %d\n", rxStat.temperatureAdcReading); //UINT16
	fprintf(STATUS_OUT, "temperatureDegrees: %d [deg] (only if calibrated)\n", rxStat.temperatureDegrees); //INT8
	fprintf(STATUS_OUT, "rssiAdcReading: %d\n", rxStat.rssiAdcReading); //UINT16
	fprintf(STATUS_OUT, "rssiDdmTenths: %d [dB/10] (only if calibrated)\n", rxStat.rssiDdmTenths); //INT16
	printSyntStatusStruct(&rxStat.syntStatus);
	printChipFilesPropertiesStruct(&rxStat.filesProperties);
	fputc('\n',STATUS_OUT);

	return SUCCESS_E;
}

RETURN_ENUM rssGet(uint8_t device, int rfRxIdx, int16_t *rss)
{
	BCM_UW_ERROR_MSG_ENUM       status;
	//BYTE rfRxIdx = 0;
	BCM_UW_RF_RX_STATUS_STRUCT rxStat;

	status = BCM_UW_API_RF_RxStatusGet (device, rfRxIdx, &rxStat);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		//fprintf(stderr, "API_RF_RxStatusGet - Error no: %d, RfRx %d\n", status, rfRxIdx);
		return(FAIL_E);
	}

	if (rss)
	{
		*rss = rxStat.rssiDdmTenths;
	}
	//fprintf(STATUS_OUT, "rssiDdmTenths: %d [dB/10] (only if calibrated)\n", rxStat.rssiDdmTenths); //INT16
	return SUCCESS_E;
}


RETURN_ENUM RR_API_RF_RxRslGet(uint8_t device, int rfRxIdx)
{
	BCM_UW_ERROR_MSG_ENUM       status;
	//BYTE rfRxIdx = 0;
	BCM_UW_RF_RX_RSL_STRUCT rslData;

	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	status = BCM_UW_API_RF_RxRslGet (device, rfRxIdx, &rslData);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_RF_RxRslGet - Error no: %d, RfRx %d\n", status, rfRxIdx);
		return(FAIL_E);
	}

	fprintf(STATUS_OUT, "    === RF_RxRslGet (RfRx %d) ===\n", rfRxIdx);
	fprintf(STATUS_OUT, "rslDbTenths0: %d\n", rslData.rslDbTenths0); //INT16
	fprintf(STATUS_OUT, "rslDbTenths1: %d\n", rslData.rslDbTenths1); //INT16
	fputc('\n',STATUS_OUT);

	return SUCCESS_E;
}


// //flash
// DLLEXPORT BCM_UW_ERROR_MSG_ENUM BCM_UW_API_FlashInfoGet                         (BYTE deviceIndex, BYTE flashIndex, BCM_UW_FLASH_INFO_STRUCT *pVer);
// DLLEXPORT BCM_UW_ERROR_MSG_ENUM BCM_UW_API_FlashFileList                        (BYTE deviceIndex, BYTE flashIndex, BYTE *buffer, UINT32 length);
// 
// // host
// DLLEXPORT BCM_UW_ERROR_MSG_ENUM BCM_UW_API_AlarmStatusGet                       (BYTE deviceIndex, BCM_UW_ALARMS_STRUCT *pAlarmsStatus);
// 




// RETURN_ENUM RR_API_ComConfigure(uint8_t device, const struct Options_S* options)
// {
// 	BCM_UW_ERROR_MSG_ENUM retVal;
// 	BCM_UW_COM_MEDIA_ENUM media = BCM_UW_COM_MEDIA_MII_DEVICE_E;
// 	BCM_UW_COM_MEDIA_ENUM boot_media = BCM_UW_COM_MEDIA_NULL_E;
// 	uint16_t port=0;
// 	char host[MAX_IP_LEN];
// 	host[0]='\0';
// 	
// 	if (options->media_mode) {
// 		media = BCM_UW_COM_MEDIA_MII_DEVICE_E;
// 		//setIpAndPortOf650(0, MIISERVER_IP, MIISERVER_PORT);
// 		strncpy(host, MIISERVER_IP, MAX_IP_LEN);
// 		port=MIISERVER_PORT;
// 	} else {
// 		media = BCM_UW_COM_MEDIA_IP_CLIENT_E;
// 		//setIpAndPortOf650(0, SOM_IP, SOM_PORT);
// 		strncpy(host, SOM_IP, MAX_IP_LEN);
// 		port=SOM_PORT;
// 	}
// 
// 	if (options->port) {
// 		port=options->port;
// 	}
// 	if (options->host[0]) {
// 		strncpy(host, options->host, MAX_IP_LEN);
// 	}
// 	host[MAX_IP_LEN-1]='\0';
// 
// 	setIpAndPortOf650(device, host, port);
// 
// 
// 	//call API
// 	retVal = BCM_UW_API_ComConfigure(device, media, boot_media, 50L * 1000000L);
// 	if (retVal != BCM_UW_SUCCESS_MSG_E)
// 	{
// 		fprintf(stderr, "API_ComConfigure - Error no: %d\n", retVal);
// 		return(FAIL_E);
// 	}
// 	printf("API_ComConfigure - Done\n");
// 	return(SUCCESS_E);
// }
// 

// eth functions

RETURN_ENUM RR_API_EthPortInitialize(uint8_t device, int scIndex)
{
	BCM_UW_ERROR_MSG_ENUM       status;
	BCM_UW_ETH_PORT_INITIALIZE_STRUCT ethMap;
	ethMap.laneMapRx[0]=2;
	ethMap.laneMapTx[0]=2;
	ethMap.laneMapRx[1]=1;
	ethMap.laneMapTx[1]=1;
	ethMap.laneMapRx[2]=0;
	ethMap.laneMapTx[2]=0;
	ethMap.laneMapRx[3]=3;
	ethMap.laneMapTx[3]=3;


	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	status = BCM_UW_API_EthPortInitialize(device, scIndex, &ethMap);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_EthPortInitialize - Error no: %d\n", status);
		return(FAIL_E);
	}

	fprintf(STATUS_OUT, "    === EthPortInitialize (%d/%d) ===\n", device, scIndex);
	fputc('\n',STATUS_OUT);

	return SUCCESS_E;
}

RETURN_ENUM RR_API_EthPortStatusGet(uint8_t device, int scIndex, int port)
{
	BCM_UW_ERROR_MSG_ENUM       status;
	BCM_UW_ETH_PORT_STATUS_STRUCT ethStat;

	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	status = BCM_UW_API_EthPortStatusGet (device, scIndex, port, &ethStat);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_EthPortStatusGet - Error no: %d\n", status);
		return(FAIL_E);
	}

	fprintf(STATUS_OUT, "    === EthPortStatusGet (%d/%d/port %d) ===\n", device, scIndex, port);
	fprintf(STATUS_OUT, "link: %d\n", ethStat.linkStatus);
	fprintf(STATUS_OUT, "speed: %d\n", ethStat.speed);
	fputc('\n',STATUS_OUT);

	return SUCCESS_E;
}

RETURN_ENUM RR_API_EthPortConfigure(uint8_t device, int scIndex, int port)
{
	BCM_UW_ERROR_MSG_ENUM       status;
	BCM_UW_ETH_PORT_CONFIGURE_STRUCT ethConf;
	ethConf.cmdSelect = (1<<0) | (1<<1) | (1<<2) | (1<<17);
	ethConf.portIf = BCM_UW_ETH_PORT_IF_1000X_E;
	ethConf.speed = BCM_UW_ETH_PORT_2500MBPS_E;
	ethConf.portEnable = BCM_UW_ETH_PORT_ENABLE_E;

	ethConf.loopbackType = BCM_UW_ETH_PORT_LOOPBACK_PCS_LINE_E;
//	ethConf.loopbackType = BCM_UW_ETH_PORT_LOOPBACK_NONE_E;

	ethConf.autoNegotiationEnable = true;
	ethConf.crcProcessingMode = BCM_UW_ETH_PORT_KEEP_CRC_E;
	ethConf.fcPauseBehavior = BCM_UW_ETH_PORT_PAUSE_BEHAVIOR_RX_DIS_TX_DIS_E;
	//ethConf.fc


	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	status = BCM_UW_API_EthPortConfigure (device, scIndex, port, &ethConf);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_EthPortConfigure - Error no: %d\n", status);
		return(FAIL_E);
	}

	fprintf(STATUS_OUT, "    === EthPortConfigure (%d/%d/port %d) ===\n", device, scIndex, port);
	fputc('\n',STATUS_OUT);

	return SUCCESS_E;
}


RETURN_ENUM RR_API_EthPortRxCountersGet(uint8_t device, int scIndex, int port)
{
	BCM_UW_ERROR_MSG_ENUM       status;
	BCM_UW_ETH_PORT_RX_COUNTERS_STRUCT ethCount;


	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	status = BCM_UW_API_EthPortRxCountersGet(device, scIndex, port, &ethCount);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_EthPortRxCountersGet - Error no: %d\n", status);
		return(FAIL_E);
	}

	fprintf(STATUS_OUT, "    === EthPortRxCountersGet (%d/%d/port %d) ===\n", device, scIndex, port);
	fprintf(STATUS_OUT, " <=64B       = %llu\n", ((((uint64_t)ethCount.rx64OctetsHi)<<32) | ((uint64_t)ethCount.rx64OctetsLo)));
	fprintf(STATUS_OUT, " 64-127B     = %llu\n", ((((uint64_t)ethCount.rx65to127OctetsHi)<<32) | ((uint64_t)ethCount.rx65to127OctetsLo)));
	fprintf(STATUS_OUT, " 128-255B    = %llu\n", ((((uint64_t)ethCount.rx128to255OctetsHi)<<32) | ((uint64_t)ethCount.rx128to255OctetsLo)));
	fprintf(STATUS_OUT, " 256-511B    = %llu\n", ((((uint64_t)ethCount.rx256to511OctetsHi)<<32) | ((uint64_t)ethCount.rx256to511OctetsLo)));
	fprintf(STATUS_OUT, " 512-1023B   = %llu\n", ((((uint64_t)ethCount.rx512to1023OctetsHi)<<32) | ((uint64_t)ethCount.rx512to1023OctetsLo)));
	fprintf(STATUS_OUT, " 1024-1518B  = %llu\n", ((((uint64_t)ethCount.rx1024to1518OctetsHi)<<32) | ((uint64_t)ethCount.rx1024to1518OctetsLo)));
	fprintf(STATUS_OUT, " 1519-1522B  = %llu\n", ((((uint64_t)ethCount.rxGv1519to1522OctetsHi)<<32) | ((uint64_t)ethCount.rxGv1519to1522OctetsLo)));
	fprintf(STATUS_OUT, " 1519-2047B  = %llu\n", ((((uint64_t)ethCount.rx1519to2047OctetsHi)<<32) | ((uint64_t)ethCount.rx1519to2047OctetsLo)));
	fprintf(STATUS_OUT, " 2048-4095B  = %llu\n", ((((uint64_t)ethCount.rx2048to4095OctetsHi)<<32) | ((uint64_t)ethCount.rx2048to4095OctetsLo)));
	fprintf(STATUS_OUT, " 4096-9216B  = %llu\n", ((((uint64_t)ethCount.rx4096to9216OctetsHi)<<32) | ((uint64_t)ethCount.rx4096to9216OctetsLo)));
	fprintf(STATUS_OUT, " 9217-16383B = %llu\n", ((((uint64_t)ethCount.rx9217to16383OctetsHi)<<32) | ((uint64_t)ethCount.rx9217to16383OctetsLo)));
	fprintf(STATUS_OUT, " rxAllPackets= %llu\n", ((((uint64_t)ethCount.rxAllPacketsHi)<<32) | ((uint64_t)ethCount.rxAllPacketsLo)));
	fputc('\n',STATUS_OUT);

	return SUCCESS_E;
}

RETURN_ENUM RR_API_EthPortTxCountersGet(uint8_t device, int scIndex, int port)
{
	BCM_UW_ERROR_MSG_ENUM       status;
	BCM_UW_ETH_PORT_TX_COUNTERS_STRUCT ethCount;


	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	status = BCM_UW_API_EthPortTxCountersGet(device, scIndex, port, &ethCount);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_EthPortTxCountersGet - Error no: %d\n", status);
		return(FAIL_E);
	}

	fprintf(STATUS_OUT, "    === EthPortTxCountersGet (%d/%d/port %d) ===\n", device, scIndex, port);
	fprintf(STATUS_OUT, " <=64B       = %llu\n", ((((uint64_t)ethCount.tx64OctetsHi)<<32) | ((uint64_t)ethCount.tx64OctetsLo)));
	fprintf(STATUS_OUT, " 64-127B     = %llu\n", ((((uint64_t)ethCount.tx65to127OctetsHi)<<32) | ((uint64_t)ethCount.tx65to127OctetsLo)));
	fprintf(STATUS_OUT, " 128-255B    = %llu\n", ((((uint64_t)ethCount.tx128to255OctetsHi)<<32) | ((uint64_t)ethCount.tx128to255OctetsLo)));
	fprintf(STATUS_OUT, " 256-511B    = %llu\n", ((((uint64_t)ethCount.tx256to511OctetsHi)<<32) | ((uint64_t)ethCount.tx256to511OctetsLo)));
	fprintf(STATUS_OUT, " 512-1023B   = %llu\n", ((((uint64_t)ethCount.tx512to1023OctetsHi)<<32) | ((uint64_t)ethCount.tx512to1023OctetsLo)));
	fprintf(STATUS_OUT, " 1024-1518B  = %llu\n", ((((uint64_t)ethCount.tx1024to1518OctetsHi)<<32) | ((uint64_t)ethCount.tx1024to1518OctetsLo)));
	fprintf(STATUS_OUT, " 1519-1522B  = %llu\n", ((((uint64_t)ethCount.txGv1519to1522OctetsHi)<<32) | ((uint64_t)ethCount.txGv1519to1522OctetsLo)));
	fprintf(STATUS_OUT, " 1519-2047B  = %llu\n", ((((uint64_t)ethCount.tx1519to2047OctetsHi)<<32) | ((uint64_t)ethCount.tx1519to2047OctetsLo)));
	fprintf(STATUS_OUT, " 2048-4095B  = %llu\n", ((((uint64_t)ethCount.tx2048to4095OctetsHi)<<32) | ((uint64_t)ethCount.tx2048to4095OctetsLo)));
	fprintf(STATUS_OUT, " 4096-9216B  = %llu\n", ((((uint64_t)ethCount.tx4096to9216OctetsHi)<<32) | ((uint64_t)ethCount.tx4096to9216OctetsLo)));
	fprintf(STATUS_OUT, " 9217-16383B = %llu\n", ((((uint64_t)ethCount.tx9217to16383OctetsHi)<<32) | ((uint64_t)ethCount.tx9217to16383OctetsLo)));
	fprintf(STATUS_OUT, " txAllPackets= %llu\n", ((((uint64_t)ethCount.txAllPacketsHi)<<32) | ((uint64_t)ethCount.txAllPacketsLo)));
	fputc('\n',STATUS_OUT);

	return SUCCESS_E;
}

RETURN_ENUM RR_API_EthPortCountersClear(uint8_t device, int scIndex, int port)
{
	BCM_UW_ERROR_MSG_ENUM       status;


	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	status = BCM_UW_API_EthPortCountersClear(device, scIndex, port);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_EthPortCountersClear - Error no: %d\n", status);
		return(FAIL_E);
	}

	fprintf(STATUS_OUT, "    === EthPortCountersClear (%d/%d/port %d) ===\n", device, scIndex, port);
	fputc('\n',STATUS_OUT);

	return SUCCESS_E;
}

///funkce acm


RETURN_ENUM RR_API_AcmConfigurationGet(uint8_t device, int scIndex)
{
	BCM_UW_ERROR_MSG_ENUM       status;
	BCM_UW_ACM_CONFIGURATION_STRUCT aConf;
	int ii;


	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	status = BCM_UW_API_AcmConfigurationGet(device, scIndex, &aConf);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_AcmConfigurationGet - Error no: %d\n", status);
		return(FAIL_E);
	}

	fprintf(STATUS_OUT, "    === AcmConfigurationGet (%d/%d) ===\n", device, scIndex);
	for( ii = 0; ii < 16; ii ++ )
		fprintf(STATUS_OUT, " acmNumOfSymbols[%d] = %u\n", ii, aConf.acmNumOfSymbols[ii]);
	for( ii = 0; ii < 16; ii ++ )
		fprintf(STATUS_OUT, " gpiPayloadSizeInBytes[%d] = %u\n", ii, aConf.gpiPayloadSizeInBytes[ii]);
	for( ii = 0; ii < 16; ii ++ )
		fprintf(STATUS_OUT, " modulation[%d] = %s\n", ii, BCM_UW_MODULATION_TYPE_ENUM_str[aConf.modulation[ii]]);
	fputc('\n',STATUS_OUT);

	return SUCCESS_E;
}

/*
RETURN_ENUM RR_API_ModemAcmProfileMaskGet(uint8_t device, int scIndex)
{
	BCM_UW_ERROR_MSG_ENUM       status;
	uint16_t pMask;


	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	status = BCM_UW_API_ModemAcmProfileMaskGet(device, scIndex, &pMask);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_ModemAcmProfileMaskGet - Error no: %d\n", status);
		return(FAIL_E);
	}

	fprintf(STATUS_OUT, "    === ModemAcmProfileMaskGet (%d/%d) ===\n", device, scIndex);
	fprintf(STATUS_OUT, " acmProfileMask =  %04X\n", pMask);
	fputc('\n',STATUS_OUT);

	return SUCCESS_E;
}
*/

RETURN_ENUM RR_API_ModemAcmProfileEnable(uint8_t device, int scIndex, uint16_t pMask)
{
	BCM_UW_ERROR_MSG_ENUM       status;


	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	status = BCM_UW_API_ModemAcmProfileEnable(device, scIndex, pMask);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_ModemAcmProfileEnable - Error no: %d\n", status);
		return(FAIL_E);
	}

	fprintf(STATUS_OUT, "    === ModemAcmProfileEnable (%d/%d) ===\n", device, scIndex);
	fprintf(STATUS_OUT, " acmProfileMask =  0x%04X\n", pMask);
	fputc('\n',STATUS_OUT);

	return SUCCESS_E;
}

RETURN_ENUM RR_API_ModemAcmEngineEnable(uint8_t device, int scIndex, uint8_t enableRxSensors, uint8_t enableTxSwitch)
{
	BCM_UW_ERROR_MSG_ENUM       status;


	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	status = BCM_UW_API_ModemAcmEngineEnable(device, scIndex, (enableRxSensors == 0)?false:true, (enableTxSwitch == 0)?false:true);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_ModemAcmEngineEnable - Error no: %d\n", status);
		return(FAIL_E);
	}

	fprintf(STATUS_OUT, "    === ModemAcmEngineEnable (%d/%d) ===\n", device, scIndex);
	fprintf(STATUS_OUT, " enableRxSensors =  %hu\n", enableRxSensors);
	fprintf(STATUS_OUT, " enableTxSwitch =  %hu\n", enableTxSwitch);
	fputc('\n',STATUS_OUT);

	return SUCCESS_E;
}

RETURN_ENUM RR_API_ModemAcmSwitch(uint8_t device, int scIndex, BCM_UW_ACM_PROFILE_ENUM acmProfile, uint8_t isLocal)
{
	BCM_UW_ERROR_MSG_ENUM       status;


	if (!isReadyWait(device, max_wait_ms))
	{
		return FAIL_E;
	}

	status = BCM_UW_API_ModemAcmSwitch(device, scIndex, acmProfile, (isLocal == 0)?false:true);
	if (status != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_ModemAcmSwitch - Error no: %d\n", status);
		return(FAIL_E);
	}

	fprintf(STATUS_OUT, "    === ModemAcmSwitch (%d/%d) ===\n", device, scIndex);
	fprintf(STATUS_OUT, " acmProfile =  %hu\n", acmProfile);
	fprintf(STATUS_OUT, " isLocal =  %hu\n", isLocal);
	fputc('\n',STATUS_OUT);

	return SUCCESS_E;
}


// =======================================================================

int main(int argc, char *argv[])
{
	int retVal;
	uint8_t device = 0;
	//uint8_t scIndex = 0;
	bool api_open = false;
	struct Options_S options;
	struct AcmParams_S acmParams;
	long int pp;
	int ii=0;


	initOptions(&options);
	initAcmParams(&acmParams);
	retVal = parseParams(&options, &acmParams, argc, argv);
	if (retVal != SUCCESS_E) {
		return retVal;
	}

	if (options.help)
	{
		help();
		return retVal;
	}

	// call init API
	retVal = BCM_UW_API_Open();
	if (retVal != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_Open - Err no: %d\n", retVal);
		return(1);
	}
	api_open=true;
	printf("API_Open - Done\n");

	retVal = RR_API_ComConfigure(device, options.media_mode, options.host, options.port);
	if (retVal != SUCCESS_E)
	{
		fprintf(stderr, "RR_API_ComConfigure failed\n");
		if (api_open) {BCM_UW_API_Close();}
		return(1);
	}
	// // udi: why do we need to sleep? was it related to threads?
	bcmUwHostSleep(100*1000); 


	RR_API_VersionGet(device);
	// if (retVal != SUCCESS_E)
	// {
	// 	if (api_open) {BCM_UW_API_Close();}
	// 	return(1);
	// }


	if ( options.modem || options.wbtx || options.wbrx || options.wbtxagc || options.wbrxagc || options.rftx || options.rfrx || options.rfrxrsl) 
	{
		for (pp=0; pp<options.count; ++pp)
		{
			if ((options.time>0) && (pp>0))
			{
				bcmUwHostSleep(options.time*1000);
			}
			if (options.modem) 
			{
				for (ii=0; ii<SC_INDEX_COUNT; ++ii)
				{
					if (options.modem & (1<<ii))
					{
						retVal = RR_API_ModemStatusGet(device, ii);
						// if (retVal != SUCCESS_E)
						// {
						// 	if (api_open) {BCM_UW_API_Close();}
						// 	return(1);
						// }
					}
				}
			}
			if (options.wbtx) 
			{
				for (ii=0; ii<=BCM_UW_WB_TX_XC_1_E; ++ii)
				{
					if (options.wbtx & (1<<ii))
					{
						retVal = RR_API_WbTxStatusGet(device,ii);
						//BCM_UW_WB_TX_NAME_ENUM wbTxIndex = BCM_UW_WB_TX_0_E; //4
					}
				}
			}
			if (options.wbrx) 
			{
				for (ii=0; ii<=BCM_UW_WB_RX_ENV1_E; ++ii)
				{
					if (options.wbrx & (1<<ii))
					{
						retVal = RR_API_WbRxStatusGet(device,ii);
						//BCM_UW_WB_RX_NAME_ENUM wbRxIndex = BCM_UW_WB_RX_0_E; //8
					}
				}
			}
			if (options.wbtxagc) 
			{
				for (ii=0; ii<BCM_UW_WB_TX_DEST_DAC_MAX_E; ++ii)
				{
					if (options.wbtxagc & (1<<ii))
					{
						//retVal = RR_API_WbTxAgcStatusGet(device, dacIndex);
						retVal = RR_API_WbTxAgcStatusGet(device, ii);
						//BCM_UW_WB_TX_DEST_NAME_ENUM dacIndex = BCM_UW_WB_TX_DEST_DAC_0_E; // BCM_UW_WB_TX_DEST_DAC_MAX_E = 6
					}
				}
			}
			if (options.wbrxagc) 
			{
				for (ii=0; ii<BCM_UW_WB_RX_SOURCE_ADC_MAX_E; ++ii)
				{
					if (options.wbrxagc & (1<<ii))
					{
						//retVal = RR_API_WbRxAgcStatusGet(device, adcIndex);
						retVal = RR_API_WbRxAgcStatusGet(device, ii);
						//BCM_UW_WB_RX_SOURCE_NAME_ENUM adcIndex = BCM_UW_WB_RX_SOURCE_ADC_0_E; 
						//for [0,6,12,15] = [BCM_UW_WB_RX_SOURCE_ADC_0_E, BCM_UW_WB_RX_SOURCE_ADC_2_E, BCM_UW_WB_RX_SOURCE_ADC_4_E, BCM_UW_WB_RX_SOURCE_ADC_5_E]
						// BCM_UW_WB_RX_SOURCE_ADC_MAX_E = 18
					}
				}
			}
			if (options.rftx) 
			{
				for (ii=0; ii<BCM_UW_RF_TX_DEV_MAX_E; ++ii)
				{
					if (options.rftx & (1<<ii))
					{
						retVal = RR_API_RF_TxStatusGet(device,ii);
						//BYTE rfTxIdx=0
						// BCM_UW_RF_TX_DEV_IDX_ENUM // 0-2
					}
				}
			}
			if (options.rfrx) 
			{
				for (ii=0; ii<BCM_UW_RF_RX_DEV_MAX_E; ++ii)
				{
					if (options.rfrx & (1<<ii))
					{
						retVal = RR_API_RF_RxStatusGet(device,ii);
						//BYTE rfRxIdx = 0; 
						// BCM_UW_RF_RX_DEV_IDX_ENUM // 0-4
					}
				}
			}
			if (options.rfrxrsl) 
			{
				for (ii=0; ii<BCM_UW_RF_RX_DEV_MAX_E; ++ii)
				{
					if (options.rfrxrsl & (1<<ii))
					{
						retVal = RR_API_RF_RxRslGet(device,ii);
						//BYTE rfRxIdx = 0; // 
						// BCM_UW_RF_RX_DEV_IDX_ENUM // 0-4
					}
				}
			}
		}
	}

	if (options.rss) 
	{
		int16_t rss=0;
		long int rssOk = 0;
		long int rssBad = 0;
		double rssSum;
		struct timeval t1, t2;
		for (ii=0; ii<BCM_UW_RF_RX_DEV_MAX_E; ++ii)
		{
			if (options.rss & (1<<ii))
			{
				rssOk=0;
				rssBad=0;
				rssSum=0;
				gettimeofday(&t1, NULL);
				for (pp=0; pp<options.count; ++pp)
				{
					retVal = rssGet(device,ii,&rss);
					//BYTE rfRxIdx = 0; 
					// BCM_UW_RF_RX_DEV_IDX_ENUM // 0-4
					if (retVal==SUCCESS_E) {++rssOk; rssSum+=rss;}
					else {++rssBad;}
				}
				gettimeofday(&t2, NULL);
				fprintf(STATUS_OUT,"RfRxIdx: %d, Count: %ld, RSS_OK: %ld, RSS_BAD %ld, RSS_mean = %f dB, duration: %lu s, %lu us\n", ii, options.count, rssOk, rssBad, rssSum*0.1/rssOk, t2.tv_sec-t1.tv_sec, (t2.tv_usec>t1.tv_usec)?(t2.tv_usec-t1.tv_usec):(t2.tv_usec+t1.tv_usec));
			}
		}
	}
	if( options.ethInit || options.ethStatus || options.ethConfig || options.ethRxCount || options.ethTxCount || options.ethCountClear )
	{
		if( options.ethInit )
		{
			for( ii = 0; ii < SC_INDEX_COUNT; ++ii )
			{
				if( options.ethInit & (1<<ii) )
				{
					retVal = RR_API_EthPortInitialize(device, ii);
				}
			}
		}
		if( options.ethConfig )
		{
			for( ii = 0; ii < SC_INDEX_COUNT; ++ii )
			{
				if( options.ethConfig & (1<<ii) )
				{
					retVal = RR_API_EthPortConfigure(device, ii, 0);
					retVal = RR_API_EthPortConfigure(device, ii, 2);
				}
			}
		}
		if( options.ethStatus )
		{
			for( ii = 0; ii < SC_INDEX_COUNT; ++ii )
			{
				if( options.ethStatus & (1<<ii) )
				{
					retVal = RR_API_EthPortStatusGet(device, ii, 0);
					retVal = RR_API_EthPortStatusGet(device, ii, 2);
				}
			}
		}
		if( options.ethRxCount )
		{
			for( ii = 0; ii < SC_INDEX_COUNT; ++ii )
			{
				if( options.ethRxCount & (1<<ii) )
				{
					retVal = RR_API_EthPortRxCountersGet(device, ii, 0);
					retVal = RR_API_EthPortRxCountersGet(device, ii, 2);
				}
			}
		}
		if( options.ethTxCount )
		{
			for( ii = 0; ii < SC_INDEX_COUNT; ++ii )
			{
				if( options.ethTxCount & (1<<ii) )
				{
					retVal = RR_API_EthPortTxCountersGet(device, ii, 0);
					retVal = RR_API_EthPortTxCountersGet(device, ii, 2);
				}
			}
		}
		if( options.ethCountClear )
		{
			for( ii = 0; ii < SC_INDEX_COUNT; ++ii )
			{
				if( options.ethCountClear & (1<<ii) )
				{
					retVal = RR_API_EthPortCountersClear(device, ii, 0);
					retVal = RR_API_EthPortCountersClear(device, ii, 2);
				}
			}
		}
	}

	if( options.acmConfig || options.acmprofile || options.acmProfileEnable || options.acmEngineEnable || options.acmSwitch )
	{
		if( options.acmConfig )
		{
			for( ii = 0; ii < SC_INDEX_COUNT; ++ii )
			{
				if( options.acmConfig & (1<<ii) )
				{
					retVal = RR_API_AcmConfigurationGet(device, ii);
				}
			}
		}
		if (options.acmprofile) 
		{
			for (ii=0; ii<SC_INDEX_COUNT; ++ii)
			{
				if (options.acmprofile & (1<<ii))
				{
					retVal = RR_API_ModemAcmProfileMaskGet(device, ii);
				}
			}
		}
		if( options.acmProfileEnable )
		{
			for( ii = 0; ii < SC_INDEX_COUNT; ++ii )
			{
				if( options.acmProfileEnable & (1<<ii) )
				{
					retVal = RR_API_ModemAcmProfileEnable(device, ii, acmParams.profileMask);
				}
			}
		}
		if( options.acmEngineEnable )
		{
			for( ii = 0; ii < SC_INDEX_COUNT; ++ii )
			{
				if( options.acmEngineEnable & (1<<ii) )
				{
					retVal = RR_API_ModemAcmEngineEnable(device, ii, acmParams.enableRxSensors, acmParams.enableTxSwitch);
				}
			}
		}
		if( options.acmSwitch )
		{
			for( ii = 0; ii < SC_INDEX_COUNT; ++ii )
			{
				if( options.acmSwitch & (1<<ii) )
				{
					retVal = RR_API_ModemAcmSwitch(device, ii, acmParams.acmProfile, acmParams.isLocal);
				}
			}
		}
	}


	if (api_open) {BCM_UW_API_Close();}

	return(0);
}
