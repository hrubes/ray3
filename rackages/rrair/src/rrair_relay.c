


#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <getopt.h>
//#include <errno.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <bcm_uw_api.h>
#include <hostProtocolStructs.h>

#include "hostDriverRrPc.h"
#include "rrair_lib.h"

#define PROGNAME "rrair_relay"


struct Options_S {
	//ACTION_ENUM action;
	bool verbose;
	bool help;
	MEDIA_MODE_E media_mode;
        char ip[MAX_IP_LEN];
	uint16_t port;
        char host[MAX_IP_LEN];
	//bool bcmver;
};

void initOptions(struct Options_S* options)
{
	//options->action=VER_E;
	options->verbose=0;
	options->help=false;
	options->media_mode=MEDIA_MODE_RR;
	options->ip[0]='\0';
	options->port=0;
	options->host[0]='\0';
	//options->bcmver = true;
}



int help(void)
{
	printf("Usage: " PROGNAME " [OPTIONS]\n");
	printf("Relay server for 85650 messages\n\n");
	printf("      --ip <server_ip> - default: all (0.0.0.0)\n");
	printf("      --port <server_port> - default: %d\n", SOM_PORT);
	printf("      --host <85650_ip> - default: %s\n", RR_IP);
	
	//printf("  -v, --verbose\n");
	printf("  -h, --help - print this help\n");
	printf("Numbers in brackets shows proper ordering of the operations.\n\n");

	return SUCCESS_E;
}





int parseParams(struct Options_S* options, int argc, char* const argv[])
{
	if (options==NULL) 
	{
		return FAIL_E;
	}

	// if (argc==2) {
	// 	options->fwload = true;
	// 	strncpy(options->filename, argv[1], FN_SIZE);
	// 	options->filename[FN_SIZE-1]=0;
	// }
	static struct option long_options_[] =
	{
		{"help", no_argument, 0, 'h'},
		//        {"debug", no_argument, 0, 'd'},
		//        {"version", no_argument, 0, 'v'},
		//        {"test", no_argument, 0, 't'},
		{"ip", required_argument, 0, 7},
		{"port", required_argument, 0, 8},
		{"host", required_argument, 0, 9},
		//{"mode", required_argument, 0, 'm'},
		{0, 0, 0, 0}
	};
	static char short_options_[]="hm:";

	int option_index = 0;
	int opt;
	optind=0;

	while((opt = getopt_long(argc, argv, short_options_, long_options_, &option_index)) != -1) {
		switch (opt) {
			case 0: {
					//"getopt_long" nastavil primo promennou
					if (long_options_[option_index].flag != 0)
						break;
					switch (option_index)
					{
						default: {
								 fprintf(stderr, "Option parsing - strange option.\n"); //k tomu by nemelo dojit
								 return FAIL_E;
							 }
					}
					break;
				}
			case 7: { // server ip
					strncpy(options->ip, optarg, MAX_IP_LEN);
					options->host[MAX_IP_LEN-1]='\0';
					if (!isValidIpAddress(options->host)) {
						fprintf(stderr, "Option parsing: parameter --ip has invalid value: %s\n", optarg);
						options->host[0]='\0';
						return FAIL_E;
					}
					break;
				}
			case 8: { // server port
					 if (!str_to_uint16(optarg, &options->port)) {
						fprintf(stderr, "Option parsing: parameter --port has invalid value: %s\n", optarg);
						return FAIL_E;
					 }
					 break;
				 }
			case 9: { // host ip
					strncpy(options->host, optarg, MAX_IP_LEN);
					options->host[MAX_IP_LEN-1]='\0';
					if (!isValidIpAddress(options->host)) {
						fprintf(stderr, "Option parsing: parameter --host has invalid value: %s\n", optarg);
						options->host[0]='\0';
						return FAIL_E;
					}
					break;
				}
			// case 'm':
			// 	if (optarg[0]=='d') {
			// 		options->media_mode=true;
			// 	} 
			// 	break;
			case 'h':
				options->help = true;
				break;
			default:
				options->help = true;
				return FAIL_E;

		}
	}


	return SUCCESS_E;
}

//========================================================================
//========================================================================
//========================================================================


int open_socket(const char *ipAddress, uint16_t port)
{
	int sock;
	struct sockaddr_in sa;
	int optval;


	sa.sin_family = AF_INET;
	sa.sin_port = htons(SOM_PORT);
	if (port) {
		sa.sin_port = htons(port);
	}
	if (ipAddress[0]) {
		if (inet_pton(AF_INET, ipAddress, &(sa.sin_addr)) != 1)
		{
			fprintf(stderr,"Invalid ip address: %s\n", ipAddress);
			return FAIL_E;
		}
	} else {
		sa.sin_addr.s_addr = htonl(INADDR_ANY);
	}


	sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sock < 0)
	{
		fprintf(stderr,"Socket Error - sock() failed %d\n", sock);
		return FAIL_E;
	}

	if (bind(sock, (struct sockaddr *) &sa, sizeof(struct sockaddr_in)) < 0)
	{
		fprintf(stderr,"Socket Error - bind() failed\n");
		close(sock);
		return FAIL_E;
	}

	// minimize UDP receive buffer - no need for more than this
	optval = 4096;
	setsockopt(sock, SOL_SOCKET, SO_RCVBUF, (void*)&optval, sizeof(int));
	return(sock);
}



int msg_relay(const char *ipAddress, uint16_t port)
{
	int server_sock;
	ssize_t received_size;
	unsigned char recv_buff[BCM_UW_HOST_PROTOCOL_MAX_MSG_LEN + BCM_UW_COM_IP_PROTOCOL_HEADER_LEN];
	unsigned char resp_buff[BCM_UW_HOST_PROTOCOL_MAX_MSG_LEN + BCM_UW_COM_IP_PROTOCOL_HEADER_LEN];
	INT32 resp_buff_len;
	BCM_UW_ERROR_MSG_ENUM retVal;
	int ret;

	server_sock = open_socket(ipAddress, port);
	if (server_sock < 0)
	{
		fprintf(stderr,"Socket Error - at open\n");
		return(0);
	}

	fprintf(stderr,"MSG_RELAY Started - Listening on port %d\n", (port?port:SOM_PORT));

	while(1)
	{
		struct      sockaddr_in addr;
		socklen_t   addr_len = sizeof(addr);

		received_size = recvfrom(server_sock, recv_buff, sizeof(recv_buff), 0, (struct sockaddr*)&addr, &addr_len);
		if (received_size <= 0)
		{
			if (received_size < 0)
			{
				fprintf(stderr,"recv() failed\n");
			} else {
				fprintf(stderr,"MSG_RELAY - Connection closed by peer\n");
			}
			continue;
		}

		//debug print
		// printf("Rcv from %s:%d Data: ", inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
		// for (i=0; i<received_size; i++)
		// { printf("%02X ", recv_buff[i]); }
		// printf("\n");

		retVal = BCM_UW_API_ComMessageForward(recv_buff, received_size, resp_buff, &resp_buff_len);
		if ( (retVal != BCM_UW_SUCCESS_MSG_E) && (retVal != BCM_UW_BOOT_PLL_ERROR_E) )
		{
			fprintf(stderr,"BCM_UW_API_ComMessageForward - error %d\n", retVal);
			//usleep(10);//it needed in order to enable context switching with other threads
		}

		if (resp_buff_len == 0)
		{
			fprintf(stderr,"BCM_UW_API_ComMessageForward - zero length answer\n");
			continue;
		}

		//debug print
		// printf("Sent to sck: ");
		// for (i=0; i<resp_buff_len; i++)
		// { printf("%02X ", resp_buff[i]); }
		// printf("\n");

		ret = sendto(server_sock, resp_buff, resp_buff_len, 0, (const struct sockaddr*)&addr, addr_len);
		if (ret < 0)
		{
			fprintf(stderr,"sendto - failed\n");
			//exit(1);
		}
	}

	close(server_sock);
	return(SUCCESS_E);
}




//========================================================================
//========================================================================
//========================================================================


int main(int argc, char *argv[])
{
	int retVal;
	uint8_t device = 0;
	bool api_open = false;
	struct Options_S options;

	initOptions(&options);
	retVal = parseParams(&options, argc, argv);
	if (retVal != SUCCESS_E) {
		return retVal;
	}

	if (options.help)
	{
		help();
		return retVal;
	}

	// call init API
	retVal = BCM_UW_API_Open();
	if (retVal != BCM_UW_SUCCESS_MSG_E)
	{
		fprintf(stderr, "API_Open - Err no: %d\n", retVal);
		return(1);
	}
	api_open=true;
	printf("API_Open - Done\n");

	retVal = RR_API_ComConfigure(device, options.media_mode, options.host, MIISERVER_PORT);
	if (retVal != SUCCESS_E)
	{
		fprintf(stderr, "RR_API_ComConfigure failed\n");
		if (api_open) {BCM_UW_API_Close();}
		return(1);
	}
	// // udi: why do we need to sleep? was it related to threads?
	// Who the hell is udi? Someone from Broadcom/Maxlinear
	bcmUwHostSleep(100*1000); 



	retVal = RR_API_VersionGet(device);
	if (retVal != SUCCESS_E)
	{
		//if (api_open) {BCM_UW_API_Close();}
		//return(1);
		fprintf(stderr, "Warning: 85650 was not accessible at relay server startup\n");
	}


	retVal = msg_relay(options.ip, options.port);


	if (api_open) {BCM_UW_API_Close();}

	return(0);
}
