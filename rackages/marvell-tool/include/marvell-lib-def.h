#ifndef _MARVELL_LIB_DEF_H_
#define _MARVELL_LIB_DEF_H_


/*AUTONEGO
* Description: Enumeration of Autonegotiation mode.
*    Auto for both speed and duplex.
*    Auto for speed only and Full duplex.
*    Auto for speed only and Half duplex. (1000Mbps is not supported)
*    Auto for duplex only and speed 1000Mbps.
*    Auto for duplex only and speed 100Mbps.
*    Auto for duplex only and speed 10Mbps.
*    1000Mbps Full duplex.
*    100Mbps Full duplex.
*    100Mbps Half duplex.
*    10Mbps Full duplex.
*    10Mbps Half duplex.
*/
typedef enum
{
	SPEED_AUTO_DUPLEX_AUTO=0,
	SPEED_1000_DUPLEX_AUTO,
	SPEED_100_DUPLEX_AUTO,
	SPEED_10_DUPLEX_AUTO,
	SPEED_AUTO_DUPLEX_FULL,
	SPEED_AUTO_DUPLEX_HALF,
	SPEED_1000_DUPLEX_FULL,
	SPEED_1000_DUPLEX_HALF,
	SPEED_100_DUPLEX_FULL,
	SPEED_100_DUPLEX_HALF,
	SPEED_10_DUPLEX_FULL,
	SPEED_10_DUPLEX_HALF
} AUTO_MODE;

// type port 
typedef enum
{
	PORT_MII_FD = 0,
	PORT_MII_PHY,
	PORT_MII_MAC,
	PORT_GMII,
	PORT_RMII_PHY,
	PORT_RMII_MAC,
	PORT_XMII_TRISTATE,
	PORT_GRMII,
	PORT_1000BASE_X,
	PORT_SGMII,
	PORT_2500BASE_X,
	PORT_10G_XAUI,
	PORT_10G_RXAUI,
	PORT_PHY,
	PORT_VACANT
} TYPE_PORT;

//flow control
typedef enum
{
	NO_PAUSE = 0,
	PAUSE,
	ASYMMETRIC_PAUSE,
	BOTH_PAUSE
} PAUSE_MODE;

//speed
typedef enum 
{
	SPEED_10 = 0,
	SPEED_100,
	SPEED_1000,
	SPEED_2500
} SPEED;



//link status
typedef enum
{
	LINK_OFF = 0,
	LINK_COPPER = 1,
	LINK_FIBER = 2
} LINK_STATUS_E;

//duplex
typedef enum
{
	DUPLEX_HALF = 0,
	DUPLEX_FULL
} DUPLEX;

typedef enum
{	NOT_FORCE_DUPLEX =0,
	FORCE_DUPLEX_HALF,
	FORCE_DUPLEX_FULL
} FORCE_DUPLEX;

//enable
typedef enum
{
	DISABLED = 0,
	ENABLED
} ENABLE;

//link
typedef enum
{
	DOWN = 0,
	UP
} LINK;

typedef enum
{	
	NOT_FORCE_LINK=0,
	FORCE_LINK_DOWN ,
	FORCE_LINK_UP
} FORCE_LINK;

//speed
typedef enum
{	
	NOT_FORCE_SPEED=0,
	FORCE_SPEED_10 ,
	FORCE_SPEED_100,
	FORCE_SPEED_1000,
	FORCE_SPEED_2500
} FORCE_SPEED;

//MDI
typedef enum
{
	MDI_NA = 0,
	MDI,
	MDIX,
} MDI_MDIX;

//pause
typedef enum 
{
	PAUSE_NOTIMPLEMENT = 0,
	PAUSE_IMPLEMENT,
} PAUSE_EN;

typedef enum 
{
	PAUSE_NOTADV = 0,
	PASE_ADV,
} MY_PAUSE;

typedef enum 
{
	PHY_NOTATTACHED = 0,
	PHY_ATTACHED,
} PHY_DETECT;

//MTU
typedef enum
{
    JUMBO_MODE_1522 = 0,
    JUMBO_MODE_2048,
    JUMBO_MODE_10240
} JUMBO_MODE;

//Port State
typedef enum
{
    PORT_STATE_DISABLED = 0,
    PORT_STATE_LISTENING,
    PORT_STATE_LEARNING,
    PORT_STATE_FORWARDING
} PORT_STATE;


//Port Scheduling
typedef enum
{
    PORT_WRR = 0,
    PORT_STRICT3_WRR,
    PORT_STRICT3_2_WRR,
    PORT_STRICT
} PORT_SCHEDUL;

typedef enum 
{
	NO_SYNC = 0,
	SYNC
} SYNC_STATUS;

typedef enum
{
	FRAME_MODE_NORMAL_NETWOK=0,
	FRAME_MODE_DSA,
	FRAME_MODE_PROVIDER,
	FRAME_MODE_ETHTYPEDSA
}FRAME_MODE;

typedef enum
{
	EGRESS_MODE_UNMODIFIED=0,
	EGRESS_MODE_UNTAGGED,
	EGRESS_MODE_TAGGED
}EGRESS_MODE;



//define autonego COPPER
#define AUTONEGO_1000_MASTER_ENABLE	0x1000 	//1 manual 0 automatic MASTER/SLAVE
#define AUTONEGO_1000_MASTER_VALUE	0x0800	//1 MAUAL A master, 0 MANUAL as slave
#define AUTONEGO_1000_PORT_TYPE		0x0400 	//1 multi port, 0 single port
#define AUTONEGO_1000_FD		0x0200	//1 Advertise 1000BASE-FD
#define AUTONEGO_1000_HD		0x0100	//1 Advertise 1000BASE-HD
#define AUTONEGO_ASYMMETRIC_PAUSE	0x0040	//advertise asymetric pause
#define AUTONEGO_PAUSE			0x0020	//advertise pause
#define AUTONEGO_100_T4			0x0010	//Advertise 100Base-T4
#define AUTONEGO_100_FULL		0x0008	//Advertise 100Base-FDX
#define AUTONEGO_100_HALF		0x0004	//Advertise 100Base-HDX
#define AUTONEGO_10_FULL		0x0002	//Advertise 10Base-FDX
#define AUTONEGO_10_HALF		0x0001 	//Advertise 10Base-HDX


//define linkPartner COPER
#define LP_1000_MASTER_FAULT		0x8000
#define LP_1000_MASTER_RES		0x4000
#define LP_1000_LOCAL_STATUS		0x2000
#define LP_1000_REMOTE_STATUS		0x1000
#define LP_1000B_TXFDX			0x0800  //LP Advertise 1000Base-TX FDX
#define LP_1000B_TXHDX			0x0400  //LP Advertise 1000Base-TX HDX
#define LP_ASYMPAUS			0x0040	//LP advertise asymmetric pause
#define	LP_PAUSE			0x0020	//LP advertise pause
#define LP_100B_T4			0x0010  //LP Advertise 100Base-T4
#define LP_100B_TXFDX			0x0008  //LP Advertise 100Base-TX FDX
#define LP_100B_TXHDX			0x0004  //LP Advertise 100Base-TX HDX
#define LP_10B_TFDX			0x0002  //LP Advertise 10Base-T FDX
#define LP_10B_THDX			0x0001  //LP Advertise 10Base-T HDX


struct LINK_STATUS {
	__u8 portNo;
	TYPE_PORT portType;
	LINK link;
	SPEED  speed;
	SYNC_STATUS sync;
	DUPLEX duplex;
	ENABLE autonego;
	ENABLE transmitPause;
	ENABLE receivePause;
	ENABLE resolvedSpeedDuplex;
	MDI_MDIX mdi;
	__u16	autonegoAd;
	__u16	linkPartner;
};

//##############################################

struct PORT_STATUS {
	__u8 portNo;
	TYPE_PORT portType;
	PAUSE_EN pauseEn;
	MY_PAUSE myPause;
	PHY_DETECT phyDetect;
	LINK link;
	DUPLEX duplex;
	SPEED speed;
	FORCE_LINK forceLink;
	FORCE_DUPLEX forceDuplex;
	FORCE_SPEED forceSpeed;
	JUMBO_MODE jumbo;
	ENABLE txPaused;
	ENABLE flowCtrl;
	PORT_STATE portState;
	FRAME_MODE frameMode;
	EGRESS_MODE egressMode;

};

//##############################################
//rmon 
//histogram mode
typedef enum
{
	COUNT_NO_SET,
	COUNT_RX_ONLY = 1,
	COUNT_TX_ONLY,
	COUNT_RX_TX,
} HISTOGRAM_MODE;
//rmon counter
typedef struct RMON_COUNTER_
{
	__u32    InGoodOctetsLo;   /* reg 0 */
	__u32    InGoodOctetsHi;   /* reg 1 */
	__u32    InBadOctets;    	/* reg 2 */
	__u32    OutFCSErr;        /* reg 3 */
	__u32    InUnicasts;       /* reg 4 */
	__u32    Deferred;         /* reg 5 */
	__u32    InBroadcasts;     /* reg 6 */
	__u32    InMulticasts;     /* reg 7 */
	/* Histogram Counters : Rx Only, Tx Only, or both Rx and Tx */
	__u32    Octets64;        	/* 64 Octets, reg 8 */
	__u32    Octets127;        /* 65-127 Octets, reg 9 */
	__u32    Octets255;        /* 128-255 Octets, reg 10 */
	__u32    Octets511;        /* 256-511 Octets, reg 11 */
	__u32    Octets1023;       /* 512-1023 Octets, reg 12 */
	__u32    OctetsMax;        /* 1024-Max Octets, reg 13 */
	__u32    OutOctetsLo;      /* reg 14 */
	__u32    OutOctetsHi;      /* reg 15 */
	__u32    OutUnicasts;      /* reg 16 */
	__u32    Excessive;        /* reg 17 */
	__u32    OutMulticasts;    /* reg 18 */
	__u32    OutBroadcasts;    /* reg 19 */
	__u32    Single;           /* reg 20 */
	__u32    OutPause;         /* reg 21 */
	__u32    InPause;          /* reg 22 */
	__u32    Multiple;         /* reg 23 */
	__u32    Undersize;        /* reg 24 */
	__u32    Fragments;        /* reg 25 */
	__u32    Oversize;         /* reg 26 */
	__u32    Jabber;           /* reg 27 */
	__u32    InMACRcvErr;      /* reg 28 */
	__u32    InFCSErr;         /* reg 29 */
	__u32    Collisions;       /* reg 30 */
	__u32    Late;             /* reg 31 */
	__u16    InFitered;
	__u16    OutFiltered;
}RMON_COUNTER;

//Serdes reg table
struct SERDES_REGS
{	
	int	status;
	__u16	value;
};

//port mode
typedef enum
{
	MODE_NONE=0,
	MODE_2500BASE_X,
	MODE_1000BASE_X,
	MODE_SGMII,
	MODE_SGMII_1000,
	MODE_SGMII_100,
	MODE_SGMII_10,
} PORT_MODE;

typedef enum
{
	REFERENCE_CLOCK_NONE=0,
	REFERENCE_CLOCK_SE_SCLK,
	REFERENCE_CLOCK_XTAL,
} REFERENCE_CLOCK;

typedef enum
{
	REC_CLOCK_NONE=0,
	REC_CLOCK_PRI,
	REC_CLOCK_SEC,
	REC_CLOCK_NO,
} REC_CLOCK;

struct SYNCE_STATUS
{
	__u8	priRecClk;
	__u8	secRecClk;
	__u16	portMacSCR[11];		//pouziti indexu dle portu, je to jen pro PHY tedy 1..8
};

//PRBS
struct PRBS_STATUS
{
	__u8	status;
	int64_t counterTx;
	int64_t counterRx;
	int64_t counterError;
};

#endif
