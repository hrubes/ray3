#ifndef _MARVELL_LIB_RMON_H_
#define _MARVELL_LIB_RMON_H_


struct TABLE_RMON {
	int port;
	LINK portLink;
	RMON_COUNTER data_rmon[PORT_NUM];
	__u32 discards;
	__u16 inFiltered;
	__u16 outFiltered;
};

#endif
