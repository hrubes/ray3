#ifndef _MARVELL_LIB_H_
#define _MARVELL_LIB_H_
#include "marvell-interface.h"
#include "marvell-lib-def.h"


//##############################################


//otevreni a zavreni interface
int mvOpen (const char * devname);
int mvClose (int fd);

// zapis nebo cteni libovolneho registru
int mvRegRead (int fd, __u8 addr, __u8 reg, __u16* value);
int mvRegWrite (int fd, __u8 addr, __u8 reg, __u16 value);
int mvRegWriteMask (int fd, __u8 addr, __u8 reg, __u16 value, __u16 mask);
// cteni libovolneho poctu registru  (numfield) o registru (reg) z adresy (addr)
int mvAllRegRead (int fd,__u8 addr,__u8 reg, __u16* field, unsigned int numfield);
// PHY
int mvPhyRegRead (int fd, __u8 addr, __u8 reg, __u16* value);
int mvPhyRegWrite (int fd, __u8 addr, __u8 reg, __u16 value);
int mvPhyRegWriteMask (int fd, __u8 addr, __u8 reg, __u16 value, __u16 mask);
//// zapis nebo cteni libovolneho registru v page
int mvPhyRegPageRead (int fd, __u8 addr, __u8 reg, __u8 page ,__u16* value);
int mvPhyRegPageWrite (int fd, __u8 addr, __u8 reg, __u8 page ,__u16 value);
int mvPhyRegPageWriteMask (int fd, __u8 addr, __u8 reg, __u8 page ,__u16 value, __u16 mask);
// cteni libovolneho poctu registru  (numfield) o registru (reg) z adresy (addr)
int mvPhyAllRegRead (int fd,__u8 addr,__u8 reg, __u16* field, unsigned int numfield);



// kontrola platnosti adresy registru nebo stranky
int mvCheckAddr (__u16 data, __s8 * table);
#ifdef RR_PRODUCT_bk2
int mvCheckPage (__u8 addr,__u16 data);
#else
int mvCheckPage (__u16 data);
#endif
int mvCheckReg (__u16 data);
int mvCheckRegSerdes (__u16 reg, __u16 *defReg);

// nastavovani a status PHY
TYPE_PORT mvCheckTypePort (int fd, __u8 port);
int mvSetPhySpeed (int fd , __u8 addr, SPEED speed);
int mvPhyReset (int fd, __u8 port, __u16 value);
int mvPhyAutoNegEnable (int fd, __u8 port, ENABLE enable);
int mvPhyPowerDown (int fd, __u8 port, ENABLE enable);
int mvPhyRestartAutoNeg (int fd, __u8 port);
int mvSetPhyDuplexMode (int fd, __u8 port, DUPLEX dMode);
int mvSetPhyAutoNego(int fd, __u8 port, AUTO_MODE mode);
int mvGetPhyLinkStatus (int fd, __u8 port, struct LINK_STATUS *linkStatus);
int mvGetPhyInError (int fd, __u8 port, __u16* errors);
int mvSetPhyPause(int fd, __u8 port, PAUSE_MODE mode);

//nastavovani serdesu
int mvSerdesReset (int fd, __u8 port, __u16 value);
int mvSerdesPowerDown (int fd, __u8 port, ENABLE enable);
int mvSerdesRestartAutoNeg (int fd, __u8 port);
int mvSerdesAutoNegEnable (int fd, __u8 port, ENABLE enable);
int mvSerdesSpeed (int fd , __u8 port ,SPEED speed);
int mvSerdesDuplexMode (int fd, __u8 port, DUPLEX dMode);
int mvGetSerdesLinkStatus (int fd, __u8 port, struct LINK_STATUS *linkStatus);


//nastavovani a status switch portu
int mvGetPortStatus (int fd, __u8 port, struct PORT_STATUS *status);
int mvSetForceDuplex (int fd, __u8 port,  FORCE_DUPLEX duplex);
int mvSetForceSpeed (int fd, __u8 port,  FORCE_SPEED speed);
int mvSetForceLink (int fd, __u8 port,  FORCE_LINK link);
int mvSetJumboMode (int fd, __u8 port,  JUMBO_MODE mtu);
int mvSetPhyDetect (int fd, __u8 port, ENABLE detect);
int mvSetPortState (int fd, __u8 port,PORT_STATE state);
int mvCheckPortCmode (int fd, __u8 port, __u16 *cmode);
int mvGetPortLink (int fd, __u8 port, LINK *link);




// rmon
int mvFlushRmonPort (int fd, __u8 port);
int mvFlushRmonAllPorts (int fd);
int mvReadRmon (int fd, __u8 port, void* data,  HISTOGRAM_MODE histogramMode);
// globali nastavovani a status
int mvSetCpuPort (int fd,__u8 port);

//SyncE + 1PPS
int set1ppsOut (int fd);
int setSynceOut (int fd);
int setSynce (int fd, __u8  port, REFERENCE_CLOCK refClock, REC_CLOCK recClock);
int getSynceStatus (int fd, SYNCE_STATUS * status);

//zjisteni nastaveni P9_SMODE a P10_SMODE
int mvCheckPxSmodes (int fd);
#endif
