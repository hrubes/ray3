#ifndef _MARVELL_INTERFACE_H_
#define _MARVELL_INTERFACE_H_

//use port


//prototypy funkci switch
int regRead (int fd, __u8 addr, __u8 reg, __u16* value);
int regWrite (int fd, __u8 addr, __u8 reg, __u16 value);
int readAllReg (int fd,__u8 addr, __u8 reg ,__u16* field, unsigned int numfield);
int regWriteMask (int fd, __u8 addr, __u8 reg, __u16 value, __u16 mask);

//prototypy funkci Phy
int setPhyPage (int fd,__u8 addr, __u8 reg ,__u8 page);
int regPhyRead (int fd, __u8 addr, __u8 reg, __u16* value);
int regPhyWrite (int fd, __u8 addr, __u8 reg, __u16 value);
int readPhyAllReg (int fd,__u8 addr, __u8 reg ,__u16* field, unsigned int numfield);
int regPhyWriteMask (int fd, __u8 addr, __u8 reg, __u16 value, __u16 mask);
int regPhyPageWrite (int fd, __u8 addr, __u8 reg, __u8 page ,__u16 value);
int regPhyPageRead (int fd, __u8 addr, __u8 reg, __u8 page ,__u16* value);
int regPhyPageWriteMask (int fd, __u8 addr, __u8 reg, __u8 page ,__u16 value, __u16 mask);

//prototypy funkci SERDES
int serdesRegWrite (int fd, __u8 port, __u16 reg, __u16 value);
int serdesRegWriteMask (int fd, __u8 port, __u16 reg, __u16 value, __u16 mask);
int serdesRegRead (int fd, __u8 port, __u16 reg, __u16* value);
int serdesReadAllReg (int fd,__u8 port, __u16 reg ,__u16* field, unsigned int numfield);

int checkAddr (__u8 addr, __s8 *defTab);
#ifdef RR_PRODUCT_bk2
int checkPage (__u8 addr, __u8 page);
#else
int checkPage ( __u8 page);
#endif
int checkReg (__u8 reg);
int checkRegSerdes (__u16 reg, __u16 *defReg);
#endif
