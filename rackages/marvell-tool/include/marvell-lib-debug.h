#ifndef _MARVELL_LIB_DEBUG_H_
#define _MARVELL_LIB_DEBUG_H_

#define INFO(args...)  { if(verbose>=0) {printf ("Info:  "); printf(args); } }
#define DEBUG(args...)  { if(verbose>0) {printf ("Debug:  "); printf(args); } }
#define DEBUG_ALL(args...)  { if(verbose>1) {printf ("Debug:  "); printf(args);} }

#endif
