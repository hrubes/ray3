#ifndef _MARVELL_PRINT_H_
#define _MARVELL_PRINT_H_


void printAllReg (__u8 addr, __u16* regs, int pos, int len);
void printAllPageReg  (__u8 addr, __s8 page, __u16* regs, int pos, int len);
void printPhyStatus  (struct LINK_STATUS *linkStatus,__u16 inErrors,  __u8 port);
void printPortStatus  (struct PORT_STATUS *portStatus, __u8 port);
void printSerdesStatus  (struct LINK_STATUS *linkStatus, __u8 port);
void printRmonCounters (TABLE_RMON * data, HISTOGRAM_MODE histogram, int portPtr, int number,int time);
void printRmonCountersDiff (TABLE_RMON * data,TABLE_RMON * data_old, HISTOGRAM_MODE histogram, int portPtr, int number,int time);
void printAllSerdesReg (__u8 port, SERDES_REGS *reg_table, int pos, int len);

#endif
