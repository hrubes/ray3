/*

    marvell-interface
    interface for connection management to switch Marvell
*/


#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/sockios.h>
#ifndef __GLIBC__
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#endif
#include <linux/types.h>

#include "marvell-interface.h"
#include "marvell-reg.h"
#include "marvell-lib-debug.h"
/*--------------------------------------------------------------------*/

struct mii_data {
	__u16	addr;
	__u16	reg;
	__u16	val_in;
	__u16	val_out;
};

struct ifreq ifr;

int verbose =-1;

__s8 allowedAddrAll[]={0,1,2,3,4,5,6,7,8,9,0xa,0x1b,0x1c,0x1f,-1};
#ifdef RR_PRODUCT_bk2
__s8 allowedAddrPhy[]={0,1,2,3,4,5,6,7,8,-1};
#else
__s8 allowedAddrPhy[]={1,2,3,4,5,6,7,8,-1};
#endif
__s8 allowedAddrPort[]={0,1,2,3,4,5,6,7,8,9,0xa,-1};
__s8 allowedAddrSerdes[]={0x9,0xa,-1};
__s8 allowedAddrGlobal[]={0x1b,0x1c,-1};
__s8 allowedAddrTacm[]={0x1f,-1};

__s8 allowedPage[]={0,1,2,3,4,5,6,7,-1};
#ifdef RR_PRODUCT_bk2
__s8 allowedPageP0[]={0,1,2,3,4,5,6,7,9,12,14,17,18,-1};
#endif

__u16 allowedSGMII[] = {0x2000,0x2001,0x2002,0x2003,0x2004,0x2005,0x2006,0x2007,0x2008,0x200f,0xa000,0xa001,0xa002,0xa003,0xf066,0xf076,0x0};
__u16 allowedSerdes10GbaseR[] = {0x1000,0x1001,0x1002,0x1003,0x1004,0x1005,0x1006,0x1008,0x100e,0x100f,0x1020,0x1021,
				 0x1022,0x1023,0x1024,0x1025,0x1026,0x1027,0x1028,0x1029,0x102a,0x102b,0x9000,0x9001,0x9002,0x0};
__u16 allowedSerdes10GbaseX[] = {0x1000,0x1001,0x1002,0x1003,0x1004,0x1005,0x1006,0x1008,0x100e,0x100f,0x1018,0x1019,
				 0x9000,0x9001,0x9002,0x9003,0x9004,0x9006,0x9010,0x9011,0x9012,0x9013,0x9014,0x9015,0x9016,0x0};
__u16 allowedSerdeCommon[]= {0xf00a,0xf00b,0xf00c,0xf010,0xf011,0xf012,0xf013,0xf016,0xf017,0xf018,0xf01b,0xf01c,0xf01d,0xf01e,
			     0xf01f,0xf020,0xf021,0xf022,0xf023,0xf024,0xf025,0xf026,0xf027,0xf028,0xf029,0xf030,0xf031,0xf032,
			     0xf033,0xf034,0xf035,0xf036,0xf037,0xf038,0xf039,0xf066, 0xf076,0x0};
__u16 allowedSerdesAll[]= {0x1000,0x1001,0x1002,0x1003,0x1004,0x1005,0x1006,0x1008,0x100e,0x100f,0x1018,0x1919,0x1020,0x1021,
			   0x1022,0x1023,0x1024,0x1025,0x1026,0x1027,0x1028,0x1029,0x102a,0x102b,
			   0x2000,0x2001,0x2002,0x2003,0x2004,0x2005,0x2006,0x2007,0x2008,0x200f,
			   0x9000,0x9001,0x9002,0x9003,0x9004,0x9006,0x9010,0x9011,0x9012,0x9013,0x9014,0x9015,0x9016,
			   0xa000,0xa001,0xa002,0xa003,
			   0xf00a,0xf00b,0xf00c,0xf010,0xf011,0xf012,0xf013,0xf016,0xf017,0xf018,0xf01b,0xf01c,0xf01d,0xf01e,
			   0xf01f,0xf020,0xf021,0xf022,0xf023,0xf024,0xf025,0xf026,0xf027,0xf028,0xf029,0xf030,0xf031,0xf032,
			   0xf033,0xf034,0xf035,0xf036,0xf037,0xf038,0xf039,0xf066,0xf076,0x0};
			

/*--------------------------------------------------------------------*/
static int mdioRead(int fd, __u8 addr, __u8 reg, __u16* value);
static int mdioWrite(int fd, __u8 addr, __u8 reg, __u16 value);
static int smiBusy (int fd , int pause);
static int smiRead (int fd, __u8 addr, __u8 reg, __u16*  value);
static int smiWrite (int fd, __u8 addr, __u8 reg, __u16 value);
static int smiClause45Read (int fd, __u8 port, __u8 dev, __u16 reg, __u16*  value);
static int smiClause45ReadBlock (int fd, __u8 port, __u8 dev, __u16 reg, __u16*  value, __u16 len);
static int smiClause45Write (int fd, __u8 port, __u8 dev, __u16 reg, __u16 value);
/*--------------------------------------------------------------------*/


// zakladni funkce
//otestuje platnost fd
static int checkFd (int fd)
{
	if (fd < 0 ) {
		fprintf (stderr, "Device does not exist\n");
		return -ENODEV;
	}
	return 0;
}

//otestuje platnost adresy
int checkAddr (__u8 addr, __s8 *defTab)
{
	int i;

	DEBUG ("Check addr:0x%02x\n",addr);
	for (i=0; defTab[i]!=-1; i++) {
		if (defTab[i] == addr)
			return addr;
	}
	fprintf (stderr, "Invalid address number\n");
	return -EINVAL;
}

//otestuje platnost stranky
#ifdef RR_PRODUCT_bk2

int checkPage (__u8 addr, __u8 page)
{
	int i;
	
	DEBUG ("Check page:0x%02x\n",page);

	if (addr == 0 ) {
		for (i=0; allowedPageP0[i]!=-1; i++) {
			if (allowedPageP0[i] == page)
				return page;
		}
	}else {
		for (i=0; allowedPage[i]!=-1; i++) {
			if (allowedPage[i] == page)
				return page;
		}
	}
	fprintf (stderr, "Invalid page number\n");
	return -EINVAL;
}

#else

int checkPage (__u8 page)

{
	int i;
	
	DEBUG ("Check page:0x%02x\n",page);
	for (i=0; allowedPage[i]!=-1; i++) {
		if (allowedPage[i] == page)
			return page;
	}
	fprintf (stderr, "Invalid page number\n");
	return -EINVAL;
}
#endif

int checkReg  (__u8 reg)
{
	DEBUG ("Check reg:0x%02x\n",reg);
	if (reg > 0x1f) {
		fprintf (stderr, "Invalid registr number\n");
		return -EINVAL;
	}
	return reg;
}

//otestuje platnost registru serdesu
int checkRegSerdes (__u16 reg, __u16 *defReg)
{
	int i;
	
	DEBUG ("Check reg:0x%02x\n",reg);
	for (i=0; defReg[i]!=0x0; i++) {
		if (defReg[i] == reg)
			return reg;
	}
//	fprintf (stderr, "Invalid SERDES reg number\n");
	return -EINVAL;
}
// cteni registru primo
static int mdioRead(int fd, __u8 addr, __u8 reg, __u16 *value)
{
	int ret;
	
	struct mii_data *mii = (struct mii_data *)&ifr.ifr_data;
	mii->addr = (__u16)addr;
	mii->reg = (__u16)reg;
	if ((ret=ioctl(fd, SIOCGMIIREG, &ifr)) < 0) {
		fprintf(stderr, "SIOCGMIIREG on %s failed: %s\n", ifr.ifr_name,strerror(errno));
		return ret;
	}
	*value=mii->val_out;
	DEBUG_ALL ("MDIO read addr:0x%02x reg:0x%02x data:0x%04x\n",addr,reg,*value);
	return 0;
}

// zapis registru primo
static int mdioWrite(int fd, __u8 addr, __u8 reg, __u16 value)
{
	int ret;
	DEBUG_ALL ("MDIO write addr:0x%02x reg:0x%02x data:0x%04x\n",addr,reg,value);
	struct mii_data *mii = (struct mii_data *)&ifr.ifr_data;
	mii->addr = (__u16)addr;
	mii->reg = (__u16)reg;
	mii->val_in = value;
	if ((ret=ioctl(fd, SIOCSMIIREG, &ifr)) < 0) {
		fprintf(stderr, "SIOCSMIIREG on %s failed: %s\n", ifr.ifr_name, strerror(errno));
		return ret;
	}
	return 0;
}

//cekani na uvolneni SMI
static int smiBusy (int fd , int pause)
{
	__u16 status; 
	int timeout;
	int ret;
	
	for (timeout =0;;timeout ++) {
		if (timeout > pause) {
			fprintf(stderr, "SMI timeout expired\n");
			return -1;
		}
		if ((ret=mdioRead(fd, GLOBAL2, 0x18,&status)) < 0 )
			return ret;
		if (!( status & 0x8000))
			break;
		usleep (100);
	}
	return 0;
}


//cteni registru pres SMI
static int smiRead (int fd, __u8 addr, __u8 reg, __u16*  value)
{
	int ret;
	__u16 st=0x9800;
#ifdef  RR_PRODUCT_bk2
	if (addr == 0)
		st=0xb800;
#endif
	if ((ret=smiBusy (fd , 10000)) < 0)
		return ret;
	if ((ret=mdioWrite(fd, GLOBAL2, 0x18,  st | (addr&0x1f)<<5 | (reg&0x1f)))  < 0)
		return ret;
	if ((ret=smiBusy (fd , 10000)) < 0)
		return ret;
	return mdioRead(fd, GLOBAL2, 0x19, value);
	
}

//zapis registru pres SMI
static int smiWrite (int fd, __u8 addr, __u8 reg, __u16 value)
{
	int ret;
	__u16 st=0x9400;
#ifdef  RR_PRODUCT_bk2
	if (addr == 0)
		st=0xb400;
#endif
	if ((ret=smiBusy (fd , 10000)) < 0)
		return ret;
	if ((ret=mdioWrite(fd, GLOBAL2, 0x19, value))  < 0)
		return ret;
	if ((ret=mdioWrite(fd, GLOBAL2, 0x18,  st | (addr&0x1f)<<5 | (reg&0x1f)))  < 0)
		return ret;
	if ((ret=smiBusy (fd , 10000)) < 0)
		return ret;
	return 0;
	
}

//zakladni funkce  pro cteni registru
int regRead (int fd, __u8 addr, __u8 reg, __u16* value)
{
	DEBUG_ALL ("Register read from: addr:0x%02x reg:0x%02x\n",addr,reg);
	if (checkFd(fd) < 0)
		return -ENODEV;
	if ((checkAddr(addr,allowedAddrAll) < 0) || (checkReg(reg) < 0))
		return -EINVAL;
	return mdioRead(fd, addr, reg, value);

}

//zakladni funkce  pro zapis do registru
int regWrite (int fd, __u8 addr, __u8 reg, __u16 value)
{
	DEBUG_ALL ("Register write to: addr:0x%02x reg:0x%02x value:0x%04x\n",addr,reg,value);
	if (checkFd(fd) < 0)
		return -ENODEV;
	if ((checkAddr(addr, allowedAddrAll) < 0) || (checkReg(reg) < 0))
		return -EINVAL;
	return mdioWrite(fd, addr, reg, value);
}

//zakladni funkce  pro zapis do registru s maskou
int regWriteMask (int fd, __u8 addr, __u8 reg, __u16 value, __u16 mask)
{
	__u16	data;
	int 	ret;
	DEBUG_ALL ("Register write to: addr:0x%02x reg:0x%02x value:0x%04x mask:0x%04x\n",addr,reg,value,mask);
	if (checkFd(fd) < 0)
		return -ENODEV;
	if ((checkAddr(addr,allowedAddrAll) < 0) || (checkReg(reg) < 0))
		return -EINVAL;
	if ((ret=mdioRead (fd, addr, reg, &data)) < 0)
		return ret;
	data &=~mask;
	data |=value&mask;
	return mdioWrite(fd, addr, reg, data);
}


// precte na dane adrese numfield  registru od registru reg (uklada do pole na adrese field)
int readAllReg (int fd,__u8 addr, __u8 reg ,__u16* field, unsigned int numfield)
{
	unsigned int i;
	int ret;
	__u16* ptr=field;
	
	DEBUG_ALL ("Read addr:0x%02x from reg:0x%02x (number:%d)\n",addr,reg,numfield);
	if (checkFd(fd) < 0)
		return -ENODEV;
	if ((checkAddr(addr, allowedAddrAll) < 0) || (checkReg(reg) < 0))
		return -EINVAL;
	for (i=0;i<numfield;i++,ptr++) {
		if ((ret =regRead(fd, addr, reg+i ,ptr)) < 0)
			return ret;
	}
	return 0;
}

// nastavi danou stranku pro PHY
int setPhyPage (int fd,__u8 addr , __u8 page)
{
	int ret;
	DEBUG_ALL ("Set page: addr:0x%02x page:0x%02x\n",addr,page);
	if (checkFd(fd) < 0)
		return -ENODEV;
#ifdef RR_PRODUCT_bk2
	if ( (checkAddr(addr,allowedAddrPhy) < 0) || (checkPage(addr, page) < 0) )
#else
	if ( (checkAddr(addr,allowedAddrPhy) < 0) || (checkPage(page) < 0) )
#endif
		return -EINVAL;
	if ((ret=regPhyWrite(fd,addr, 22, page))<0) {
		regPhyWrite(fd,addr, 22, 0 );
		return ret;
	}
	return 0;
}

//zakladni funkce  pro cteni registru PHY
int regPhyRead (int fd, __u8 addr, __u8 reg, __u16* value)
{
	DEBUG_ALL ("PHY: Register read from: addr:0x%02x reg:0x%02x\n",addr,reg);
	if (checkFd(fd) < 0)
		return -ENODEV;
	if ((checkAddr(addr,allowedAddrPhy) < 0) || (checkReg(reg) < 0))
		return -EINVAL;
	return smiRead ( fd, addr, reg, value);
}

//zakladni funkce  pro zapis do registru PHY
int regPhyWrite (int fd, __u8 addr, __u8 reg, __u16 value)
{
	DEBUG_ALL ("PHY: Register write to: addr:0x%02x reg:0x%02x value:0x%04x\n",addr,reg,value);
	if (checkFd(fd) < 0)
		return -ENODEV;
	if ((checkAddr(addr, allowedAddrPhy) < 0) || (checkReg(reg) < 0))
		return -EINVAL;
	return smiWrite (fd, addr, reg, value);
}

//zakladni funkce  pro zapis do registru PHY s maskou
int regPhyWriteMask (int fd, __u8 addr, __u8 reg, __u16 value, __u16 mask)
{
	__u16	data;
	int 	ret;
	DEBUG_ALL ("PHY: Register write to: addr:0x%02x reg:0x%02x value:0x%04x mask:0x%04x\n",addr,reg,value,mask);
	if (checkFd(fd) < 0)
		return -ENODEV;
	if ((checkAddr(addr,allowedAddrPhy) < 0) || (checkReg(reg) < 0))
		return -EINVAL;
	if ((ret=smiRead (fd, addr, reg, &data)) < 0)
		return ret;
	data &=~mask;
	data |=value&mask;
	return smiWrite (fd, addr, reg, data);
}


// precte na dane adrese numfield  PHY registru od registru reg (uklada do pole na adrese field)
int readPhyAllReg (int fd,__u8 addr, __u8 reg ,__u16* field, unsigned int numfield)
{
	unsigned int i;
	int ret;
	__u16* ptr=field;
	
	DEBUG_ALL ("PHY: Read addr:0x%02x from reg:0x%02x (number:%d)\n",addr,reg,numfield);
	if (checkFd(fd) < 0)
		return -ENODEV;
	if ((checkAddr(addr, allowedAddrPhy) < 0) || (checkReg(reg) < 0))
		return -EINVAL;
	for (i=0;i<numfield;i++,ptr++) {
		if ((ret =regPhyRead(fd, addr, reg+i ,ptr)) < 0)
			return ret;
	}
	return 0;
}


//cteni libovolneho registru na dane strance
int regPhyPageRead (int fd, __u8 addr, __u8 reg, __u8 page ,__u16* value)
{
	int ret;
	if ((ret=setPhyPage (fd,addr, page ))<0)
		return ret;
	ret=regPhyRead (fd, addr, reg, value);
	regPhyWrite(fd,addr, 22, 0 );
	return ret;
}

//zapis do libovolneho registru na dane strance
int regPhyPageWrite (int fd, __u8 addr, __u8 reg, __u8 page ,__u16 value)
{
	int ret;
	if ((ret=setPhyPage (fd,addr, page ))<0)
		return ret;
	ret=regPhyWrite (fd, addr, reg, value);
	regPhyWrite(fd,addr, 22, 0 );
	return ret;
}

//zapis do libovolneho registru na dane strance dle masky
int regPhyPageWriteMask (int fd, __u8 addr, __u8 reg, __u8 page ,__u16 value, __u16 mask)
{
	int ret;
	
	if ((ret=setPhyPage (fd,addr, page ))<0)
		return ret;
	ret=regPhyWriteMask (fd, addr, reg, value,mask);
	regPhyWrite(fd,addr, 22, 0 );
	return ret;
}


//********************************************************************************
//cteni registru pres SMI Clause45
static int smiClause45Read (int fd, __u8 port, __u8 dev, __u16 reg, __u16*  value)
{
	int ret;
	
	if ((ret=smiBusy (fd , 10000)) < 0)
		return ret;
	if ((ret=mdioWrite(fd, GLOBAL2, 0x19,  reg))  < 0)
		return ret;
	if ((ret=mdioWrite(fd, GLOBAL2, 0x18,  0x8000 | (port&0x1f)<<5 | (dev&0x1f)))  < 0)
		return ret;
	if ((ret=smiBusy (fd , 10000)) < 0)
		return ret;
	if ((ret=mdioWrite(fd, GLOBAL2, 0x18,  0x8c00 | (port&0x1f)<<5 | (dev&0x1f)))  < 0)
		return ret;
	if ((ret=smiBusy (fd , 10000)) < 0)
		return ret;
	return mdioRead(fd, GLOBAL2, 0x19, value);
	
}

//cteni bloku registru pres SMI Clause45
static int smiClause45ReadBlock (int fd, __u8 port, __u8 dev, __u16 reg, __u16*  value, __u16 len)
{
	int ret;
	__u16 i;
	
	if ((ret=smiBusy (fd , 10000)) < 0)
		return ret;
	if ((ret=mdioWrite(fd, GLOBAL2, 0x19,  reg))  < 0)
		return ret;
	if ((ret=mdioWrite(fd, GLOBAL2, 0x18,  0x8000 | (port&0x1f)<<5 | (dev&0x1f)))  < 0)
		return ret;
	if ((ret=smiBusy (fd , 10000)) < 0)
		return ret;
	if ((ret=mdioWrite(fd, GLOBAL2, 0x18,  0x8800 | (port&0x1f)<<5 | (dev&0x1f)))  < 0)
		return ret;
	for (i=0; i< len; i++, value++) {
		if ((ret=smiBusy (fd , 10000)) < 0)
			return ret;
		if ((ret=mdioRead(fd, GLOBAL2, 0x19, value)) < 0)
			return ret;
	}
	return 0;
	
}

//zapis registru pres SMI v Clause45
static int smiClause45Write (int fd, __u8 port, __u8 dev, __u16 reg, __u16 value)
{
	int ret;
	
	if ((ret=smiBusy (fd , 10000)) < 0)
		return ret;
	if ((ret=mdioWrite(fd, GLOBAL2, 0x19,  reg))  < 0)
		return ret;
	if ((ret=mdioWrite(fd, GLOBAL2, 0x18,  0x8000 | (port&0x1f)<<5 | (dev&0x1f)))  < 0)
		return ret;
	if ((ret=smiBusy (fd , 10000)) < 0)
		return ret;
	if ((ret=mdioWrite(fd, GLOBAL2, 0x19, value))  < 0)
		return ret;
	if ((ret=mdioWrite(fd, GLOBAL2, 0x18,  0x8400| (port&0x1f)<<5 | (dev&0x1f)))  < 0)
		return ret;
	if ((ret=smiBusy (fd , 10000)) < 0)
		return ret;
	return 0;
	
}

//zakladni funkce  pro zapis do registru SERDESu
int serdesRegWrite (int fd, __u8 port, __u16 reg, __u16 value)
{
	DEBUG_ALL ("Register write to: port:0x%02x reg:0x%02x value:0x%04x\n",port,reg,value);
	if (checkFd(fd) < 0)
		return -ENODEV;
	if ((checkAddr(port,allowedAddrSerdes) < 0) || (checkRegSerdes(reg,allowedSerdesAll) < 0))
		return -EINVAL;
	return smiClause45Write(fd, port, SERDES_DEV, reg, value);
}

//zakladni funkce  pro zapis do registru SERDESus maskou
int serdesRegWriteMask (int fd, __u8 port, __u16 reg, __u16 value, __u16 mask)
{
	__u16	data;
	int 	ret;
	DEBUG_ALL ("Register write to: port:0x%02x reg:0x%02x value:0x%04x mask:0x%04x\n",port,reg,value,mask);
	if (checkFd(fd) < 0)
		return -ENODEV;
	if ((checkAddr(port,allowedAddrSerdes) < 0) || (checkRegSerdes(reg,allowedSerdesAll) < 0))
		return -EINVAL;
	if ((ret=smiClause45Read (fd, port,SERDES_DEV, reg, &data)) < 0)
		return ret;
	data &=~mask;
	data |=value&mask;
	return smiClause45Write(fd, port, SERDES_DEV,reg, data);
}

//zakladni funkce  pro cteni registru SERDESUu
int serdesRegRead (int fd, __u8 port, __u16 reg, __u16* value)
{
	DEBUG_ALL ("Register read from: port:0x%02x reg:0x%02x\n",port,reg);
	if (checkFd(fd) < 0)
		return -ENODEV;
	if ((checkAddr(port,allowedAddrSerdes) < 0) || (checkRegSerdes(reg,allowedSerdesAll) < 0))
		return -EINVAL;
	return smiClause45Read(fd, port, SERDES_DEV ,reg, value);

}

// precte na dane adrese numfield  registru od registru reg (uklada do pole na adrese field)
int serdesReadAllReg (int fd,__u8 port, __u16 reg ,__u16* field, unsigned int numfield)
{
	
	
	DEBUG_ALL ("Read port:0x%02x from reg:0x%02x (number:%d)\n",port,reg,numfield);
	if (checkFd(fd) < 0)
		return -ENODEV;
	if ((checkAddr(port, allowedAddrSerdes) < 0) || (checkRegSerdes(reg,allowedSerdesAll) < 0) || (numfield > 32))
		return -EINVAL;
	return smiClause45ReadBlock (fd, port, SERDES_DEV, reg, field, numfield);
}

