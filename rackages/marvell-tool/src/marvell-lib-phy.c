/*

    marvell-phy: status phy Marvell 88E6390 (X) switch
    
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/sockios.h>
#ifndef __GLIBC__
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#endif
#include <linux/types.h>

#include "marvell-interface.h"
#include "marvell-lib-def.h"
#include "marvell-reg.h"
#include "marvell-lib-debug.h"
#include "marvell-lib.h"
// PHY

extern int verbose;
static int phyPowerDownCopper (int fd, __u8 port, ENABLE enable);
static int phyResetCopper (int fd, __u8 addr , __u16 value );
static int phyRestartAutoNegCopper (int fd, __u8 port);
static int phyAutoNegEnableCopper (int fd, __u8 port, ENABLE enable);
static int phySetSpeedCopper (int fd , __u8 port ,SPEED speed);
static int phySetDuplexModeCopper (int fd, __u8 port, DUPLEX dMode);
static int phySetAutoNegoCopper (int fd, __u8 port, AUTO_MODE mode);
static int phySetPauseCopper(int fd, __u8 port, PAUSE_MODE mode);
static int phyGetLinkStatusCopper (int fd, __u8 port, struct LINK_STATUS *linkStatus);


/*
* provede reset PHY a nastavi pozadovanou rychlost, duplex .... nastavovani v registru control (PHY reg 0x0)
* 
*
* */
int mvPhyReset (int fd, __u8 port, __u16 value)
{
	int 	portType;
	DEBUG ("PHY%d Reset.\n",port);
	
	
	if ((portType=mvCheckTypePort ( fd, port)) < 0)
		return portType;
	if  (portType == PORT_PHY) 
		return phyResetCopper (fd, port,value);
	return 0;
}
//provede reset PHY a nastavi pozadovanou rychlost, duplex a loopback
static int phyResetCopper (int fd, __u8 addr , __u16 value )
{
	__u16 	regv;
	int 	ret;
	int 	retryCount;
	int    	pd = 0;
	DEBUG ("Reset PHY addr:0x%02x,\n",addr);
	if((ret=regPhyRead(fd,addr,PHY_CONTROL_REG,&regv)) < 0)
		return ret;
	if (regv & PHY_CONTROL_POWER)
		pd = 1;
	if (value != 0xFF)
		regv= value;

	/* Set the desired bits to 0. */
	if (pd)
		regv|= PHY_CONTROL_POWER;
	else
		regv |= PHY_CONTROL_RESET;

	if((ret=regPhyWrite(fd,addr,PHY_CONTROL_REG,regv)) < 0)
		return ret;
     
	if (pd)
		return 0;
	for (retryCount = 0x1000; retryCount > 0; retryCount--) {
		if((ret=regPhyRead(fd,addr,PHY_CONTROL_REG,&regv)) < 0)
			return ret;
		if ((regv & PHY_CONTROL_RESET) == 0)
			break;
	}
	if (retryCount == 0) {
		fprintf(stderr,"Reset bit is not cleared\n");
		return -ENODEV;
	}
	return 0;
}



/****************************************************************************
* Enable/disable (power down) on specific port.
* 
* ENABED = power down
* */
int mvPhyPowerDown (int fd, __u8 port, ENABLE enable)
{
	int 	portType;
	
	DEBUG ("PHY%d set PowerDown.\n",port);
	
	
	if ((portType=mvCheckTypePort ( fd, port)) < 0)
		return portType;
	if  (portType == PORT_PHY) 
		return phyPowerDownCopper (fd, port,enable);
	return 0;
}

/****************************************************************************
* Enable/disable (power down) on specific port.
* 
* ENABED = power down
* */
static int phyPowerDownCopper (int fd, __u8 port, ENABLE enable)
{
	int 	ret;

	DEBUG ("PHY%d set copper PowerDown.\n",port);

	if (enable) {
		if((ret=regPhyWriteMask (fd,port,PHY_SPEC_CONTROL_REG,PHY_SPEC_CONTROL_POWER_DOWN,PHY_SPEC_CONTROL_POWER_DOWN)) < 0)
			return ret;
		return regPhyWriteMask (fd,port,PHY_CONTROL_REG,PHY_CONTROL_POWER,PHY_CONTROL_POWER);
	} else {
		if((ret=regPhyWriteMask (fd,port,PHY_SPEC_CONTROL_REG,0,PHY_SPEC_CONTROL_POWER_DOWN)) < 0)
			return ret;
		return regPhyWriteMask (fd,port,PHY_CONTROL_REG,0,PHY_CONTROL_POWER);
	}
}


/****************************************************************************
*        Enable and  Restart AutoNegotiation.
*	 
*	
*/
int mvPhyRestartAutoNeg (int fd, __u8 port) 
{     
	int 	portType;
	
	DEBUG ("PHY%d restart AutoNegotiation.\n",port);
	
	if ((portType=mvCheckTypePort ( fd, port)) < 0)
		return portType;
	if  (portType == PORT_PHY) 
		return  phyRestartAutoNegCopper (fd, port) ;
	return 0;
}


static int phyRestartAutoNegCopper (int fd, __u8 port) 
{

	DEBUG ("PHY%d restart AutoNegotiation Coper.\n",port);
	return regPhyWriteMask(fd,port,PHY_CONTROL_REG,PHY_CONTROL_RESTART_AUTONEGO | PHY_CONTROL_AUTONEGO,PHY_CONTROL_RESTART_AUTONEGO | PHY_CONTROL_AUTONEGO);
}


/****************************************************************************
*         Enable/disable an Auto-Negotiation.
*     
* 
*/

int mvPhyAutoNegEnable (int fd, __u8 port, ENABLE enable)
{
	int 	portType;
	
	DEBUG ("PHY%d set autonego enable/disable.\n",port);
	if ((portType=mvCheckTypePort ( fd, port)) < 0)
		return portType;
	if  (portType == PORT_PHY) 
		return  phyAutoNegEnableCopper (fd, port, enable) ;
	return 0;
}


static int phyAutoNegEnableCopper (int fd, __u8 port, ENABLE enable)
{

	DEBUG ("PHY%d set autonego enable/disable Cooper.\n",port);
	if(enable)
		return regPhyWriteMask(fd,port,PHY_CONTROL_REG,PHY_CONTROL_AUTONEGO, PHY_CONTROL_AUTONEGO);
	else
		return regPhyWriteMask(fd,port,PHY_CONTROL_REG,0, PHY_CONTROL_AUTONEGO);
}

/*
*   	Sets speed for a specific  port. 
* 
*/
int  mvSetPhySpeed (int fd , __u8 port ,SPEED speed)

{
	int 	portType;
	
	DEBUG ("PHY%d set speed.\n",port);

	if ((portType=mvCheckTypePort ( fd, port)) < 0)
		return portType;
	if  (portType == PORT_PHY) 
		return phySetSpeedCopper ( fd , port ,speed);
	return 0;
}


static int  phySetSpeedCopper (int fd , __u8 port ,SPEED speed)

{
	__u16	data;
	int	ret;

	DEBUG ("PHY%d set speed copper.\n",port);
	if ((ret=regPhyRead(fd,port,PHY_CONTROL_REG,&data)) < 0)
		return ret;

	switch(speed){
		case SPEED_10:
			data &=  ~(PHY_CONTROL_SPEED | PHY_CONTROL_SPEED_MSB );
			break; 
		case SPEED_100:
			data &=  ~PHY_CONTROL_SPEED_MSB;
			data |=  PHY_CONTROL_SPEED;
			break;
		case SPEED_1000:
			data &=  ~PHY_CONTROL_SPEED;
			data |=  PHY_CONTROL_SPEED_MSB;
			break;
		default:
			return -EINVAL;
	}
	return regPhyWrite(fd,port,PHY_CONTROL_REG,data);
}

/***************************************************************************
*       Sets duplex mode 
* 
*/
int mvSetPhyDuplexMode (int fd, __u8 port, DUPLEX dMode)
{
	
	int 	portType;
	
	DEBUG ("PHY%d set Duplex Mode.\n",port);

	if ((portType=mvCheckTypePort ( fd, port)) < 0)
		return portType;
	if  (portType == PORT_PHY) 
		return phySetDuplexModeCopper ( fd , port ,dMode);
	return 0;
}


static int phySetDuplexModeCopper (int fd, __u8 port, DUPLEX dMode)
{
	
	DEBUG ("PHY%d set Duplex Mode Copper.\n",port);
	if(dMode)
		return regPhyWriteMask (fd, port, PHY_CONTROL_REG, PHY_CONTROL_DUPLEX,  PHY_CONTROL_DUPLEX);
	else
		return regPhyWriteMask (fd, port, PHY_CONTROL_REG, 0,  PHY_CONTROL_DUPLEX);
}


/*******************************************************************************/
/*
 * This routine set Auto-Negotiation  speed 

*  This routine sets up the port with given Auto Mode.
*        Supported mode is as follows:
*        - Auto for both speed and duplex.
*        - Auto for speed only and Full duplex.
*        - Auto for speed only and Half duplex.
*        - Auto for duplex only and speed 1000Mbps.
*        - Auto for duplex only and speed 100Mbps.
*        - Auto for duplex only and speed 10Mbps.
*        - Speed 1000Mbps and Full duplex.
*        - Speed 1000Mbps and Half duplex.
*        - Speed 100Mbps and Full duplex.
*        - Speed 100Mbps and Half duplex.
*        - Speed 10Mbps and Full duplex.
*        - Speed 10Mbps and Half duplex.
*/
int mvSetPhyAutoNego(int fd, __u8 port, AUTO_MODE mode)
{
	int 	portType;
	
	DEBUG ("PHY%d set AutoNegotiation on port.\n",port);

	if ((portType=mvCheckTypePort ( fd, port)) < 0)
		return portType;
	if  (portType == PORT_PHY) 
		return phySetAutoNegoCopper(fd,port,mode);
	return 0;
}


static int phySetAutoNegoCopper(int fd, __u8 port, AUTO_MODE mode)
{
	__u16	data, data1;
	int ret;
	DEBUG ("Copper PHY%d set AutoMode.\n",port);
	if ((ret=regPhyRead(fd ,port,PHY_AUTONEGO_REG,&data)) < 0)
		return ret;
	data &= ~PHY_MODE_AUTO_AUTO;
	if (( ret=regPhyRead(fd,port,PHY_CONTROL_1000_REG,&data1)) < 0)
		return ret;
	data1 &= ~(PHY_CONTROL_1000_FD|PHY_CONTROL_1000_HD);
	switch(mode) {
		case SPEED_AUTO_DUPLEX_AUTO:
			data |= PHY_MODE_AUTO_AUTO;
		case SPEED_1000_DUPLEX_AUTO:
			data1 |= PHY_CONTROL_1000_FD|PHY_CONTROL_1000_HD;
			break;
		case SPEED_AUTO_DUPLEX_FULL:
			data  |= PHY_MODE_AUTO_FULL;
			data1 |= PHY_CONTROL_1000_FD;
			break;
		case SPEED_1000_DUPLEX_FULL:
			data1 |= PHY_CONTROL_1000_FD;
			break;
		case SPEED_1000_DUPLEX_HALF:
			data1 |= PHY_CONTROL_1000_HD;
			break;
		case SPEED_AUTO_DUPLEX_HALF:
			data  |= PHY_MODE_AUTO_HALF;
			data1 |= PHY_CONTROL_1000_HD;
			break;
		case SPEED_100_DUPLEX_AUTO:
			data |= PHY_MODE_100_AUTO;
			break;
		case SPEED_10_DUPLEX_AUTO:
			data |= PHY_MODE_10_AUTO;
			break;
		case SPEED_100_DUPLEX_FULL:
			data |= PHY_AUTONEGO_100_FULL;
			break;
		case SPEED_100_DUPLEX_HALF:
			data |= PHY_AUTONEGO_100_HALF;
			break;
		case SPEED_10_DUPLEX_FULL:
			data |= PHY_AUTONEGO_10_FULL;
			break;
		case SPEED_10_DUPLEX_HALF:
			data |= PHY_AUTONEGO_10_HALF;
			break;
		default:
			return -EINVAL;
	}
	/* Write to Phy AutoNegotiation Advertisement Register.  */
	if ((ret=regPhyWrite(fd,port,PHY_AUTONEGO_REG,data)) < 0)
		return ret;
	/* Write to Phy AutoNegotiation 1000B Advertisement Register.  */
	if ((ret=regPhyWrite(fd,port,PHY_CONTROL_1000_REG,data1)) < 0)
		return ret;
	return 0;
}

/*******************************************************************************/
/*
 * This routine set Auto-Negotiation pause 
 * 
*/
int mvSetPhyPause(int fd, __u8 port, PAUSE_MODE mode)
{
	int 	portType;
	
	DEBUG ("PHY%d set Pause on port.\n",port);

	if ((portType=mvCheckTypePort ( fd, port)) < 0)
		return portType;
	if  (portType == PORT_PHY) 
		return phySetPauseCopper(fd,port,mode);
	return 0;
}


static int phySetPauseCopper(int fd, __u8 port, PAUSE_MODE mode)
{
	DEBUG ("Copper PHY%d set Pause.\n",port);
	return regPhyWriteMask (fd,port,PHY_AUTONEGO_REG,mode << 10, PHY_AUTONEGO_ASYMMETRIC_PAUSE | PHY_AUTONEGO_PAUSE);
}


/********************************************************************************
*       This routine retrieves the Link status.
*
* */

int mvGetPhyLinkStatus (int fd, __u8 port, struct LINK_STATUS *linkStatus)
{
	int portType;
	
	DEBUG ("PHY%d set Pause on port.\n",port);
	memset (linkStatus,0,sizeof(linkStatus));
	if ((portType=mvCheckTypePort ( fd, port)) < 0)
		return portType;
	linkStatus->portType =  (TYPE_PORT) portType;
	if  (portType == PORT_PHY) 
		return phyGetLinkStatusCopper (fd,port,linkStatus);
	return 0;
}

static int phyGetLinkStatusCopper (int fd, __u8 port, struct LINK_STATUS *linkStatus)
{

	__u16	data;
	__u16	data1;
	int 	ret;
	
	DEBUG ("PHY%d get  Link Status Copper.\n",port);
	if ((ret=regPhyRead (fd,port,PHY_SPEC_STATUS_REG,&data)) < 0)
		return ret;
	if ((ret=regPhyRead (fd,port,PHY_CONTROL_REG,&data1)) < 0)
		return ret;
	if (data & PHY_SPEC_STATUS_LINK)
		linkStatus->link=UP;
	if (data & PHY_SPEC_STATUS_RESOLVED) {
		linkStatus->resolvedSpeedDuplex=ENABLED;
		if (data & PHY_SPEC_STATUS_TRANSMIT_PAUSE)
			linkStatus->transmitPause=ENABLED;
		if (data & PHY_SPEC_STATUS_RECEIVE_PAUSE)
			linkStatus->receivePause=ENABLED;
		if (data & PHY_SPEC_STATUS_DUPLEX)
			linkStatus->duplex=DUPLEX_FULL;
		if (data & PHY_SPEC_STATUS_MDI_CROSSOVER)
			linkStatus->mdi=MDIX;
		else
			linkStatus->mdi=MDI;
		linkStatus->speed=(SPEED)((data & PHY_SPEC_STATUS_SPEED_MASK) >>14);
	}
	if (data1 & PHY_CONTROL_AUTONEGO)
		linkStatus->autonego=ENABLED;
	if ((ret=regPhyRead (fd,port,PHY_AUTONEGO_REG,&data)) < 0)
		return ret;
	if ((ret=regPhyRead (fd,port,PHY_CONTROL_1000_REG,&data1)) < 0)
		return ret;
	linkStatus->autonegoAd=((data&0x0fe0) >> 5) | (data1 & 0x1f00);
	if ((ret=regPhyRead (fd,port,PHY_LP_REG,&data)) < 0)
		return ret;
	if ((ret=regPhyRead (fd,port,PHY_LP_1000_REG,&data1)) < 0)
		return ret;
	linkStatus->linkPartner=((data&0x0fe0) >> 5 ) | (data1 & 0xfc00);
	return 0;
}

/********************************************************************************
*       
*
* */
int mvGetPhyInError (int fd, __u8 port, __u16* errors)
{
	
	int 	ret;
	
	DEBUG ("PHY%d get recive error conter.\n",port);
	if ((ret=regPhyRead (fd,port,PHY_ERROR_COUNTER_REG,errors)) < 0)
		return ret;
	return 0;
}

