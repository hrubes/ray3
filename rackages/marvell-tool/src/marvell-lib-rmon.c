#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/sockios.h>
#ifndef __GLIBC__
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#endif
#include <linux/types.h>

#include "marvell-interface.h"
#include "marvell-lib-def.h"
#include "marvell-reg.h"
#include "marvell-lib-debug.h"

extern int verbose;

static int setRmonReg ( int fd, __u16 stats)
{
	__u16 regv, hdata ;
	int timeout,ret;
	
	if ((ret=regRead(fd,GLOBAL1, 0x1d ,&hdata)) < 0 )
		return ret;
	hdata&= 0x0c00;
	for (timeout=0;timeout < 100000;timeout++) {
		if ((ret=regRead(fd,GLOBAL1, 0x1d,&regv)) < 0 )
				return ret;
		if (! (regv & 0x8000) ) {
			if ((ret=regWrite(fd, GLOBAL1, 0x1d,stats | hdata )) < 0 )
				return ret;
			for (timeout=0;timeout < 10000;timeout++) {
				if ((ret=regRead(fd,GLOBAL1, 0x1d ,&regv)) < 0 )
					return ret;
				if (! (regv & 0x8000) ) 
					return 0;
				usleep(100);
			}
			return -EIO;
		}
		usleep(100);
	}
	return -EIO;
}

/*--------------------------------------------------------------------*/
int mvFlushRmonAllPorts (int fd)
{
	DEBUG ("Flush rmon counters for all ports .\n");
	return setRmonReg (fd, 0x9000);
}
/*--------------------------------------------------------------------*/
int mvFlushRmonPort (int fd, __u8 port)
{
	DEBUG ("Flush rmon counters for port %d .\n",port);
	return setRmonReg (fd, 0x9000 |((port +1) <<5));
}
/*--------------------------------------------------------------------*/
int mvReadRmon (int fd, __u8 port, void* data,  HISTOGRAM_MODE histogramMode)
{
	
	__u16 regv, regv1;
	int i;
	int ret;
	__u16 reg;
	__u32* rmonData=(__u32*) data;
	
	
	DEBUG ("Read rmon counters for port %d .\n",port);
	if (( port < 0) || (port > PORT_NUM))
		return -EINVAL;
	if ( ! histogramMode) {
		if ((ret=regRead(fd,GLOBAL1, 0x1d ,&reg)) < 0 )
			return ret;
		if ((ret=setRmonReg(fd,0xd000|((port+1) <<5) | reg & 0x0c00)) < 0 )
			return ret;
	} else {
		if ((ret=setRmonReg(fd,0xd000|((port+1) <<5) | (histogramMode <<10))) < 0 )
			return ret;
	}
	for (i=0; i < 0x20; i++) {
		if ((ret=setRmonReg(fd, 0xC000| i | ((port +1) <<5))) < 0)
			return ret;
		if ((ret=regRead(fd,GLOBAL1, 0x1e, &regv)) < 0 )
			return ret;
		if ((ret=regRead(fd,GLOBAL1, 0x1f , &regv1)) < 0 )
			return ret;
		*rmonData=(((__u32)regv& 0xffff) << 16) | ( regv1 & 0xffff);
		rmonData++;
	}
	return 0;
}

/*--------------------------------------------------------------------*/

