/*

    marvell-init: init for Marvell 88E6390 (X) switch
*/



#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <time.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/sockios.h>
#ifndef __GLIBC__
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#endif
#include <linux/types.h>


#include "marvell-lib.h"
#include "marvell-lib-def.h"
#include "marvell-reg.h"

//cinnost
typedef enum
{
	PARAM_NONE=0,
	PARAM_STATUS,
	PARAM_SET,
} PARAM;
	
const char * ETHERNET={"eth1"};

extern int verbose;

PARAM mode;

/*--------------------------------------------------------------------*/

struct option longopts[] = {
 /* { name  has_arg  *flag  val } */
    {"verbose",		1, NULL, 'v'},
    {"help",        0, NULL, 'h'},
	{"status", 		0, NULL, 's'},
	{"refclock",	1, NULL, 'f'},
	{"refclock",	1, NULL, 'r'},
    {"port",        1, NULL, 'p'},
    { 0, 0, NULL, 0 }
};


void portStatusClk (__u16 value, char* txt)
{
	switch (value & 0x0480) {
		case 0:
			printf ("none");
			break;
		case 0x400 | 0X480:
			printf ("slave");
			break;
		case 0x80:
			printf ("master");
			break;
	}
	printf ("\n	Reference Clock: "); 
	if (value & 0x80)
		printf ("SE_SCLK");
	else
		printf ("XTAL");
	printf ("\n	Recovered Rx Clock: "); 
	if (value & 0x400)
		printf ("USE %s",txt);
	else
		printf ("NO");
}

/*--------------------------------------------------------------------*/
const char *usage =
"usage: marvell-synce  -v -h -t \n"
"	-v  --verbose					verbose \n"
"		param:	0 | 1 |2\n"
"	-h  --help					help\n"
"	-s  --status					get status syncE\n"
"	-f  --refclock					set Reference Clock [must be set param -p (1-8)]\n"
"		param:	SE_SCLK | XTAL\n"
"	-r  --recclock					set Recovered Clock [must be set param -p (0-10)]\n"
"		param:	PRI | SEC | NO\n"
"	-p  --port					port\n"
"		param: 0 .. 10\n"
"\n";


int main(int argc, char **argv)
{
	int fd;
	int c;
	int i;
	int port=-1;
	char* txt;
	SYNCE_STATUS  status;
	REC_CLOCK	recClock=REC_CLOCK_NONE;
	REFERENCE_CLOCK refClock=REFERENCE_CLOCK_NONE;
	
    
	while ((c = getopt_long(argc, argv, "shv:r:f:p:", longopts, 0)) != EOF) {
		switch (c) {
			case 'v': 
				verbose=atoi(optarg);
				if ((verbose <0) || (verbose > 2)) {
					printf ("%s",usage);
					exit (-1);
				}
				break;	
			case 'h': 
				printf ("%s",usage);
				return 0;
				break;
			case 's': 
                mode = PARAM_STATUS;
				break;
			case 'f':
                mode = PARAM_SET;
				if (strcmp(optarg,"SE_SCLK") == 0)
					refClock=REFERENCE_CLOCK_SE_SCLK;
				if (strcmp(optarg,"XTAL") == 0)
					refClock=REFERENCE_CLOCK_XTAL;
				break;
			case 'r':
                mode = PARAM_SET;
				if (strcmp(optarg,"NO") == 0)
					recClock=REC_CLOCK_NO;
				if (strcmp(optarg,"PRI") == 0)
					recClock=REC_CLOCK_PRI;
				if (strcmp(optarg,"SEC") == 0)
					recClock=REC_CLOCK_SEC;
				break;
            case 'p':
                port=atoi(optarg);
				if ((port < 0 ) || (port >10)) {
					fprintf (stderr,"Invalid port number, (valid 0 .. 10)\n");
					printf ("%s",usage);
					exit (-1);
				}
				break;
		}
	}
	
	if (mode == PARAM_NONE) {
            printf ("%s",usage);
            return -EINVAL;
	}
	if ((fd = mvOpen(ETHERNET)) < 0) {
		exit(-1);
	}
    if (mode == PARAM_STATUS) {
        if (getSynceStatus (fd, &status) < 0) {
            printf ("Error\n");
        } else {
			txt=(char*)"";
			printf ("Recovered Clock Select:\n");
			printf ("   PriRecClkSel:  Port%d\n", status.priRecClk );
            printf ("   SecRecClkSel:  Port%d\n",status.secRecClk);
			printf ("Port SyncE status:\n");
			for (i=1; i< 9; i++) {
				printf ("   Port[%d]:", i);
				if (i==status.priRecClk )
					txt=(char*)"(PriRecClk)";
				if (i==status.secRecClk )
					txt=(char*)"(SecRecClk)";
				portStatusClk(status.portMacSCR[i],txt);
				printf ("\n");
			}
        }
        
    }
	if (mode == PARAM_SET) {
        if (port > 10) {
            printf ("%s",usage);
			close (fd);
            exit (-EINVAL);
        }
        if (setSynce (fd, port, refClock, recClock) < 0)
            printf ("Error\n");
        else 
            printf ("OK\n");
    }
     
	close (fd);
	exit (0);
}
