/*

    marvell-port: status portu Marvell 88E6390 (X) switch
    
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/sockios.h>
#ifndef __GLIBC__
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#endif
#include <linux/types.h>

#include "marvell-interface.h"
#include "marvell-lib-def.h"
#include "marvell-reg.h"
#include "marvell-lib-debug.h"
#include "marvell-lib.h"

extern int verbose;


int mvCheckPortCmode (int fd, __u8 port, __u16 *cmode)

{

	__u16	data;
	int 	ret;
	DEBUG ("PORT%d get  Status .\n",port);
	port+=PORT_BASE;
	if ((ret=regRead (fd,port,PORT_STATUS_REG,&data)) < 0)
		return ret;
	*cmode=0x000f & data;
	return 0;
	
}

/********************************************************************************
*       This routine retrieves the Link status.
*
* */
int mvGetPortLink (int fd, __u8 port, LINK *link)
{

	__u16	data;
	int 	ret;
	
	DEBUG ("PORT%d get  link .\n",port);
	port+=PORT_BASE;
	if ((ret=regRead (fd,port,PORT_STATUS_REG,&data)) < 0)
		return ret;
	if (data & STATUS_LINK)
		*link=UP;
	else
		*link=DOWN;
	return 0;
}

/********************************************************************************
*       This routine retrieves the Link status.
*
* */
int mvGetPortStatus (int fd, __u8 port, struct PORT_STATUS *status)
{

	__u16	data, data1, data2, data3;;
	int 	ret;
	
	DEBUG ("PORT%d get  Status .\n",port);
	status->portNo=port;
	port+=PORT_BASE;
	
	status->portType=mvCheckTypePort (fd, port);
	if ((ret=regRead (fd,port,PORT_STATUS_REG,&data)) < 0)
		return ret;
	if ((ret=regRead (fd,port,PORT_CONTROL_REG,&data3)) < 0)
		return ret;
	if ((ret=regRead (fd,port,PORT_CONTROL2_REG,&data1)) < 0)
		return ret;
	if ((ret=regRead (fd,port,PORT_PHYSICAL_CONTROL_REG,&data2)) < 0)
		return ret;
	memset (status,0,sizeof(status));
	if (data & STATUS_PAUSEEN)
		status->pauseEn=PAUSE_IMPLEMENT;
	if (data & STATUS_MYPAUSE)
		status->myPause=PASE_ADV;
	if (data & STATUS_PHYDETECT)
		status->phyDetect=PHY_ATTACHED;
	if (data & STATUS_LINK)
		status->link=UP;
	if (data & STATUS_DUPLEX)
		status->duplex=DUPLEX_FULL;
	if (data & STATUS_TXPAUSED)
		status->txPaused=ENABLED;
	if (data & STATUS_FLOWCTRL)
		status->flowCtrl=ENABLED;
	status->speed=(SPEED)(0x3&((data & STATUS_SPEED_MASK) >> 8));
	status->jumbo=(JUMBO_MODE)(0x3&((data1 & CONTROL2_JUMBO_MASK) >> 12));
	status->forceSpeed=NOT_FORCE_SPEED;
	if (data2 & CONTROL_FORCED_SPEED)
		status->forceSpeed=(FORCE_SPEED)((data2 & CONTROL_FORCE_SPEED_MASK)+1);
	if (data2 & CONTROL_FORCED_DPX) {
		if (data2 & CONTROL_DPX_VALUE)
			status->forceDuplex=FORCE_DUPLEX_FULL;
		else
			status->forceDuplex=FORCE_DUPLEX_HALF;
	}
	if (data2 & CONTROL_FORCED_LINK) {
		if (data2 & CONTROL_LINK_VALUE)
			status->forceLink=FORCE_LINK_UP;
		else
			status->forceLink=FORCE_LINK_DOWN;
	}
	status->portState=(PORT_STATE) (data3 & PORT_STATE_MASK);
	status->frameMode=(FRAME_MODE) ((data3 & FRAME_MODE_MASK) >> 8);
	status->egressMode=(EGRESS_MODE) ((data3 & EGRESS_MODE_MASK) >> 12);
	return 0;
}



/********************************************************************************
*       
*
* */

/********************************************************************************
*       
*
* */

int mvSetForceDuplex (int fd, __u8 port,  FORCE_DUPLEX duplex)
{
	int 	ret;
	__u16	data;

	DEBUG ("Port%d set Force duplex.\n",port);
	port += PORT_BASE;
	if((ret=regRead(fd,port,PORT_PHYSICAL_CONTROL_REG,&data)) < 0 )
		return ret;
	switch (duplex) {
		case FORCE_DUPLEX_FULL:
			data|= CONTROL_FORCED_DPX | CONTROL_DPX_VALUE;
			break;
		case FORCE_DUPLEX_HALF:
			data |= CONTROL_FORCED_DPX;
			data &=~CONTROL_DPX_VALUE;
			break;
		case NOT_FORCE_DUPLEX:
			data &=~(CONTROL_FORCED_DPX | CONTROL_DPX_VALUE);
			break;	
		default:
			return -EINVAL;
	}	
	return regWrite(fd,port,PORT_PHYSICAL_CONTROL_REG,data);

}

/********************************************************************************
*       
*
* */

int mvSetForceSpeed (int fd, __u8 port,  FORCE_SPEED speed)
{
	int 	ret;
	__u16	data;

	DEBUG ("Port%d set Force speed.\n",port);
	port += PORT_BASE;
	if((ret=regRead(fd,port,PORT_PHYSICAL_CONTROL_REG,&data)) < 0 )
		return ret;
	switch (speed) {
		case FORCE_SPEED_2500:
			data &=~CONTROL_FORCE_SPEED_MASK;
			data |=CONTROL_FORCE_SPEED_2500;
			data |=CONTROL_FORCED_SPEED;
			break;
		case FORCE_SPEED_1000:
			data &=~CONTROL_FORCE_SPEED_MASK;
			data |=CONTROL_FORCE_SPEED_1000;
			data |=CONTROL_FORCED_SPEED;
			break;
		case FORCE_SPEED_100:
			data &=~CONTROL_FORCE_SPEED_MASK;
			data |=CONTROL_FORCE_SPEED_100;
			data |=CONTROL_FORCED_SPEED;
			break;
		case FORCE_SPEED_10:
			data &=~CONTROL_FORCE_SPEED_MASK;
			data |=CONTROL_FORCED_SPEED;
			break;
		case NOT_FORCE_SPEED:
			data &=~CONTROL_FORCE_SPEED_MASK;
			data &=~CONTROL_FORCED_SPEED;
			break;	
		default:
			return -EINVAL;
	}	
	return regWrite(fd,port,PORT_PHYSICAL_CONTROL_REG,data);

}

/********************************************************************************
*       
*
* */

int mvSetForceLink (int fd, __u8 port,  FORCE_LINK link)
{
	int 	ret;
	__u16	data;

	DEBUG ("Port%d set Force link.\n",port);
	port += PORT_BASE;
	if((ret=regRead(fd,port,PORT_PHYSICAL_CONTROL_REG,&data)) < 0 )
		return ret;
	switch (link) {
		case FORCE_LINK_DOWN:
			data &=~CONTROL_LINK_VALUE;
			data |=CONTROL_FORCED_LINK;
			break;
		case FORCE_LINK_UP:
			data |=CONTROL_LINK_VALUE;
			data |=CONTROL_FORCED_LINK;
			break;
		case NOT_FORCE_LINK:
			data &=~CONTROL_LINK_VALUE;
			data &=~CONTROL_FORCED_LINK;
			break;	
		default:
			return -EINVAL;
	}	
	return regWrite(fd,port,PORT_PHYSICAL_CONTROL_REG,data);

}

/********************************************************************************
*       
*
* */

int mvSetJumboMode (int fd, __u8 port,  JUMBO_MODE mtu)
{
	int 	ret;
	__u16	data;

	DEBUG ("Port%d set Force speed.\n",port);
	port += PORT_BASE;
	if((ret=regRead(fd,port,PORT_CONTROL2_REG,&data)) < 0 )
		return ret;
	switch (mtu) {
		case JUMBO_MODE_10240:
			data &=~CONTROL2_JUMBO_MASK;
			data |=CONTROL2_JUMBO_10240;
			break;
		case JUMBO_MODE_2048:
			data &=~CONTROL2_JUMBO_MASK;
			data |=CONTROL2_JUMBO_2048;
			break;
		case JUMBO_MODE_1522:
			data &=~CONTROL2_JUMBO_MASK;
			break;	
		default:
			return -EINVAL;
	}	
	return regWrite(fd,port,PORT_CONTROL2_REG,data);

}

/********************************************************************************
*       
*
* */
int mvSetPhyDetect (int fd, __u8 port, ENABLE detect)
{
	int 	ret;
	__u16	data;

	DEBUG ("Port%d set PHY detect.\n",port);
	port += PORT_BASE;
	if((ret=regRead(fd,port,PORT_STATUS_REG,&data)) < 0 )
		return ret;
	switch (detect) {
		
		case ENABLED:

			data |=STATUS_PHYDETECT;
			break;
		case DISABLED:
			data &=~STATUS_PHYDETECT;
			break;	
		default:
			return -EINVAL;
	}	
	return regWrite(fd,port,PORT_STATUS_REG,data);

}

/********************************************************************************
*       
*
* */
int mvSetPortState (int fd, __u8 port,PORT_STATE state)
{
	int 	ret;
	__u16	data;

	DEBUG ("Port%d set PHY detect.\n",port);
	port += PORT_BASE;
	if((ret=regRead(fd,port,PORT_CONTROL_REG,&data)) < 0 )
		return ret;
	data&=~PORT_STATE_MASK;
	data|=PORT_STATE_MASK&state;
	return regWrite(fd,port,PORT_CONTROL_REG,data);

}



