/*

    marvell-serdes: cteni statusu serdesu (SGMII) 88E6390 (X) switch
    
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/sockios.h>
#ifndef __GLIBC__
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#endif
#include <linux/types.h>

#include "marvell-interface.h"
#include "marvell-lib-def.h"
#include "marvell-reg.h"
#include "marvell-lib-debug.h"
#include "marvell-lib.h"



extern int verbose;
extern __s8 allowedAddrSerdes[];

static int checkSerdesType (int fd, __u8 port);
static int sgmiiReset (int fd, __u8 port, __u16 value);
static int sgmiiPowerDown (int fd, __u8 port, ENABLE enable);
static int sgmiiRestartAutoNeg (int fd, __u8 port);
static int sgmiiAutoNegEnable (int fd, __u8 port, ENABLE enable);
static int  sgmiiSetSpeed (int fd , __u8 port ,SPEED speed);
static int sgmiiSetDuplexMode (int fd, __u8 port, DUPLEX dMode);
static int sgmiiGetLinkStatus(int fd, __u8 port, struct LINK_STATUS *linkStatus);
/*
*  otestuje platnost adresy a dle cmodu vrati  typ Serdesu
* 
*
* */
static int checkSerdesType (int fd, __u8 port)
{
	__u16  cmode;
	int ret;
	
	if ((ret=checkAddr (port, allowedAddrSerdes )) < 0)
		return ret;
	if ((ret=mvCheckPortCmode (fd, port, &cmode)) < 0)
		return 0;
	return (int) mvCheckTypePort(fd,port); 
}



/*
* provede reset SGMII a nastavi pozadovanou rychlost, duplex a loopback
* 
*
* */

int mvSerdesReset (int fd, __u8 port, __u16 value)
{
	int ret;
	if ((ret=checkSerdesType (fd, port)) < 0)
		return ret;
	if (((TYPE_PORT)ret == PORT_SGMII) || ((TYPE_PORT)ret == PORT_1000BASE_X))
		return sgmiiReset (fd,port,value);
	DEBUG ("Only for cmode SGMII or 1000Base-X\n");
	return -ENODEV;
}	

int sgmiiReset (int fd, __u8 port, __u16 value)
{
	int 	ret;
	__u16 	regv;
	int 	retryCount;
	int    	pd = 0;
	
	DEBUG ("Reset SGMII port:0x%02x,\n",port);
	if((ret=serdesRegRead(fd,port,SGMII_CONTROL_REG,&regv)) < 0)
		return ret;
	if (regv & SGMII_CONTROL_POWER)
		pd = 1;
	if (value != 0xFF)
		regv= value;

	/* Set the desired bits to 0. */
	if (pd)
		regv &= ~SGMII_CONTROL_POWER;
	else
		regv |= SGMII_CONTROL_RESET;

	if((ret=serdesRegWrite(fd,port,SGMII_CONTROL_REG,regv)) < 0)
		return ret;
     
	if (pd)
		return 0;
	for (retryCount = 0x1000; retryCount > 0; retryCount--) {
		if((ret=serdesRegRead(fd,port,SGMII_CONTROL_REG,&regv)) < 0)
			return ret;
		if ((regv & SGMII_CONTROL_RESET) == 0)
			break;
	}
	if (retryCount == 0) {
		fprintf(stderr,"Reset bit is not cleared\n");
		return -ENODEV;
	}
	return 0;
}


/****************************************************************************
* Enable/disable (power down) on specific port.
* 
* ENABED = power down
* 
* */


int mvSerdesPowerDown (int fd, __u8 port, ENABLE enable)
{
	int ret;
	if ((ret=checkSerdesType (fd, port)) < 0)
		return ret;
	if (((TYPE_PORT)ret == PORT_SGMII) || ((TYPE_PORT)ret == PORT_1000BASE_X))
		return sgmiiPowerDown (fd,port,enable);
	DEBUG ("Only for cmode SGMII or 1000Base-X\n");
	return -ENODEV;
}

static int sgmiiPowerDown (int fd, __u8 port, ENABLE enable)
{
	
	DEBUG ("SGMII %d set PowerDown.\n",port);

	if (enable) {
		return serdesRegWriteMask (fd,port,SGMII_CONTROL_REG,SGMII_CONTROL_POWER,SGMII_CONTROL_POWER);
	} else {
		return serdesRegWriteMask (fd,port,SGMII_CONTROL_REG,0,SGMII_CONTROL_POWER);
	}
}


/****************************************************************************
*        Enable and  Restart AutoNegotiation.
*	 
*	
*/
int mvSerdesRestartAutoNeg (int fd, __u8 port) 
{     
	int ret;
	
	if ((ret=checkSerdesType (fd, port)) < 0)
		return ret;
	if (((TYPE_PORT)ret == PORT_SGMII) || ((TYPE_PORT)ret == PORT_1000BASE_X))
		return sgmiiRestartAutoNeg (fd,port);
	DEBUG ("Only for cmode SGMII or 1000Base-X\n");
	return -ENODEV;
}


static int sgmiiRestartAutoNeg (int fd, __u8 port) 
{

	DEBUG ("SGMII %d restart AutoNegotiation.\n",port);
	return serdesRegWriteMask(fd,port,SGMII_CONTROL_REG, SGMII_CONTROL_RESTART_AUTONEGO | SGMII_CONTROL_AUTONEGO,SGMII_CONTROL_RESTART_AUTONEGO | SGMII_CONTROL_AUTONEGO);
}

/****************************************************************************
*         Enable/disable an Auto-Negotiation.
*     
* 
*/

int mvSerdesAutoNegEnable (int fd, __u8 port, ENABLE enable)
{
	int ret;
	
	if ((ret=checkSerdesType (fd, port)) < 0)
		return ret;
	if (((TYPE_PORT)ret == PORT_SGMII) || ((TYPE_PORT)ret == PORT_1000BASE_X))
		return sgmiiAutoNegEnable (fd,port,enable);
	DEBUG ("Only for cmode SGMII or 1000Base-X\n");
	return -ENODEV;

}


static int sgmiiAutoNegEnable (int fd, __u8 port, ENABLE enable)
{

	DEBUG ("SGMII %d set autonego enable/disable .\n",port);
	if(enable)
		return serdesRegWriteMask(fd,port,SGMII_CONTROL_REG, SGMII_CONTROL_AUTONEGO, SGMII_CONTROL_AUTONEGO);
	else
		return serdesRegWriteMask(fd,port,SGMII_CONTROL_REG,0, SGMII_CONTROL_AUTONEGO);
}



/****************************************************************************
*   	Sets speed for a specific  port. 
* 
*/
int  mvSerdesSpeed (int fd , __u8 port ,SPEED speed)

{

	int ret;
	
	if ((ret=checkSerdesType (fd, port)) < 0)
		return ret;
	if (((TYPE_PORT)ret == PORT_SGMII) || ((TYPE_PORT)ret == PORT_1000BASE_X))
		return sgmiiSetSpeed (fd,port,speed);
	DEBUG ("Only for cmode SGMII or 1000Base-X\n");
	return -ENODEV;


}


static int  sgmiiSetSpeed (int fd , __u8 port ,SPEED speed)

{
	__u16	data;
	int	ret;

	DEBUG ("SGMII %d set speed copper.\n",port);
	if ((ret=serdesRegRead(fd,port,SGMII_CONTROL_REG,&data)) < 0)
		return ret;

	switch(speed){
		case SPEED_10:
			data &=  ~(SGMII_CONTROL_SPEED | SGMII_CONTROL_SPEED_MSB );
			break; 
		case SPEED_100:
			data &=  ~SGMII_CONTROL_SPEED_MSB;
			data |=  SGMII_CONTROL_SPEED;
			break;
		case SPEED_1000:
			data &=  ~SGMII_CONTROL_SPEED;
			data |=  SGMII_CONTROL_SPEED_MSB;
			break;
		default:
			return -EINVAL;
	}
	return serdesRegWrite(fd,port,SGMII_CONTROL_REG,data);
}

/***************************************************************************
*       Sets duplex mode 
* 
*/
int mvSerdesDuplexMode (int fd, __u8 port, DUPLEX dMode)
{
	int ret;
	
	if ((ret=checkSerdesType (fd, port)) < 0)
		return ret;
	if (((TYPE_PORT)ret == PORT_SGMII) || ((TYPE_PORT)ret == PORT_1000BASE_X))
		return sgmiiSetDuplexMode (fd,port,dMode);
	DEBUG ("Only for cmode SGMII or 1000Base-X\n");
	return -ENODEV;

}


static int sgmiiSetDuplexMode (int fd, __u8 port, DUPLEX dMode)
{
	
	DEBUG ("SGMII %d set Duplex Mode Copper.\n",port);
	if(dMode)
		return serdesRegWriteMask (fd, port, SGMII_CONTROL_REG, SGMII_CONTROL_DUPLEX,  SGMII_CONTROL_DUPLEX);
	else
		return serdesRegWriteMask (fd, port, SGMII_CONTROL_REG, 0,  SGMII_CONTROL_DUPLEX);
}




/********************************************************************************
*       This routine retrieves the Link status.
*
* */

int mvGetSerdesLinkStatus (int fd, __u8 port, struct LINK_STATUS *linkStatus)
{
	int ret;
	
	memset (linkStatus,0,sizeof(linkStatus));
	if ((ret=checkSerdesType (fd, port)) < 0)
		return ret;
	if (((TYPE_PORT)ret == PORT_SGMII) || ((TYPE_PORT)ret == PORT_1000BASE_X)) {
		linkStatus->portType =  (TYPE_PORT) ret;
		linkStatus->portNo =  port;
		return sgmiiGetLinkStatus (fd,port,linkStatus);
	}
	printf ("Only for cmode SGMII or 1000Base-X\n");
	return -ENODEV;
	
}

static int sgmiiGetLinkStatus(int fd, __u8 port, struct LINK_STATUS *linkStatus)
{

	__u16	data, data1;
	int 	ret;
	
	DEBUG ("SGMII %d get  Link Status Copper.\n",port);
	if ((ret=serdesRegRead (fd,port,SGMII_SPEC_STATUS_REG,&data)) < 0)
		return ret;
	if ((ret=serdesRegRead (fd,port,SGMII_CONTROL_REG,&data1)) < 0)
		return ret;
	if (data & SGMII_SPEC_STATUS_LINK)
		linkStatus->link=UP;
	if (data & SGMII_SPEC_STATUS_RESOLVED) {
		linkStatus->resolvedSpeedDuplex=ENABLED;
		if (data & SGMII_SPEC_STATUS_TX_PAUSE_ENA)
			linkStatus->transmitPause=ENABLED;
		if (data & SGMII_SPEC_STATUS_RX_PAUSE_ENA)
			linkStatus->receivePause=ENABLED;
		if (data & SGMII_SPEC_STATUS_DUPLEX)
			linkStatus->duplex=DUPLEX_FULL;
		linkStatus->speed=(SPEED)((data & SGMII_SPEC_STATUS_SPEED_MASK) >>14);
	}
	if (data1 & SGMII_CONTROL_AUTONEGO)
		linkStatus->autonego=ENABLED;
	if ((ret=serdesRegRead (fd,port,SGMII_AUTONEGO_REG,&data)) < 0)
		return ret;
	linkStatus->autonegoAd=(data&0x1fc0);
	return 0;
}

