/*

    marvell-tool: monitor and control for Marvell 88E6390 (X) switch
*/



#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include <linux/types.h>
#include "marvell-lib-def.h"
#include "marvell-lib.h"
#include "marvell-reg.h"
#include "marvell-lib-rmon.h"
#include <math.h>

#define PRINTCNT32(a,b) { \
	{ \
		if (a < b) \
			fprintf(stderr,"\t%20u",a-b + 0xffffffff); \
		else \
			fprintf(stderr,"\t%20u",a-b); \
	}\
}

#define PRINTCNT32SAVE(a,b,c) { \
	{ \
		if (a < b) {\
			*c+=a-b + 0xffffffff;\
			fprintf(stderr,"\t%20u",a-b + 0xffffffff); \
		} else {\
			*c+=a-b;\
			fprintf(stderr,"\t%20u",a-b); \
		}\
	}\
}


static double convert_mw_to_dbm(double mw)
{
	return (10. * log10(mw / 1000.)) + 30.;
}


const char * octets64[] = {"\nOctets64","\nInOctets64","\nOutOctets64","\nOctets64"};
const char * octets127[] = {"\nOctets127","\nInOctets127","\nOutOctets127","\nOctets127"};
const char * octets255[] = {"\nOctets255","\nInOctets255","\nOutOctets255","\nOctets255"};
const char * octets511[] = {"\nOctets511","\nInOctets511","\nOutOctets511","\nOctets511"};
const char * octets1023[] = {"\nOctets1023","\nInOctets1023","\nOutOctets1023","\nOctets1023"};
const char * octetsMax[] = {"\nOctetsMax","\nInOctetsMax","\nOutOctetsMax","\nOctetsMax"};


/*--------------------------------------------------------------------*/
//  vytiskne  len registru od pocatecni adresy z prostoru regs
//  addr je adresa odkud bylo cteno
void printAllSerdesReg (__u8 port, SERDES_REGS* reg_table, int pos, int len)
{
	int i;
	
	for (i=0;i<len;i++) {
		if (!((pos+i)%0x10))
			printf("\nPort:%02x Reg:%04x: ",port,(i+pos) & 0x0000ffff);
		if (reg_table[i].status < 0)
			printf("xxxx ");
		else
			printf("%04x ",reg_table[i].value);
	}
	printf("\n");
	return ;
}


/*--------------------------------------------------------------------*/
//  vytiskne  len registru od pocatecni adresy z prostoru regs
//  addr je adresa odkud bylo cteno
void printAllReg (__u8 addr, __u16* regs, int pos, int len)
{
	int i;
	__u16* ptr=regs;
	for (i=0;i<len;i++,ptr++) {
		if (!((pos+i)%0x10))
			printf("\nAddr:%02x Reg:%02x: ",addr,i&0x000000ff);
		printf("%04x ",*ptr);
	}
	printf("\n");
	return ;
}
/*--------------------------------------------------------------------*/


// vytiskne vsechny page na danem PHY 
void printAllPageReg (__u8 addr , __s8 page, __u16* regs, int pos, int len)
{
	int i;
	__u16* ptr=regs;
	for (i=0x0; i<len; i++, ptr++) {
		if (!((pos+i)%0x10)) 
			printf("\nAddr:%02x Page:%02x Reg:%02x: ",addr,page, i&0x000000ff);
		printf("%04x ",*ptr);
	}
	printf("\n");
	return;
}

/*-------------------------------------------------------------------*/
void printPortType  (TYPE_PORT portType, char* pr)
{
	printf("\ttype:%s",pr);
	switch (portType) {
		case PORT_PHY:
			printf("\t\tinternal PHY\n");
			break;
		case PORT_1000BASE_X:
			printf("\t\t1000Base-X\n");
			break;
		case PORT_SGMII:
			printf("\t\tSGMII\n");
			break;
		case PORT_2500BASE_X:
			printf("\t\t2500Base-X\n");
			break;	
		case PORT_10G_XAUI:
			printf("\t\t10G-XAUI\n");
			break;	
		case PORT_10G_RXAUI:
			printf("\t\t10G-RXAUI\n");
			break;
		case PORT_MII_FD:
			printf("\t\tFD MII\n");
			break;
		case PORT_MII_PHY:
			printf("\t\tMII PHY\n");
			break;
		case PORT_MII_MAC:
			printf("\t\tMII MAC or MII to PHY\n");
			break;
		case PORT_GMII:
			printf("\t\tGMII\n");
			break;
		case PORT_RMII_PHY:
			printf("\t\tRMII PHY\n");
			break;
		case PORT_RMII_MAC:
			printf("\t\tRMII MAC\n");
			break;
		case PORT_XMII_TRISTATE:
			printf("\t\txMII Tristate\n");
			break;
		case PORT_GRMII:
			printf("\t\tRGMII\n");
			break;
		case PORT_VACANT:
			printf("\t\tnone\n");
			break;
	}
}



/*--------------------------------------------------------------------*/
// vypise status Serdesu daneho portu 
void printSerdesStatus  (struct LINK_STATUS *linkStatus, __u8 port)
{
	printf("Serdes%d status:\n",port);
	printPortType (linkStatus->portType,(char*)"");
	if ((linkStatus->portType == PORT_SGMII ) ||(linkStatus->portType == PORT_1000BASE_X ))  {
		if (linkStatus->link == UP) {
			printf("\tlink:\t\tup\n");
			if (linkStatus->resolvedSpeedDuplex == ENABLED) {
				printf("\tspeed:");
				switch (linkStatus->speed) {
					case SPEED_10:
						printf("\t\t10Mbps\n");
						break;
					case SPEED_100:
						printf("\t\t100Mbps\n");
						break;
					case SPEED_1000:
						printf("\t\t1000Mbps\n");
						break;
					case SPEED_2500:
						printf("\t\t2500Mbps\n");
						break;
				}
				printf ("\tduplex:");
				if (linkStatus->duplex)
					printf ("\t\tfull\n");
				else
					printf ("\t\thalf\n");
			} else {
				printf("\tspeed:\t\tNOT resolved\n");
				printf("\tduplex:\t\tNOT resolved\n");
			}
#if 0
			printf ("\tautonegotiation:");
			if (linkStatus->autonego) {
				printf ("enable\n");
				printf ("\t\t\tSpeed: ");
				switch (linkStatus->autonegoAd & SGMII_AUTONEGO_SPEED_MASK) {
					case 0x0:
						printf ("10Mbps\n");
						break;
					case 0x0400:
						printf ("100Mbps\n");
						break;
					case 0x0800:
						printf ("1000Mbps\n");
						break;
					default:
						printf ("Reserved\n");
				}
				if (linkStatus->autonegoAd & SGMII_AUTONEGO_DUPLEX )
					printf ("\t\t\tDuplex: full\n");
				else
					printf ("\t\t\tDuplex: half \n");
				if (linkStatus->autonegoAd & SGMII_AUTONEGO_TX_PAUSE )
					printf ("\t\t\tTransmit Pause: enabled\n");
				else
					printf ("\t\t\tTransmit Pause: disabled \n");
				if (linkStatus->autonegoAd & SGMII_AUTONEGO_RX_PAUSE )
					printf ("\t\t\tReceive Pause: enabled \n");
				else
					printf ("\t\t\tReceive Pause: disabled\n");
				if (linkStatus->autonegoAd & SGMII_AUTONEGO_MEDIA )
					printf ("\t\t\tMeda: fiber \n");
				else
					printf ("\t\t\tMedia: copper\n");
				if (linkStatus->autonegoAd & SGMII_AUTONEGO_EEE )
					printf ("\t\t\tEEE mode: enabled \n");
				else
					printf ("\t\t\tEEE mode: disabled\n");

			} else {
				printf ("disable\n");
			}
#endif
			if (linkStatus->resolvedSpeedDuplex == ENABLED) {
				printf ("\ttransmitPause:");
				if (linkStatus->transmitPause)
					printf ("\tenable\n");
				else
					printf ("\tdisable\n");
				printf ("\treceivePause:");
				if (linkStatus->receivePause)
					printf ("\tenable\n");
				else
					printf ("\tdisable\n");

			} else {
				printf ("\ttransmitPause:\tNOT enabled\n");
				printf ("\treceivePause:\tNOT enabled\n");
			}
		}else {
			printf("\tlink:\t\tdown\n");
		}
	}
}

/**********************************************************************************/
// vypise status PHY daneho portu 
void printPhyStatus  (struct LINK_STATUS *linkStatus,__u16 inErrors, __u8 port)
{
	printf("PHY%d status:\n",port);
	printPortType (linkStatus->portType,(char*)"");
	if (linkStatus->portType == PORT_PHY ) {
		if (linkStatus->link == UP) {
			printf("\tlink:\t\tup\n");
			if (linkStatus->resolvedSpeedDuplex == ENABLED) {
				printf("\tspeed:");
				switch (linkStatus->speed) {
					case SPEED_10:
						printf("\t\t10Mbps\n");
						break;
					case SPEED_100:
						printf("\t\t100Mbps\n");
						break;
					case SPEED_1000:
						printf("\t\t1000Mbps\n");
						break;
					case SPEED_2500:
						printf("\t\t2500Mbps\n");
						break;
				}
				printf ("\tduplex:");
				if (linkStatus->duplex)
					printf ("\t\tfull\n");
				else
					printf ("\t\thalf\n");
			} else {
				printf("\tspeed:\t\tNOT resolved\n");
				printf("\tduplex:\t\tNOT resolved\n");
			}
			printf ("\tautonegotiation:");
			if (linkStatus->autonego) {
				printf ("enable\n");
				printf ("\tadvertise:\n");
				if (linkStatus->autonegoAd & AUTONEGO_1000_MASTER_ENABLE) {
					printf ("\t\t\tMASTER/SLAVE: Manual ");
					if (linkStatus->autonegoAd & AUTONEGO_1000_MASTER_VALUE)
						printf ("- Master\n");
					else
						printf ("- Slave\n");
				} else {
					printf ("\t\t\tMASTER/SLAVE: Automatic\n");
				}
				if (linkStatus->autonegoAd & AUTONEGO_1000_PORT_TYPE)
					printf ("\t\t\tPort: multi\n");
				else
					printf ("\t\t\tPort: Single\n");
				if (linkStatus->autonegoAd & AUTONEGO_PAUSE) {
					if (linkStatus->autonegoAd & AUTONEGO_ASYMMETRIC_PAUSE )
						printf ("\t\t\tPause: Asymmetric and symmetric (RX)\n");
					else
						printf ("\t\t\tPause: Symmetric\n");
				} else {
					if (linkStatus->autonegoAd & AUTONEGO_ASYMMETRIC_PAUSE )
						printf ("\t\t\tPause: Asymmetric (TX)\n");
					else
						printf ("\t\t\tPause: Not implemented\n");
				}
				if (linkStatus->autonegoAd & AUTONEGO_1000_FD)
					printf ("\t\t\t1000Base-T FD: Advertise\n");
				else
					printf ("\t\t\t1000Base-T FD: Not advertise\n");
				if (linkStatus->autonegoAd & AUTONEGO_1000_HD)
					printf ("\t\t\t1000Base-T HD: Advertise\n");
				else
					printf ("\t\t\t1000Base-T HD: Not advertise\n");
				if (linkStatus->autonegoAd & AUTONEGO_100_T4)
					printf ("\t\t\t100Base-T4: Advertise\n");
				else
					printf ("\t\t\t100Base-T4: Not advertise\n");
				if (linkStatus->autonegoAd & AUTONEGO_100_FULL)
					printf ("\t\t\t100Base-TX FD: Advertise\n");
				else
					printf ("\t\t\t100Base-TX FD: Not advertise\n");
				if (linkStatus->autonegoAd & AUTONEGO_100_HALF)
					printf ("\t\t\t100Base-TX HD: Advertise\n");
				else
					printf ("\t\t\t100Base-TX HD: Not advertise\n");
				if (linkStatus->autonegoAd & AUTONEGO_10_FULL)
					printf ("\t\t\t10Base-TX FD: Advertise\n");
				else
					printf ("\t\t\t10Base-TX FD: Not advertise\n");
				if (linkStatus->autonegoAd & AUTONEGO_10_HALF)
					printf ("\t\t\t10Base-TX HD: Advertise\n");
				else
					printf ("\t\t\t10Base-TX HD: Not advertise\n");
				printf ("\t1000Base-T Status:\n");
				if (linkStatus->linkPartner & LP_1000_MASTER_FAULT)
					printf ("\t\t\tMASTER/SLAVE: Faut detect\n");
				else
					printf ("\t\t\tMASTER/SLAVE: No fault detect\n");
				if (linkStatus->linkPartner & LP_1000_MASTER_RES)
					printf ("\t\t\tMASTER/SLAVE: Local PHY configuration resolved to Master\n");
				else
					printf ("\t\t\tMASTER/SLAVE: Local PHY configuration resolved to Slave\n");
				if (linkStatus->linkPartner & LP_1000_LOCAL_STATUS)
					printf ("\t\t\tMASTER/SLAVE: Local OK\n");
				else
					printf ("\t\t\tMASTER/SLAVE: Local not OK\n");
				if (linkStatus->linkPartner & LP_1000_REMOTE_STATUS)
					printf ("\t\t\tMASTER/SLAVE: Remote OK\n");
				else
					printf ("\t\t\tMASTER/SLAVE: Remote not OK\n");
				printf ("\tlink partner:\n");
				if (linkStatus->linkPartner & LP_PAUSE) {			
					if (linkStatus->linkPartner & LP_ASYMPAUS)
						printf ("\t\t\tPause: Asymmetric and symmetric (RX)\n");
					else
						printf ("\t\t\tPause: Symmetric\n");
				} else {
					if (linkStatus->linkPartner & LP_ASYMPAUS )
						printf ("\t\t\tPause: Asymmetric (TX)\n");
					else
						printf ("\t\t\tPause: Not capable\n");
				}
				if (linkStatus->linkPartner & LP_1000B_TXFDX)
					printf ("\t\t\t1000Base-T FD: Capable\n");
				else
					printf ("\t\t\t1000Base-T FD: Not capable\n");
				if (linkStatus->linkPartner & LP_1000B_TXHDX)
					printf ("\t\t\t1000Base-T HD: Capable\n");
				else
					printf ("\t\t\t1000Base-T HD: Not capable\n");
				if (linkStatus->linkPartner & LP_100B_T4)
					printf ("\t\t\t100Base-T4: Capable\n");
				else
					printf ("\t\t\t100Base-T4: Not capable\n");
				if (linkStatus->linkPartner & LP_100B_TXFDX)
					printf ("\t\t\t100Base-TX FD: Capable\n");
				else
					printf ("\t\t\t100Base-TX FD: Not capable\n");
				if (linkStatus->linkPartner & LP_100B_TXHDX)
					printf ("\t\t\t100Base-TX HD: Capable\n");
				else
					printf ("\t\t\t100Base-TX HD: Not capable\n");
				if (linkStatus->linkPartner & LP_10B_TFDX)
					printf ("\t\t\t10Base-TX FD: Capable\n");
				else
					printf ("\t\t\t10Base-TX FD: Not capable\n");
				if (linkStatus->linkPartner & LP_10B_THDX)
					printf ("\t\t\t10Base-TX HD: Capable\n");
				else
					printf ("\t\t\t10Base-TX HD: Not capable\n");
			} else {
				printf ("disable\n");
			}
			if (linkStatus->resolvedSpeedDuplex == ENABLED) {
				printf ("\ttransmitPause:");
				if (linkStatus->transmitPause)
					printf ("\tenable\n");
				else
					printf ("\tdisable\n");
				printf ("\treceivePause:");
				if (linkStatus->receivePause)
					printf ("\tenable\n");
				else
					printf ("\tdisable\n");
				printf ("\tMDI:");
				if (linkStatus->mdi == MDIX)
					printf ("\t\tMDI-X\n");
				else
					printf ("\t\tMDI\n");

			} else {
				printf ("\ttransmitPause:\tNOT enabled\n");
				printf ("\treceivePause:\tNOT enabled\n");
				printf ("\tMDI:\t\tNOT enabled\n");
			}
			printf ("\tinError:\t%d\n",inErrors);
		}else {
			printf("\tlink:\t\tdown\n");
		}
	}
}
/*--------------------------------------------------------------------*/

// vypise se status switch portu 
void printPortStatus  (struct PORT_STATUS *portStatus, __u8 port)
{
	printf("Port%d status:\n",port);	
	if (portStatus->link == UP) {
		printPortType (portStatus->portType,(char*)"\t");
		printf("\tlink:\t\t\tup\n");
		printf ("\tPHY detect:");
		if (portStatus->phyDetect)
			printf ("\t\tattached\n");
		else
			printf ("\t\tnot attached\n");
		printf("\tspeed:");
		switch (portStatus->speed) {
			case SPEED_10:
				printf("\t\t\t10Mbps\n");
				break;
			case SPEED_100:
				printf("\t\t\t100Mbps\n");
				break;
			case SPEED_1000:
				printf("\t\t\t1000Mbps\n");
				break;
			case SPEED_2500:
				printf("\t\t\t2500Mbps\n");
				break;
		}
		printf ("\tduplex:");
		if (portStatus->duplex)
			printf ("\t\t\tfull\n");
		else
			printf ("\t\t\thalf\n");
		printf ("\tmyPause:");
		if (portStatus->myPause)
			printf ("\t\tadvertiset\n");
		else
			printf ("\t\tnot advetrised\n");
		printf ("\tenablePause:");
		if (portStatus->pauseEn)
			printf ("\t\timplemented\n");
		else
			printf ("\t\tnot implemented\n");
		if (portStatus->txPaused)
			printf ("\ttxPaused:\t\ttransmiter paused\n");
		if (portStatus->flowCtrl)
			printf ("\tflowCtrl:\t\tRx MAC determines that no more data should be entering this port\n");
		printf ("\tMTU:");
		switch (portStatus->jumbo){
			case JUMBO_MODE_1522:
				printf ("\t\t\t1522 byte\n");
				break;
			case JUMBO_MODE_2048:
				printf ("\t\t\t2048 byte\n");
				break;
			case JUMBO_MODE_10240:
				printf ("\t\t\t10240 byte\n");
				break;
		}
		
		printf ("\tforceLink:");
		switch (portStatus->forceLink) {
			case NOT_FORCE_LINK:
				printf ("\t\tnot forced\n");
				break;
			case FORCE_LINK_DOWN:
				printf ("\t\tdown\n");
				break;	
			case FORCE_LINK_UP:
				printf ("\t\tup\n");
				break;
		}	
		printf ("\tforceDuplex:");
		switch (portStatus->forceDuplex) {
			case NOT_FORCE_DUPLEX:
				printf ("\t\tnot forced\n");
				break;
			case FORCE_DUPLEX_HALF:
				printf ("\t\thalf\n");
				break;	
			case FORCE_DUPLEX_FULL:
				printf ("\t\tfull\n");
				break;
		}
		printf ("\tforceSpeed:");
		switch (portStatus->forceSpeed){
			case FORCE_SPEED_2500:
				printf ("\t\t2500Mbps\n");
				break;
			case FORCE_SPEED_1000:
				printf ("\t\t1000Mbps\n");
				break;
			case FORCE_SPEED_100:
				printf ("\t\t100Mbps\n");
				break;
			case FORCE_SPEED_10:
				printf ("\t\t10Mbps\n");
				break;
			default:
				printf ("\t\tnot forced\n");
				break;
		}
		printf ("\tportState:");
		switch (portStatus->portState) {
			case PORT_STATE_DISABLED:
				printf ("\t\tdisabled\n");
				break;
			case PORT_STATE_LISTENING:
				printf ("\t\tlistening\n");
				break;
			case PORT_STATE_LEARNING:
				printf ("\t\tlearning\n");
				break;
			case PORT_STATE_FORWARDING:
				printf ("\t\tforwarding\n");
				break;
		}
		printf ("\tframeMode:");
		switch (portStatus->frameMode) {
			case FRAME_MODE_NORMAL_NETWOK:
				printf ("\t\tnormal network\n");
				break;
			case FRAME_MODE_DSA:
				printf ("\t\tDSA\n");
				break;
			case FRAME_MODE_PROVIDER:
				printf ("\t\tprovider\n");
				break;
			case FRAME_MODE_ETHTYPEDSA:
				printf ("\t\tether type DSA\n");
				break;
		}
		printf ("\tegressMode:");
		switch (portStatus->egressMode) {
			case EGRESS_MODE_UNMODIFIED:
				printf ("\t\tunmodified\n");
				break;
			case EGRESS_MODE_UNTAGGED:
				printf ("\t\tuntagged\n");
				break;
			case EGRESS_MODE_TAGGED:
				printf ("\t\ttagged\n");
				break;
		}
	}else {
		printPortType (portStatus->portType,(char*)"");
		printf("\tlink:\t\tdown\n");
	}
}



void printRmonCounters(TABLE_RMON * data, HISTOGRAM_MODE histogram, int portPtr, int number, int time)
{


	int ii;
	__u64 allInPktBroadcast=0;
	__u64 allInPktMulticast=0;
	__u64 allInPktUnicast=0;
	__u64 allOutPktBroadcast=0;
	__u64 allOutPktMulticast=0;
	__u64 allOutPktUnicast=0;
	__u64 allDiffMulticast=0;
	__u64 allDiffBroadcast=0;
	__u64 allDiffUnicast=0;
	int portLink=0;
	
	if (histogram >3)
		return;
	for (ii=0; ii<portPtr;ii++) {
		if (data[ii].portLink == UP)
			portLink++;
	}
	if (number > 0) {
		fprintf( stderr,"----------------------------------------------------------------------\n");
		fprintf( stderr,"Measurement (number/time)\t%d/%d(sec)\n",number,time);
	}
	fprintf( stderr,"%-20s","Port\t"); 
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20d",data[ii].port); 
	
	fprintf( stderr,"%-20s","\nInGoodOctets");
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20llu", (__u64) data[ii].data_rmon->InGoodOctetsHi <<32|  data[ii].data_rmon->InGoodOctetsLo);
	fprintf( stderr,"%-20s","\nInBadOctets");
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].data_rmon->InBadOctets); 
	fprintf( stderr,"%-20s","\nInUnicasts");
	allInPktUnicast=0;
	for (ii=0; ii<portPtr;ii++) {
		fprintf( stderr,"\t%20u",data[ii].data_rmon->InUnicasts); 
		allInPktUnicast+=data[ii].data_rmon->InUnicasts; 
	}
	fprintf( stderr,"%-20s","\nInPause\t");
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].data_rmon->InPause); 
	fprintf( stderr,"%-20s","\nInBroadcasts");
	allInPktBroadcast=0;
	for (ii=0; ii<portPtr;ii++) {
		fprintf( stderr,"\t%20u",data[ii].data_rmon->InBroadcasts); 
		allInPktBroadcast+=data[ii].data_rmon->InBroadcasts;
	}
	fprintf( stderr,"%-20s","\nInMulticasts");
	allInPktMulticast=0;
	for (ii=0; ii<portPtr;ii++) {
		fprintf( stderr,"\t%20u",data[ii].data_rmon->InMulticasts); 
		allInPktMulticast+=data[ii].data_rmon->InMulticasts;
	}
	fprintf( stderr,"%-20s","\nInMACRcvErr");
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].data_rmon->InMACRcvErr); 
	fprintf( stderr,"%-20s","\nInFCSErr");
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].data_rmon->InFCSErr);
	fprintf( stderr,"%-20s","\nInDiscradsFrame");
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].discards); 
	fprintf( stderr,"%-20s","\nInFiltered");
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].inFiltered); 
    /* Histogram Counters : Rx Only, Tx Only, or both Rx and Tx */
	fprintf( stderr,"%-20s",octets64[histogram]);
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].data_rmon->Octets64); 
	fprintf( stderr,"%-20s",octets127[histogram]);
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].data_rmon->Octets127); 
	fprintf( stderr,"%-20s",octets255[histogram]);
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].data_rmon->Octets255); 
	fprintf( stderr,"%-20s",octets511[histogram]);
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].data_rmon->Octets511); 
	fprintf( stderr,"%-20s",octets1023[histogram]);
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].data_rmon->Octets1023); 
	fprintf( stderr,"%-20s",octetsMax[histogram]);
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].data_rmon->OctetsMax); 
	fprintf( stderr,"%-20s","\nOutOctets");
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20llu",(__u64)data[ii].data_rmon->OutOctetsHi <<32 | data[ii].data_rmon->OutOctetsLo); 
	fprintf( stderr,"%-20s","\nOutUnicasts");
	allOutPktUnicast=0;
	for (ii=0; ii<portPtr;ii++) {
		fprintf( stderr,"\t%20u",data[ii].data_rmon->OutUnicasts); 
		allOutPktUnicast+=data[ii].data_rmon->OutUnicasts;
	}
	fprintf( stderr,"%-20s","\nOutMulticasts");
	allOutPktMulticast=0;
	for (ii=0; ii<portPtr;ii++) {
		fprintf( stderr,"\t%20u",data[ii].data_rmon->OutMulticasts); 
		allOutPktMulticast+=data[ii].data_rmon->OutMulticasts;
	}
	fprintf( stderr,"%-20s","\nOutBroadcasts");
	allOutPktBroadcast=0;
	for (ii=0; ii<portPtr;ii++) {
		fprintf( stderr,"\t%20u",data[ii].data_rmon->OutBroadcasts); 
		 allOutPktBroadcast+=data[ii].data_rmon->OutBroadcasts;
	}
	fprintf( stderr,"%-20s","\nOutPause");
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].data_rmon->OutPause); 
	fprintf( stderr,"%-20s","\nOutFCSErr");
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].data_rmon->OutFCSErr); 
		fprintf( stderr,"%-20s","\nOutFiltered");
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].outFiltered); 
	fprintf( stderr,"%-20s","\nExcessive");
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].data_rmon->Excessive); 
	fprintf( stderr,"%-20s","\nDeferred");
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].data_rmon->Deferred); 
	fprintf( stderr,"%-20s","\nSingle  ");
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].data_rmon->Single); 
	fprintf( stderr,"%-20s","\nMultiple");
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].data_rmon->Multiple); 
	fprintf( stderr,"%-20s","\nUndersize");
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].data_rmon->Undersize); 
	fprintf( stderr,"%-20s","\nFragments");
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].data_rmon->Fragments); 
	fprintf( stderr,"%-20s","\nOversize");
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].data_rmon->Oversize); 
	fprintf( stderr,"%-20s","\nJabber\t");
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].data_rmon->Jabber); 
	fprintf( stderr,"%-20s","\nCollisions");
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].data_rmon->Collisions); 
	fprintf( stderr,"%-20s","\nLate\t");
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20u",data[ii].data_rmon->Late); 
	fprintf( stderr,"\n\n");
	allDiffUnicast=allInPktUnicast-allOutPktUnicast;
	allDiffBroadcast=((portLink-1)*allInPktBroadcast)-allOutPktBroadcast,portLink-1;
	allDiffMulticast=((portLink-1)*allInPktMulticast)-allOutPktMulticast,portLink-1;
	fprintf( stderr,"Statistics for all measured port switch (sum of items):\n");
	fprintf( stderr,"Packets                       IN                         OUT                    diff\n");
	fprintf( stderr,"Broadcast:  %20llu\t%20llu\t%20lld (%d*IN-OUT)\n",allInPktBroadcast,allOutPktBroadcast,allDiffBroadcast,portLink-1);
	fprintf( stderr,"Multicast:  %20llu\t%20llu\t%20lld (%d*IN-OUT)\n",allInPktMulticast,allOutPktMulticast,allDiffMulticast,portLink-1);
	fprintf( stderr,"Unicast:    %20llu\t%20llu\t%20lld (IN-OUT)\n",allInPktUnicast,allOutPktUnicast,allDiffUnicast);
	fprintf( stderr,"All:        %20llu\t%20llu\t%20lld\n",allInPktBroadcast+allInPktMulticast+allInPktUnicast, allOutPktBroadcast+allOutPktMulticast+allOutPktUnicast,allDiffUnicast+allDiffBroadcast+allDiffMulticast);
}


void printRmonCountersDiff(TABLE_RMON * data,TABLE_RMON * data_old, HISTOGRAM_MODE histogram, int portPtr, int number, int time)
{

	int ii;
	__u64 xa, xb;
	__u64 allInPktBroadcast=0;
	__u64 allInPktMulticast=0;
	__u64 allInPktUnicast=0;
	__u64 allOutPktBroadcast=0;
	__u64 allOutPktMulticast=0;
	__u64 allOutPktUnicast=0;
	__u64 allDiffMulticast=0;
	__u64 allDiffBroadcast=0;
	__u64 allDiffUnicast=0;
	int portLink=0;
	
	if (histogram >3)
		return;
	for (ii=0; ii<portPtr;ii++) {
		if (data[ii].portLink == UP)
			portLink++;
	}
	
	if (number > 1) {
		fprintf( stderr," ----------------------------------------------------------------------\n");
		fprintf( stderr,"Measurement (number/time)\t%d/%d(sec)\n",number,time);
	}
	fprintf(stderr,"%-20s","Port\t"); 
	for (ii=0; ii<portPtr;ii++) fprintf( stderr,"\t%20d",data[ii].port); 

	fprintf( stderr,"%-20s","\nInGoodOctets");
	for (ii=0; ii<portPtr;ii++) {
		xa=(__u64)data[ii].data_rmon->InGoodOctetsHi <<32|  data[ii].data_rmon->InGoodOctetsLo;
		xb=(__u64)data_old[ii].data_rmon->InGoodOctetsHi <<32|  data_old[ii].data_rmon->InGoodOctetsLo;
		if (xa < xb)
			fprintf(stderr,"\t%20llu", xa-xb + 0xffffffffffffffff);
		else 
			fprintf(stderr,"\t%20llu", xa-xb);
	}
	fprintf( stderr,"%-20s","\nInBadOctets");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].data_rmon->InBadOctets,data_old[ii].data_rmon->InBadOctets);
	fprintf( stderr,"%-20s","\nInUnicasts");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32SAVE(data[ii].data_rmon->InUnicasts,data_old[ii].data_rmon->InUnicasts,&allInPktUnicast); 
	fprintf( stderr,"%-20s","\nInPause");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].data_rmon->InPause,data_old[ii].data_rmon->InPause); 
	fprintf( stderr,"%-20s","\nInBroadcasts");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32SAVE(data[ii].data_rmon->InBroadcasts,data_old[ii].data_rmon->InBroadcasts,&allInPktBroadcast); 
	fprintf( stderr,"%-20s","\nInMulticasts");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32SAVE(data[ii].data_rmon->InMulticasts,data_old[ii].data_rmon->InMulticasts,&allInPktMulticast);
	fprintf( stderr,"%-20s","\nInMACRcvErr");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].data_rmon->InMACRcvErr,data_old[ii].data_rmon->InMACRcvErr); 
	fprintf( stderr,"%-20s","\nInFCSErr");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].data_rmon->InFCSErr,data_old[ii].data_rmon->InFCSErr);
	fprintf( stderr,"%-20s","\ninDiscradsFrame");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].discards,data_old[ii].discards);
	fprintf( stderr,"%-20s","\ninFiltered");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].inFiltered,data_old[ii].inFiltered);
    /* Histogram Counters : Rx Only, Tx Only, or both Rx and Tx */
	fprintf( stderr,"%-20s",octets64[histogram]);
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].data_rmon->Octets64,data_old[ii].data_rmon->Octets64); 
	fprintf( stderr,"%-20s",octets127[histogram]);
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].data_rmon->Octets127,data_old[ii].data_rmon->Octets127); 
	fprintf( stderr,"%-20s",octets255[histogram]);
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].data_rmon->Octets255,data_old[ii].data_rmon->Octets255); 
	fprintf( stderr,"%-20s",octets511[histogram]);
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].data_rmon->Octets511,data_old[ii].data_rmon->Octets511); 
	fprintf( stderr,"%-20s",octets1023[histogram]);
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].data_rmon->Octets1023,data_old[ii].data_rmon->Octets1023); 
	fprintf( stderr,"%-20s",octetsMax[histogram]);
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].data_rmon->OctetsMax,data_old[ii].data_rmon->OctetsMax); 
	fprintf( stderr,"%-20s","\nOutOctets");
	for (ii=0; ii<portPtr;ii++) {
		xa=(__u64)data[ii].data_rmon->OutOctetsHi <<32 | data[ii].data_rmon->OutOctetsLo;
		xb=(__u64)data_old[ii].data_rmon->OutOctetsHi <<32 | data_old[ii].data_rmon->OutOctetsLo;
		if (xa < xb)
			fprintf(stderr,"\t%20llu", xa-xb + 0xffffffffffffffff);
		else 
			fprintf(stderr,"\t%20llu", xa-xb);
	}
	fprintf( stderr,"%-20s","\nOutUnicasts");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32SAVE(data[ii].data_rmon->OutUnicasts,data_old[ii].data_rmon->OutUnicasts,&allOutPktUnicast);
	fprintf( stderr,"%-20s","\nOutMulticasts");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32SAVE(data[ii].data_rmon->OutMulticasts,data_old[ii].data_rmon->OutMulticasts,&allOutPktMulticast); 
	fprintf( stderr,"%-20s","\nOutBroadcasts");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32SAVE(data[ii].data_rmon->OutBroadcasts,data_old[ii].data_rmon->OutBroadcasts,&allOutPktBroadcast); 
	fprintf( stderr,"%-20s","\nOutPause");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].data_rmon->OutPause,data_old[ii].data_rmon->OutPause); 
	fprintf( stderr,"%-20s","\nOutFCSErr");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].data_rmon->OutFCSErr,data_old[ii].data_rmon->OutFCSErr); 
		fprintf( stderr,"%-20s","\noutFiltered");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].outFiltered,data_old[ii].outFiltered);
	fprintf( stderr,"%-20s","\nExcessive");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].data_rmon->Excessive,data_old[ii].data_rmon->Excessive); 
	fprintf( stderr,"%-20s","\nDeferred");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].data_rmon->Deferred,data_old[ii].data_rmon->Deferred); 
	fprintf( stderr,"%-20s","\nSingle  ");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].data_rmon->Single,data_old[ii].data_rmon->Single); 
	fprintf( stderr,"%-20s","\nMultiple");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].data_rmon->Multiple,data_old[ii].data_rmon->Multiple); 
	fprintf( stderr,"%-20s","\nUndersize");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].data_rmon->Undersize,data_old[ii].data_rmon->Undersize); 
	fprintf( stderr,"%-20s","\nFragments");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].data_rmon->Fragments,data_old[ii].data_rmon->Fragments); 
	fprintf( stderr,"%-20s","\nOversize");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].data_rmon->Oversize,data_old[ii].data_rmon->Oversize); 
	fprintf( stderr,"%-20s","\nJabber\t");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].data_rmon->Jabber,data_old[ii].data_rmon->Jabber); 
	fprintf( stderr,"%-20s","\nCollisions");
	for (ii=0; ii<portPtr;ii++) PRINTCNT32(data[ii].data_rmon->Collisions,data_old[ii].data_rmon->Collisions); 
	fprintf( stderr,"%-20s","\nLate\t");
	for (ii=0; ii<portPtr; ii++) PRINTCNT32(data[ii].data_rmon->Late,data_old[ii].data_rmon->Late);
	fprintf( stderr,"\n\n");
	allDiffUnicast=allInPktUnicast-allOutPktUnicast;
	allDiffBroadcast=((portLink-1)*allInPktBroadcast)-allOutPktBroadcast,portLink-1;
	allDiffMulticast=((portLink-1)*allInPktMulticast)-allOutPktMulticast,portLink-1;
	fprintf( stderr,"Statistics for all measured port switch (sum of items):\n");
	fprintf( stderr,"Packets                       IN                         OUT                    diff\n");
	fprintf( stderr,"Broadcast:  %20llu\t%20llu\t%20lld (%d*IN-OUT)\n",allInPktBroadcast,allOutPktBroadcast,allDiffBroadcast,portLink-1);
	fprintf( stderr,"Multicast:  %20llu\t%20llu\t%20lld (%d*IN-OUT)\n",allInPktMulticast,allOutPktMulticast,allDiffMulticast,portLink-1);
	fprintf( stderr,"Unicast:    %20llu\t%20llu\t%20lld (IN-OUT)\n",allInPktUnicast,allOutPktUnicast,allDiffUnicast);
	fprintf( stderr,"All:        %20llu\t%20llu\t%20lld\n",allInPktBroadcast+allInPktMulticast+allInPktUnicast,allOutPktBroadcast+allOutPktMulticast+allOutPktUnicast,allDiffMulticast+allDiffBroadcast+allDiffUnicast);
	fprintf( stderr,"\nPackets/sec                   IN                         OUT\n");
	fprintf( stderr,"Broadcast:  %20.3f\t%20.3f\n",(float)allInPktBroadcast/(time/number),(float)allOutPktBroadcast/(time/number));
	fprintf( stderr,"Multicast:  %20.3f\t%20.3f\n",(float)allInPktMulticast/(time/number),(float)allOutPktMulticast/(time/number));
	fprintf( stderr,"Unicast:    %20.3f\t%20.3f\n",(float)allInPktUnicast/(time/number),(float)allOutPktUnicast/(time/number));

	
}


/*--------------------------------------------------------------------*/
























