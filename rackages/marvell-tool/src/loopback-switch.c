/*

    prbs-switch: PRBS for Marvell 88E6390 (X) switch
*/



#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <time.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/sockios.h>
#ifndef __GLIBC__
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#endif
#include <linux/types.h>


#include "marvell-lib.h"
#include "marvell-lib-def.h"
#include "marvell-reg.h"




typedef enum
{
	LOOPBACK_NONE=0,
	LOOPBACK_MAC10,
	LOOPBACK_MAC100,
	LOOPBACK_MAC1000,
	LOOPBACK_LINE,
	LOOPBACK_HOST,
	LOOPBACK_CORE
} LOOPBACK;

typedef enum
{
	STATE_NONE=0,
	STATE_DISABLE,
	STATE_ENABLE
} STATE;

const char * ETHERNET={"eth1"};


	
extern int verbose;

extern __s8 allowedPage[];
extern __s8 allowedAddrAll[];
extern __s8 allowedAddrSerdes[];
extern __u16 allowedSerdesAll[];
/*--------------------------------------------------------------------*/



/*--------------------------------------------------------------------*/

struct option longopts[] = {
 /* { name  has_arg  *flag  val } */
    {"port",			1, NULL, 'p'},
    {"loopback",		1, NULL, 'l'},
	{"status",			0, NULL, 't'},
	{"set",				1, NULL, 's'},
    {"verbose",			1, NULL, 'v'},
    {"help", 			0, NULL, 'h'},	
    { 0, 0, NULL, 0 }
};


//Loopback status
static int loopbackStatus (int fd, __u8 port)
{
	int ret;
	__u16 data;
	__u16 data1;
	
	if (port > 8) {
		if ((ret=serdesRegRead (fd, port, 0x2000, & data)) < 0 )
			return ret;
		if ((ret=serdesRegRead (fd, port, 0xf066, & data1)) < 0 )
			return ret;
		printf ("Port%d (SERDES) Loopback:",port);
		if ((data >> 14) & 0x1)
			printf (" CORE");
		if (data1 & 0x1)
			printf (" HOST");
		if ((!((data >> 14) & 0x1)) && (!(data1 & 0x1)))
			printf (" Disabled");
		printf ("\n");
	} else {
		if ((ret=regPhyPageRead (fd, port , 21, 2 , &data)) < 0)
			return ret;
		if ((ret=regPhyRead (fd, port , 0, &data1)) < 0)
			return ret;
		printf ("Port%d (internal PHY) Loopback:",port);
		if ((data >> 14) & 0x1)
			printf (" LINE");
		if ((data1 >> 14) & 0x1) {
			printf (" MAC");
			switch (data & 0x7) {
				case 4:
					printf ("(10Mbps)");
					break;
				case 5:
					printf ("(100Mbps)");
					break;
				case 6:
					printf ("(1000Mbps)");
					break;
				default:
					break;
			}
		}
		if ((!((data >> 14) & 0x1)) && (!((data1 >> 14) & 0x1)))
			printf (" Disabled");
		printf ("\n");
		
	}
	return ret;
}

//SERDES Loopback set
static int loopbackSerdesSet (int fd, __u8 port, LOOPBACK type, STATE state)
{

	int ret;
	if ((state == STATE_NONE) || (type == LOOPBACK_NONE))
		return -EINVAL;
	switch (type) {
		case LOOPBACK_CORE:
			if (state== STATE_DISABLE)
				ret=serdesRegWriteMask (fd, port, 0x2000 ,0x0000, 0x4000);
			else
				ret=serdesRegWriteMask (fd, port, 0x2000 ,0x4000, 0x4000);
			break;
			
		case LOOPBACK_HOST:
			if (state == STATE_DISABLE)
				ret=serdesRegWriteMask (fd, port, 0xf066 ,0x0000, 0x0001);
			else
				ret=serdesRegWriteMask (fd, port, 0xf066 ,0x0001, 0x0001);
			break;
		default:
			break;
	}
	return ret;
}

//PHY Loopback set
static int loopbackPhySet (int fd, __u8 port, LOOPBACK type, STATE state)
{

	int ret;
	if ((state == STATE_NONE) || (type == LOOPBACK_NONE))
		return -EINVAL;
	switch (state) {
		case STATE_DISABLE:
			if (type == LOOPBACK_LINE)
				ret=regPhyPageWriteMask (fd, port , 21, 2 , 0x0000 ,0x4000);
			else if ((type == LOOPBACK_MAC10) || ( type == LOOPBACK_MAC100) || (type = LOOPBACK_MAC1000))  
				ret=regPhyWriteMask (fd, port , 0, 0x0000 ,0x4000);
			else 
				return -EINVAL;
			break;
		case STATE_ENABLE:
			switch (type) {
				case LOOPBACK_LINE:
					ret=regPhyPageWriteMask (fd, port , 21, 2 , 0x4000 ,0x4000);
					break;
				case LOOPBACK_MAC10:
					if ((ret=regPhyWriteMask (fd, port , 0, 0x4000 ,0x4000)) < 0 )
						return ret;
					ret=regPhyPageWriteMask (fd, port , 21, 2 , 0x0004 ,0x0007);
					break;
				case LOOPBACK_MAC100:
					if ((ret=regPhyWriteMask (fd, port , 0, 0x4000 ,0x4000)) < 0 )
						return ret;
					ret=regPhyPageWriteMask (fd, port , 21, 2 , 0x0005 ,0x0007);
					break;
				case LOOPBACK_MAC1000:
					if ((ret=regPhyWriteMask (fd, port , 0, 0x4000 ,0x4000)) < 0 )
						return ret;
					ret=regPhyPageWriteMask (fd, port , 21, 2 , 0x0006 ,0x0007);
					break;
				default:
					return -EINVAL;
			}
			break;
				
		default:
			break;
	}
	
	return ret;
}
/*--------------------------------------------------------------------*/





/*--------------------------------------------------------------------*/
const char *usage =
"usage: loopback-switch [-vhs][-p port] [-l loopback ] [-s set]\n"
"	-p  --port			port \n"
"		param:  port 		number of port (1..10)\n"
"					internal PHY (port 1..8)\n"
"					serdes (port 9 or 10)\n"
"	-l  --loopback			loopback type \n"
"		param:  MAC10		loopback for the PHY port (from MAC to MAC, speed 10Mbps)\n"
"			MAC100		loopback for the PHY port (from MAC to MAC, speed 100Mbps)\n"
"			MAC1000		loopback for the PHY port (from MAC to MAC, speed 1000Mbps)\n"
"			LINE		loopback for the PHY (from line to line)\n"
"			HOST		loopback for the SERDES (from RxP to TxP) port\n"
"			CORE		loopback for the SERDES (from Core to Core)\n"
"	-s  --set			loopback enable or disable \n"
"		param: ENABLE | DISABLE\n"
"	-t  --status			loopback status\n"
"	-v  --verbose			verbose \n"
"		param:	0 | 1 |2		\n"
"	-h  --help			help\n";

int main(int argc, char **argv)
{
	int fd =-1;
	int c;
	int ret=-1;
	
	__u8 port=0;
	
	LOOPBACK loopback=LOOPBACK_NONE;
	STATE state =STATE_NONE;
	int status = 0;

	
	if (argc == 1) {
		printf ("%s",usage);
		return 0;
	}

	while ((c = getopt_long(argc, argv, "htv:l:s:p:", longopts, 0)) != EOF) {
		switch (c) {
			case 'p':
				port=atoi(optarg);
				if ((port < 1 ) || (port >10)) {
					fprintf (stderr,"Invalid port number, (valid 1.. 10)\n");
					printf ("%s",usage);
					exit (-1);
				}
				break;
			case 't':
				status =1;
				break;
			case 'l': 
				if (strcmp(optarg,"MAC10") == 0)
					loopback=LOOPBACK_MAC10;
				else if (strcmp(optarg,"MAC100") == 0)
					loopback=LOOPBACK_MAC100;
				else if (strcmp(optarg,"MAC1000") == 0)
					loopback=LOOPBACK_MAC1000;
				else if (strcmp(optarg,"LINE") == 0)
					loopback=LOOPBACK_LINE;
				else if (strcmp(optarg,"HOST") == 0)
					loopback=LOOPBACK_HOST;
				else if (strcmp(optarg,"CORE") == 0)
					loopback=LOOPBACK_CORE;
				
				else {
					printf ("%s",usage);
					exit (-1);
				}
				break;	
			case 's': 
				if (strcmp(optarg,"DISABLE") == 0)
					state=STATE_DISABLE;
				else if (strcmp(optarg,"ENABLE") == 0)
					state=STATE_ENABLE;
				else {
					printf ("%s",usage);
					exit (-1);
				}
				break;	
			case 'v': 
				verbose=atoi(optarg);
				if ((verbose <0) || (verbose > 2)) {
					printf ("%s",usage);
					exit (-1);
				}
				break;	
			case 'h': 
				printf ("%s",usage);
				return 0;
				break;
			
		}
	}
	if ((port < 1 ) || (port > 10 )) {
		printf ("%s",usage);
		exit (-1);
	}
		
	if ((fd = mvOpen(ETHERNET)) < 0) {
		exit(-1);
	}
	if ((loopback != LOOPBACK_NONE) && (state != STATE_NONE)) {
		if (port < 9) {
			if ((ret=loopbackPhySet (fd, port, loopback,state)) == 0)
				printf ("OK\n");
		} else {
			if ((ret=loopbackSerdesSet (fd, port, loopback, state)) == 0)
				printf ("OK\n");
		}
	}
	if (status !=0)
		ret=loopbackStatus (fd,port);
	if (ret < 0)
		printf ("%s",usage);
	exit (ret);
}
