/*

    marvell-init: init for Marvell 88E6390 (X) switch
*/



#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <time.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/sockios.h>
#ifndef __GLIBC__
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#endif
#include <linux/types.h>


#include "marvell-lib.h"
#include "marvell-lib-def.h"
#include "marvell-reg.h"
#include "marvell-lib-debug.h"

#define SYSFS_GPIO_DIR 	"/sys/class/gpio"
#define WORKAROUND		0
#define RESET			1

#ifdef RR_PRODUCT_bma
	#define SWITCH_RESET 	506		//GPIO pin
	#define PORT_CPU		0
	#define PORT_ETH 		1
	#define PORT_SFP		10
	#define PORT_AIR		9
#else
	#define SWITCH_RESET		507	//GPIO pin
	#define PORT_CPU		9
	#define PORT_SFP		10
#endif	

const char * ETHERNET={"eth1"};

extern int verbose;


PAUSE_MODE flowControl = BOTH_PAUSE;
/*--------------------------------------------------------------------*/

struct option longopts[] = {
 /* { name  has_arg  *flag  val } */
    {"verbose",		1, NULL, 'v'},
    {"help", 		0, NULL, 'h'},	
	{"workaround", 		0, NULL, 'w'},
	{"reset", 		0, NULL, 'r'},
    { 0, 0, NULL, 0 }
};

static const char *probe(int fd, int addr)
{
	int ret;
	__u16 value;
	
	if ((ret=mvRegRead (fd, addr, 0x03, &value)) < 0)
		return NULL;
	if ((value & 0xfff0) == 0x3900)
		return "Marvell 88E6390";
	if ((value & 0xfff0) == 0x0a10)
		return "Marvell 88E6390X";
	return NULL;
}

static int workaround (int fd)
{
	int ret;
	INFO ("Apply the workaround\n");
	//workaround 3.3
	if ((ret= mvRegWrite (fd, 5, 0x1A ,0x01C0)) < 0)
		return ret;
	if ((ret= mvRegWrite (fd, 4, 0x1A ,0xFC00)) < 0)
		return ret;
	if ((ret= mvRegWrite (fd, 4, 0x1A ,0xFC20)) < 0)
		return ret;
	if ((ret= mvRegWrite (fd, 4, 0x1A ,0xFC40)) < 0)
		return ret;
	if ((ret= mvRegWrite (fd, 4, 0x1A ,0xFC60)) < 0)
		return ret;
	if ((ret= mvRegWrite (fd, 4, 0x1A ,0xFC80)) < 0)
		return ret;
	if ((ret= mvRegWrite (fd, 4, 0x1A ,0xFCA0)) < 0)
		return ret;
	if ((ret= mvRegWrite (fd, 4, 0x1A ,0xFCC0)) < 0)
		return ret;
	if ((ret= mvRegWrite (fd, 4, 0x1A ,0xFCE0)) < 0)
		return ret;
	if ((ret= mvRegWrite (fd, 4, 0x1A ,0xFD00)) < 0)
			return ret;
	//vworkaround 3.2
	//port 9
	if ((ret= mvRegWrite (fd, 5, 0x1A ,0x1C00)) < 0)
		return ret;
	if ((ret= mvRegWrite (fd, 4, 0x1A ,0xFD20)) < 0)
		return ret;
	//port 10
	if ((ret= mvRegWrite (fd, 5, 0x1A ,0x1C00)) < 0)
		return ret;
	if ((ret= mvRegWrite (fd, 4, 0x1A ,0xFD40)) < 0)
		return ret;
	//reser switch
	if ((ret= mvRegWriteMask (fd, 0x1b, 0x4 ,0x8000,0x8000)) < 0)
		return ret;
	//workaroud C-mode
	if ((ret= mvRegWrite (fd, 5, 0x1A ,0x0030)) < 0)
		return ret;
	if ((ret= mvRegWrite (fd, 4, 0x1A ,0xFD20)) < 0)
		return ret;
	if ((ret= mvRegWrite (fd, 4, 0x1A ,0xFD40)) < 0)
		return ret;
	if ((ret= mvRegWrite (fd, 5, 0x1A ,0x0000)) < 0)
		return ret;
	if ((ret= mvRegWrite (fd, 4, 0x1A ,0xFD20)) < 0)
		return ret;
	if ((ret= mvRegWrite (fd, 4, 0x1A ,0xFD40)) < 0)
		return ret;

	return 0;
}



static int gpioSetValue(unsigned int gpio, int value)
{
	int fd;
	char buf[64];

	if (value > 1)
		return -EINVAL;
	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);
	fd=open(buf, O_WRONLY);
	if (fd < 0) {
		fprintf(stderr,"Failed to set GPIO value: %s.\n",strerror (errno));
		return fd;
	}
	if (value == 1)
		write(fd, "1", 2);
	if  (value == 0)
		write(fd, "0", 2);
 
	close(fd);
	return 0;
}

static int resetSwitch (void)
{

	INFO ("Perform HW reset\n");
	gpioSetValue(SWITCH_RESET, 1);
	usleep(20000);
	gpioSetValue(SWITCH_RESET, 0);
	usleep(20000);
	return 0;
}

#ifdef RR_PRODUCT_bk2
static int resetExternalPhy (void)
{

	INFO ("External PHY reset\n");
	gpioSetValue(505, 1);
	usleep(20000);
	gpioSetValue(505, 0);
	usleep(20000);
	return 0;
}


static int init_ext_phy (int fd)
{
	int port = 0;
	int ret;
	
	resetExternalPhy ();
	//set MODE 0 (RGMII to copper)
	if ((ret=mvPhyRegPageWrite(fd,0,20,18,0)) < 0) {
		printf("Error: Failed change MODE the ext PHY\n");
		return ret;
	}
	// sw reset (aplly change mode)
	if ((ret=mvPhyRegPageWrite(fd,0,20,18,0x8000)) < 0) {
		printf("Error: Failed soft-reset the ext PHY\n");
		return ret;
	}
	if ((ret=mvPhyRegPageWrite(fd,0,20,18,0x8000)) < 0) {
		printf("Error: Failed soft-reset the ext PHY\n");
		return ret;
	}
	if ((ret=regPhyWrite(fd,port,PHY_CONTROL_1000_REG,0xf00)) < 0) {
		printf("Error: Failed set AutoNego 1000 on port %d\n",port);
		return ret;
	}
	return ret;
}
#endif

static int init_phy (int fd)
{
	int port = 1;
	int ret;
	
#if (defined  RR_PRODUCT_bk1) || (defined RR_PRODUCT_bk2)
	for (port =1; port < 9; port++) {
#endif
		//PHY power up
		if  ((ret=mvPhyPowerDown (fd, port, DISABLED)) < 0) {
			printf("Error: Failed to power down on port %d\n",port);
			return ret;
		}
		// nastav adver - off
		if ((ret=   mvPhyAutoNegEnable (fd, port, DISABLED)) < 0) {
			printf("Error: Failed set PHY autono disable on port %d\n",port);
			return ret;
		}
		//nastav config PHY - Advertisement 1000 |100|10|Pause |Asymmetric
		if ((ret=mvSetPhyAutoNego(fd,port,SPEED_AUTO_DUPLEX_AUTO)) < 0) {
			printf("Error: Failed set AutoNego on port %d\n",port);
			return ret;
		}
		if ((ret= mvSetPhyPause( fd, port, NO_PAUSE)) < 0) {
			printf("Error: Failed set AutoNego on port %d\n",port);
			return ret;
		}
		//restart aneg 
		if ((ret=mvPhyRestartAutoNeg (fd, port)) < 0) {
			printf("Error: Failed set PHY detect disable on port %d\n",port);
			return ret;
		}
		// set duplex full speed 1000
		if ((ret=mvSetPhySpeed (fd ,port ,SPEED_1000)) < 0) {
			printf("Error: Failed set speed on port %d\n",port);
			return ret;
		}
		if ((ret=mvSetPhyDuplexMode ( fd,port, DUPLEX_FULL)) < 0) {
			printf("Error: Failed set duplex on port %d\n",port);
			return ret;
		}
		// enable PHY detect
		if ((ret= mvSetPhyDetect (fd, port, ENABLED)) < 0) {
			printf("Error: Failed set PHY detect disable on port %d\n",port);
			return ret;
		}
#if (defined RR_PRODUCT_bk1)|| (defined RR_PRODUCT_bk2)
	}
#endif
	return 0;
}



static int init_port (int fd)
{
	int port;
	int ret;
	int smode;
	if ((smode=mvCheckPxSmodes (fd)) <  0 ){
		printf("Error: Failed read Px_SMODE\n");
		return smode;
	}

	for (port =0; port < 11; port++) {	
#ifdef RR_PRODUCT_bma
		if ((port == PORT_CPU) || (port == PORT_AIR) || (port == PORT_SFP)) {
			if ((ret= mvSetForceDuplex (fd, port, FORCE_DUPLEX_FULL)) < 0) {
				printf("Error: Failed set forced full duplex on port %d\n",port);
				return ret;
			}
			
		}
		if (port == PORT_ETH) {
			// max jumbo packet 10240
			if ((ret= mvSetJumboMode (fd, port,  JUMBO_MODE_10240)) < 0) {
				printf("Error: Failed set jumbo mode on port %d\n",port);
				return ret;
			}
			//enble port 
			if ((ret= mvSetPortState (fd, port,PORT_STATE_FORWARDING)) < 0) {
				printf("Error: Failed set port forwarding %d\n",port);
				return ret;
			}
		}
		if (port == PORT_SFP) {
			
			// max jumbo packet 10240
			if ((ret= mvSetJumboMode (fd, port,  JUMBO_MODE_10240)) < 0) {
				printf("Error: Failed set jumbo mode on port %d\n",port);
				return ret;
			}
			// Power up
			if ((ret= serdesRegWriteMask (fd, port, SGMII_CONTROL_REG, 0, SGMII_CONTROL_POWER)) < 0) {
				printf("Error: Failed set power up on port %d\n",port);
				return ret;
			}
			//check P10_SMODE
			if ((smode >>1) &0x1) {
			// set cmode 1000Base-X 
				if ((ret= mvRegWriteMask (fd, port, 0x0,0x0009,0x100f)) < 0) {
					printf("Error: Failed set cmode on port %d\n",port);
					return ret;
				}
				if ((ret= mvSetForceSpeed (fd, port, FORCE_SPEED_1000)) < 0) {
					printf("Error: Failed set forced speed 1000 on port %d\n",port);
					return ret;
				}
			}else {
				//detect 2500Base-X
				// set cmode 2500Base-x
				if ((ret= mvRegWriteMask (fd, port, 0x0,0x000b,0x100f)) < 0) {
					printf("Error: Failed set cmode on port %d\n",port);
					return ret;
				}
				//nastv AltSpeed pro 2500M
				if ((ret= mvRegWriteMask (fd, port, 0x1,0x1000,0x1000)) < 0) {
					printf("Error: Failed set AltSpeed on port %d\n",port);
					return ret;
				}
				// nastav force speed na 2500M
				if ((ret= mvSetForceSpeed (fd, port, FORCE_SPEED_2500)) < 0) {
					printf("Error: Failed set forced speed 2500 on port %d\n",port);
					return ret;
				}
			}
			//enble port 
			if ((ret= mvSetPortState (fd, port,PORT_STATE_FORWARDING)) < 0) {
				printf("Error: Failed set port forwarding %d\n",port);
				return ret;
			}
			//port force UP
			if ((ret= mvSetForceLink (fd, port, FORCE_LINK_UP)) < 0) {
				printf("Error: Failed set forced link up on port %d\n",port);
				return ret;
			}
		}

		if (port == PORT_AIR) {
			// max jumbo packet 10240
			if ((ret= mvSetJumboMode (fd, port,  JUMBO_MODE_10240)) < 0) {
				printf("Error: Failed set jumbo mode on port %d\n",port);
				return ret;
			}
			// Power up
			if ((ret= serdesRegWriteMask (fd, port, SGMII_CONTROL_REG, 0, SGMII_CONTROL_POWER)) < 0) {
				printf("Error: Failed set power up on port %d\n",port);
				return ret;
			}
			//check P9_SMODE
			if (smode &0x1) {
				//detect 1000Base-X or SGMII
				// set cmode 1000Base-X 
				if ((ret= mvRegWriteMask (fd, port, 0x0,0x0009,0x100f)) < 0) {
					printf("Error: Failed set cmode on port %d\n",port);
					return ret;
				}
				if ((ret= mvSetForceSpeed (fd, port, FORCE_SPEED_1000)) < 0) {
					printf("Error: Failed set forced speed 1000 on port %d\n",port);
					return ret;
				}
			} else {
				//detect 2500Base-X
				// set cmode 2500Base-x
				if ((ret= mvRegWriteMask (fd, port, 0x0,0x000b,0x100f)) < 0) {
					printf("Error: Failed set cmode on port %d\n",port);
					return ret;
				}
				//nastv AltSpeed pro 2500M
				if ((ret= mvRegWriteMask (fd, port, 0x1,0x1000,0x1000)) < 0) {
					printf("Error: Failed set AltSpeed on port %d\n",port);
					return ret;
				}
				// nastav force speed na 2500M
				if ((ret= mvSetForceSpeed (fd, port, FORCE_SPEED_2500)) < 0) {
					printf("Error: Failed set forced speed 2500 on port %d\n",port);
					return ret;
				}
			}
			//enble port 
			if ((ret= mvSetPortState (fd, port,PORT_STATE_FORWARDING)) < 0) {
				printf("Error: Failed set port forwarding %d\n",port);
				return ret;
			}
			//port force UP
			if ((ret= mvSetForceLink (fd, port, FORCE_LINK_UP)) < 0) {
				printf("Error: Failed set forced link up on port %d\n",port);
				return ret;
			}
		}
		if (port == PORT_CPU) {
			//max jumbo packet 1522
			if ((ret= mvSetJumboMode (fd, port,  JUMBO_MODE_1522)) < 0) {
				printf("Error: Failed set jumbo mode on port %d\n",port);
				return ret;
			}
			if ((ret= mvSetForceSpeed (fd, port, FORCE_SPEED_1000)) < 0) {
				printf("Error: Failed set forced speed 1000 on port %d\n",port);
				return ret;
			}
			// set RGMII Rx (Tx) Timing
			if ((ret= mvRegWriteMask (fd, port, 0x1,0xc000,0xc000)) < 0) {
				printf("Error: Failed set RGMII Timing on port %d\n",port);
				return ret;
			}
			//enble port 
			if ((ret= mvSetPortState (fd, port,PORT_STATE_FORWARDING)) < 0) {
				printf("Error: Failed set port forwarding %d\n",port);
				return ret;
			}
			//port force UP
			if ((ret= mvSetForceLink (fd, port, FORCE_LINK_UP)) < 0) {
				printf("Error: Failed set forced link up on port %d\n",port);
				return ret;
			}
		}
		
#else
		if (port != PORT_CPU) { 
		// max jumbo packet 10240
			if ((ret= mvSetJumboMode (fd, port,  JUMBO_MODE_10240)) < 0) {
				printf("Error: Failed set jumbo mode on port %d\n",port);
				return ret;
			}
		}
#ifdef RR_PRODUCT_bk2
		if (port == 0) {
				// PHY DETECT 
				if ((ret= mvRegWriteMask (fd, port, 0x0,0x1000,0x1000)) < 0) {
					printf("Error: Failed set PHY_DETECT on port %d\n",port);
					return ret;
				}
		}
#endif
		if (port > 8) {
			// Power up
			if ((ret= serdesRegWriteMask (fd, port, SGMII_CONTROL_REG, 0, SGMII_CONTROL_POWER)) < 0) {
				printf("Error: Failed set power up on port %d\n",port);
				return ret;
			}
		}
		if (port == PORT_SFP) {
			// max jumbo packet 10240
			if ((ret= mvSetJumboMode (fd, port,  JUMBO_MODE_10240)) < 0) {
				printf("Error: Failed set jumbo mode on port %d\n",port);
				return ret;
			}
			// Power up
			if ((ret= serdesRegWriteMask (fd, port, SGMII_CONTROL_REG, 0, SGMII_CONTROL_POWER)) < 0) {
				printf("Error: Failed set power up on port %d\n",port);
				return ret;
			}
			//check P10_SMODE
			if ((smode >>1) &0x1) {
			// set cmode 1000Base-X 
				if ((ret= mvRegWriteMask (fd, port, 0x0,0x0009,0x100f)) < 0) {
					printf("Error: Failed set cmode on port %d\n",port);
					return ret;
				}
				if ((ret= mvSetForceSpeed (fd, port, FORCE_SPEED_1000)) < 0) {
					printf("Error: Failed set forced speed 1000 on port %d\n",port);
					return ret;
				}
			}else {
				//detect 2500Base-X
				// set cmode 2500Base-x
				if ((ret= mvRegWriteMask (fd, port, 0x0,0x000b,0x100f)) < 0) {
					printf("Error: Failed set cmode on port %d\n",port);
					return ret;
				}
				//nastv AltSpeed pro 2500M
				if ((ret= mvRegWriteMask (fd, port, 0x1,0x1000,0x1000)) < 0) {
					printf("Error: Failed set AltSpeed on port %d\n",port);
					return ret;
				}
				// nastav force speed na 2500M
				if ((ret= mvSetForceSpeed (fd, port, FORCE_SPEED_2500)) < 0) {
					printf("Error: Failed set forced speed 2500 on port %d\n",port);
					return ret;
				}
			}
			//port force UP
			if ((ret= mvSetForceLink (fd, port, FORCE_LINK_UP)) < 0) {
				printf("Error: Failed set forced link up on port %d\n",port);
				return ret;
			}
			
	
		}
		if (port == PORT_CPU) {
			// Change polarity
			if ((ret= serdesRegWriteMask (fd, port, 0xf076,0x4 , 0x4)) < 0) {
				printf("Error: Failed change polarity Rx L0 on port %d\n",port);
				return ret;
			}
			
			//check P9_SMODE
			if (smode &0x1) {
				//detect 1000Base-X or SGMII
				// set cmode 1000Base-X 
				if ((ret= mvRegWriteMask (fd, port, 0x0,0x0009,0x100f)) < 0) {
					printf("Error: Failed set cmode on port %d\n",port);
					return ret;
				}
				if ((ret= mvSetForceSpeed (fd, port, FORCE_SPEED_1000)) < 0) {
					printf("Error: Failed set forced speed 1000 on port %d\n",port);
					return ret;
				}
			} else {
				//detect 2500Base-X
				// set cmode 2500Base-x
				if ((ret= mvRegWriteMask (fd, port, 0x0,0x000b,0x100f)) < 0) {
					printf("Error: Failed set cmode on port %d\n",port);
					return ret;
				}
				//nastv AltSpeed pro 2500M
				if ((ret= mvRegWriteMask (fd, port, 0x1,0x1000,0x1000)) < 0) {
					printf("Error: Failed set AltSpeed on port %d\n",port);
					return ret;
				}
				// nastav force speed na 2500M
				if ((ret= mvSetForceSpeed (fd, port, FORCE_SPEED_2500)) < 0) {
					printf("Error: Failed set forced speed 2500 on port %d\n",port);
					return ret;
				}
			}
			
			//max jumbo packet 1522
			if ((ret= mvSetJumboMode (fd, port,  JUMBO_MODE_1522)) < 0) {
				printf("Error: Failed set jumbo mode on port %d\n",port);
				return ret;
			}
			//port force UP
			if ((ret= mvSetForceLink (fd, port, FORCE_LINK_UP)) < 0) {
				printf("Error: Failed set forced link up on port %d\n",port);
				return ret;
			}
			if ((ret= mvSetForceDuplex (fd, port, FORCE_DUPLEX_FULL)) < 0) {
				printf("Error: Failed set forced full duplex on port %d\n",port);
				return ret;
			}
		}
		//enble port 
		if ((ret= mvSetPortState (fd, port,PORT_STATE_FORWARDING)) < 0) {
			printf("Error: Failed set port forwarding %d\n",port);
			return ret;
		}
#endif

	}
	return 0;
}


static int switch_default(int fd, int par)
{

	int ret;
	//reset, workaround
	const char* txt;
	if ((par >> RESET) && 0x1)
		resetSwitch();
	if ((txt=probe(fd, PORT_CPU)) == NULL) {
		printf ("Error: could not detect attached switch\n");
		return -1;
	} else {
		printf ("Detect switch: %s\n",txt);
	}
	if ((par >> WORKAROUND) && 0x1) {
		if (workaround (fd) < 0 )
			printf("Error: workaround\n");
	}
	if  ((ret=mvSetCpuPort (fd, PORT_CPU)) < 0) {
		printf("Error: Failed set CPU port %d\n",PORT_CPU);
		return ret;
	}
#ifdef RR_PRODUCT_bk2
	// enable SMI for external PHY (P0) 
	if ((ret= mvRegWrite (fd, 0x1c, 0x1a,0x8200)) < 0) {
		printf("Error: Failed set Normal SMI\n");
		return ret;
	}
#endif
	//default phy
	if ((ret=init_phy (fd)) < 0 )
		return ret;
	//port	
	if ((ret=init_port (fd)) < 0 )
		return ret;
#ifdef RR_PRODUCT_bk2
	//ext PHY	
	if ((ret=init_ext_phy (fd)) < 0 )
		return ret;
#endif
	return 0;
}


/*--------------------------------------------------------------------*/
const char *usage =
"usage: marvell-init -v -h -s\n"
"	-w  --workaround				use workaround\n"
"	-r  --reset						use HW reset\n"
"	-v  --verbose					verbose \n"
"		param:	0 | 1 |2			INFO/DEBUG/ALL	\n"
"	-h  --help						help\n";

int main(int argc, char **argv)
{
	int fd;
	int c;
	int par=0;
	int ret=0;
	
	while ((c = getopt_long(argc, argv, "wrhv:", longopts, 0)) != EOF) {
		switch (c) {
			case 'v': 
				verbose=atoi(optarg);
				if ((verbose <0) || (verbose > 2)) {
					printf ("%s",usage);
					exit (-1);
				}
				break;	
			case 'h': 
				printf ("%s",usage);
				return 0;
				break;
			case 'r': 
				par|=1<<RESET;
				break;
			case 'w': 
				par|=1<<WORKAROUND;
				break;
		}
	}

	if ((fd = mvOpen(ETHERNET)) < 0) {
		exit(-1);
	}
	if (switch_default(fd, par) < 0) {
		ret =-1;
		printf ("The default setting switches - Error\n");
		close (fd);
		exit (ret);
	} else {
	    printf ("The default setting switches - OK\n");
	}
	if (set1ppsOut (fd) < 0) {
		ret=-1;
		printf("Settings 1PPS output - Error\n");
	} else {
		printf ("Settings 1PPS output - OK\n");
	}
	if (setSynceOut (fd) < 0) {
		ret=-1;
		printf ("Settings SyncE output - Error\n");
	} else {
		printf ("Settings SyncE output - OK\n");
	}
	close (fd);
	exit (ret);
}
