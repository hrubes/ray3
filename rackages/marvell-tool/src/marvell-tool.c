/*

    marvell-tool: monitor and control for Marvell 88E6390 (X) switch
*/



#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <time.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/sockios.h>
#ifndef __GLIBC__
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#endif
#include <linux/types.h>


#include "marvell-lib.h"
#include "marvell-lib-def.h"
#include "marvell-reg.h"
#include "marvell-lib-rmon.h"
#include "marvell-print.h"



#define GET_ALL		1
#define GET_GLOBAL	2
#define GET_PHY		3
#define GET_PORT	4
#define GET_SERDES	5

#define PAGE_NO 	0
#define PAGE_YES	1

//definice RMON
#define FLUSHALL	1
#define FLUSHPORT	2
#define COUNTERS_RX	3
#define COUNTERS_TX	4
#define COUNTERS_BOTH	5
#define COUNTERS	6

const char * ETHERNET={"eth1"};

struct rmon_ {
	int rmon_reg;
	char* rmon_name;
	int type;
};

extern int verbose;

extern __s8 allowedPage[];
#ifdef RR_PRODUCT_bk2
extern __s8 allowedPageP0[];
#endif
extern __s8 allowedAddrAll[];
extern __s8 allowedAddrSerdes[];
extern __u16 allowedSerdesAll[];
/*--------------------------------------------------------------------*/


#ifdef RR_PRODUCT_bk1
	#define SYSFS_GPIO_DIR		"/sys/class/gpio"
#endif


#ifdef RR_PRODUCT_bma
	#define SYSFS_GPIO_DIR		"/sys/class/gpio"
	#define SWITCH_P10_2500		511
#endif

#ifdef RR_PRODUCT_bk2
	#define SYSFS_GPIO_DIR		"/sys/class/gpio"
	#define SWITCH_P10_2500		511
	#define SWITCH_P9_2500		510
#endif

static int gpioSetValue(int gpio,int value, int port, int smode)
{
	int fd;
	char buf[64];
  //      int gpio=SWITCH_P10_2500;
	if (value > 1)
		return -EINVAL;
		if ((port < 9 ) || (port > 10))
			return -EINVAL;
		if (port == 9) {
			if ((value & 01) == (smode & 0x1))
				return 0;
		}
		if (port == 10) {
			if ((value & 01) == ((smode>>1) & 0x1))
				return 0;
		}
	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);
	fd=open(buf, O_WRONLY);
	if (fd < 0) {
		fprintf(stderr,"Failed to set GPIO value: %s.\n",strerror (errno));
		return fd;
	}
	if (value == 1)
		write(fd, "1", 2);
	if  (value == 0)
		write(fd, "0", 2);
	close(fd);
		printf ("Warning: The switch will be reset in the next step\n");
		system ("sync");
		sleep (1);
		system ("/sbin/marvell-init -r -w > /dev/null");
        return 0;
}


/*--------------------------------------------------------------------*/

struct option longopts[] = {
 /* { name  has_arg  *flag  val } */
    {"set",		1, NULL, 's'},	
    {"page",		0, NULL, 'e'},
    {"get",		1, NULL, 'g'},
    {"getall",		1, NULL, 'G'},
    {"status",		1, NULL, 't'},	
    {"port",		1, NULL, 'p'},
    {"rmon",		1, NULL, 'r'},
    {"verbose",		1, NULL, 'v'},
    {"measurements",	1, NULL, 'm'},
    {"data_user",	1, NULL, 'u'},
    {"PHY_register",	0, NULL, 'y'},
    {"diff",		0, NULL, 'd'},
	{"sfp",		1, NULL, 'f'},
    {"help", 		0, NULL, 'h'},	
    { 0, 0, NULL, 0 }
};


struct cmdData_ {
	int data1;
	int data2;
	int data3;
	int data4;
	int data5;
	int num;
};



// precte vsechny registry na dane adrese 
static int readAllReg (int fd, __u8 addr, char* title)
{
	static __u16 regs[32];
	int ret;
	if ((ret=mvAllRegRead (fd, addr,0, regs, 32))< 0)
		return ret;
	printf("\n%s",title);
	printAllReg (addr, regs,0, 32);
	return 0;
}

static int readAllPhyReg (int fd, __u8 addr, char* title)
{
	static __u16 regs[32];
	int ret;
	if ((ret=mvPhyAllRegRead (fd, addr,0, regs, 32))< 0)
		return ret;
	printf("\n%s",title);
	printAllReg (addr, regs,0, 32);
	return 0;
}

/*--------------------------------------------------------------------*/

// precte vsechny page na danem PHY 
static int readAllPhyPageReg (int fd, __u8 addr, char* title)
{
	unsigned int j;
	static __u16 regs[32];
	int  ret;
	
	printf("\n%s",title);
#ifdef RR_PRODUCT_bk2 
	if (addr == 0) {
		for (j=0; allowedPageP0[j]!=-1; j++) {
			if ((ret=mvPhyRegWrite (fd, addr, 22 , allowedPageP0[j])) < 0)
				return ret;
			if ((ret=mvPhyAllRegRead (fd, addr,0, regs, 32)) < 0)
				return ret;
			printAllPageReg (addr, allowedPageP0[j], regs,0, 32);
		}
		mvPhyRegWrite(fd,addr, 22, 0 );
	} else {
#endif
	for (j=0; allowedPage[j]!=-1; j++) {
		if ((ret=mvPhyRegWrite (fd, addr, 22 , allowedPage[j])) < 0)
			return ret;
		if ((ret=mvPhyAllRegRead (fd, addr,0, regs, 32)) < 0)
			return ret;
		printAllPageReg (addr, allowedPage[j], regs,0, 32);
	}
#ifdef RR_PRODUCT_bk2
	}
#endif
	return (mvPhyRegWrite(fd,addr, 22, 0 ));

}
/*--------------------------------------------------------------------*/
//cteni PHY 
// precte vsechny registry PHY na danem portu
static int readPhyAllReg (int fd, __u8 port, int withPage)
{
	char title[64];
	int ret;
	if (port > 8) {
		fprintf(stderr, "Maximum allowable PHY address is 0x08 (current is 0x%x\n",port);
		return -EINVAL;
	}
	sprintf (title,"PHY P%d",port);
	if (withPage) 
		ret=readAllPhyPageReg (fd,port,title);
	else
		ret=readAllPhyReg (fd,port,title);
	return ret;
}
/*--------------------------------------------------------------------*/
// precte vsechny registry vsech PHY 
static int readAllPhyAllReg (int fd,int withPage)
{
	int 	ret;
	__u8 	port;
	
	for (port=PHY_BASE; port < PHY_BASE+PHY_NUM; port ++) {
		if ((ret=readPhyAllReg (fd,port,withPage))<0) {
			return ret;
		}
	}
	return 0;
}



/*--------------------------------------------------------------------*/
//cteni Serdes
// precte vsechny registry SERDES
static int readSerdesAllReg (int fd, __u8 port)
{

	__u16 j;
	struct SERDES_REGS reg_table[32];
	
	printf("\nSERDES");
	for (j=0;j<32; j++) {
		reg_table[j].status=serdesRegRead (fd, port, 0x1000+j, &reg_table[j].value);
		
	}
	printAllSerdesReg (port, &reg_table[0] , 0x1000, 32);
	for (j=0;j<32; j++) {
		reg_table[j].status=serdesRegRead (fd, port, 0x1020+j, &reg_table[j].value);
	}
	printAllSerdesReg (port, reg_table , 0x1020, 32);
	for (j=0;j<32; j++) {
		reg_table[j].status=serdesRegRead (fd, port, 0x2000+j, &reg_table[j].value);
	}
	printAllSerdesReg (port, reg_table , 0x2000, 32);
	for (j=0;j<32; j++) {
		reg_table[j].status=serdesRegRead (fd, port, 0x9000+j, &reg_table[j].value);
	}
	printAllSerdesReg (port, reg_table , 0x9000, 32);
	for (j=0;j<32; j++) {
		reg_table[j].status=serdesRegRead (fd, port, 0xa000+j, &reg_table[j].value);
	}
	printAllSerdesReg (port, reg_table , 0xa000, 32);
	for (j=0;j<32; j++) {
		reg_table[j].status=serdesRegRead (fd, port, 0xf000+j, &reg_table[j].value);
	}
	printAllSerdesReg (port, reg_table , 0xf000, 32);
	for (j=0;j<32; j++) {
		reg_table[j].status=serdesRegRead (fd, port, 0xf020+j, &reg_table[j].value);
	}
	printAllSerdesReg (port, reg_table , 0xf020, 32);
	
	for (j=0;j<16; j++) {
		reg_table[j].status=serdesRegRead (fd, port, 0xf060+j, &reg_table[j].value);
	}
	printAllSerdesReg (port, reg_table , 0xf060, 16);
	
	for (j=0;j<32; j++) {
		reg_table[j].status=serdesRegRead (fd, port, 0xf070+j, &reg_table[j].value);
	}
	printAllSerdesReg (port, reg_table , 0xf070, 32);
	return 0;
}

// precte vsechny registry vsech SERDESu
static int readAllSerdesAllReg (int fd )
{
	__u8 port;
	for (port=SERDES_BASE; port < SERDES_BASE+SERDES_NUM; port ++) {
		readSerdesAllReg (fd,port);
	}
	return 0;
}

/*--------------------------------------------------------------------*/
//cteni switch Port
// precte vsechny registry Switch Port na danem portu (0 or.. or 6)
static int readPortAllReg (int fd,__u8 port)
{
	char title[64];
	sprintf (title,"P%d",port);
	return readAllReg (fd,port,title);
}
/*--------------------------------------------------------------------*/
// precte vsechny registry vsech switch portu
static int readAllPortAllReg (int fd)
{

	__u8 port;
	for (port=0; port < PORT_NUM; port ++) {
		readPortAllReg (fd,port);
	}
	return 0;
}
/*--------------------------------------------------------------------*/

// precte vsechny registry vsech Global
static int readAllGlobalAllReg (int fd)
{
	int ret;
	
	if ((ret= readAllReg (fd,GLOBAL1,(char*)"GLOBAL1")) < 0)
		return ret;
	return readAllReg (fd,GLOBAL2,(char*)"GLOBAL2");
}
/*--------------------------------------------------------------------*/
static int getSwitchReg(int fd,int get,int port)
{
	
	
	int ret; 
	printf("\nSwitch registers ");
	if (get == GET_ALL) { 
			if ((ret=readAllPhyAllReg (fd,PAGE_NO))<0)
				return ret;
	}
	if (get == GET_PHY) { 
		if (port <0) {
			if ((ret=readAllPhyAllReg (fd,PAGE_YES))<0)
				return ret;
		} else {
#ifdef RR_PRODUCT_bk2
			if ((port<0) || (port > 8))
#else
			if ((port<1) || (port > 8))
#endif
				return -EINVAL;
			if ((ret=readPhyAllReg (fd,port,PAGE_YES))<0)
			return ret;
			
		}
	}
	if (get == GET_SERDES) { 
		if  (port < 0 ) {
			if ((ret=readAllSerdesAllReg (fd))<0)
				return ret;
		} else {
			if ((port < 9 ) || (port > 0xa))
				return -EINVAL;
			if ((ret=readSerdesAllReg (fd, port)) < 0)
				return ret;
			    
		}
	}
	if (get == GET_ALL)  { 
		if ((ret=readAllSerdesAllReg (fd))<0)
			return ret;
	}
	if ((get == GET_ALL) || (get == GET_PORT)) { 
		if (port <0) {
			if ((ret=readAllPortAllReg (fd))<0)
				return ret;
		} else {
			if (port > 10)
				return -EINVAL;
			if ((ret=readPortAllReg (fd,port))<0)
				return ret;
		}
	}
	if ((get == GET_ALL) || (get == GET_GLOBAL)) { 
		if ((ret=readAllGlobalAllReg (fd))<0)
			return ret;
	}
	return 0;
}
/*--------------------------------------------------------------------*/
static int linkStatus (int fd, int port)
{
	static struct LINK_STATUS linkStatus;
	static __u16  inErrors;
	int ret;
	
	if ((ret=mvGetPhyLinkStatus (fd, port, &linkStatus)) < 0)
		return ret;
	if ((ret=mvGetPhyInError (fd, port, &inErrors)) < 0)
		return ret;
	printPhyStatus  (&linkStatus, inErrors, port);
	return 0;
}
/*--------------------------------------------------------------------*/
static int serdesStatus (int fd, int port)
{
	static struct LINK_STATUS linkStatus;
	int ret;
	if ((ret=mvGetSerdesLinkStatus (fd, port, &linkStatus)) < 0)
		return ret;
	printSerdesStatus  (&linkStatus,port);
	return 0;
}
/*--------------------------------------------------------------------*/
static int portStatus (int fd, int port)
{
	static struct PORT_STATUS status;
	int ret;
	
	if ((ret=mvGetPortStatus (fd, port, &status)) < 0)
		return ret;
	printPortStatus  (&status,port);
	return 0;
}

/*--------------------------------------------------------------------*/
static int getStatus(int fd,int get,int port)
{
	
	int ret; 
	
	
	if (get == GET_PHY) {
		if ((port > 7) || port < 1)
			return -EINVAL;
		if ((ret=linkStatus (fd,port))<0)
			return ret;
	}
	if (get == GET_SERDES) {
		if ((port > 10) || port < 9)
			return -EINVAL;
		if ((ret=serdesStatus (fd,port))<0)
			return ret;
	}
	if (get == GET_PORT) { 
		if ((port > 0xa) || port < 0)
			return -EINVAL;
		if ((ret=portStatus (fd,port))<0)
			return ret;
	}
/*	
	if (get == GET_GLOBAL) { 
		if ((ret=readAllGlobalAllReg (fd))<0)
			return ret;
	}
	*/
	return 0;
}

/*--------------------------------------------------------------------*/
static int setPortMode (int fd,PORT_MODE portMode, int port)
{
	int ret;
	int smode;

	int gpio=-1;
	
#ifdef RR_PRODUCT_bk2
	if (port == 9)
		gpio=SWITCH_P9_2500;
	else if (port == 10)
		gpio=SWITCH_P10_2500;
	else
		return -1;
#endif
#ifdef RR_PRODUCT_bma
	if (port == 10)
		gpio=SWITCH_P10_2500;
	else
		return -1;
#endif
	
	if ((smode=mvCheckPxSmodes (fd)) < 0) {
		printf("Error: Failed read Px_SMODE\n");
		return smode;
	}
	if (portMode != MODE_NONE) {
		if ((ret= mvRegWrite (fd, 5, 26,0xa100)) < 0) {
			printf("Error: Failed set workaround on port %d\n",port);
			return ret;
		}
		if ((ret= mvRegWrite (fd, 4, 26,0xfd40)) < 0) {
			printf("Error: Failed set workaround on port %d\n",port);
			return ret;
		}
	}
	switch (portMode) {
		case MODE_2500BASE_X:
			printf ("2500BaseX: ");
#if (defined RR_PRODUCT_bma) || (defined RR_PRODUCT_bk2)
			gpioSetValue(gpio, 0,port,smode);
#endif
			if ((ret= mvRegWrite (fd, port, 0x0,0x000b)) < 0) {
				printf("Error: Failed set cmode on port %d\n",port);
				return ret;
			}
			if ((ret= mvRegWriteMask (fd, port, 0x1,0x1000,0x1000)) < 0) {
				printf("Error: Failed set AltSpeed on port %d\n",port);
				return ret;
			}
			if ((ret= mvSetForceSpeed (fd, port, FORCE_SPEED_2500)) < 0) {
				printf("Error: Failed set forced speed 2500 on port %d\n",port);
				return ret;
			}
			if ((ret= mvSetForceLink (fd, port, FORCE_LINK_UP)) < 0) {
				printf("Error: Failed set forced link up on port %d\n",port);
				return ret;
			}
			if ((ret= mvSetForceDuplex (fd, port, FORCE_DUPLEX_FULL)) < 0) {
				printf("Error: Failed set forced full duplex on port %d\n",port);
				return ret;
			}
			if ((ret= serdesRegWrite (fd, port,SGMII_CONTROL_REG, 0x1140)) < 0) {
				printf("Error: Failed reset SGMII/1000Base-X port %d\n",port);
				return ret;
			}
			break;
		case MODE_1000BASE_X:
			printf ("1000BaseX: ");
#if (defined RR_PRODUCT_bma) || (defined RR_PRODUCT_bk2)
			gpioSetValue(gpio, 1,port,smode);
#endif
			if ((ret= mvRegWrite (fd, port, 0x0,0x0009)) < 0) {
				printf("Error: Failed set cmode on port %d\n",port);
				return ret;
			}
			if ((ret= mvRegWriteMask (fd, port, 0x1,0x0,0x333f)) < 0) {
				printf("Error: Failed clear force on port %d\n",port);
				return ret;
			}
			if ((ret= mvSetForceSpeed (fd, port, FORCE_SPEED_1000)) < 0) {
				printf("Error: Failed set forced speed 1000 on port %d\n",port);
				return ret;
			}
			if ((ret= mvSetForceLink (fd, port, FORCE_LINK_UP)) < 0) {
				printf("Error: Failed set forced link up on port %d\n",port);
				return ret;
			}
			if ((ret= mvSetForceDuplex (fd, port, FORCE_DUPLEX_FULL)) < 0) {
				printf("Error: Failed set forced full duplex on port %d\n",port);
				return ret;
			}
			if ((ret= serdesRegWrite (fd, port,SGMII_CONTROL_REG, 0x9340)) < 0) {
				printf("Error: Failed reset SGMII/1000Base-X port %d\n",port);
				return ret;
			}
			break;
		case MODE_SGMII:
			printf ("SGMII: ");
#if (defined RR_PRODUCT_bma) || (defined RR_PRODUCT_bk2)
			gpioSetValue(gpio,1,port,smode);
#endif
			if ((ret= mvRegWrite (fd, port, 0x0,0x100a)) < 0) {
				printf("Error: Failed set cmode on port %d\n",port);
				return ret;
			}
			if ((ret= mvRegWriteMask (fd, port, 0x1,0x0,0x333f)) < 0) {
				printf("Error: Failed clear force on port %d\n",port);
				return ret;
			}
			if ((ret= serdesRegWrite (fd, port,SGMII_CONTROL_REG, 0x9340)) < 0) {
				printf("Error: Failed reset SGMII/1000Base-X port %d\n",port);
				return ret;
			}
			break;
		case MODE_SGMII_1000:
			printf ("SGMII_1000: ");
#if (defined RR_PRODUCT_bma) || (defined RR_PRODUCT_bk2)
			gpioSetValue(gpio,1,port,smode);
#endif
			if ((ret= mvRegWrite (fd, port, 0x0,0x100a)) < 0) {
				printf("Error: Failed set cmode on port %d\n",port);
				return ret;
			}
			if ((ret= mvRegWriteMask (fd, port, 0x1,0x0,0x333f)) < 0) {
				printf("Error: Failed clear force on port %d\n",port);
				return ret;
			}
			if ((ret= mvSetForceSpeed (fd, port, FORCE_SPEED_1000)) < 0) {
				printf("Error: Failed set forced speed 1000 on port %d\n",port);
				return ret;
			}
			if ((ret= mvSetForceDuplex (fd, port, FORCE_DUPLEX_FULL)) < 0) {
				printf("Error: Failed set forced full duplex on port %d\n",port);
				return ret;
			}
			if ((ret= serdesRegWrite (fd, port,SGMII_CONTROL_REG, 0x9340)) < 0) {
				printf("Error: Failed reset SGMII/1000Base-X port %d\n",port);
				return ret;
			}
			break;
		case MODE_SGMII_100:
			printf ("SGMII_100: ");
#if (defined RR_PRODUCT_bma) || (defined RR_PRODUCT_bk2)
			gpioSetValue(gpio,1,port,smode);
#endif
			if ((ret= mvRegWrite (fd, port, 0x0,0x100a)) < 0) {
				printf("Error: Failed set cmode on port %d\n",port);
				return ret;
			}
			if ((ret= mvRegWriteMask (fd, port, 0x1,0x0,0x333f)) < 0) {
				printf("Error: Failed clear force on port %d\n",port);
				return ret;
			}
			if ((ret= mvSetForceSpeed (fd, port, FORCE_SPEED_100)) < 0) {
				printf("Error: Failed set forced speed 100 on port %d\n",port);
				return ret;
			}
			if ((ret= mvSetForceDuplex (fd, port, FORCE_DUPLEX_FULL)) < 0) {
				printf("Error: Failed set forced full duplex on port %d\n",port);
				return ret;
			}
			if ((ret= serdesRegWrite (fd, port,SGMII_CONTROL_REG, 0xb100)) < 0) {
				printf("Error: Failed reset SGMII/1000Base-X port %d\n",port);
				return ret;
			}
			break;
		case MODE_SGMII_10:
			printf ("SGMII_10: ");
#if (defined RR_PRODUCT_bma) || (defined RR_PRODUCT_bk2)
			gpioSetValue(gpio,1,port,smode);
#endif
			if ((ret= mvRegWrite (fd, port, 0x0,0x100a)) < 0) {
				printf("Error: Failed set cmode on port %d\n",port);
				return ret;
			}
			if ((ret= mvRegWriteMask (fd, port, 0x1,0x0,0x333f)) < 0) {
				printf("Error: Failed clear force on port %d\n",port);
				return ret;
			}
			if ((ret= mvSetForceSpeed (fd, port, FORCE_SPEED_10)) < 0) {
				printf("Error: Failed set forced speed 10 on port %d\n",port);
				return ret;
			}
			if ((ret= mvSetForceDuplex (fd, port, FORCE_DUPLEX_FULL)) < 0) {
				printf("Error: Failed set forced full duplex on port %d\n",port);
				return ret;
			}
			if ((ret= serdesRegWrite (fd, port,SGMII_CONTROL_REG, 0x9100)) < 0) {
				printf("Error: Failed reset SGMII/1000Base-X port %d\n",port);
				return ret;
			}
			break;
		default:
			break;
	}	
	printf ("OK\n");
	return 0;
}


/*--------------------------------------------------------------------*/
static int checkParam (struct cmdData_ *cmdData, int page)
{
	
	if (mvCheckAddr (cmdData->data1, allowedAddrAll) <0)
		return -EINVAL;
	if (page) {
		if (mvCheckReg ( cmdData->data3) <0)
			return -EINVAL;
#ifdef RR_PRODUCT_bk2
		if (mvCheckPage (cmdData->data1,cmdData->data2) <0)
#else
		if (mvCheckPage (cmdData->data2) <0)
#endif
			return -EINVAL;
	}else {
		if (mvCheckReg ( cmdData->data2) <0)
			return -EINVAL;
	}
	return 0;
}
/*--------------------------------------------------------------------*/

static int getOneSwitchReg (int fd, struct cmdData_ *cmdData, int page, int phy)
{
	
	int ret;
	__u16 data;
	if (page) {
		if (cmdData->num == 3) {
			if ((ret=checkParam (cmdData, page)) < 0) {
				return ret;
			}	
			if ((ret=mvPhyRegPageRead(fd, (__u8)cmdData->data1, (__u8)cmdData->data3,(__u8)cmdData->data2,&data)) < 0) 
				return ret;
			printf("Addr:%02x Page:%02x Reg:%02x Value:%04x\n ",(__u8)cmdData->data1,(__u8)cmdData->data2,(__u8)cmdData->data3,data);
			return 0;
		} else {
			return -EINVAL;
		}
	}else{
		if (cmdData->num == 2) {
			if (cmdData->data2 >= 0x1000) {
				if (mvCheckAddr (cmdData->data1, allowedAddrSerdes) <0)
					return -EINVAL;
				if (mvCheckRegSerdes ( cmdData->data2, allowedSerdesAll) <0)
					return -EINVAL;
				if ((ret=serdesRegRead (fd, (__u8)cmdData->data1, (__u16)cmdData->data2, &data)) < 0 )
					return ret;
				printf("Addr:%02x Reg:%04x Value:%04x\n",(__u8)cmdData->data1,(__u16)cmdData->data2,data);
				return 0;
			}
			if ((ret=checkParam (cmdData, page)) < 0) {
				return ret;
			}	
			if (phy) {
				if ((ret=mvPhyRegRead(fd, (__u8)cmdData->data1,(__u8)cmdData->data2,&data)) < 0) 
					return ret;
			} else {
				if ((ret=mvRegRead(fd, (__u8)cmdData->data1,(__u8)cmdData->data2,&data)) < 0) 
					return ret;
			}
			printf("Addr:%02x Reg:%02x Value:%04x\n",(__u8)cmdData->data1,(__u8)cmdData->data2,data);
			return 0;
		} else {
			return -EINVAL;
		}
	}
	return -EINVAL;
}
/*--------------------------------------------------------------------*/
static int setSwitchReg(int fd, struct cmdData_ *cmdData, int page, int phy)
{
		
	int ret;
	
	if (page) {
		
		switch (cmdData->num) {
		case 4 :
			if ((ret=checkParam (cmdData, page)) < 0)
				return ret;
			if ((ret=mvPhyRegPageWrite(fd, (__u8)cmdData->data1, (__u8)cmdData->data3,(__u8)cmdData->data2, (__u16)cmdData->data4 )) < 0) 
				return ret;
			
			break;
		case 5 :
			if ((ret=checkParam (cmdData, page)) < 0)
				return ret;
			if ((ret=mvPhyRegPageWriteMask(fd, (__u8)cmdData->data1,  (__u8)cmdData->data3,(__u8)cmdData->data2,(__u16)cmdData->data4,(__u16)cmdData->data5 )) < 0) 
				return ret;
			break;
		default : 
			return -EINVAL;
			break;	
		
		}
	}else{
		switch (cmdData->num) {
		case 3 :
			if (cmdData->data2 >= 0x1000) {
				if (mvCheckAddr (cmdData->data1, allowedAddrSerdes) <0)
					return -EINVAL;
				if (mvCheckRegSerdes ( cmdData->data2,allowedSerdesAll) <0)
					return -EINVAL;
				if ((ret=serdesRegWrite (fd, (__u8)cmdData->data1, (__u16)cmdData->data2, (__u16)cmdData->data3 )) < 0 )
					return ret;
				return 0;
			}
			if ((ret=checkParam (cmdData, page)) < 0)
				return ret;
			if (phy) {
				if ((ret=mvPhyRegWrite(fd, (__u8)cmdData->data1, (__u8)cmdData->data2, (__u16)cmdData->data3 )) < 0)
					return ret;
			} else {
				if ((ret=mvRegWrite(fd, (__u8)cmdData->data1, (__u8)cmdData->data2, (__u16)cmdData->data3 )) < 0)
					return ret;
			}
			break;
		case 4 :
			if (cmdData->data2 >= 0x1000) {
				if (mvCheckAddr (cmdData->data1, allowedAddrSerdes) <0)
					return -EINVAL;
				if (mvCheckRegSerdes ( cmdData->data2,allowedSerdesAll) <0)
					return -EINVAL;
				if ((ret=serdesRegWriteMask (fd, (__u8)cmdData->data1, (__u16)cmdData->data2, (__u16)cmdData->data3, (__u16)cmdData->data4  )) < 0 )
					return ret;
				return 0;
			}
			if ((ret=checkParam (cmdData, page)) < 0)
				return ret;
			if (phy) {
				if ((ret=mvPhyRegWriteMask(fd, (__u8)cmdData->data1,  (__u8)cmdData->data2, (__u16)cmdData->data3, (__u16)cmdData->data4 )) < 0)
				return ret;
			} else {
				if ((ret=mvRegWriteMask(fd, (__u8)cmdData->data1,  (__u8)cmdData->data2, (__u16)cmdData->data3, (__u16)cmdData->data4 )) < 0)
					return ret;
			}
			break;
		default : 
			return -EINVAL;
			break;	
		}
	}
	return 0;
}
/*--------------------------------------------------------------------*/
int getRmon(int fd ,__u8 rmon ,int* port,int portPtr, int diff, int number, int deltaTime)
{
	
	TABLE_RMON data[PORT_NUM];
	TABLE_RMON data_old[PORT_NUM];
	int ii;
	int xx;
	int ret = -1;
	
	HISTOGRAM_MODE histogramMode;
	switch (rmon) {
		case FLUSHALL:
			printf ("Flush all counters for all ports\n");
			return mvFlushRmonAllPorts (fd);
			break;
		case FLUSHPORT:
			for (ii=0; ii < portPtr; ii++) {
				if (port[ii] < 0)
					return -EINVAL;
				printf ("Flush all counters for  port %d\n",port[ii]);
				ret=mvFlushRmonPort (fd, port[ii]);
				if (ret !=0)
					break;
			}
			return ret;
			break;
		case COUNTERS :	
		case COUNTERS_RX :
		case COUNTERS_TX :
		case COUNTERS_BOTH :
			histogramMode=COUNT_NO_SET;
			if (rmon==COUNTERS_TX)
				histogramMode=COUNT_TX_ONLY;
			else if (rmon==COUNTERS_RX)
				histogramMode=COUNT_RX_ONLY;
			else if (rmon==COUNTERS_BOTH)
				histogramMode=COUNT_RX_TX;
			for (xx = 0; xx <= number; xx++) {
				for (ii=0; ii < portPtr; ii++) {
					if (port[ii] < 0)
						return -EINVAL;
					data[ii].port=port[ii];
					if ((ret=mvGetPortLink (fd,(__u8) port[ii], &data[ii].portLink)) < 0)
						return ret;
					if ((ret= mvReadRmon (fd, port[ii], &data[ii].data_rmon, histogramMode)) < 0)
						return ret;
				}
				if (diff > 0 ) {
					if (xx > 0)
						printRmonCountersDiff (data,data_old,histogramMode,portPtr,xx,xx*deltaTime);
					memcpy (&data_old,&data, sizeof data_old);
				} else {
					printRmonCounters (data,histogramMode,portPtr,xx,xx*deltaTime);
				}
				if ((deltaTime> 0 ) && ( xx < number))
					sleep (deltaTime);
			}
			return 0;
			break;
		default:
			return -EINVAL;
	}
	return -1;
}


/*--------------------------------------------------------------------*/
const char *usage =
"usage: marvell-tool [-vhe][-p port][-m time:number [-d]][-s | -g | -G | -t | r | f ] param\n"
"	-s, --set              		 		set switch register\n" 
"		param:  ADDR:[:PAGE]:REG:VALUE[:MASK]   numeric value [param PAGE - must be set -e]\n"
"							PHY: must be set -y\n"							
"	-g, --getreg					get one register\n"
"		param:  ADDR:[PAGE]:REG  		numeric value [param PAGE - must be set -e]\n"
"							PHY: must be set -y\n"
"	-e, --page					set PHY page [only for -s -g]\n"
"	-G, --get					get switch registers\n"
"		param:	ALL				get all registers (without PHY pages)\n"
"			PHY				get PHYs registers (with PHY pages)\n"
"			SERDES				get SERDES registers\n"
"			PORT				get PORTs registers\n"
"			GLOBAL				get GLOBALs registers\n"
"	-t, --status					get status\n"
"		param:	PHY				get PHY status [must be set param -p (1-8)]\n"
"			SERDES				get Serdes status [must be set param -p (9-10)]\n"
"			PORT				get PORT [must be set param -p (0-10)]\n"
"			GLOBAL				get GLOBALs registers\n"
"	-r  --rmon					rmon counters \n"
"		param:  FLUSHPORT			flush all counters for a port [must be set param -p]\n" 
"			FLUSHALL			flush all counters for all ports\n"
"			COUNTERS			capture and read all counters a port [must be set param -p]\n"
//"			COUNTERS_RX			capture and read all counters a port [must be set param -p] (ingress frame by length)\n"
//"			COUNTERS_TX			capture and read all counters a port [must be set param -p] (egress frame by length)\n"
//"			COUNTERS_BOTH			capture and read all counters a port [must be set param -p] (egress and ingress frame by length)\n"
"	-m  --measurements 				repeated measurements after the specified timeand number of repetion [only for -r]\n"
"		param: 	time:number			time between the measured (1-600) number of repetion (1-14400) \n"
"	-d  --diff					show increments values between the measured  [only for -r , must be set up -m]\n"
"	-p  --port					set port \n"
"		param:  port 				number of port (0-6) [only for -G, -t, -r]\n"
"	-v  --verbose					verbose \n"
"		param:	0 | 1 |2			\n"
"	-y						read/write to internal PHY register (only for -s and -g)\n"
#ifdef RR_PRODUCT_bma
"	-f  --sfp					set  mode [only for the port p10]\n"
"		param: 	2500BASE-X 			2500Base-X\n"
"			1000BASE-X 			1000Base-X\n"
"			SGMII				SGMI auto-nego 10/100/1000\n"
"			SGMII_1000			SGMII force speed 1000\n"
"			SGMII_100			SGMII force speed 100\n"
"			SGMII_10			SGMII gorce speed 10\n"
#endif
#ifdef RR_PRODUCT_bk2
"	-f  --sfp					set  mode [only for the port p10,p9; must be set param -p]\n"
"		param: 	2500BASE-X 			2500Base-X\n"
"			1000BASE-X 			1000Base-X\n"
"			SGMII				SGMI auto-nego 10/100/1000\n"
"			SGMII_1000			SGMII force speed 1000\n"
"			SGMII_100			SGMII force speed 100\n"
"			SGMII_10			SGMII gorce speed 10\n"
#endif

"	-h  --help					help\n";

int main(int argc, char **argv)
{
	int fd =-1;
	int c, ret=0;
	int numcmd=0;
	int get = 0;
	int set = 0;
	int phy= 0;
	int getreg = 0;
	int page = 0;
	int status=0;
	int portPtr=0;
	int port[PORT_NUM] ={-1,-1,-1,-1,-1,-1,-1};
	int rmon =0;
	int number=0;
	int deltaTime=0;
	int diff=0;
	PORT_MODE portMode=MODE_NONE;
	static struct cmdData_ cmdData;
	
	if (argc == 1) {
		printf ("%s",usage);
		return 0;
	}

	while ((c = getopt_long(argc, argv, "hedv:s:p:g:G:t:r:m:u:yf:", longopts, 0)) != EOF) {
		switch (c) {
			case 's': 
				set=1;
				numcmd++;
				cmdData.num=sscanf(optarg,"%i:%i:%i:%i:%i",&cmdData.data1, &cmdData.data2, &cmdData.data3, &cmdData.data4,&cmdData.data5);
				break;
			case 'g': 
				getreg=1;
				numcmd++;
				cmdData.num=sscanf(optarg,"%i:%i:%i",&cmdData.data1, &cmdData.data2, &cmdData.data3);
				break;
			case 'e': 
				page=1;
				break;
			case 'y': 
				phy=1;
				break;
			case 'p':
				if (portPtr > 10) {
					fprintf (stderr,"Maximum port number, (valid 0 .. 10)\n");
					printf ("%s",usage);
					exit (-1);
				}
				port[portPtr]=atoi(optarg);
				
				if ((port[portPtr] < 0 ) || (port[portPtr] >10)) {
					fprintf (stderr,"Invalid port number, (valid 0 .. 10)\n");
					printf ("%s",usage);
					exit (-1);
				}
				portPtr++;
				break;
			case 'G': 
				numcmd++;
				get=1;
				if (strcmp(optarg,"ALL") == 0)
					get=GET_ALL;
				if (strcmp(optarg,"PHY") == 0)
					get=GET_PHY;
				if (strcmp(optarg,"SERDES") == 0)
					get=GET_SERDES;
				if (strcmp(optarg,"PORT") == 0)
					get=GET_PORT;
				if (strcmp(optarg,"GLOBAL") == 0)
					get=GET_GLOBAL;
				if (get ==0) {
					printf ("%s",usage);
					exit (-1);
				}
				break;

			case 't': 
				numcmd++;
				if (strcmp(optarg,"ALL") == 0)
					status=GET_ALL;
				if (strcmp(optarg,"PHY") == 0)
					status=GET_PHY;
				if (strcmp(optarg,"PORT") == 0)
					status=GET_PORT;
				if (strcmp(optarg,"GLOBAL") == 0)
					status=GET_GLOBAL;
				if (strcmp(optarg,"SERDES") == 0)
					status=GET_SERDES;
				if (status ==0) {
					printf ("%s",usage);
					exit (-1);
				}
				break;
			case 'r': 
				numcmd++;
				if (strcmp(optarg,"FLUSHALL") == 0)
					rmon=FLUSHALL;
				if (strcmp(optarg,"FLUSHPORT") == 0)
					rmon=FLUSHPORT;
				if (strcmp(optarg,"COUNTERS") == 0)
					rmon=COUNTERS;
//				if (strcmp(optarg,"COUNTERS_RX") == 0)
//					rmon=COUNTERS_RX;
//				if (strcmp(optarg,"COUNTERS_TX") == 0)
//					rmon=COUNTERS_TX;
//				if (strcmp(optarg,"COUNTERS_BOTH") == 0)
//					rmon=COUNTERS_BOTH;
				if (rmon ==0) {
					printf ("%s",usage);
					exit (-1);
				}
				break;
			case 'f': 
				numcmd++;
				if (strcmp(optarg,"2500BASE-X") == 0)
					portMode=MODE_2500BASE_X;
				if (strcmp(optarg,"1000BASE-X") == 0)
					portMode=MODE_1000BASE_X;
				if (strcmp(optarg,"SGMII") == 0)
					portMode=MODE_SGMII;
				if (strcmp(optarg,"SGMII_1000") == 0)
					portMode=MODE_SGMII_1000;
				if (strcmp(optarg,"SGMII_100") == 0)
					portMode=MODE_SGMII_100;
				if (strcmp(optarg,"SGMII_10") == 0)
					portMode=MODE_SGMII_10;
				if (portMode == MODE_NONE) {
					printf ("%s",usage);
					exit (-1);
				}
				break;
			case 'v': 
				verbose=atoi(optarg);
				if ((verbose <0) || (verbose > 2)) {
					printf ("%s",usage);
					exit (-1);
				}
				break;	
			case 'm': 
				sscanf(optarg,"%d:%d",&deltaTime, &number);
				if ((deltaTime <1) || (deltaTime > 600) || (number <1) || (number > 14400)) {
					printf ("%s",usage);
					exit (-1);
				}
				break;
			case 'd': 
				diff=1;
				break;
			case 'h': 
				printf ("%s",usage);
				return 0;
				break;
			
		}
	}
	if (numcmd == 0) {
		printf ("%s",usage);
		exit (-1);
	}
	if (numcmd > 1) {
		printf ("Too many parameters\n");
		printf ("%s",usage);
		exit (-1);
	}
	if ((diff > 0 ) && ((deltaTime <1) || (number < 1))) {
		printf ("Parameters -m must be set\n");
		printf ("%s",usage);
		exit (-1);
	}

	if ((fd = mvOpen(ETHERNET)) < 0) {
		exit(-1);
	}
	if (set)
		ret=setSwitchReg(fd,&cmdData,page,phy);
	if (getreg)
		ret=getOneSwitchReg(fd,&cmdData,page,phy); 
	if (get) {
		if ( portPtr == 0)
			ret=getSwitchReg(fd,get,-1);
		else
			ret=getSwitchReg(fd,get,port[portPtr-1]); 
	}
	if (status) {
		if ( portPtr == 0)
			ret=getStatus(fd,status,-1);
		else
			ret=getStatus(fd,status,port[portPtr-1]);
	}
	if (rmon) {
		ret=getRmon(fd,rmon,port,portPtr,diff,number,deltaTime);
	}
#ifdef RR_PRODUCT_bma
	if (portMode) {
		if (portPtr > 0 )
			if ( port[portPtr-1] != 10) {
				mvClose(fd);
				printf ("%s",usage);
				exit (-1);
			}
		ret=setPortMode (fd,(PORT_MODE)portMode,10);
	}

#endif
#ifdef RR_PRODUCT_bk2
	if (portMode) { 
		if (portPtr != 1 )
			ret = -1;
		else
			ret=setPortMode (fd,(PORT_MODE)portMode,port[portPtr-1]);
	}
#endif
	mvClose(fd);
	if (ret < 0)
		printf ("%s",usage);
	exit (ret);
}
