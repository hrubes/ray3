#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/sockios.h>
#ifndef __GLIBC__
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#endif
#include <linux/types.h>

#include "marvell-interface.h"
#include "marvell-lib-def.h"
#include "marvell-reg.h"
#include "marvell-lib.h"
#include "marvell-lib-debug.h"

extern int verbose;

static int setScracthReg ( int fd, __u8 pointer, __u8 data)
{
	__u16 hdata ;
	int timeout,ret;
	
	for (timeout=0;timeout < 100000;timeout++) {
		if ((ret=regRead(fd,GLOBAL2, 0x1a,&hdata)) < 0 )
				return ret;
		if (! (hdata & 0x8000) ) {
			if ((ret=regWrite(fd, GLOBAL2, 0x1a, 0x8000 | ((pointer & 0x7f) << 8) | (0xff&data) )) < 0 )
				return ret;
			for (timeout=0;timeout < 10000;timeout++) {
				if ((ret=regRead(fd,GLOBAL2, 0x1a ,&hdata)) < 0 )
					return ret;
				if (! (hdata & 0x8000) ) 
					return 0;
				usleep(100);
			}
			return -EIO;
		}
		usleep(100);
	}
	return -EIO;
}


static int setAvbReg ( int fd, __u16  data, __u8 port, __u8 avbAddr)
{
	__u16 hdata ;
	int timeout,ret;
	
	for (timeout=0;timeout < 100000;timeout++) {
		if ((ret=regRead(fd,GLOBAL2, 0x16 ,&hdata)) < 0 )
				return ret;
		if (! (hdata & 0x8000) ) {
			if ((ret=regWrite(fd, GLOBAL2, 0x17, data )) < 0 )
				return ret;
			if ((ret=regWrite(fd, GLOBAL2, 0x16 , 0x8000 | (0x3 << 13) | ((0x1f & port) << 8) |  (0x1f & avbAddr))) < 0 )
				return ret;
			for (timeout=0;timeout < 10000;timeout++) {
				if ((ret=regRead(fd,GLOBAL2, 0x16 ,&hdata)) < 0 )
					return ret;
				if (! (hdata & 0x8000) ) 
					return 0;
				usleep(100);
			}
			return -EIO;
		}
		usleep(100);
	}
	return -EIO;
}

static int getAvbReg ( int fd, __u16 *  data, __u8 port, __u8 avbAddr)
{
	__u16 hdata ;
	int timeout,ret;
	
	for (timeout=0;timeout < 100000;timeout++) {
		if ((ret=regRead(fd,GLOBAL2, 0x16 ,&hdata)) < 0 )
				return ret;
		if (! (hdata & 0x8000) ) {
			if ((ret=regWrite(fd, GLOBAL2, 0x16 , 0x8000 | ((0x1f & port) << 8) | (0x1f & avbAddr))) < 0 )
				return ret;
			for (timeout=0;timeout < 10000;timeout++) {
				if ((ret=regRead(fd,GLOBAL2, 0x16 ,&hdata)) < 0 )
					return ret;
				if (! (hdata & 0x8000) ) {
					if ((ret=regRead(fd, GLOBAL2, 0x17, data )) < 0 )
					    return ret;
					return 0;
				}
				usleep(100);
			}
			return -EIO;
		}
		usleep(100);
	}
	return -EIO;
}

/*--------------------------------------------------------------------*/

int set1ppsOut ( int fd)
{
	return setScracthReg ( fd, 0x6e , 0x60);
}

/*--------------------------------------------------------------------*/

int setSynceOut ( int fd)
{
	return setScracthReg ( fd, 0x6f , 0x54);
}

/*--------------------------------------------------------------------*/
/*--------------------------------------------------------------------*/
// syncE fce

int getSynceStatus (int fd, SYNCE_STATUS * status)
{
	__u16 data;
	int ret;
	int i;
	
	if ((ret= getAvbReg (fd, &data, 0x1e, 0x1e)) < 0)
		return ret;
	status->priRecClk = data & 0x1f;
	status->secRecClk = (data>>8) & 0x1f;
	for (i=1; i<9; i++ ) {
		if ((ret=mvPhyRegPageRead (fd, (__u8) i, 16 , 2, &data)) < 0)
			return ret;
		status->portMacSCR[i]=data;
	}
	return 0;
		
	
}

int setSynce (int fd, __u8  port, REFERENCE_CLOCK refClock, REC_CLOCK recClock)
{
	int ret=-1;
	__u16  val;
	if ( port > 11)
		return -1;
	if (recClock != REC_CLOCK_NONE) {
		if ((ret =getAvbReg (fd, &val, 0x1e, 0x1e)) < 0)
			return ret;
		switch (recClock) {
			case REC_CLOCK_PRI:
				val&=~(0x001F);
				val|=port;
				break;
			case REC_CLOCK_SEC:
				val&=~(0x1F00);
				val|=port<<8;
				break;
			default:
				break;
		}
		if ((ret=setAvbReg (fd, val, 0x1e, 0x1e)) < 0)
			return ret;
	}
	if ((port > 0 ) && (port < 9)) {
		//set reg 16 page 2 bit 7 and 10
		if ((ret=mvPhyRegPageRead (fd, port, 16, 2, &val)) < 0) 
			return ret;
		switch (recClock) {
			case REC_CLOCK_NO:
				val&=~0x400;
				break;
			case REC_CLOCK_PRI:
				val|=0x400;
				break;
			case REC_CLOCK_SEC:
				val|=0x400;
				break;
			default:
				break;
		}
		switch (refClock) {
			case REFERENCE_CLOCK_SE_SCLK:
				val|=0x80;
				break;
			case REFERENCE_CLOCK_XTAL:
				val&=~0x80;
				break;
			default:
				break;
		}
		if ((ret=mvPhyRegPageWrite (fd, port, 16, 2, val)) < 0) 
			return ret;
		//slave more (RXCLK)
		if ((recClock == REC_CLOCK_PRI) || (recClock == REC_CLOCK_SEC)) {
			if ((ret=mvPhyRegWriteMask (fd, port, 9, 0x1400, 0x1c00)) < 0)
				return ret;
		}
		//sw reset
		if ((recClock != REC_CLOCK_NONE) || (refClock != REFERENCE_CLOCK_NONE)) {
			if ((ret=mvPhyRegWriteMask (fd, port, 0, 0x800, 0x8000)) < 0)
				return ret;
		}
	}
	return ret;
	
}
