/*

    prbs-switch: PRBS for Marvell 88E6390 (X) switch
*/



#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <time.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/sockios.h>
#ifndef __GLIBC__
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#endif
#include <linux/types.h>


#include "marvell-lib.h"
#include "marvell-lib-def.h"
#include "marvell-reg.h"






const char * ETHERNET={"eth1"};

struct data_cmd_ {
	unsigned int timeout;
	unsigned int number;
	unsigned int pattern;
	__u16 immCount;
	__u8 port;
};

struct counters_ {
	int valid;
	__u64	Tx;
	__u64	Rx;
	__u64	Errors;
};

struct summaryCounters_ {
	long long unsigned int	Tx;
	long long unsigned int	Rx;
	long long unsigned int	Errors;
};
	
extern int verbose;

extern __s8 allowedPage[];
extern __s8 allowedAddrAll[];
extern __s8 allowedAddrSerdes[];
extern __u16 allowedSerdesAll[];
/*--------------------------------------------------------------------*/



/*--------------------------------------------------------------------*/

struct option longopts[] = {
 /* { name  has_arg  *flag  val } */
    {"timeout",		1, NULL, 't'},	
    {"port",		1, NULL, 'p'},
    {"pattern",		1, NULL, 'a'},
	{"number",		1, NULL, 'n'},
    {"verbose",		1, NULL, 'v'},
    {"help", 		0, NULL, 'h'},
	{"counting", 		0, NULL, 'c'},	
    { 0, 0, NULL, 0 }
};


/*--------------------------------------------------------------------*/
//globalni PRBS citac
static int prbsCounters (summaryCounters_ * counters, counters_ * diffCounters)
{
	if (diffCounters->valid) {
		counters->Tx += diffCounters->Tx;
		counters->Rx += diffCounters->Rx;	
		counters->Errors += diffCounters->Errors;
		return 0;
	}
	return -EINVAL;
}


/*--------------------------------------------------------------------*/
//cteni PRBS citacu
static int readPrbsCounters (int fd, __u8 port, counters_ * counters)
{

	__u16 data1;
	__u16 data2;
	__u16 data3;
	int ret=0;

	counters->valid=0;
	counters->Tx=0;
	counters->Rx=0;
	counters->Errors=0;
	
	if ((ret=serdesRegRead (fd, port, 0xf030,&data1))< 0)
		return ret;
	if (!((data1 >>8)&0x1))
		return ret;
	if ((ret=serdesRegRead (fd, port, 0xf031,&data1))< 0)
		return ret;
	if ((ret=serdesRegRead (fd, port, 0xf032,&data2))< 0)
		return ret;
	if ((ret=serdesRegRead (fd, port, 0xf033,&data3))< 0)
		return ret;
	counters->Tx=data1| ((__u64)data2 << 16) | ((__u64)data3 <<32) ;
		
	if ((ret=serdesRegRead (fd, port, 0xf034,&data1))< 0)
		return ret;
	if ((ret=serdesRegRead (fd, port, 0xf035,&data2))< 0)
		return ret;
	if ((ret=serdesRegRead (fd, port, 0xf036,&data3))< 0)
		return ret;
	counters->Rx=data1| ((__u64)data2 << 16) | ((__u64)data3 <<32) ;
	
	if ((ret=serdesRegRead (fd, port, 0xf037,&data1))< 0)
		return ret;
	if ((ret=serdesRegRead (fd, port, 0xf038,&data2))< 0)
		return ret;
	if ((ret=serdesRegRead (fd, port, 0xf039,&data3))< 0)
		return ret;
	counters->Errors=data1| ((__u64)data2 << 16) | ((__u64)data3 <<32) ;
	
	counters->valid=1;
	return 0;
}

/*--------------------------------------------------------------------*/
//stop  PRBS
static int stopPrbsCounters (int fd, __u8 port)
{
	
	int ret;
	counters_ counters;
	readPrbsCounters (fd, port, &counters);
	if ((ret=serdesRegWrite (fd, port, 0xf030 , 0x0200))< 0)
		return ret;
	return 0;
}


/*--------------------------------------------------------------------*/
//start  PRBS
static int startPrbsCounters (int fd, data_cmd_ *cmd)
{
	int ret;
	if ((ret=serdesRegWrite (fd, cmd->port, 0xf030 , 0x2230 | (cmd->immCount <<7 ) |(cmd->pattern & 0x000f)))< 0)
		return ret;
	return 0;
}

/*--------------------------------------------------------------------*/





/*--------------------------------------------------------------------*/
const char *usage =
"usage: prbs-switch [-vh][-p port] [-a pattern ] [-t timeout ] [- n number]\n"
"	-p  --port			port \n"
"		param:  port 		number of port (9 or 10)\n"
"	-a  --pattern			PRBS pattern\n"
"		param:  PRBS_31		IEEE 49.2.8 - PRBS 31 (default)\n"
"			PRBS_7		PRBS 7\n"
"			PRBS_9		PRBS 9 IEEE 83.7\n"
"			PRBS_23		PRBS 23\n"
"			PRBS_31inv	PRBS 31 Inverted\n"
"			PRBS_7inv	PRBS 7 Inverted\n"
"			PRBS_15		PRBS 15\n"
"			PRBS_15inv	PRBS 15 Inverted\n"
"			PRBS_9inv	PRBS 9 Inverted\n"
"			PRBS_23inv	PRBS 23 Inverted\n"
"			HIGH		High frequency pattern\n"
"			LOW		Low frequency pattern\n"
"			MIXED		Mixed frequency pattern\n"
"			SQUARE		Square Wave pattern \n"
"	-n  --number			Number of measuring cycles \n"
"		param: number		1 - 1000, default 1\n"
"	-t  --timeout			The number of repetitions \n"
"		param: timeout		timeoout[sec] 0 - 3600, default 1\n"
"	-c --counting			Count PRBS errors before locking\n"
"					default: Wait until PRBS locks before locking\n"
"	-v  --verbose			verbose \n"
"		param:	0 | 1 |2		\n"
"	-h  --help			help\n";

int main(int argc, char **argv)
{
	int fd =-1;
	int c,ret;
	int locked=0;
	unsigned int i;
	static struct summaryCounters_ counters = {0,0,0};
	static struct counters_ diffCounters;
	static struct  data_cmd_ cmd = {1,1,0,0,0};
	
	struct data_cmd_ {
	unsigned int timeout;
	unsigned int repeat;
	unsigned int pattern;
	__u8 port;
	}; 
	
	if (argc == 1) {
		printf ("%s",usage);
		return 0;
	}

	while ((c = getopt_long(argc, argv, "hcv:t:p:n:a:", longopts, 0)) != EOF) {
		switch (c) {
			case 'n': 
				cmd.number=atoi(optarg);
				if ((cmd.number < 1 ) || (cmd.number >1000)) {
					printf ("%s",usage);
					exit (-1);
				}
				break;
			case 't': 
				cmd.timeout=atoi(optarg);
				if ((cmd.timeout < 0 ) || (cmd.timeout >3600)) {
					printf ("%s",usage);
					exit (-1);
				}
				break;
			case 'p':
				cmd.port=atoi(optarg);
				if ((cmd.port < 9 ) || (cmd.port >10)) {
					fprintf (stderr,"Invalid port number, (valid 9 or 10)\n");
					printf ("%s",usage);
					exit (-1);
				}
				break;
			case 'a': 

				if (strcmp(optarg,"PRBS_31") == 0)
					cmd.pattern=0;
				else if (strcmp(optarg,"PRBS_7") == 0)
					cmd.pattern=1;
				else if (strcmp(optarg,"PRBS_9") == 0)
					cmd.pattern=2;
				else if (strcmp(optarg,"PRBS_23") == 0)
					cmd.pattern=3;
				else if (strcmp(optarg,"PRBS_31inv") == 0)
					cmd.pattern=4;
				else if (strcmp(optarg,"PRBS_7inv") == 0)
					cmd.pattern=5;
				else if (strcmp(optarg,"PRBS_15") == 0)
					cmd.pattern=8;
				else if (strcmp(optarg,"PRBS_15inv") == 0)
					cmd.pattern=9;
				else if (strcmp(optarg,"PRBS_9inv") == 0)
					cmd.pattern=6;
				else if (strcmp(optarg,"PRBS_23inv") == 0)
					cmd.pattern=7;
				else if (strcmp(optarg,"HIGH") == 0)
					cmd.pattern=0xc;
				else if (strcmp(optarg,"LOW") == 0)
					cmd.pattern=0xd;
				else if (strcmp(optarg,"MIXED") == 0)
					cmd.pattern=0xe;
				else if (strcmp(optarg,"SQUARE") == 0)
					cmd.pattern=0xf;
				else {
					printf ("%s",usage);
					exit (-1);
				}
				break;

			case 'v': 
				verbose=atoi(optarg);
				if ((verbose <0) || (verbose > 2)) {
					printf ("%s",usage);
					exit (-1);
				}
				break;	
			case 'c': 
			cmd.immCount=1;
				break;
			case 'h': 
				printf ("%s",usage);
				return 0;
				break;
			
		}
	}
	if ((cmd.port < 9 ) || (cmd.port > 10 )) {
		printf ("%s",usage);
		exit (-1);
	}
	
	if ((fd = mvOpen(ETHERNET)) < 0) {
		exit(-1);
	}

	if ((ret=startPrbsCounters (fd, &cmd)) < 0) {
		mvClose(fd);
		printf ("PRBS start: Error\n");
		exit (ret);
	}
// test na smycku
	if ((ret=readPrbsCounters (fd, cmd.port,  &diffCounters)) < 0) {
		stopPrbsCounters (fd, cmd.port);
		mvClose(fd);
		printf ("PRBS read counters: Error\n");
		exit (ret);
	}
	printf ("Testing the lock:\n");
	if ( diffCounters.valid )	
			locked |= 1;
	if ( locked ) {	
		printf ("PRBS Lock:              %20s\n","Locked");
	} else {
		printf ("PRBS Lock:              %20s\n","Not locked");
		stopPrbsCounters (fd, cmd.port);
		mvClose(fd);
		exit (ret);
	}
//mereni
	for (i =0 ; i < cmd.number; i++){
		sleep (cmd.timeout);
		if ((ret=readPrbsCounters (fd, cmd.port,  &diffCounters)) < 0) {
			stopPrbsCounters (fd, cmd.port);
			mvClose(fd);
			printf ("PRBS read counters: Error\n");
			exit (ret);
		}
		if (verbose >=0) {
			printf ("--------------------------------------------\n");
			printf ("Measurement No:         %20d\n",i+1);
			printf ("Seconds from the start: %20d\n",cmd.timeout * (i+1));
			
			if ( diffCounters.valid ) {	
				printf ("PRBS Lock:              %20s\n","Locked");
				printf ("Send bits:              %20llu\n",diffCounters.Tx);
				printf ("Received bits:          %20llu\n",diffCounters.Rx);
				printf ("Errors:                 %20llu\n",diffCounters.Errors);
			} else {
				printf ("PRBS Lock:              %20s\n","Not locked");
				break;
			}
		}
		prbsCounters ( & counters, & diffCounters);
		
	}
	stopPrbsCounters (fd, cmd.port);
	mvClose(fd);
	printf ("============================================\n");
	printf ("Summary Report\n");
	if ( locked ) {	
		printf ("Total Seconds:          %d\n",cmd.timeout * (cmd.number));
		printf ("Total bits send:        %llu\n",counters.Tx);
		printf ("Total bits received:    %llu\n",counters.Rx);
		printf ("Total Errors:           %llu\n",counters.Errors);
		if (counters.Rx > 0 )
			printf ("BER:                    %.3e\n",(float)counters.Errors/counters.Tx);
		else
			printf ("BER:                    NaN\n");
	} else {
		printf ("PRBS Lock:              %20s\n","Not locked");
	}
	exit (ret);
}
