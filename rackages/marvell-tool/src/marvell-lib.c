/*

    marvell-tool: monitor and control for Marvell 88E6390 (X) switch
    
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/sockios.h>
#ifndef __GLIBC__
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#endif
#include <linux/types.h>

#include "marvell-interface.h"
#include "marvell-lib.h"
#include "marvell-reg.h"

//funkce vraci 0 - pro OK; zapornou hodnotu pro chybu (mozno dekodovat pres strerror(errno))

extern struct ifreq ifr;


// otevreni soketu pro nastavovani marvell switche
// vrati filesescriptor nebo chybu
//devname je eth0 nebo eth1 .... )
int mvOpen (const char* devname)
{
	int fd = -1;
	if (sizeof (devname) > IFNAMSIZ)
		return -EINVAL;
	strncpy(ifr.ifr_name, devname, IFNAMSIZ);
	if ((fd = socket(AF_INET, SOCK_DGRAM,0)) < 0)
		perror("socket");
	return fd;
	
}

// zavreni socketu pro nastavovani marvell switche
int mvClose (int fd)
{
	return close(fd);
}


//otestuje platnost adresy, registru nebo stranky
int mvCheckAddr (__u16 data, __s8 * table)
{
	return checkAddr (data & 0xff, table);
}

#ifdef RR_PRODUCT_bk2
int mvCheckPage (__u8 addr, __u16 data)
{
	return checkPage(addr, data & 0xff);
}
#else
int mvCheckPage (__u16 data)
{
	return checkPage(data & 0xff);
}
#endif

int mvCheckReg (__u16 data)
{
	return checkReg (data & 0xff);
}


int mvCheckRegSerdes (__u16 reg, __u16 *defReg)
{
	return checkRegSerdes (reg, defReg);
}

//zapis do libovolneho registru
int mvRegWrite (int fd, __u8 addr, __u8 reg, __u16 value)
{
	return regWrite (fd, addr, reg, value);
}

//zapis do libovolneho registru dle masky
int mvRegWriteMask (int fd, __u8 addr, __u8 reg, __u16 value, __u16 mask)
{
	return regWriteMask (fd, addr, reg, value , mask);
}

//cteni libovolneho registru
int mvRegRead (int fd, __u8 addr, __u8 reg, __u16* value)
{
	return regRead (fd, addr, reg, value);
}


//zapis do libovolneho registru PHY
int mvPhyRegWrite (int fd, __u8 addr, __u8 reg, __u16 value)
{
	return regPhyWrite (fd, addr, reg, value);
}

//zapis do libovolneho registru PHY dle masky
int mvPhyRegWriteMask (int fd, __u8 addr, __u8 reg, __u16 value, __u16 mask)
{
	return regPhyWriteMask (fd, addr, reg, value , mask);
}

//cteni libovolneho registru Phy
int mvPhyRegRead (int fd, __u8 addr, __u8 reg, __u16* value)
{
	return regPhyRead (fd, addr, reg, value);
}
//cteni libovolneho registru na dane strance
int mvPhyRegPageRead (int fd, __u8 addr, __u8 reg, __u8 page ,__u16* value)
{
	return regPhyPageRead (fd, addr, reg, page ,value);
}

//zapis do libovolneho registru na dane strance
int mvPhyRegPageWrite (int fd, __u8 addr, __u8 reg, __u8 page ,__u16 value)
{
	return regPhyPageWrite (fd, addr,reg,page ,value);
}

//zapis do libovolneho registru na dane strance
int mvPhyRegPageWriteMask (int fd, __u8 addr, __u8 reg, __u8 page ,__u16 value, __u16 mask)
{
	return regPhyPageWriteMask (fd, addr,reg,page ,value, mask);
}

// precte na dane adrese od registru dany pocet registru
// pouzivat jen pokud vime ze mame nastavenou page 0
// tipicke pro precteni vsech registru je
// field je ukazatel na __u16[32], pocet je 32 a reg je 0
int mvAllRegRead (int fd,__u8 addr,__u8 reg, __u16* field, unsigned int numfield)
{
	if ((reg + numfield) > 32) {
		fprintf (stderr,"Wrong number of loaded registry\n");
		return -EINVAL;
	}
	return readAllReg (fd,addr, reg ,field ,numfield);
}

int mvPhyAllRegRead (int fd,__u8 addr,__u8 reg, __u16* field, unsigned int numfield)
{
	if ((reg + numfield) > 32) {
		fprintf (stderr,"Wrong number of loaded registry\n");
		return -EINVAL;
	}
	return readPhyAllReg (fd,addr, reg ,field ,numfield);
}

int mvSetCpuPort (int fd,__u8 port)
{
	__u16 data;
	int ret;
	int timeout;
	for (timeout =0;;timeout ++) {
		if (timeout > 100) {
			fprintf  (stderr,"MGMT register : busy\n");
			return -ENODEV ;
		}
		if ((ret=regRead (fd,0x1b,0x1a,&data)) < 0)
			return ret;
		if (!(data & 0x8000))
				break;
		usleep (100);
	}
	return regWrite (fd,0x1b,0x1a,0x8000 | (0x30 << 8) | (0x3 << 5) | (port & 0x1f)  );
}

TYPE_PORT mvCheckTypePort (int fd, __u8 port)
{
	__u16 cmode;
	
	if  ((mvCheckPortCmode (fd, port, &cmode)) < 0 )
		return PORT_VACANT;
	switch (cmode) {
		case 0xf:
			return 	PORT_PHY;
		case 0x9:
			return PORT_1000BASE_X;
		case 0xA:
			return PORT_SGMII;
		case 0xB:
			return PORT_2500BASE_X;
		case 0xC:
			return PORT_10G_XAUI;
		case 0xd:
			return PORT_10G_RXAUI;
		case 0x0:
			return PORT_MII_FD;
		case 0x1:
			return PORT_MII_PHY;
		case 0x2:
			return PORT_MII_MAC;
		case 0x3:
			return PORT_GMII;
		case 0x4:
			return PORT_RMII_PHY;
		case 0x5:
			return PORT_RMII_MAC;
		case 0x6:
			return PORT_XMII_TRISTATE;
		case 0x7:
			return PORT_GRMII;
	}
	return PORT_VACANT;
}

int mvCheckPxSmodes (int fd)
{
	__u16 data;
	int ret;
	
	if ((ret=regWrite(fd, GLOBAL2, 0x1a, 0x7300)) < 0 )
		return ret;
	if ((ret=regRead(fd, GLOBAL2, 0x1a, &data)) < 0 )
		return ret;
	return (data & 0x3);
}
