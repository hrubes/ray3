#!/bin/sh
# Needs root privilegies
# Script for composition of packages. 
#
# input parameters:
# RRC_SRC_DIR - directory with resources
# RRC_INPUT_FILE - file with list of files or stdin
#
# use variable VERBOSE to set verbose level
# ver 2. - change of input parameters! see help

#~/rr_o_bm_/RAy3_devel_kit_hu1/work/build/rackages/pokushu/b_x86-linux $ fakeroot sh ../../../../../firmware/build_scripts/rrcompositor.sh ../../../../../rackages/pokushu/files.def stag

RRC_TARGET_DIR=$PWD
RRC_SRC_DIR=$1
RRC_INPUT_FILE=$2
[ -z "${VERBOSE}" ] && VERBOSE=0

# help
if [ "pre$1" = "pre-h" -o "pre$1" = "pre--help" ]; then
    cat <<EOF
Script for composition of a filesystem for packaging.
Should be executed using fakeroot or with root privilegies (needed for chmod, chown, mknod).
The directory tree with files is created in current directory.

Usage: rrcompositor.sh TARGET_DIR [INPUT_FILE]
SRC_DIR - path to directory with resources
INPUT_FILE - file with definitions (what, where, how), see format below
           - if input file is omitted then stdin is used


Input file description:
-----------------------
Format is csv (with semicolon as separator) and can contain comments prefixed with #.
<file_type>;<permissions>;<owner:group>;<target_name>;<value> # comment

<file_type>: according POSIX 1003.1-2004 + "r" for removing
 -   Regular file (cp or touch)
 d   Directory files (mkdir)
 b   Block file (mknod)
 c   Character device file (mknod)
 p   Named pipe file or just a pipe file (mkfifo)
 l   Symbolic link file (ln)
 s   Socket file (not implemented)
 r   remove file 

<permissions>: in octal format according to chmod
If it is set to dash "-", then default permissions will be used (755 for directories, 644 for others).
Ignored when creating link.

<owner:group>: according to chown
Representation with numbers (uid:gid) is preffered, because this program is for host system.
If it is set to dash "-", then default owner and group will be used (0:0 i.e root:root).
Ignored when creating link.

<target_name>: relative path (including file name)
The file will be created with full name TARGET_DIR/<target_name>.
Parent directories of the file should be prepared first.

<value>: interpreted according <file_type>
 - Regular file: \${SRC_DIR}/<value> is source path of the file
      If value is set to dash "-", then target file will be just touched and permissions and owner:group will be set.
      If value is set to asterix "*", then source file name (including path) is considered the same as the <target_name>.
 l Symbolic link: <value> is target of the link
 b,c Device: <value> is <major minor> pair according to mknod
 d Directory: if <value> is set to "S", then permissions are used for every subpath
 p,s,r: <value> is ignored

EOF
    exit 0
fi



die()
{
        #presmerovani na stderr kvuli funkcim, ktere volaji die a soucasne pisou na stdout.
        echo "rrcompositor: $1" 1>&2
        exit 42;
}

info()
{
        echo "rrcompositor: $@"
}

echo_cmd()
{
	[ "${VERBOSE}" -gt 5 ] && echo "$@"
	"$@"
	return $?
}


apply_perm()
{
    local AP_FILE="$1"
    local AP_PERM="$2"
    local AP_OWN="$3"

    echo_cmd chmod "${AP_PERM}" "${AP_FILE}" || die "cannot access ${AP_FILE}"
    echo_cmd chown "${AP_OWN}" "${AP_FILE}" || die "cannot access ${AP_FILE}"
}


if [ -n "${EUID}" -a "${EUID}" != 0 ];then
    info "Do'nt you want to become a root?"
    info "press any key to continue, CTRL-C to exit"
    read
fi

[ -d "${RRC_TARGET_DIR}" ] || die "output directory \"${RRC_TARGET_DIR}\" is missing."
if [ -z "${RRC_INPUT_FILE}" -o "pre${RRC_INPUT_FILE}" = "pre-" ]; then
    RRC_INPUT_FILE="" 
    info  "Processing: stdin"
else
    [ -r "${RRC_INPUT_FILE}" ] || die "cannot read input file ${RRC_INPUT_FILE}."
    info  "Processing: ${RRC_INPUT_FILE}"
fi

RRC_IFS=${IFS}

[ "${VERBOSE}" -gt 1 ] && echo "t perm user:group filename"
# sed odstrani komentare a nasledne prazdne radky
sed -e "s/#.*$//g" -e "/^[[:space:]]*$/d" ${RRC_INPUT_FILE} | \
 while read -r LINE || [ -n "${LINE}" ]; do
    [ "${VERBOSE}" -gt 6 ] && echo "LINE: ${LINE}"
    # musi tu byt while, abych si udrzel subshell a v nem pozadovane promenne
    echo "${LINE}" | while IFS=';' read RTYPE RPERM ROWN RNAME RVALUE RREST; do
        # set defaults
        [ -z "${ROWN}" -o "pre${ROWN}" = "pre-" ] && ROWN="0:0"
        if [ -z "${RPERM}" -o "pre${RPERM}" = "pre-" ]; then
            if [ "pre_${RTYPE}" = "pre_d" ]; then
                RPERM="755"
            else
                RPERM="644"
            fi
        fi
        [ "${VERBOSE}" -gt 0 ] && printf "%s %4s %10s %s\n" ${RTYPE} ${RPERM} ${ROWN} ${RNAME} #${RVALUE}
        RRC_NAME_FULL="${RRC_TARGET_DIR}/${RNAME}"
        case ${RTYPE} in
            "-") # Regular file
                if [ -z "${RVALUE}" -o "pre${RVALUE}" = "pre-" ]; then
                    # hodnota neni, cilovy soubor se vytvori pomoci touch (nebo uz existoval)
                    echo_cmd touch "${RRC_NAME_FULL}" || die "cannot touch ${RRC_NAME_FULL}"
                else
                    if [ "pre${RVALUE}" = "pre*" ]; then
                        # name of the source and target file is the same
                        RVALUE=${RNAME}
                    fi
                    [ -d "${RRC_NAME_FULL}" ] && RRC_NAME_FULL="${RRC_NAME_FULL}/${RVALUE##*/}" # add file name from RVALUE
                    echo_cmd cp -a "${RRC_SRC_DIR}/${RVALUE}" "${RRC_NAME_FULL}" || die "cannot copy to ${RRC_NAME_FULL}"
                fi
                #apply_perm "${RRC_NAME_FULL}.crash" "${RPERM}" "${ROWN}" 
                apply_perm "${RRC_NAME_FULL}" "${RPERM}" "${ROWN}" 
                ;;
            "d") # Directory
                echo_cmd mkdir -p "${RRC_NAME_FULL}" || die "cannot create directory ${RRC_NAME_FULL}"
                if [ "pre${RVALUE}" = "preS" ]; then
                    # use permissions to every subpath
                    f=${RNAME}
                    while [ "pre${f}" != "pre." ]; do 
                        apply_perm "${RRC_TARGET_DIR}/${f}" "${RPERM}" "${ROWN}"
                        f=$(dirname ${f});
                    done;
                else
                    apply_perm "${RRC_NAME_FULL}" "${RPERM}" "${ROWN}"
                fi
                ;;
            "b"|"c") # Block or character device
                [ -h "${RRC_NAME_FULL}" -o -e "${RRC_NAME_FULL}" ] && rm "${RRC_NAME_FULL}"
                echo_cmd mknod "${RRC_NAME_FULL}" "${RTYPE}" ${RVALUE} || die "cannot create device ${RRC_NAME_FULL}"
                apply_perm "${RRC_NAME_FULL}" "${RPERM}" "${ROWN}"
                ;;
            "p") # Named pipe
                [ -h "${RRC_NAME_FULL}" -o -e "${RRC_NAME_FULL}" ] && rm "${RRC_NAME_FULL}"
                echo_cmd mkfifo "${RRC_NAME_FULL}" || die "cannot create named pipe ${RRC_NAME_FULL}"
                apply_perm "${RRC_NAME_FULL}" "${RPERM}" "${ROWN}"
                ;;
            "l") # Symbolic link
                [ -h "${RRC_NAME_FULL}" -o -e "${RRC_NAME_FULL}" ] && rm "${RRC_NAME_FULL}"
                echo_cmd ln -s "${RVALUE}" "${RRC_NAME_FULL}" || die "cannot create symlink ${RRC_NAME_FULL}"
                # apply_perm "${RRC_NAME_FULL}" "${RPERM}" "${ROWN}"
                ;;
            "s") # named socket
                die "socket is not implemented"
                ;;
            "r") # remove file
                echo_cmd rm -r "${RRC_NAME_FULL}" || die "cannot remove ${RRC_NAME_FULL}"
                ;;
            *) # unknown
                die "unknown file type \"${RTYPE}\""
                ;;
        esac
        #ls -l "${RRC_NAME_FULL}"

    done
    # terminate loop if subshell returned error and resend error code
    [ $? -eq 0 ] || exit $?
done
