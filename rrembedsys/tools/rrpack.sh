#!/bin/sh
#
# input parameters:
# RRP_ARCHIVE - archive name
# RRP_SRC_DIR - directory with resources
# RRP_INPUT_FILE - file with list of files or stdin
#
# idealne spoustet pres fakeroot
# a pridat rrcompositor do cesty
# fakeroot bash rrpack <archive name> <input file>
#
# ver 2. - zmena vstupnich parametru !

#~/rr_o_bm_/RAy3_devel_kit_hu1/work/build/rackages/pokushu/b_x86-linux $ VERBOSE=3 fakeroot sh ../../../../../firmware/build_scripts/rrpack.sh ../../../../../rackages/pokushu/files.def /tmp/ahoj.tgz

die()
{
        #presmerovani na stderr kvuli funkcim, ktere volaji die a soucasne pisou na stdout.
        echo "rrpack: $@" 1>&2
        exit 42;
}

info()
{
	echo "rrpack: $@"
}

RRP_WORK=${PWD}
RRP_ARCHIVE="$1"
RRP_SRC_DIR="$2"
RRP_INPUT_FILE="$3"
[ -z "${VERBOSE}" ] && VERBOSE=0


# help
if [ "pre$1" = "pre-h" -o "pre$1" = "pre--help" ]; then
    cat <<EOF
Script for composition of a filesystem and creating package.
Should be executed using fakeroot or with root privilegies (needed for chmod, chown, mknod).
It calls rrcompositor.sh

Usage: rrcompositor.sh ARCHIVE_NAME [INPUT_FILE]
ARCHIVE_NAME - name of target package
INPUT_FILE - file with definitions (what, where, how),
           - if input file is omitted then stdin is used
           - for format description see help of rrcompositor.sh

EOF
    exit 0
fi

[ -z "${RRP_ARCHIVE}" ] && die "Parameter ARCHIVE_NAME is missing."

RRP_SCRIPT_DIR=$(dirname "$0")
RRP_ARCHIVE_BASE=$(basename "${RRP_ARCHIVE}")
RRP_STAGING_DIR="/tmp/${RRP_ARCHIVE_BASE}.rrpack"
#if RRP_SRC_DIR is relative then prefix it with PWD to make it absolute
[ "${RRP_SRC_DIR}" != "${RRP_SRC_DIR#/}" ] || RRP_SRC_DIR=${PWD}/${RRP_SRC_DIR}

mkdir -p "${RRP_STAGING_DIR}" || die "Failed to create staging directory ${RRP_STAGING_DIR}"

cd ${RRP_STAGING_DIR}
${RRP_SCRIPT_DIR}/rrcompositor.sh "${RRP_SRC_DIR}" "${RRP_INPUT_FILE}" || die "failed to compose files for package."

# podpera s archive.tmp kvuli absolutnim a relativnim cestam v nazvu balicku a demenci parametru -C ve spojeni s wildcards
#cd ${RRP_STAGING_DIR} && tar -czf /tmp/${RRP_ARCHIVE_BASE}.tmp *
find . -depth -print | cpio -oaVH crc > /tmp/${RRP_ARCHIVE_BASE}.tmp
[ $? -ne 0 ] && die "failed to create package"
cd ${RRP_WORK}

mv /tmp/${RRP_ARCHIVE_BASE}.tmp ${RRP_ARCHIVE}
rm -r ${RRP_STAGING_DIR}

#[ "${VERBOSE}" -gt 5 ] && tar --verbose --list -f ${RRP_ARCHIVE}
[ "${VERBOSE}" -gt 5 ] && cpio -ivt < ${RRP_ARCHIVE}
info "Package ${RRP_ARCHIVE} created."


