#!/bin/bash

echo "je to rozbite, neb nema version.mkf" >2
exit

# base schema: b_${RR_PRODUCT}-${RR_BRAND}-${RR_PLATFORM}
# variants:
#     b_generic
#     b_${RR_PRODUCT}
#     b_${RR_BRAND}
#     b_${RR_PLATFORM}
#     b_${RR_PRODUCT}-${RR_BRAND}
#     b_${RR_BRAND}-${RR_PLATFORM}
#     b_${RR_PRODUCT}-${RR_PLATFORM}
#     b_${RR_PRODUCT}-${RR_BRAND}-${RR_PLATFORM}
#
# note:
#   PRODUCT and BRAND may not contain dash "-"

PATH_PREFIX=/home/hrubes/racom/work/rr_o_bm_/RAy3_devel_kit_hu1/system/linux
RR_PROJECT_ROOT=../..

TEST=( \
b_generic \
b_bma \
b_RACOM \
b_x86-linux \
b_bma-RACOM \
b_RACOM-x86-linux \
b_bma-x86-linux \
b_bma-RACOM-x86-linux \
b_bmx-RACO-x666-linux \
b_bma-RACO-x86-linux \
b_bma-RACOMA-x86-linux \
)

for II in "${TEST[@]}"
do
	#echo "$II"
	make test -f common_pkgbuild.mkf RR_PROJECT_ROOT=${RR_PROJECT_ROOT} PWD=${PATH_PREFIX}/${II}
done
