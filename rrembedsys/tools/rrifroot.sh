#!/bin/sh
# simple wrapper, 
# it will use fakeroot if needed

if [ $# -eq 0 ]; then
    echo "If it is executed as non-root user, then command wil be executed using fakeroot."
    echo ""
    echo "Usage: rrifroot.sh <command>"
    echo "echo \"Some text\" | rrifroot.sh sh path/to/some_script.sh --and its parameters" 
    exit 1;
fi

IFROOT_RETV=0
if [ ${EUID} = 0 ];then
	$@
	IFROOT_RETV=$?
else
	echo "Using fakeroot ..."
	fakeroot $@
	IFROOT_RETV=$?
	echo "Exiting fakeroot"
fi
exit ${IFROOT_RETV}
