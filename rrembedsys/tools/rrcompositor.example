# Executing this example (for rrcompositor ver 2.)
# ----------------------
# create input file:
#  $ echo "lore ipsum" > fileA.txt
#
# compose tree (root permissions needed)
#  $ mkdir pkg
#  # cd pkg && VERBOSE=10 rrcompositor.sh ../ ../rrcompositor.example
#
# or create package using fakeroot
#  $ fakeroot rrpack.sh pkg.cpio ./ rrcompositor.example
# verify package content
#  $ cat pkg.cpio | cpio -ivt
#
#
# Example tree
# ------------
# -rw-r--r--  root/root       11  fileA.txt
# -rw-r--r--  100/100          0  file_in_root.txt
# drwxr-xr-x  root/root        0  foo/
# -rw-------  100/100          0  foo/private_file
# lrwxrwxrwx  root/root        0  foo/link_to_nonexistent -> /some/path/file.txt
# lrwxrwxrwx  root/root        0  foo/link_to_fileA -> bar/fileA.txt
# lrwxrwxrwx  root/root        0  foo/link_to_bar -> bar
# drwxr-x---  root/root        0  foo/bar/
# brw-r--r--  root/root    123,2  foo/bar/block_device
# crw-r--r--  root/root    123,1  foo/bar/character_device
# -rw-r-----  root/root       11  foo/bar/fileB.txt
# -rw-r--r--  root/root       11  foo/bar/fileA.txt
# -rwxr-xr-x  root/root       11  foo/bar/executable
# prw-------  root/root        0  foo/bar/named_pipe
# -rw-r--r--  root/root       11  foo/bar/name with spaces.txt
#
#
# File format
# -----------
# <file_type>;<permissions>;<owner:group>;<target_name>;<value> # comment
# for more info run 
#  $ rrcompositor.sh --help
#

# create directory foo using default owner (0:0) and permission (755 for directory)
d;-;-;foo;-

# create subdirectory
d;750;0:0;foo/bar;- # testing comment with ; semicolon

# create empty file
-;600;100:100;foo/private_file;-
-;-;100:100;file_in_root.txt;-

# copy file with same file-name using default owner (0:0) and permission (644 for file)
-;;;fileA.txt;* # copy file using the same name (including path) for the source and using empty values for perm and own
-;-;-;foo/bar/;fileA.txt

# copy file with a new file-name
-;640;-;foo/bar/fileB.txt;fileA.txt
-;640;-;foo/bar/file_to_delete.txt;fileA.txt
-;755;-;foo/bar/executable;fileA.txt
-;-;-;foo/bar/name with spaces.txt;fileA.txt

# create symbolic link (relative) to file (permissions and owner:group are ignored)
l;640;-;foo/link_to_fileA;bar/fileA.txt

# create symbolic link (relative) to directory
l;-;-;foo/link_to_bar;bar

# create symbolic link to invalid absolute target
l;-;-;foo/link_to_nonexistent;/some/path/file.txt

# create named pipe
p;600;-;foo/bar/named_pipe;-

# create character device (node) with major=123 and minor=1
c;-;-;foo/bar/character_device;123 1

# create block device (node) with major=123 and minor=2
b;-;-;foo/bar/block_device;123 2

# remove file
r;-;-;foo/bar/file_to_delete.txt;-

