#!/bin/sh

# run this script to install new firmware


VERBOSE=0



REQUIRED_FILES="VERSION u-boot-mmc.bin uImage uInitramfs n.tgz"

#Name of the rootfs file
PKG_N="n.tgz"

# ==================================================================
# functions
# ==================================================================

invalid()
{ # use to terminate due to incompatibility or damaged components
        log "invalid: $*"
	cleanup
        exit 42
}

fatal()
{ # use to terminate when critical process failed 
  #and station will be malfunctional after reboot
        log "fatal: $*"
	cleanup
        exit 42
}

die()
{ # use to terminate due to other errors, INSTALL script can be executed second time
        log "error: $*"
	cleanup
        exit 1
}


warning()
{
        log "warning: $*"
}

log()
{
        echo "FW INSTALL: $*" >&2
}

info()
{
        [ "${VERBOSE}" -gt 0 ] && echo "FW INSTALL: $*"
}

cleanup()
{
	info "cleaning temporary files"
	rm -rf "${FW_WORK}"
	rm -f uImage
	rm -f uInitramfs
	rm -f u-boot-mmc.bin
	rm -f n.tgz
	#stop led blinking
	killall control_gpio 2>/dev/null
	if [ -f /sbin/control_gpio ]; then
		/sbin/control_gpio --gpio=LED_SYS_GRN --set=${SGLED_STATE}
	fi
}

ii_fw_dotver2num()
{
        #Convert FW version in dot format to decimal number
        #Parameters:
        local IFD2N_FWVERDOT="$1" #version in dot format
        local IFD2N_VAR_FWVER="$2" #name of variable for storing numeric version

        local IFD2N_EXPR=$( sed -n 's/^\([0-9]\+\)\.\([0-9]\+\)\.\([0-9]\+\)\.\([0-9]\+\)$/$(( ( \1 << 24 ) + ( \2 << 16 ) + ( \3 << 8 ) + \4 ))/p' <<EOF
$IFD2N_FWVERDOT
EOF
        )
        eval "$IFD2N_VAR_FWVER=$IFD2N_EXPR"
}

ii_fw_dotver_cmp_lt()
{
        #Compare version
        #Parameters:
        local IFDCL_VER1="$1" #first version in dot format
        local IFDCL_VER2="$2" #second version in dot format
        #Return value:
        #       0 - VER1 < VER2
        #       other - VER1 >= VER2

        local IFDCL_VER1_NUM
        local IFDCL_VER2_NUM
        ii_fw_dotver2num "$IFDCL_VER1" IFDCL_VER1_NUM
        ii_fw_dotver2num "$IFDCL_VER2" IFDCL_VER2_NUM
        #return value is inverted
        return $(( IFDCL_VER1_NUM >= IFDCL_VER2_NUM ))
}

install_system()
{
	if [ -f "$1/uImage" ]; then
		if [ -f "$1/uInitramfs" ]; then
			e2fsck -p ${BOOT_DEV}
			if [ $? -ne 0 ]; then
				log "Error e2fsck \"${BOOT_MNT}\"";
				return 1;
			fi;
			mount -t ${VFSTYPE} ${BOOT_DEV} ${BOOT_MNT}
			if [ $? -ne 0 ]; then
				log "Error mount \"${BOOT_MNT}\"";
				return 1;
			fi;
			cp "$1/uImage" ${BOOT_MNT}
			if [ $? -ne 0 ]; then
				log "Error copy \"$1/uImage\" to \"${BOOT_MNT}\"";
				umount ${BOOT_MNT}
				return 1;
			fi;
			rm -f "$1/uImage"
			cp "$1/uInitramfs" ${BOOT_MNT}
			if [ $? -ne 0 ]; then
				log "Error copy \"$1/uInitramfs\" to \"${BOOT_MNT}\"";
				umount ${BOOT_MNT}
				return 1;
			fi;
			rm -f "$1/uInitramfs"
			umount ${BOOT_MNT}
			return 0;
		fi;
		log "Error: files \"$1/uInitramfs\" not exist";
		return 1;
	fi;
	log "Error: files \"$1/uImage\" not exist";
	return 1;
}

install_uboot()
{

	if [ -f "$1/u-boot-mmc.bin" ]; then
		force_ro 0
			if [ $? -ne 0 ]; then
			return 1;
		fi;
		dd bs=512 if="$1/u-boot-mmc.bin" of=/${BOOT0_DEV}; 
		if [ $? -ne 0 ]; then
			force_ro 1
			return 1;
		fi;
		force_ro 1
		rm -f "$1/u-boot-mmc.bin";
		return 0;
	fi;
	log "Error: files \"$1\" not exist";
	return 1;
}

install_rootfs()
{
	if [ -f "$1/${PKG_N}" ]; then
		if [ -d /mnt/rootfs ]; then
			mount -o rw,remount /mnt/rootfs
			rm -fr ${ROOTFS_MNT}/*
			tar -xzf "$1/${PKG_N}" -C "${ROOTFS_MNT}"
			if [ $? -ne 0 ]; then
				log "Error unpacking the archive   \"$1/${PKG_N}\"";
				return 1;
			fi;
			sync;
			rm -f "$1/${PKG_N}"
			mount -o ro,remount /mnt/rootfs
			return 0;
		fi;
		log "Error: directory \"${ROOTFS_MNT}\" not exist";
		return 1;
	fi;
	log "Error: files \"$1/${PKG_N}\" not exist";
	return 1;
}

move_n()
{
	# move n to target destination
	info "moving n.tgz to ${FW_WORK} ..."
	cp $1/n.tgz "${FW_WORK}/n.tgz" || die "unable to copy rootfs package to ${FW_WORK}"
	chmod 600 "${FW_WORK}/n.tgz" || warning "failed to set access rights on n.tgz"
	return 0
}

# ==================================================================
# script start
# ==================================================================

log "start"

SGLED_STATE=0
if [ -f /sbin/control_gpio ]; then
	SGLED_STATE=$( /sbin/control_gpio --gpio=LED_SYS_GRN )
	[ ${SGLED_STATE} -ne 1 ] && SGLED_STATE=0
	/sbin/control_gpio --gpio=LED_SYS_GRN --set=0 --pulse=1000 --t1=500000 </dev/null >/dev/null 2>&1 3>&1 & #factory settings led on
fi
# test files (they should be already checked with chcksums.md5 by current firmware)
info "checking files ..."
MISSING_FILES=""
for FILE_NEEDED in ${REQUIRED_FILES}; do
        [ -f "${FILE_NEEDED}" ] || MISSING_FILES="${MISSING_FILES} ${FILE_NEEDED}"
done
[ -z "${MISSING_FILES}" ] || invalid "missing firmware components: ${MISSING_FILES}"

# ==================================================================

# change paths to use binaries from tools
if [ -f /tmp/.oldsys/etc/rrmmc_tools ]; then
        #main mode
        source /tmp/.oldsys/etc/rrmmc_tools
elif [ -f /etc/rrmmc_tools ]; then
        #service mode
        source /etc/rrmmc_tools
fi

# ==================================================================
info "checking versions ..."
# source version of this new firmware
source VERSION

# version check
FW_TYPE=""
[ -f /etc/rrprd/ver_u ] && source /etc/rrprd/ver_u
[ -f /etc/rrprd/ver_r ] && source /etc/rrprd/ver_r
[ -f /etc/rrprd/ver_k ] && source /etc/rrprd/ver_k
[ -f /etc/rrprd/ver_n ] && source /etc/rrprd/ver_n
if [ -n "${RR_VERSION_R}" ]; then
	FW_TYPE="R"
	[ -f /mnt/rootfs/etc/rrprd/ver_n ] && source /mnt/rootfs/etc/rrprd/ver_n
elif [ -n "${RR_VERSION_K}" ]; then
	FW_TYPE="K"
	[ -f /mnt/rootfs/etc/rrprd/ver_n ] && source /mnt/rootfs/etc/rrprd/ver_n
elif [ -n "${RR_VERSION_N}" ]; then
	FW_TYPE="N"
	[ -f /tmp/.oldsys/etc/rrprd/ver_k ] && source /tmp/.oldsys/etc/rrprd/ver_k
	[ -f /tmp/.oldsys/etc/rrprd/ver_u ] && source /tmp/.oldsys/etc/rrprd/ver_u
else 
	invalid "Unable to detect version of current firmware"
fi

# ==================================================================

info "checking integrity of firmware packages ..."


# test n component
tar -ztf n.tgz >/dev/null
if [ $? -ne 0 ]; then
	invalid "Component package n.tgz rejected - invalid archive."
fi

# ==================================================================

# prepare target directory
info "preparing directory for rootfs archive ..."
if [ -e "${FW_WORK}" ]; then
	rm -rf "${FW_WORK}" || die "unable to clean directory ${FW_WORK}"
fi
mkdir -p ${FW_WORK} || die "unable to create directory ${FW_WORK}"

# reserve space for configuration and data backup during rootfs installation
# file will be removed by rrfirmware after reboot
#dd if=/dev/urandom of="${FW_WORK}/${FW_RESERVATION}" bs="100000" count=1 >/dev/null 2>/dev/null || die "Failed to create reservation file (${FW_WORK}/${FW_RESERVATION}, 100000B)."

ACT_DIR=`pwd`;

#logovani vyrobniho logu
if [ ! -d "${I_PERSISTENT_DIR}" ]; then
	mkdir -p "${I_PERSISTENT_DIR}"
fi

if [ "${FW_TYPE}" = "R" ]; then
	date >>"${I_FACTORY_LOG}"
	echo "fw_install" >>"${I_FACTORY_LOG}"
	install_uboot ${ACT_DIR} && echo "u_boot, NEW_VER:${RR_VER_U}" >>"${I_FACTORY_LOG}"
	install_system ${ACT_DIR} && echo "kernel, NEW_VER:${RR_VER_K}" >>"${I_FACTORY_LOG}"
	install_rootfs ${ACT_DIR} && echo "rootfs, NEW_VER:${RR_VER_N}" >>"${I_FACTORY_LOG}"
	date >>"${I_FACTORY_LOG}"
	echo "-----------------------------------------------------------------------------" >>"${I_FACTORY_LOG}"
fi

if [ "${FW_TYPE}" = "K" ]; then
	date >>"${I_FACTORY_LOG}"
	echo "fw_install" >>"${I_FACTORY_LOG}"
	ii_fw_dotver_cmp_lt ${RR_VERSION_U} ${RR_VER_U} && install_uboot ${ACT_DIR} && echo "u_boot, OLD_VER:${RR_VERSION_U} , NEW_VER:${RR_VER_U}" >>"${I_FACTORY_LOG}"
	ii_fw_dotver_cmp_lt ${RR_VERSION_K} ${RR_VER_K} && install_system ${ACT_DIR} && echo "kernel, OLD_VER:${RR_VERSION_K} , NEW_VER:${RR_VER_K}" >>"${I_FACTORY_LOG}"
	ii_fw_dotver_cmp_lt ${RR_VERSION_N} ${RR_VER_N} && install_rootfs ${ACT_DIR} && echo "rootfs, OLD_VER:${RR_VERSION_N} , NEW_VER:${RR_VER_N}" >>"${I_FACTORY_LOG}"
	date >>"${I_FACTORY_LOG}"
	echo "-----------------------------------------------------------------------------" >>"${I_FACTORY_LOG}"
fi

if [ "${FW_TYPE}" = "N" ]; then
	date >>"${I_FACTORY_LOG}"
	echo "fw_install" >>"${I_FACTORY_LOG}"
	ii_fw_dotver_cmp_lt ${RR_VERSION_U} ${RR_VER_U} && install_uboot ${ACT_DIR} && echo "u_boot, OLD_VER:${RR_VERSION_U} , NEW_VER:${RR_VER_U}" >>"${I_FACTORY_LOG}"
	ii_fw_dotver_cmp_lt ${RR_VERSION_K} ${RR_VER_K} && install_system ${ACT_DIR} && echo "kernel, OLD_VER:${RR_VERSION_K} , NEW_VER:${RR_VER_K}" >>"${I_FACTORY_LOG}"
	ii_fw_dotver_cmp_lt ${RR_VERSION_N} ${RR_VER_N} && move_n ${ACT_DIR} && echo "rootfs, OLD_VER:${RR_VERSION_N} , NEW_VER:${RR_VER_N}" >>"${I_FACTORY_LOG}"
	date >>"${I_FACTORY_LOG}"
	echo "-----------------------------------------------------------------------------" >>"${I_FACTORY_LOG}"
fi


if [ -f /sbin/control_gpio ]; then
	killall control_gpio 2>/dev/null
	/sbin/control_gpio --gpio=LED_SYS_GRN --set=${SGLED_STATE}
fi

log "done"
sync
# reboot nakonec udela japonec (volajici skript)
# reboot must be done by calling process/user

exit 0





