#!/bin/sh
# script for MWL firmware upgrading from local station
# the station should be fully operational (booted using n - rootfs)
# otherwise (station in service mode - r/k) use STEP-local.sh for firmware installation

SSH="ssh -C"
#FWDIR=/mnt/rwmnt
#PRODUCTION_KEY="production" # klicove slove pro vytvoreni vyrobniho flagu, ktery zpusobi smazani vsech oddilu
#FORCE_KEY="force" # klicove slove pro vytvoreni vyrobniho flagu, ktery zpusobi ignorovani branche/produktu

usage ()
{
        echo "$0 [--key=\"key file\"] [--host=\"ip station\"] [--production[=\"yes/no\"] | --force[=\"yes/no\"]]"
	echo "  --production:       Causes total cleaning of station. No files will preserve."
	echo "                      You should run SET_SERIAL and SET_TYPE after installation."
	echo "  --force:            Installation process skip all branch and versions checks."
	echo "                      It is recomended to use it together with --production."
	echo "  --key_root=<file>   File with SSH public key for root."
	echo "                      Used in force or production mode."
}


job()
{
local JOB_ADDR="$1"
local JOB_KEYF="$2"
local JOB_PRODUCTION="$3"
local JOB_FORCE="$4"
local JOB_RKEY="$5"

local FW_BUFF_STEP="/tmp/fw_buffer_step/"
local KEY_BUFF_STEP="/tmp/ssh_key_buffer/"
local JOB_COMMAND=":"

if [ "pre_${JOB_PRODUCTION}" = pre_yes ]; then
 	JOB_COMMAND="${JOB_COMMAND} && touch ${FW_BUFF_STEP}/production_format.flag"
fi
if [ "pre_${JOB_FORCE}" = pre_yes ]; then
 	JOB_COMMAND="${JOB_COMMAND} && touch ${FW_BUFF_STEP}/force.flag"
fi



echo "RAy3Eval installation"
if [ -n "${JOB_RKEY}" ]; then
	echo "Sending SSH root key ..."
	cat "${JOB_RKEY}" | ${SSH} ${JOB_KEYF} root@${JOB_ADDR} "echo \"Receiving root key file ...\"; if [ -e ${KEY_BUFF_STEP} ]; then rm -rf ${KEY_BUFF_STEP}; fi; mkdir -p ${KEY_BUFF_STEP}; cd ${KEY_BUFF_STEP} && cat >./authorized_keys.add && echo \"Key file received\""
	if [ $? -ne 0 ]; then
		exit 1
	fi
fi
echo "Sending data ..."
cat fw.cpio  | ${SSH} ${JOB_KEYF} root@${JOB_ADDR} "echo \"Receiving data ...\";if [ -e ${FW_BUFF_STEP} ]; then rm -rf ${FW_BUFF_STEP}; fi;mkdir -p ${FW_BUFF_STEP}; cd ${FW_BUFF_STEP} && echo \"Extracting firmware components ... \" && cpio -iud && md5sum -c checksums.md5 && ${JOB_COMMAND} && echo \"Inicializing firmware installation ...\" && sh INSTALL && sleep 1 && reboot"
# to sleep 1 tam byt nemusi


RET=$?
if [ $RET -ne 0 ]; then
        echo "job error: $RET"
else
	echo "job O.K."
fi
return $RET
}



ADDR=""
KEY=""
KEYF=""
MODE_PRODUCTION="no"
MODE_FORCE="no"
KEYROOT=""


while [ $# -gt 0 ]; do
        case "$1" in
                -*=*) arg=$(echo "$1" | sed 's/[-_a-zA-Z0-9]*=//');;
                *) arg="" ;;
        esac
        case $1 in
                --key=*)
                        KEY="${arg}"
                        if [ ! -s $KEY ]; then
                                echo "Nonexist file $KEY"
                                exit 0
                        fi
                        ;;
                --host=*)
                        ADDR="${arg}"
                        ;;
                --production|--production=*)
			if [ -z "${arg}" ] || [ "pre_${arg}" = pre_yes ]; then
				MODE_PRODUCTION="yes"
			fi
                        ;;
                --force|--force=*)
			if [ -z "${arg}" ] || [ "pre_${arg}" = pre_yes ]; then
				MODE_FORCE="yes"
			fi
                        ;;
		--key_root=*)
			if [ -n "${arg}" ]; then
				KEYROOT="${arg}"
			fi
			;;
                *)
                        usage
                        exit 1
                        ;;
        esac
        shift
done

if [ ! "pre_${MODE_PRODUCTION}" = "pre_yes" ]; then
	if [ ! -z "${KEYROOT}" ]; then
		if [ ! -f "${KEYROOT}" ]; then
			echo "Missing SSH root key file: ${ROOTKEY}"
			exit 1
		fi
	fi
else
	KEYROOT=""
fi


if [ $KEY ]; then
        KEYF=" -i $KEY "
fi


if [ -z "$ADDR"  ]; then
	#if [ "pp_${PRODUCTION}" = "pp_${PRODUCTION_KEY}" ]; then
	#	ADDR="192.168.169.168"
	#else
		ADDR="192.168.169.168"
	#fi
	echo "No host specified --> using default ${ADDR}"
	#arp -d ${ADDR}
fi

job "${ADDR}" "${KEYF}" "${MODE_PRODUCTION}" "${MODE_FORCE}" "${KEYROOT}"



