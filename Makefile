
RR_PROJECT_ROOT := ${PWD}

# natazeni uzitecnych cest
include ${RR_PROJECT_ROOT}/rrembedsys/settings/paths.mkf
include ${RR_VALUES_FILE}

# defaultni pravidlo vypise napovedu
.PHONY: all
all: help

# pravidla na nastavovani a mazani produktu
include ${RR_TOOLS_PATH}/product_set.mkf

#---------------------------------------------

# order of packages is important
SYSTEM_LIST_BASE= u-boot linux
RACKAGES_LIST_BASE= interfaces marvell-tool timing_unit tiny_apps
PACKAGES_LIST_BASE= ccrypt


SYSTEM_LIST= $(addprefix system/,${SYSTEM_LIST_BASE})
RACKAGES_LIST= $(addprefix rackages/,${RACKAGES_LIST_BASE})
PACKAGES_LIST= $(addprefix packages/,${PACKAGES_LIST_BASE})

ALL_PKG_LIST= ${SYSTEM_LIST} ${RACKAGES_LIST} ${PACKAGES_LIST}
ALL_PKG_ROOT_LIST= $(addprefix ${RR_PROJECT_ROOT}/,${ALL_PKG_LIST})
ALL_PKG_BUILD_LIST= $(addprefix ${RR_BUILD_DIR}/,${ALL_PKG_LIST})
ALL_PKG_BUILD_MAKEFILE_LIST= $(addsuffix /Makefile,${ALL_PKG_BUILD_LIST})

#---------------------------------------------

.PHONY: help
help:
	@echo " "
	@echo "   Releases help:"


.PHONY: work_dir
work_dir: ${RR_BUILD_DIR_FLAG}

.PHONY: pkg_build_dirs
pkg_build_dirs: ${ALL_PKG_LIST}

.PHONY: ${ALL_PKG_LIST}
${ALL_PKG_LIST}: %: ${RR_PROJECT_ROOT}/%/Makefile
	@echo $^
	cd ${^D} && make build_dir
# 
# ${ALL_PKG_BUILD_MAKEFILE_LIST}: ${RR_BUILD_DIR}/%/Makefile: ${RR_PROJECT_ROOT}/%/Makefile
# 	@echo $^
# 	cd ${^D} && make build_dir

${RR_BUILD_DIR_FLAG}:
	# adresare se vytvari v paths.mkf
	#mkdir -p ${RR_BUILD_DIR}
	#mkdir -p ${RR_OUT_DIR}
	#mkdir -p ${RR_OUTFW_DIR}
	touch $@


.PHONY: host_tools
host_tools:
	cd rrembedsys && ${MAKE} -f host_tools.mkf


.PHONY: build_dir
build_dir:
	cd system && ${MAKE} build_dir
	cd packages && ${MAKE} build_dir
	cd rackages && ${MAKE} build_dir
	cd rrembedsys && ${MAKE} build_dir



